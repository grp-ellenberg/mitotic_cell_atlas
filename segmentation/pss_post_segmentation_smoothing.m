function pss_post_segmentation_smoothing(curCellDir, fpOutRoot, expDatabase, cellIdx)
% This function displays the results of segmentation for validation
% Created on 2015_01_16
%curCellDir = 'C:\Research Materials\Data\Mitosys\ProofOFConcept\140423_MIS12\MitoSys2\Result3\cell0015_R0019\';
disp(curCellDir);

fpOut = [fpOutRoot expDatabase.filepath{cellIdx} '\Preprocessing\Segmentation\'];
if ~exist(curCellDir, 'dir')
    disp('Input directory is missing');
    return;
else
    if ~exist(fpOut, 'dir')
        mkdir(fpOut);
    end
end

fn = dir([curCellDir '*.mat']);
if isempty(fn) == 1
    return;
end

zFactor = 3;
mPhase = {'G2','Pro', 'Prometa', 'Meta', 'Ana', 'Telo'};

tpoint = round(length(fn)/2);
prevStageIdx = -1;
selTpoint = -1;
while tpoint <= length(fn) && tpoint>=1
    curMat = load([curCellDir fn(tpoint).name]);
    stageIdx = strmatch(curMat.mitoTime, mPhase);
    
    if prevStageIdx == 4 && stageIdx == 5
        selTpoint = tpoint - 1;
        break;
    end
    if prevStageIdx == 5 && stageIdx == 4
        selTpoint = tpoint;
        break;
    end
    if stageIdx <= 4
        tpoint = tpoint + 1;
    else
        tpoint = tpoint - 1;
    end
    prevStageIdx = stageIdx;
end

if selTpoint == -1
    return;
end

%try
tpoint = selTpoint;
dirFlag = -1;
while tpoint <= length(fn)
    disp(['Processing tpoint: ' num2str(tpoint)]);
    curMat = load([curCellDir fn(tpoint).name]);
    cellVolume = curMat.cellVolume;
    nucVolume = curMat.nucVolume;
    if tpoint ~= selTpoint
        cellVolume = pss_temporal_smoothing(cellVolume, prevCellVolume, nucVolume, zFactor);
    else
        selCellVolume = cellVolume;
    end
    bgMask = curMat.bgMask;
    %cellVolume = curMat.cellVolume;
    neg = curMat.neg;
    nucVolume = curMat.nucVolume;
    nuc = curMat.nuc;
    poi = curMat.poi;
    chrCentMic = curMat.chrCentMic;
    isectPointsPix = curMat.isectPointsPix;
    midPlane = curMat.midPlane;
    regPointsMic = curMat.regPointsMic;
    eigVectors = curMat.eigVectors;
    eigValuesMic = curMat.eigValuesMic;
    cellAxis = curMat.cellAxis;
    chrVolMic = curMat.chrVolMic;
    chrDistPix = curMat.chrDistPix;
    numChr = curMat.numChr;
    mitoTime = curMat.mitoTime;
    avgBgInt = curMat.avgBgInt;
    
    
    savefile = [fpOut fn(tpoint).name];
    if strcmp(curMat.mitoTime, 'Ana') && curMat.numChr ==1
        chrSptDistPix = curMat.chrSptDistPix;
        save(savefile,'bgMask','cellVolume', 'neg', 'nucVolume', 'nuc','poi', 'chrCentMic', 'isectPointsPix', 'midPlane', 'regPointsMic', 'eigVectors', 'eigValuesMic', 'cellAxis', 'chrVolMic', 'chrDistPix', 'numChr', 'mitoTime', 'avgBgInt', 'chrSptDistPix');
    else
        save(savefile,'bgMask','cellVolume', 'neg', 'nucVolume', 'nuc','poi', 'chrCentMic', 'isectPointsPix', 'midPlane', 'regPointsMic', 'eigVectors', 'eigValuesMic', 'cellAxis', 'chrVolMic', 'chrDistPix', 'numChr', 'mitoTime', 'avgBgInt');
    end
    
    prevCellVolume = cellVolume;
    tpoint = tpoint+dirFlag;
    if tpoint == 0
        tpoint = selTpoint + 1;
        prevCellVolume = selCellVolume;
        dirFlag = 1;
    end
end
end

