function [intersectingPoints, intersectingPointsIso] = ls_generate_intersecting_points(Vma, xg, yg, zg, contourPMthreed, voxelSizeX, voxelSizeY, voxelSizeZ, zFactor)
%This functions detects two intersecting points of cell division axis on the cell surface 

intersectingPoints = zeros(2,3);
intersectingPointsIso = zeros(2,3);
t = -25: 0.1: -4.5;
xma=t*Vma(1)+xg;
yma=t*Vma(2)+yg;
zma=t*Vma(3)+zg;
minDist = 50000;
for i = 1: round(size(xma,2))
    for j = 1: size(contourPMthreed)
        dist = sqrt((xma(i) - contourPMthreed(j,1))^2+ (yma(i) - contourPMthreed(j,2))^2 +(zma(i) - contourPMthreed(j,3))^2);
        if(dist <minDist)
            minDist = dist;
            minCont = j;
        end
    end
end

intersectingPoints(1,:) = contourPMthreed(minCont,:);
intersectingPoints(1,1) = round(intersectingPoints(1,1)/voxelSizeX);
intersectingPoints(1,2) = round(intersectingPoints(1,2)/voxelSizeY);
intersectingPoints(1,3) = round(intersectingPoints(1,3)/voxelSizeZ);
intersectingPointsIso(1,:) = intersectingPoints(1,:);
intersectingPoints(1,3) = ceil(intersectingPoints(1,3)/zFactor); 

t = 4.5: 0.1: 25;
xma=t*Vma(1)+xg;
yma=t*Vma(2)+yg;
zma=t*Vma(3)+zg;
minDist = 50000;
for i = round(size(xma,2)/2)+1: size(xma,2)
    for j = 1: size(contourPMthreed)
        dist = sqrt((xma(i) - contourPMthreed(j,1))^2+ (yma(i) - contourPMthreed(j,2))^2 +(zma(i) - contourPMthreed(j,3))^2);
        if(dist <minDist)
            minDist = dist;
            minCont = j;
        end
    end
end

intersectingPoints(2,:) = contourPMthreed(minCont,:);
intersectingPoints(2,1) = round(intersectingPoints(2,1)/voxelSizeX);
intersectingPoints(2,2) = round(intersectingPoints(2,2)/voxelSizeY);
intersectingPoints(2,3) = round(intersectingPoints(2,3)/voxelSizeZ);
intersectingPointsIso(2,:) = intersectingPoints(2,:);
intersectingPoints(2,3) = ceil(intersectingPoints(2,3)/zFactor); 
end