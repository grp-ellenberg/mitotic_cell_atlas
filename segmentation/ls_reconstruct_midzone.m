function [cRegion] = ls_reconstruct_midzone(cellAxis, negStack, cRegion, xgCh, ygCh, zgCh, voxelSizeX, voxelSizeY, voxelSizeZ, dx, dy, Nz)
%Generate binary image containing midplane
%This function is modified to create midZone considering the axisIdx
bFactor = 0.2;
intRegion = zeros(dx,dy,Nz);
t=-10:0.05:10;
se = ones(15,15,15);
xma=round((t*cellAxis(1)+xgCh)/voxelSizeX);
yma=round((t*cellAxis(2)+ygCh)/voxelSizeY);
zma=round((t*cellAxis(3)+zgCh)/voxelSizeZ);

for j = 1: size(xma,2)
    if xma(j)>= 1 && xma(j) <=dx && yma(j)>= 1 && yma(j) <=dy && zma(j)>= 1 && zma(j) <=Nz
        intRegion(xma(j),yma(j),zma(j)) = 1;
    end
end

intRegion = imdilate(intRegion, se);
negStack = imgaussian(negStack, 5, 3);
negStack(intRegion(:,:,:) == 0) = 0;
negStack(cRegion(:,:,:) == 1) = 0;
[cThresh3D, histRatio] = ls_otsu_3d_image(negStack, 1);
segRegion = zeros(dx,dy,Nz);
for i = 1:Nz
    cThresh2D = ls_otsu_2d_image(negStack(:,:,i), 1);
    segRegion(:,:,i) = double((negStack(:,:,i) < (cThresh3D * (1-bFactor) + cThresh2D * bFactor)) & negStack(:,:,i) ~=0);
    segRegion(:,:,i) = imfill(segRegion(:,:,i),'holes');
end

segRegion = imdilate(segRegion, ones(3,3,3));
%segRegion = double(negStack(:,:,:)< cThresh3D);
segRegion(intRegion(:,:,:) == 0) = 0;
cRegion = double(or(cRegion, segRegion));
end