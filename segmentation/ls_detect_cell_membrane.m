function [procStat] = ls_detect_cell_membrane(out_dir, out_dir_iso, seg_dir, path_gen, ...
    cellChanIdx, chrChanIdx, protChanIdx, ratioFlag, parameters)
%% LS_DETECT_CELL_MEMBRANE
% This function detects cell boundary and extracts parameters.
% Detected chromosomes by 'ls_detect_chromosomes' are used as markers for watershed
% INPUT:
%   * out_dir: Main directory to save the data
%   * out_dir_iso: Main directory to save the isotropic data
%   * seg_dir: subdirectory to save the data (i.e. results are saved in out_dir\seg_dir and out_dir_iso\seg_dir)
%   * patg_gen: Class instance of pathgen_mitosys to create all possible subpaths
%   * cellChanIdx: Index of cell boundary channel
%   * chrChanIdx: Index of chromosome channel
%   * protChanIdx: Index of protein channele
%   * ratioFlag: If 1 uses ratio of cell and chromatin to improve segmentation
%   * options: Options to process data
% Author: Julius Hossain, EMBL, Heidelberg, Germany, julius.hossain@embl.de
% Created: 2014-11-13
% Updates:
%   2016-06-21
%   2017-09-11 Comments and documentation. options variable. Read in using bioformat. Antonio
%   2018-03-15 Use path_gen class and strut inputs

parameters_default = struct(...
        'saveIsotropicMatFiles', 1, ... % Save isotropic sampling files
        'nRemLowerSlices', 0, ...
        'rCRegionNeg', 1, ... % radius of disk structuring element
        'pairDistNeg', 5, ... % distance used in ls_generate_referenc_points
        'bFactor2DNeg', 0.20, ...  % Contribution of 2D threshold for membranes
        'sigmaBlurNeg', 5, ... %Sigma for negative staining channel
        'hSizeBlurNeg', 7, ... %Kernel size for negative staining channel
        'sigmaRatioNeg', 3, ... %Sigma for gaussian blur in ratio image.
        'hSizeRatioNeg', 5, ... %Kernel size for gaussian blur in ratio image
        'minVolNeg', 500, ...  % minimal Volume of cell
        'sigmaBlurChr', 3, ...       % Sigma for gaussian blur in chromosome segmentation
        'hSizeBlurChr', 5, ...       % Kernel size for gaussian blur in chromosome segmentation
        'bFactor2DChr', 0.33, ...    % Contribution of 2D threshold for membranes
        'smoothcontourNeg', 25);        %number of points used in smoothing;

if nargin < 9
    parameters = parameters_default;
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('ls_detect_cell_membrane: Eighth argument parameters does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
end

close all;
assert(isa(path_gen, 'pathgen_mitosys'), 'Fourth entry must be of class pathgen_mitosys found in directory IO');

disp(['Detect cell boundary: ' path_gen.getcelldir(out_dir, '')]);

%% Some parameters
se = strel(ones(3,3,3)); % Define structuring element
metaPhase = 4;           % index of metaphase found during segmentation of chromosomes
procStat = -1;           % process status
tmax = path_gen.tmax;    % maximal number of time points

%% make directory for isoformat data
if parameters.saveIsotropicMatFiles
    [out_dir_iso, status] = verifyDirectory(out_dir_iso, 'Specify results output directory', true);
    if ~status
        return
    end
end


%% Check if sequence can be processed
matfiles = path_gen.getmatpaths(out_dir, seg_dir);
matfiles_iso = path_gen.getmatpaths(out_dir_iso, seg_dir);
reportfiles = path_gen.getreportpaths(out_dir, seg_dir);
reportfiles_iso = path_gen.getreportpaths(out_dir_iso, seg_dir);
if path_gen.verifypath(matfiles)
    for i = 1:length(matfiles)
        variablesInfo = who('-file', matfiles{i});
        expectedValues = {'chrMarker', 'chrThresh2D', 'chrThresh3D', ...
            'chromosomes', 'detCoords', 'numChr'};
        for ival = 1:length(expectedValues)
            if ~ismember(expectedValues{ival}, variablesInfo)
                fprintf('Format of mat segmentation file for %s is not conform\n', matfiles{i});
                return
            end
        end
    end
else
   fprintf('No mat files for segmentation have been found\n'); 
end
try
    segtable = readtable(reportfiles.res, 'Delimiter','\t');
    mPhase = segtable.mitoticPhase;
catch
    fprintf('Could not find file %s', reportfiles.res)
    return;
end

%% Processing
tinit = 1;
zinit = 1;
% get dimension of file
[dx, dy, NzOrig, Nz, Nc, voxelSizeX, voxelSizeY, voxelSizeZ,  voxelSizeZ_Iso, voxelVolume_Iso, zFactor] = ....
    get_image_dimensions_bf(path_gen.imgpaths{end}, zinit);

tpointInc = -1; % Processing direction (-1:backward, +1 forward)
% Determine last metaphase timepoints from the timefile to start segmentation
tStart = find(mPhase(:) == metaPhase, 1,'last');
tpoint = tStart;
fprintf('Processing time point: ');
while tpoint <= tmax

    if size(mPhase, 1) < tpoint
        disp('Time file is incomplete');
        return;
    else
        phase = mPhase(tpoint);
    end
    %% Read in the image data and creating working stacks
    [reader, dim] = getBioformatsReader(path_gen.imgpaths{tpoint});
    
    if all(Nc >= [chrChanIdx, protChanIdx, cellChanIdx])
        protStackOrig = getTPointBioFormats(reader, dim, 1, protChanIdx);
        chStackOrig = getTPointBioFormats(reader, dim, 1, chrChanIdx);
        negStackOrig = getTPointBioFormats(reader, dim, 1, cellChanIdx);
        protStackOrig =  protStackOrig(:,:,zinit:NzOrig);
        chStackOrig =  chStackOrig(:,:,zinit:NzOrig);
        negStackOrig = negStackOrig(:,:,zinit:NzOrig);
        reader.close();
    else
        error('ls_detect_chromosomes: Stack contains % channels.\nChromosome channel is %d; Protchannel is %d; neg/cell channel is %d', ...
            Nc, chrChanIdx, protChanIdx, cellChIdx);
    end
    % Removing the effect of lower slices by populating them with higher one
    chStackMan = chStackOrig;
    negStackMan = negStackOrig;
    if parameters.nRemLowerSlices > 0
        for zplane = 1:parameters.nRemLowerSlices
            chStackMan(:,:,zplane)  = chStackMan(:,:,parameters.nRemLowerSlices+1);
            negStackMan(:,:,zplane) = negStackMan(:,:,parameters.nRemLowerSlices+1);
        end
    end
       
    %% Generate intermediate slices
    fprintf('%d, ', tpoint);
    protStack = ls_gen_intermediate_slices(protStackOrig, zFactor);
    chStack = ls_gen_intermediate_slices(chStackMan, zFactor);
    negStack = ls_gen_intermediate_slices(negStackMan, zFactor);
    
    chStackBack  = chStack;
    negStackBack = negStack;
    protStackBack = protStack;
    
    if tpoint == tStart
        chrCentMic = zeros(3,1);
        tempVol = zeros(dx,dy,Nz);      % Store current segmented volume  as a referece for next/prev time point
        cRegion = zeros(dx,dy,Nz);      % Store the detected cytoplasm region
        chRegion = zeros(dx,dy,Nz);     % Store the detected chromosome region
        midPlaneIso = zeros(dx, dy, Nz);% Midplane for anaphase onwards - constructed from chromosome shape
    end
    
    %% Process negative staining stack
    chrPack = load(matfiles{tpoint});    % load file containing the markers and nucleus segmentation
    chStack = imgaussian(chStack, parameters.sigmaBlurNeg, parameters.hSizeBlurNeg);
    negStack = imgaussian(negStack, parameters.sigmaBlurNeg, parameters.hSizeBlurNeg);
    if ratioFlag == 1 % this is used when mCherry is imaged together with dextran.
        % Rerun detection of chromosome to compute ratio image
        chThresh2D = chrPack.chrThresh2D;
        chThresh3D = chrPack.chrThresh3D;
        chStackSeg = imgaussian(chStackBack, parameters.sigmaBlurChr, parameters.hSizeBlurChr);
        chRegion(:,:,:) = 0;
        % Update threshold for different slices
        for i = 1:Nz
            chRegion(:,:,i) = double(chStackSeg(:,:,i) >= (chThresh3D * (1 - parameters.bFactor2DChr) + chThresh2D(i) * parameters.bFactor2DChr));
        end
        
        chStackNorm = chStack;
        chStackNorm(chRegion(:,:,:) == 1) = 0;
        % displaySubplots(chStackNorm, 'Normalized chromosome stack', disRow, disCol, Nz, zFactor, 2);
        sumBgAvgInt = 0;
        for i = 1: Nz
            totalBgInt = sum(sum(chStackNorm(:,:,i)));
            totalBgPix = sum(sum(chRegion(:,:,i) == 0));
            avgBgInt = totalBgInt/totalBgPix;
            tempFrame = chStack(:,:,i);
            tempFrame(chStackSeg(:,:,i) < avgBgInt) = avgBgInt;
            chStackNorm(:,:,i) = tempFrame;
            sumBgAvgInt = sumBgAvgInt + avgBgInt;
        end
        gAvgBgInt = sumBgAvgInt/Nz;
        ratioImage = (gAvgBgInt * negStack)./chStackNorm;
        
        ratioImage = imgaussian(ratioImage, parameters.sigmaRatioNeg, parameters.hSizeRatioNeg);
        clear chStackNorm;
        clear tempFrame;
    else
        ratioImage = negStack;
    end
    
    [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
    negNumVoxels = dx*dy*Nz*0.01;
    sumVoxels = 0;
    for i = length(histRatio):-1: 1
        sumVoxels = sumVoxels + histRatio(i);
        if sumVoxels >= negNumVoxels
            break;
        end
    end
    
    ratioImage = i - ratioImage;
    ratioImage(ratioImage(:,:,:) <0) = 0;
    
    if parameters.nRemLowerSlices > 0
        ratioImage(:,:,1:parameters.nRemLowerSlices*zFactor) = 0;
        [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 1);
    else
        [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
    end
    
    
    cRegion(:,:,:) = 0;
    for i = 1:Nz
        cThresh2D = ls_otsu_2d(ratioImage(:,:,i));
        cRegion(:,:,i) = double(ratioImage(:,:,i) >= (cThresh3D * (1 - parameters.bFactor2DNeg) + cThresh2D * parameters.bFactor2DNeg));
    end
    
    bgMaskInt = cRegion;
    threeDLabel = bwconncomp(cRegion,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    for i = 1 : numel(numPixels)
        if numPixels(i) * voxelVolume_Iso < parameters.minVolNeg
            cRegion(threeDLabel.PixelIdxList{i}) = 0;
        end
    end
    % displaySubplots(cRegion, 'Detected cell regions - scattered blobs removed', disRow, disCol, Nz, zFactor, 2);
    %% Filling of thresholded image
    for zplane = 1:Nz
        cRegion(:,:,zplane)   = imdilate(cRegion(:,:,zplane), strel('disk', parameters.rCRegionNeg,0));
        cRegion(:,:,zplane)   = imfill(cRegion(:,:,zplane), 'holes');
        bgMaskInt(:,:,zplane) = imdilate(bgMaskInt(:,:,zplane), strel('disk', parameters.rCRegionNeg,0));
        bgMaskInt(:,:,zplane) = imfill(bgMaskInt(:,:,zplane), 'holes');
        for i = 1:parameters.rCRegionNeg
            cRegion(:,:,zplane) = imerode(cRegion(:,:,zplane), strel('diamond',1));
        end
    end
    
    bgMaskInt = 1 - bgMaskInt;
    bgStack = negStackBack;
    bgStack(bgMaskInt(:,:,:) == 0) = 0;
    totBgInt = sum(sum(sum((bgStack(:,:,round(Nz*0.25):round(Nz*0.75))))));
    totBgPix = sum(sum(sum((bgMaskInt(:,:,round(Nz*0.25):round(Nz*0.75))))));
    
    %% Combining distance transform and watershed to identify cell region of interest
    distImage = bwdistsc(~cRegion,[1 1 voxelSizeZ_Iso/voxelSizeX]);
    
    cRegionEroded = imerode(cRegion, se);
    
    chrPack.chrMarker(cRegionEroded(:,:,:) == 0) = 0;
    chrPack.chrMarker(:,1,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(1,:,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(:,dy,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(dx,:,round(Nz/20):round(Nz/2)) = 1;
    
    clear cRegionEroded;
    
    distImage = -distImage; % invert distance image
    %displaySubplots(distImage, 'DT of inverse', disRow, disCol, Nz, zFactor, 2);
    distImage = imimposemin(distImage,chrPack.chrMarker, 18); %suppress local minima other than markers
    %fn_PC_DisplaySubplots(distImage, 'DT after suppression', disRow, disCol, Nz, zFactor, 2);
    distImage(~cRegion) = -Inf; % Set outside of cell region with infinity
    %fn_PC_DisplaySubplots(distImage, 'DT after suppression - inf', disRow, disCol, Nz, zFactor, 2);
    wsLabel = watershed_old(distImage); %Apply watershed algorithm
    
    wsLabel(~cRegion) = 0;
    %fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    
    threeDLabel = bwconncomp(wsLabel,18);
    wsLabel(:,:,:) = 0;
    for i = 1: threeDLabel.NumObjects
        wsLabel(threeDLabel.PixelIdxList{i}) = i;
    end
    
    if chrPack.numChr == 1
        labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 1))));
        labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 1,  wsLabel(:,:,:) >=1))));
        lbl1 = round(labSum/labCount);
        wsLabel(wsLabel(:,:,:) ~= lbl1) = 0;
    else
        tempVol(:,:,:) = 0;
        labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 1))));
        labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 1, wsLabel(:,:,:) >=1))));
        lbl1 = round(labSum/labCount);
        
        tempVol(wsLabel(:,:,:) == lbl1) = 1;
        labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 2))));
        labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 2, wsLabel(:,:,:) >=1))));
        lbl2 = round(labSum/labCount);
        tempVol(wsLabel(:,:,:) == lbl2) = 1;
        
        wsLabel = tempVol;
    end
    wsLabel(wsLabel(:,:,:)>0) = 1;
    % fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    wsLabel = imclose(wsLabel, se);
    % To deal with the filtering
    wsLabel = imclose(wsLabel,se);
    wsLabel = imdilate(wsLabel,se);
    
    % Store  initial detected cell region as 'cRegion'
    cRegion = wsLabel;
    % displaySubplots(cRegion, 'Selected blob using Watershed + DT raw', disRow, disCol, Nz, zFactor, 2);
    
    cRegionDis = cRegion;
    cRegion(:,:,:) = 0;
    for zplane = 1:Nz
        curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane), 8);
        if sum(sum(curBlob))* voxelSizeX * voxelSizeY <= 10
            continue;
        end
        
        cRegionDis(:,:,zplane) = imsubtract(cRegionDis(:,:,zplane), curBlob);
        cRegion(:,:,zplane) = curBlob;
        
        curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
        if sum(curBlob(:)) * voxelSizeX * voxelSizeY <=10
            continue;
        end
        cRegion(:,:,zplane) = or(cRegion(:,:,zplane), curBlob);
    end
    threeDLabel = bwconncomp(cRegion,18);
    if threeDLabel.NumObjects == 0
        disp(['FAILED detect_cell_membrane at time point ' num2str(tpoint) '. ' curCellInDir]);
        break
    end
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    [biggest, idx] = max(numPixels);
    cRegion(:,:,:) = 0;
    cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    numPixels(idx) = 0;
    [biggest, idx] = max(numPixels);
    if biggest * voxelSizeX * voxelSizeY * voxelSizeZ_Iso >=1000
        cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    end
    %% End of DT + WS
    
    %         cRegion = smooth3(cRegion,'box',7);
    %         cRegion(cRegion(:,:,:)>=0.5) = 1;
    %         cRegion(cRegion(:,:,:)<0.5)  = 0;
    refVol = sum(cRegion(:)) * voxelSizeX * voxelSizeY * voxelSizeZ_Iso;
    cRegion = ls_smooth_and_equalize(cRegion, refVol, voxelSizeX*voxelSizeY*voxelSizeZ_Iso);
    %% Predict cell axis from detected chromosome and calculate midplane
    [x,y,z] = ls_threed_coord(find(chrPack.chromosomes), dx, dy);
    [VcH, RcH, xgCh, ygCh, zgCh] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ_Iso);
    
    angBlendFlag = 0;
    if tpoint == tStart
        Vma = VcH(:,1); %Select the smallest Eigen vector in metaPhase
        minAngIndex = 1;
        prevVma = Vma;
        tStartVma = Vma;
    else
        if phase == metaPhase
            Vma = VcH(:,1); %Select the smallest Eigen vector in metaPhase
            prevVma = Vma;
            minAngIndex = 1;
        elseif phase >= 6 ||(phase == 5 && chrPack.numChr == 2) || (tpoint<=tmax-2 && phase <=3 && mPhase(tpoint+2) ~= metaPhase)
            Vma = VcH(:,3); %Select the largest Eigen vector in telo or later
            prevVma = Vma;
            minAngIndex = 3;
        else
            minAng = 90;
            minAngIndex = 1; %Otherwise chose the one that has minimum angle with previous
            for comp = 1:3
                ang = min(acos(abs(prevVma'*VcH(:,comp))) *180/pi, acos(abs(prevVma'*-VcH(:,comp))) *180/pi);
                if ang<minAng
                    minAng = ang;
                    minAngIndex = comp;
                end
            end
            Vma = VcH(:,minAngIndex);
            %Correction of anaphase
            if minAng >= 12
                dotProd = prevVma' * Vma;
                if dotProd <0
                    Vma = -Vma;
                end
                prevWeight = 0.65;
                curWeight = 1- prevWeight;
                bisectVma = (prevVma * prevWeight + Vma * curWeight)/norm(prevVma * prevWeight + Vma * curWeight);
                Vma = bisectVma;
                angBlendFlag = 1;
            end
            
            prevVma = Vma;
        end
    end
    
    if angBlendFlag == 1
        A = VcH(:,minAngIndex);
        B = Vma;
        G = [ dot(A,B)         -norm(cross(A,B)) 0;...
            norm(cross(A,B))  dot(A,B)         0;...
            0                 0                1];
        F = [ A (B-dot(A,B)*A)/norm(B-dot(A,B)*A) cross(B,A) ];
        U = F*G*inv(F);
        
        VcH(:,1) = U * VcH(:,1);
        VcH(:,2) = U * VcH(:,2);
        VcH(:,3) = U * VcH(:,3);
    end
    %End of prediction
    
    if phase>=6
        cRegion = ls_reconstruct_midzone(Vma, negStackBack, cRegion, xgCh, ygCh, zgCh, voxelSizeX, voxelSizeY, voxelSizeZ_Iso, dx, dy, Nz);
    end
    if phase>=5 && chrPack.numChr == 2
        cRegion = ls_process_upper_cell_region(cRegion, zgCh, Nz, voxelSizeZ_Iso);
    end
    
    %Smooth contour
    [cRegionBord, contourCRegionThreeD] = ls_smooth_boundary(cRegion, parameters.smoothcontourNeg);
    for i=1:Nz
        cRegion(:,:,i) = imfill(cRegionBord(:,:,i), 'holes');
    end
    
    for i = round(Nz*0.7):Nz-1
        cRegion(:,:,i+1) = double(and(cRegion(:,:,i+1), cRegion(:,:,i)));
    end
    chrPack.chromosomes(cRegion(:,:,:) == 0) = 0;
    volChr = sum(chrPack.chromosomes(:) > 0) * voxelVolume_Iso;
    
    %% End of masking chStack
    contourCRegionThreeD(:,1) = contourCRegionThreeD(:,1) * voxelSizeX;
    contourCRegionThreeD(:,2) = contourCRegionThreeD(:,2) * voxelSizeY;
    contourCRegionThreeD(:,3) = contourCRegionThreeD(:,3) * voxelSizeZ_Iso;
    
    midPlaneIso(:,:,:) = 0;
    
    if phase>=5
        midPlaneIso = ls_generate_midplane(VcH,minAngIndex, xgCh, ygCh, zgCh, voxelSizeX, voxelSizeY, voxelSizeZ_Iso, dx, dy, Nz);
    end
    
    refPoints = ls_generate_referenc_points(xgCh, ygCh, zgCh, Vma, VcH, minAngIndex, parameters.pairDistNeg);
    
    [intersectingPoints, intersectingPointsIso] = ls_generate_intersecting_points(Vma, xgCh, ygCh, zgCh, contourCRegionThreeD, voxelSizeX, voxelSizeY, voxelSizeZ_Iso, zFactor);
    
    if chrPack.numChr == 2
        chrDistPix = norm(chrPack.detCoords(1,:) - chrPack.detCoords(2,:));
        chrDistMic = norm(chrPack.detCoords(1,:).*[voxelSizeX voxelSizeY voxelSizeZ_Iso] - chrPack.detCoords(2,:).*[voxelSizeX voxelSizeY voxelSizeZ_Iso]); %considering voxelSize along d
    else
        chrDistPix = 0;
        chrDistMic = 0;
    end
    if phase == 5 && chrPack.numChr ==1
        [chrSptDistPix, chrDistMic] = ls_calculate_chr_dist_early_ana(chrPack.chromosomes, midPlaneIso, voxelSizeX, voxelSizeY, voxelSizeZ_Iso);
    else
        chrSptDistPix = chrDistPix; % this is to have all variables consistent
    end
    %% Save results in mat file
    bgMask = ls_remove_intermediate_slices(bgMaskInt, zFactor);
    cellVolume = ls_remove_intermediate_slices(cRegion, zFactor);
    nucVolume = ls_remove_intermediate_slices(chrPack.chromosomes, zFactor);
    nuc = chStackOrig;
    neg = negStackOrig;
    poi = protStackOrig;
    chrCentMic(1) = xgCh;
    chrCentMic(2) = ygCh;
    chrCentMic(3) = zgCh;
    isectPointsPix = intersectingPoints;
    midPlane = ls_remove_intermediate_slices(midPlaneIso, zFactor);
    regPointsMic = refPoints;
    eigVectors = VcH;
    eigValuesMic = RcH;
    cellAxis = Vma;
    chrVolMic = volChr;
    
    numChr = chrPack.numChr;
    cellPhases = {'Inter', 'Prophase', 'Prometa', 'Meta', 'Ana', 'Telo', 'Telolate'};
    mitoTime =  cellPhases(phase);
    avgBgInt = totBgInt/totBgPix;
    
    save(matfiles{tpoint}, 'bgMask', 'cellVolume', 'neg', 'nucVolume', 'nuc','poi', ...
    'chrCentMic', 'isectPointsPix', 'midPlane', 'regPointsMic', ...
    'eigVectors', 'eigValuesMic', 'cellAxis', 'chrVolMic', 'chrDistPix', ...
    'numChr', 'mitoTime', 'avgBgInt', 'chrSptDistPix', 'voxelSizeX', ...
    'voxelSizeY', 'voxelSizeZ', 'voxelSizeZ_Iso', 'parameters', '-append');
    clear bgMask;
    clear cellVolume;
    clear neg;
    if tpoint < tmax
        clear nucVolume;
    end
    clear nuc;
    clear poi;
    clear midPlane;
    
    if parameters.saveIsotropicMatFiles == 1
        cellIsodir = path_gen.getcelldir(out_dir_iso, seg_dir);
        if ~exist(cellIsodir, 'dir')
            mkdir(cellIsodir);
        end
        cellRegion = cRegion;
        chrRegion = chrPack.chromosomes;
        neg  = negStackBack;
        poi = protStackBack;
        chr = chStackBack;
        midPlane = midPlaneIso;
        isectPointsPix = intersectingPointsIso;
        save(matfiles_iso{tpoint}, 'cellRegion', 'chrRegion', 'poi', 'chrCentMic', 'isectPointsPix',  ...
            'regPointsMic', 'eigVectors', 'eigValuesMic', ...
            'cellAxis', 'chrVolMic', 'chrDistMic', ...
            'numChr', 'mitoTime', 'avgBgInt', 'parameters', ...
            'voxelSizeX', 'voxelSizeY', 'voxelSizeZ', 'voxelSizeZ_Iso');
    end
    
    % end of processing
    clear cellRegion;
    clear chrRegion;
    clear neg;
    clear poi;
    clear chr;
    clear isectPointsPix;
    clear midPlane;
    clear regPointsMic;
    clear eigVectors;
    clear eigValuesMic;
    clear cellAxis;
    clear chrDistPix;
    clear chrDistMic;
    clear numChr;
    clear mitoTime;
    clear avgBgInt;
    
    %% End of displaying volume and predicting parameters
    if tpoint == tmax
        procStat = 1;
    end
    
    tpoint = tpoint + tpointInc;
    if tpoint < tinit
        tpointInc = 1;
        tpoint = tStart + 1;
        prevVma = tStartVma;
    end
    
    clear maskedchStack;
    clear wsLabel;
    clear distImage;
    clear chrMarker;
    clear X;
    clear Y;
    clear Z;
    clear contourCRegionThreeD;
    clear dumCol;
    clear hist;
    close all;
    clear chrPack;
end
fidtxt = fopen(reportfiles_iso.meta, 'w');
fprintf(fidtxt, 'voxelSizeX\t voxelSizeY\t voxelSizeZ\t zFactor\n');
fprintf(fidtxt, '%0.4f\t%0.4f\t%0.4f\t%0.4f\n', voxelSizeX, voxelSizeY, voxelSizeZ_Iso, zFactor);
fclose(fidtxt);
fprintf('-|\n');
end