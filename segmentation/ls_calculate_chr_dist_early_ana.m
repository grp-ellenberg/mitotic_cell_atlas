function [chrSptDistPix, chrSptDistMic] = ls_calculate_chr_dist_early_ana(nucVolume, midPlane, voxelSizeX, voxelSizeY, voxelSizeZ)
% This function forcefully devides chromosome volume for early anaphase
% into two and calculates distance between this two separated region
% Author: M. Julius Hossain, EMBL Heidelberg
% Created: 2016-06-02
% Last update: 2016-06-02
spNucVolume = nucVolume;
spNucVolume1 = zeros(size(nucVolume));
spNucVolume2 = zeros(size(nucVolume));
spMidPlane = imdilate(midPlane, ones(3,3,3));
spNucVolume(spMidPlane(:,:,:) == 1) = 0;

threeDLabel = bwconncomp(spNucVolume,18);
stat = regionprops(threeDLabel,'Centroid');
numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
[~, idx] = max(numPixels);
%spNucVolume1(threeDLabel.PixelIdxList{idx}) = 1;
xg1 = stat(idx).Centroid(2);
yg1 = stat(idx).Centroid(1);
zg1 = stat(idx).Centroid(3);
numPixels(idx) = 0;
[~, idx] = max(numPixels);
%spNucVolume2(threeDLabel.PixelIdxList{idx}) = 1;

xg2 = stat(idx).Centroid(2);
yg2 = stat(idx).Centroid(1);
zg2 = stat(idx).Centroid(3);

chrSptDistPix = norm([xg1 yg1 zg1] - [xg2 yg2 zg2]);
chrSptDistMic = norm([xg1 yg1 zg1].*[voxelSizeX voxelSizeY voxelSizeZ] - [xg2 yg2 zg2].*[voxelSizeX voxelSizeY voxelSizeZ]);
end

