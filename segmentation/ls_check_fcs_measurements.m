function [fcsFlag] = ls_check_fcs_measurements(curCellInDir, curCellOutDir, dirStatFn, nucVolume, voxelSizeX, newDataStructure )
% This function checks fcs measurements both number of mesurement
% location with respect to segmented chromosome volumes
fpFcsTxt = curCellInDir;
if newDataStructure == 0
    fpFcsTxt(end-13) = '2';
    fullPathFcsFile = fopen([fpFcsTxt 'FCS1_' fpFcsTxt(end-15:end-1) '_T0001.txt'],'r');
else
    fnFcsTxt = dir([curCellInDir '*.txt']);
    numTxtFile = length(fnFcsTxt);
    fullPathFcsFile = fopen([curCellInDir fnFcsTxt(numTxtFile).name],'r');
end
curLine = fgets(fullPathFcsFile);
mesFlag = 0;
fcsCoords = zeros(2,6);
coordIdx = 0;
while ischar(curLine)
    matchLoc = strfind(curLine,'%X Y Z (px)');
    if ~isempty(matchLoc)
        mesFlag = 1;
    end
    curLine = fgets(fullPathFcsFile);
    if mesFlag == 1 && coordIdx <6
        xyz = sscanf(curLine, '%d');
        coordIdx = coordIdx + 1;
        fcsCoords(1,coordIdx) = xyz(1);
        fcsCoords(2,coordIdx) = xyz(2);
    end
end
fclose(fullPathFcsFile);

fcsFlag = 1;
if coordIdx <6
    fcsFlag = 2;
else
    %Check position of measurments
    projChr = permute(max(permute(nucVolume ,[3,1,2])),[2,3,1]);
    distProjChr = bwdist(projChr); %Used volume without interpolated slices to speed up
    
    rPosCount = 0;
    for i = 1:6
        if distProjChr(fcsCoords(2,i), fcsCoords(1,i)) *voxelSizeX <=0.6
            rPosCount = rPosCount + 1;
        end
    end
    if rPosCount < 2
        fcsFlag = 3;
    end
end
%End of checking fcs measurements

fidDirStat = fopen([curCellOutDir dirStatFn],'w');
fprintf(fidDirStat,'%d\r\n',fcsFlag);
if fcsFlag == 1
    fprintf(fidDirStat,'Processed successfully\r\n\r\n');
elseif fcsFlag == 2
    fprintf(fidDirStat,'Processed - FCS measurements are incomplete\r\n\r\n');
elseif fcsFlag == 3
    fprintf(fidDirStat,['Processed - ' num2str(rPosCount) ' FCS measurements are around chromosomes\r\n\r\n']);
end
fclose(fidDirStat);
end

