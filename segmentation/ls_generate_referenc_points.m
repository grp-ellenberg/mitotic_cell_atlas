function [refPoints] = ls_generate_referenc_points(xgCh, ygCh, zgCh, Vma, VcH, minAngIndex, pairDist)
%This function generates three reference points for registration
refPoints = zeros(3,4);
%Select the second reference point as the centroid of chromosome
refPoints(:,2) = [xgCh ygCh zgCh]';

%Select the first reference point from the predicted axis
curVect = Vma;
if curVect(3) <zgCh
    curVect = curVect * -1;
end

xCoord = pairDist * curVect(1) + xgCh;
yCoord = pairDist * curVect(2) + ygCh;
zCoord = pairDist * curVect(3) + zgCh;

refPoints(:, 1) = [xCoord yCoord zCoord]';

if minAngIndex == 1 || minAngIndex == 2
    curVect = VcH(:,3);
else
    curVect = VcH(:,2);
end

if curVect(2) <0
    curVect = curVect * -1;
end

for curDist=pairDist:0.005:pairDist*10
    xCoord = curDist*curVect(1)+xgCh;
    yCoord = curDist*curVect(2)+ygCh;
    if norm([xCoord-xgCh yCoord-ygCh])>=pairDist
        break;
    end
end
zCoord = zgCh;

refPoints(:, 3) = [xCoord yCoord zCoord]';

xCoord = -curDist*curVect(1)+xgCh;
yCoord = -curDist*curVect(2)+ygCh;
refPoints(:, 4) = [xCoord yCoord zCoord]';

cross3 = cross((refPoints(:,3)-refPoints(:,2)), (refPoints(:,1)-refPoints(:,2)));
cross4 = cross((refPoints(:,4)-refPoints(:,2)), (refPoints(:,1)-refPoints(:,2)));
if cross4(3)>cross3(3)
    temp= refPoints(:,3);
    refPoints(:,3) = refPoints(:,4);
    refPoints(:,4) = temp;
end
end