function [cRegionBord, contourCRegionThreeD] = ls_smooth_boundary( cRegion, smoothcontour )
%This function smooths the boundary of cRegion
%% Smoothing cRegion
flag = 0;
cRegionBord = zeros(size(cRegion));
cRegionDis = cRegion;
Nz = size(cRegion, 3);
for zplane=1:Nz
    curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
    if sum(sum(curBlob)) == 0
        continue;
    end
    cRegionDis(:,:,zplane) = imsubtract(cRegionDis(:,:,zplane), curBlob);
    for row=1:size(curBlob,1)
        for col=1:size(curBlob,2)
            if curBlob(row,col)
                break;
            end
        end
        if curBlob(row,col)
            break;
        end
    end
    contourCRegion = bwtraceboundary(curBlob,[row col],'E');
    
    if(~isempty(contourCRegion))
        contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
        contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);
        contourCRegion=[contourCRegion((floor(length(contourCRegion)/2)+1):length(contourCRegion),:);contourCRegion(1:floor(length(contourCRegion)/2),:)];
        contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
        contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);
        
        if flag==0
            contourCRegionThreeD=[contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
        else
            contourCRegionThreeD=[contourCRegionThreeD;contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
        end
        flag = 1;
        
        for i=1:length(contourCRegion(:,1))
            cRegionBord(round(contourCRegion(i,1)),round(contourCRegion(i,2)),zplane)=1;
        end
    end
    
    curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
    if sum(sum(curBlob)) == 0
        continue;
    end
    for row=1:size(curBlob,1)
        for col=1:size(curBlob,2)
            if curBlob(row,col)
                break;
            end
        end
        if curBlob(row,col)
            break;
        end
    end
    contourCRegion=bwtraceboundary(curBlob,[row col],'E');
    if(~isempty(contourCRegion))
        contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
        contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);
        contourCRegion=[contourCRegion((floor(length(contourCRegion)/2)+1):length(contourCRegion),:);contourCRegion(1:floor(length(contourCRegion)/2),:)];
        contourCRegion(:,1)=smooth(contourCRegion(:,1),smoothcontour);
        contourCRegion(:,2)=smooth(contourCRegion(:,2),smoothcontour);
        
        if flag==0
            contourCRegionThreeD=[contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
        else
            contourCRegionThreeD=[contourCRegionThreeD;contourCRegion(:,1),contourCRegion(:,2),zplane*ones(length(contourCRegion(:,1)),1)];
        end
        flag = 1;
        
        for i=1:length(contourCRegion(:,1))
            cRegionBord(round(contourCRegion(i,1)),round(contourCRegion(i,2)),zplane)=1;
        end
    end
    %cRegion(:,:,zplane) = imfill(cRegionBord(:,:,zplane), 'holes');
end
clear cRegionDis;
%% End of smoothing cRegion

end

