function [midPlane] = ls_generate_midplane(VcH, axisIdx, xgCh, ygCh, zgCh, voxelSizeX, voxelSizeY, voxelSizeZ, dx, dy, Nz)
%Generate midplane using eigen vectors, saves as a binary image volume
midPlane = zeros(dx,dy,Nz);
t=-10:0.05:10;

if axisIdx == 1
    Vma1 = VcH(:,2);
    Vma2 = VcH(:,3);
elseif axisIdx == 2
    Vma1 = VcH(:,1);
    Vma2 = VcH(:,3);
else
    Vma1 = VcH(:,1);
    Vma2 = VcH(:,2);
end

xma1=t*Vma1(1)+xgCh;
yma1=t*Vma1(2)+ygCh;
zma1=t*Vma1(3)+zgCh;

xma2=t*Vma2(1)+xgCh;
yma2=t*Vma2(2)+ygCh;
zma2=t*Vma2(3)+zgCh;

for i = 1: size(xma1,2)
    yma2cur = round((yma2 - yma1(i) + ygCh)/voxelSizeY);
    xma2cur = round((xma2 - xma1(i) + xgCh)/voxelSizeX);
    zma2cur = round((zma2 - zma1(i) + zgCh)/voxelSizeZ);
    for j = 1: size(xma2cur,2)
        if xma2cur(j)>= 1 && xma2cur(j) <=dx && yma2cur(j)>= 1 && yma2cur(j) <=dy && zma2cur(j)>= 1 && zma2cur(j) <=Nz
            midPlane(xma2cur(j),yma2cur(j),zma2cur(j)) = 1;
        end
    end
end
end