function ls_segment_landmarks(gen_opt, parameters)
%% LS_SEGMENT_LANDMARKS
% This function segments landmarks (cell and chromosome volumes) and extracts
% different parameters from the segmented data. Function is a master function
% to call ls_detect_chromosomes and ls_detect_membranes
% LS_SEGMENT_LANDMARKS() For testing purposes. Function will ask for input to experiment directory and results directory. If a database file is found
%                        will process one cell.
% LS_SEGMENT_LANDMARKS(gen_opt, options) Specify paths in struct gen_opt and segmentation parameters in options
% INPUT:
%   * gen_opt: A struct with directories and filenames
%   * parameters: Options for segmentation of chromosomes and cell boundary
%
% Author: M. Julius Hossain, EMBL Heidelberg.
% Created: 2016-06-20
% Updates
%   2016-06-21
%   2017-09-11 Comments and documentation. options variable. Read in using bioformat. Antonio
%   2018-03-15 Struct as parameter input

%% Setup of parameters and folders consistency checks
[gen_opt_default, parameters_default] = get_default();

if nargin < 1
    gen_opt = gen_opt_default;
end

if nargin < 2
    % these are the complete options for ls_detect_chromosomes and ls_detect_cell_membrane
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('lSecond argument parameters does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
end

if gen_opt.segmentation_reprocess == -2
    return
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

if parameters.saveIsotropicMatFiles
    [gen_opt.out_dir_iso, status] = verifyDirectory(gen_opt.out_dir_iso, 'Specify results output directory', true);
    if ~status
        return
    end
end


%% Read QC values
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
if gen_opt.segmentation_reprocess == 1
    vec_QC = -ones(length(summary_database.segmentation),1);
else
    vec_QC = summary_database.segmentation;
end
proc_history = (summary_database.manualQC == 1); % Landmarks extraction is independent of FCS calibration
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);

%% Process each cell
for cell_idx = vec_idx
    if vec_QC(cell_idx) ~= -1 % check if cell has been processed or not
        continue
    end
    fprintf('Segment landmarks Cell_idx: %d\n',  cell_idx)
    ratioFlag = summary_database.segmodel(cell_idx) == 1;
    cellChanIdx  = summary_database.Ccell(cell_idx);
    chrChanIdx   = summary_database.Cdna(cell_idx);
    protChanIdx  = summary_database.Cpoi(cell_idx);
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    
    % verify that images exist
    if ~path_gen.verifypath(path_gen.imgpaths)
        warning('Image sequence for %s is incomplete!', summary_database.filepath{cell_idx})
        continue
    end
    
    chrProcStat = 0;
    % Verify that if mat files exist and that they are conform to format for chromosome segmentation
    matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
    
    if path_gen.verifypath(matfiles)
        chrProcStat = 1;
        for i = 1:length(matfiles)
            variablesInfo = who('-file', matfiles{i});
            expectedValues = {'chrMarker', 'chrThresh2D', 'chrThresh3D', ...
                'chromosomes', 'detCoords', 'numChr'};
            for ival = 1:length(expectedValues)
                if ~ismember(expectedValues{ival}, variablesInfo)
                    fprintf('ls_segment_landmarks:Format of mat segmentation file for %s is not conform\n', matfiles{i});
                    chrProcStat = 0;  % Reprocess matfile if not conform
                end
            end
        end
    end
    
    if chrProcStat == 0 || parameters.overWriteMarker
        try
            chrProcStat = ls_detect_chromosomes(gen_opt.out_dir, gen_opt.segmentation_dir, path_gen, chrChanIdx,  parameters);
        catch ME
            % Pass over bug in ls_detect_chromosomes
            getReport(ME, 'extended', 'hyperlinks', 'off')
        end
    end
    
    % continue processing when success in chromosome segmentation
    if chrProcStat == 1
        % try to find a slicestart.txt file
        rawtif_dir = path_gen.getpoidir(gen_opt.exp_dir);
        fname_slicestart = fullfile(rawtif_dir, 'slicestart.txt');
        
        if ~exist(fname_slicestart, 'file')
            fname_slicestart = fullfile(rawtif_dir, 'startslice.txt');
        end
        
        if exist(fname_slicestart, 'file')
            fid_slicestart = fopen(fname_slicestart, 'r');
            curLine = fgets(fid_slicestart);
            sliceStart = sscanf(curLine, '%d');
            parameters.nRemLowerSlices = max(sliceStart - 1, 0);
            fclose(fid_slicestart);
        end
        
        try
            cellProcStat = ls_detect_cell_membrane(gen_opt.out_dir, gen_opt.out_dir_iso, gen_opt.segmentation_dir, ...
                path_gen, cellChanIdx, chrChanIdx, protChanIdx, ratioFlag, parameters);
            summary_database.segmentation(cell_idx) = cellProcStat;
        catch ME
            getReport(ME, 'extended', 'hyperlinks', 'off')
        end
    else
        summary_database.segmentation(cell_idx) = chrProcStat;
    end
    writetable(summary_database, exp_database, 'Delimiter','\t');
end
end
function [gen_opt_default, parameters_default] = get_default()
gen_opt_default = struct(...
    'exp_dir', '', ...  % data directory
    'out_dir',  '',...  % directory to store the results
    'exp_database', 'full_database.txt', ... % filename for experiment database text file (tab delimited file)
    'cellIdx', [1], ... % cell to process
    'out_dir_iso', '', ...  % directory to store the isotropic sampled results
    'segmentation_dir', 'Segmentation',  ... % name of subdirectory for segmentation
    'segmentation_reprocess', 1); % reprocess if 1 , pass if -2

parameters_default = struct( ...
    'nRemLowerSlices', 0, ...       % remove slices above this number. It is base 0. Applies to membranes first and then to chromatin. Overwritten with local inputs startslice
    'overWriteMarker', 0, ...       % Rerun chromosome segmentation
    'sigmaBlurChr', 3, ...          % Sigma for Gaussian filter
    'hSizeBlurChr', 5, ...          % Kernel size for Gaussian filter
    'bFactor2DChr', 0.33, ...       % Contribution of 2D threshold
    'splitVolChr',  2000, ...       % Minimum volume for to be considered for splitting
    'mergeVolChr',  150, ...        % Maximum volume of a blob that is considered for merging
    'minProphaseVolChr', 910, ...   % minimum volume of prophase chromatin
    'maxCentDispChr',   11, ...     % maximum displacement between centroids and 2 subsequent time points
    'maxNoInitObjChr',  450, ...    % maximum number of initial objects  % options for detect_cell_membranes
    'saveIsotropicMatFiles', 1, ... % Save isotropic sampling files
    'rCRegionNeg', 1, ...           % radius of disk structuring element
    'pairDistNeg', 5, ...           % distance used in ls_generate_referenc_points
    'bFactor2DNeg', 0.20, ...       % Contribution of 2D threshold for membranes
    'sigmaBlurNeg', 5, ...          % Sigma for negative staining channel
    'hSizeBlurNeg', 7, ...          % Kernel size for negative staining channel
    'sigmaRatioNeg', 3, ...         % Sigma for gaussian blur in ratio image.
    'hSizeRatioNeg', 5, ...         % Kernel size for gaussian blur in ratio image
    'minVolNeg', 500, ...           % minimal Volume of cell
    'smoothcontourNeg', 25);        % number of points used in smoothing;\
end
