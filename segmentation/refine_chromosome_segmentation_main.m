
current_path = fileparts(mfilename('fullpath'));

addpath(fullfile(current_path,'segmentation_code'));
addpath(fullfile(current_path,'segmentation_external_code'));
refIntPor = 0.85;
expDatabaseFilename = 'full_database.txt';
expDir = fullfile('V:\MitoSys_data\Data', filesep);
outRootDir = expDir;
%outRootDir = fullfile('W:', 'Julius', 'Data', 'MitoSysCondensins', filesep);
dataB = readtable(fullfile(expDir,expDatabaseFilename),'Delimiter','\t');
numCells = size(dataB,1);
reprocess = 0;
toprocess = (dataB.segmentation > 0).*(dataB.manualQC>0).*(dataB.time_alignment>0).*(dataB.segmentation>0);
toprocessIdx = find(toprocess');

% cells missing processing
 pathmissing = {'140501_RACGAP1\MitoSys1\Result\cell0008_R0022', '140501_RACGAP1\MitoSys2\Result\cell0017_R0009',...
     '140508_AURKB\MitoSys2\Result\cell0015_R0013' , '150219_CENPA\MitoSys2\Result\cell0007_R0008', ...   
 '150220_NES\MitoSys1\Result\cell0014_R0027',  '160825_gfpNUP107z26z31\MitoSys1\Result\cell0016_R0020', ... 
 '170413_gfpKIF4Ac173\MitoSys2\Result\cell0001_R0007'}

for cellIdx = toprocessIdx
    
        for i= 1:length(pathmissing)
            if strcmp(dataB.filepath{cellIdx},  pathmissing{i})
             pathmissing{i}
             curCellDir = fullfile(expDir, dataB.filepath{cellIdx}, 'Preprocessing', 'Segmentation', filesep);
             ls_refine_chromosome_segmentation(curCellDir,  outRootDir, ...
                dataB, cellIdx, refIntPor, reprocess);
            end
        end
    
end
%  for cellIdx = toprocessIdx
%         curCellDir = fullfile(expDir, dataB.filepath{cellIdx}, 'Preprocessing', 'Segmentation', filesep);
%         ls_refine_chromosome_segmentation(curCellDir,  outRootDir, ...
%             dataB, cellIdx, refIntPor, reprocess);
% end