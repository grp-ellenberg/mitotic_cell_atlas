function [procState] = ls_detect_chromosomes(out_dir, seg_dir, path_gen, chrChanIdx, parameters)
% LS_DETECT_CHROMOSOMES
% This function segments chromosome from chrChan, anotate mitotic stages, and 
% predict cell axis and save chromsome chrMarker for plasma membrane detection
% LS_DETECT_CHROMOSOMES(out_dir, seg_dir, path_gen, chrChanIdx) Run segmentation using default options
% LS_DETECT_CHROMOSOMES(out_dir, seg_dir, path_gen, chrChanIdx, options)  Run segmentation with specified segmentation options
% INPUTS:
%   * out_dir: Main directory to store results
%   * seg_dir: Subdirectory name for storing results of ls_detect_chromosomes
%   mat files
%   * path_gen: Object of class pathgen_mitosys to generate name of files
%   * chrChaIdx: Index of chromosome channel
%   * options: options for segmentation (see declaration of default)
%
% Author: Julius Hossain, EMBL, Heidelberg, Germany: julius.hossain@embl.de, 2014-07-25
% Update:
%   2016-06-21
%   2017-06-01 Antonio Politi removed xlswrite to speed up
%              processing
%   2017-09-11 Antonio Politi, option struct
%   2018-03-15 Antonio Politi, Path generator class used. Struct parameters
parameters_default = struct(...
        'sigmaBlurChr', 3, ...          % Sigma for Gaussian filter
        'hSizeBlurChr', 5, ...          % Kernel size for Gaussian filter
        'bFactor2DChr', 0.33, ...       % Contribution of 2D threshold
        'splitVolChr',  2000, ...       % Minimum volume for to be considered for splitting
        'mergeVolChr',  150, ...        % Maximum volume of a blob that is considered for merging
        'minProphaseVolChr', 900, ...   % minimum volume of prophase chromatin
        'maxCentDispChr',   11, ...     % maximum displacement between centroids and 2 subsequent time points
        'maxNoInitObjChr',  450);      % maximum number of initial objects
if nargin < 5
    parameters = parameters_default;
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    fprintf('ls_detect_chromosomes: Fifth argument options does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
    return
end
close all;
assert(isa(path_gen, 'pathgen_mitosys'), 'Third entry must be of class pathgen_mitosys found in directory IO');

disp(['Detect Chromosomes: ' path_gen.getcelldir(out_dir, '')]);
if ~exist(path_gen.getcelldir(out_dir, seg_dir), 'dir')
    mkdir(path_gen.getcelldir(out_dir, seg_dir));
end


%Define the maximum number of subplots to display intermediate results
%(currently disabled)
%disRow = 4; 
%disCol = 4;
procState = 0;             % 0 or 1 if success
tStart = 1;                % time point to start

%% Read image dimensions 
zinit = 1; % Set higher than one if you do not want to process some lower slices
[dx, dy, NzOrig, Nz, Nc, voxelSizeX, voxelSizeY, voxelSizeZ, voxelSizeZ_Iso,  voxelVolume_Iso, zFactor] = ....
    get_image_dimensions_bf(path_gen.imgpaths{end}, zinit);


detCoords = zeros(2,3);     % Coordinates center of mass
ncVolume = zeros(path_gen.tmax, 1);  % nuclear volume
minEigen = zeros(path_gen.tmax, 1);  % minimal eigenvalue
detCH = zeros(path_gen.tmax, 1);     % ??
inHomCount = 0; % Defines the number of times we need to apply fn_InhomToHom(stk), should be a global parameter
inHomCheck = 1; % Flag whether to check homogeinity or not, should be a global paramter


outstruct = struct('tpoint', [], 'ncVolume1', [], 'ncVolume2', [], 'Rv', [], 'phase', [], 'cent_nuc1', [], 'cent_nuc2', []);
tpoint = tStart;
curLoopCount = 1;
matfiles = path_gen.getmatpaths(out_dir, seg_dir);
reportfiles = path_gen.getreportpaths(out_dir, seg_dir);
tmax = path_gen.tmax;
fprintf('Processing time point: ');
prevTpoint = tpoint-1;
while tpoint <= tmax
    %% Read channel for chromosomes
    [reader, dim] = getBioformatsReader(path_gen.imgpaths{tpoint});
    if Nc >= chrChanIdx 
        chrStackOrig = getTPointBioFormats(reader, dim, 1, chrChanIdx);
        chrStackOrig = chrStackOrig(:, :, zinit:NzOrig);
        reader.close()
    else
        error('ls_detect_chromosomes: Stack contains %d channels chromosome channel is %d', Nc, chrChanIdx);
    end
    % Diplay input image stack
    % displaySubplots(chrStackOrig, 'Display input image stack', disRow, disCol, NzOrig, 1, 2);
    
    %% Processing chromsome stack
    if tpoint>prevTpoint
        fprintf('%d, ', tpoint);
        prevTpoint = tpoint;
    end
    
    chrStack = ls_gen_intermediate_slices(chrStackOrig, zFactor);
    clear chrStackOrig;
    
    if tpoint == tStart
        chrRegion = zeros(dx,dy,Nz);%Store the detected chromosome region
        chrRegionCur1 = zeros(dx,dy,Nz);
        chrRegionCur2 = zeros(dx,dy,Nz);
        chrRegionPrev1 = zeros(dx,dy,Nz);
        bordImg = zeros(dx,dy,Nz);
        bordImg(1,:,:) = 1; bordImg(end,:,:) = 1;
        bordImg(:,1,:) = 1; bordImg(:,end,:) = 1;
        imgCentX = (dx+1)/2;
        imgCentY = (dy+1)/2;
        chromosomes = zeros(dx,dy,Nz);
    end
    % Display filtered data
    % displaySubplots(chrStack, 'Filtered chromosome', disRow, disCol, Nz, zFactor, 2);
    
    %% Detection of chromosome
    chrStack = imgaussian(chrStack, parameters.sigmaBlurChr, parameters.hSizeBlurChr); 
    
    % This part deals with inhomogeinity where intensity of chr of interest is dim
    for iter = 1:inHomCount
        chrStack = ls_inhomogeneous_to_homogeneous(chrStack);
    end
    
    [chrThresh3D, chrThresh2D] = ls_get_thresholds(chrStack);
    
    for i = 1:Nz
        chrRegion(:,:,i) = double(chrStack(:,:,i) >= (chrThresh3D * (1 - parameters.bFactor2DChr) + chrThresh2D(i) * parameters.bFactor2DChr));
        chrRegion(:,:,i) = imfill(chrRegion(:,:,i), 'holes');
    end
    
    threeDLabel = bwconncomp(chrRegion,18);
    nCompI = threeDLabel.NumObjects;
    %disp(sprintf('Number of connected components is: %g', nCompI));
    
    %Check whether it went too low
    if inHomCheck == 1 && tpoint == tStart && nCompI >= parameters.maxNoInitObjChr && inHomCount >=1
        inHomCount = inHomCount - 1;
        inHomCheck = 0;
        fileID = fopen(reportfiles.par, 'a');
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'nCompI:', nCompI);
        fclose(fileID);
        continue;
    end
    
    if (inHomCheck == 0 || tpoint == tStart) && nCompI >= parameters.maxNoInitObjChr
       [chrRegion, chrThresh3D, chrThresh2D, threeDLabel]= ls_adjust_thresholds_up(chrStack, chrThresh3D, chrThresh2D, parameters.maxNoInitObjChr, parameters.bFactor2DChr, threeDLabel); 
    end
    
    nCompF = threeDLabel.NumObjects;
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    [initVol, idx] = max(numPixels);
    if threeDLabel.NumObjects > parameters.maxNoInitObjChr * 0.10 % This is just to speed up the process
        chrRegion(:,:,:) = 0;
        for i=1:min(round(parameters.maxNoInitObjChr * 0.10),threeDLabel.NumObjects)
            [~, idx] = max(numPixels);
            chrRegion(threeDLabel.PixelIdxList{idx}) = 1;
            numPixels(idx) = 0;
        end
    end
    
    initVol = initVol * voxelVolume_Iso;
    
    if inHomCheck == 1 && tpoint == tStart && initVol < 3*parameters.mergeVolChr
        if inHomCount == 0
            fileID = fopen(reportfiles.par,'w');
        else
            fileID = fopen(reportfiles.par,'a');
        end
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'initVol:', initVol);
        fclose(fileID);
        inHomCount = inHomCount + 1;
        continue;
    end
    
    %fn_PC_DisplaySubplots(chrRegion, 'Detected chromosome', disRow, disCol, Nz, zFactor, 2);
    %disp(sprintf('chrThresh3D = %g', chrThresh3D));

    chrMarker = ls_split_connected_chromosomes(chrRegion, voxelSizeX, voxelSizeY, voxelSizeZ_Iso, bordImg, parameters.splitVolChr, parameters.mergeVolChr);
    %displaySubplots(chrMarker, 'Chromosome - initial chrMarker after split', disRow, disCol, Nz, zFactor, 2);

    chrMarker = ls_merge_small_regions(chrRegion, chrMarker, bordImg, voxelSizeX, voxelSizeY, voxelSizeZ_Iso, parameters.mergeVolChr);
    %displaySubplots(chrMarker, 'chrMarker image - merged', disRow, disCol, Nz, zFactor, 2);
    
    chrMarker = ls_remove_small_objects(chrMarker, 18, parameters.mergeVolChr, bordImg, voxelVolume_Iso);
    %Display chrMarker image - refined
    %fn_PC_DisplaySubplots(chrMarker, 'chrMarker image - refined', disRow, disCol, Nz, zFactor, 2);
     
    %%
    threeDLabel = bwconncomp(chrMarker,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    %Use location information to select the nucleus of interest
    stat = regionprops(threeDLabel,'Centroid');
    
    if tpoint == tStart
        %Normalize the number of pixels based on three properties
        [chrRegionCur1, curCentX1, curCentY1, curCentZ1] = ls_detect_chromosome_in_first_stack(threeDLabel, stat, imgCentX, imgCentY, numPixels, dx, dy, Nz);
        
        distCent = sqrt((imgCentX-curCentX1)^2 + (imgCentY-curCentY1)^2) * voxelSizeX;
        ncVolume1 = sum(sum(sum(chrRegionCur1))) * voxelVolume_Iso; 
        %disp(sprintf('distCent = %g', distCent));
        
        if inHomCount == 0 && inHomCheck == 1
            fileID = fopen(reportfiles.par,'w');
        else
            fileID = fopen(reportfiles.par,'a');
        end
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f   %s %04.f   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'volChr: ', ncVolume1, 'nCompI: ', nCompI, 'nCompF: ', nCompF);
        fclose(fileID);
        numChr = 1;
        if inHomCheck == 1
            if ncVolume1 > parameters.minProphaseVolChr * 2 && inHomCount >= 1
                inHomCount = inHomCount - 1;
                inHomCheck = 0;
                curLoopCount = curLoopCount +1;
                if curLoopCount >5
                    procState = 0;
                    return;
                end
                continue; %TODO this skips the end of the time point calculation. some variable are not initiated
            elseif distCent > parameters.maxCentDispChr || ncVolume1 < parameters.minProphaseVolChr
                inHomCount = inHomCount + 1;
                curLoopCount = curLoopCount +1;
                if curLoopCount >5
                    procState = 0;
                    return;
                end
                continue; %TODO this skips the end of the time point calculation. some variable are not initiated
            else
                inHomCheck = 0;
            end
        end
    else
        if numChr == 1
            prevCentX2 = prevCentX1;
            prevCentY2 = prevCentY1;
        end
        [chrRegionCur1, chrRegionCur2, curCentX1, curCentY1, curCentZ1, curCentX2, curCentY2, curCentZ2, numChr] = ls_detect_chromosome_in_later_stacks(threeDLabel, stat, chrRegionPrev1, prevCentX1, prevCentY1, prevCentX2, prevCentY2, numPixels, numChr, voxelSizeX, voxelSizeY, voxelSizeZ_Iso);
        
        ncVolume1 = sum(chrRegionCur1(:)) * voxelVolume_Iso;
        ncVolume2 = sum(chrRegionCur2(:)) * voxelVolume_Iso;
        fileID = fopen(reportfiles.par,'a');
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f   %s %04.f   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'volChr: ', ncVolume1+ncVolume2, 'nCompI: ', nCompI, 'nCompF: ', nCompF);
        fclose(fileID);
    end
       
    prevCentX1 = curCentX1;
    prevCentY1 = curCentY1;
    chrRegionPrev1 = chrRegionCur1;
    
    if numChr == 2
        prevCentX2 = curCentX2;
        prevCentY2 = curCentY2;
    end
       
    %%  End of masking chrStack       
    if numChr == 1
        [x,y,z] = ls_threed_coord(find(chrRegionCur1),dx,dy);
        [V, R, xg, yg, zg] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ_Iso);
        ncVolume(tpoint) = ncVolume1;
        minEigen(tpoint) = R(1,1);
        detCH(tpoint) = 1;
    else
        tempVol = or(chrRegionCur1, chrRegionCur2);
        [x,y,z] = ls_threed_coord(find(tempVol),dx,dy);
        [V, R, xg, yg, zg] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ_Iso);
        ncVolume(tpoint) = ncVolume1 + ncVolume2;
        minEigen(tpoint) = R(1,1);
        detCH(tpoint) = 2;
        clear tempVol;
    end
    
    chromosomes(:,:,:) = 0;
    chromosomes(chrRegionCur1(:,:,:) == 1) = 1;
    chromosomes(chrRegionCur2(:,:,:) == 1) = 2;
    
    if numChr == 1
        detCoords = [curCentX1 curCentY1 curCentZ1; curCentX1 curCentY1 curCentZ1];
    else
        detCoords = [curCentX1 curCentY1 curCentZ1; curCentX2 curCentY2 curCentZ2];
    end
    
    save(matfiles{tpoint}, 'chrMarker', 'chromosomes', 'numChr', 'detCoords', 'chrThresh3D', 'chrThresh2D', 'parameters');
    % fill struct for output
    outstruct(tpoint).tpoint = tpoint;
    outstruct(tpoint).ncVolume1 = ncVolume1;
    outstruct(tpoint).Rv =  [R(1,1), R(2,2), R(3,3)];
    outstruct(tpoint).cent_nuc1 =  [curCentX1, curCentY1, curCentZ1];
    if numChr == 2
        outstruct(tpoint).ncVolume2 = ncVolume2;
        outstruct(tpoint).cent_nuc2 =  [curCentX2, curCentY2, curCentZ2];
    end  
    %%End of displaying volume and predicting parameters
    tpoint = tpoint + 1;
    curLoopCount = 1;
    clear maskedchrStack;
    clear wsLabel;
    clear distImage;
    clear chrMarker;
    clear X;
    clear Y;
    clear Z;
    clear contourCRegionThreeD;
    clear dumCol;
    clear hist;
    close all;    
end

phase = ls_annotate_mitotic_stages(minEigen, ncVolume, tmax, detCH);
for tpoint = tStart:tmax
    outstruct(tpoint).phase = phase(tpoint);
end

fidtxt = fopen(reportfiles.res, 'w');
fprintf(fidtxt, 'tpoint\tncVolume1_um3\tncVolume2_um3\teigenValue1\teigenValue2\teigenValue3\tmitoticPhase\tcentX1\tcentY1\tcentZ1\tcentX2\tcentY2\tcentZ2\n');
% write to txt file
for tpoint = tStart:tmax
    if ~isempty(outstruct(tpoint).ncVolume2)
            fprintf(fidtxt, '%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\n', ...
            outstruct(tpoint).tpoint, outstruct(tpoint).ncVolume1, outstruct(tpoint).ncVolume2, outstruct(tpoint).Rv(1), ...
            outstruct(tpoint).Rv(2), outstruct(tpoint).Rv(3), outstruct(tpoint).phase, outstruct(tpoint).cent_nuc1(1), outstruct(tpoint).cent_nuc1(2),...
            outstruct(tpoint).cent_nuc1(3), outstruct(tpoint).cent_nuc2(1), outstruct(tpoint).cent_nuc2(2), outstruct(tpoint).cent_nuc2(3));
    else
            fprintf(fidtxt, '%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\t%.4f\n', ...
            outstruct(tpoint).tpoint, outstruct(tpoint).ncVolume1, outstruct(tpoint).ncVolume2, outstruct(tpoint).Rv(1), ...
            outstruct(tpoint).Rv(2), outstruct(tpoint).Rv(3), outstruct(tpoint).phase, outstruct(tpoint).cent_nuc1(1), outstruct(tpoint).cent_nuc1(2),...
            outstruct(tpoint).cent_nuc1(3), [], [], []);
    end
end
fclose(fidtxt);
fprintf('-|\n');
procState = 1;
end
% Image display has been moved to a different function

% this will be done at end of landmark segmentation
% 8 corner points are set to 1 in order to keep effective volume same
% in all timepoints
% display chromosomes
% this should be done at the end for all files that passes the qc with
% the membranes
%     if removeMarkerDir == 0
%         chrRegionCur1(1,1,1) = 1;     chrRegionCur1(1,1,end) = 1; chrRegionCur1(1,end,1)   = 1;
%         chrRegionCur1(1,end,end) = 1; chrRegionCur1(end,1,1) = 1; chrRegionCur1(end,1,end) = 1;
%         chrRegionCur1(end,end,1) = 1; chrRegionCur1(end,end,end) = 1; 
%         [X,Y,Z] = meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
%         hVol = figure('Name', strcat('Reconstructed 3D volume - timepoint: ', num2str(tpoint)), 'NumberTitle','off', 'Visible', 'off');
%         isosurface(X,Y,Z,chrRegionCur1,0.9);
%         alpha(0.5)
%         isosurface(X,Y,Z,chrRegionCur2,0.7);
%         alpha(0.7)
%         axis equal
%        
%         saveas(hVol, savefile_jpg, 'jpg');
%         delete(hVol);
%     end