function [smoothCellVolume] = pss_temporal_smoothing(cellVolume, prevCellVolume, nucVolume, zFactor)
%   UNTITLED Summary of this function goes here
%   Detailed explanation goes here
maxIteration = 14;
morphSe = zeros(3,3,3);
morphSe(:,:,1) = [0 0 0; 0 1 0; 0 0 0];
morphSe(:,:,2) = [0 1 0; 1 1 1; 0 1 0];
morphSe(:,:,3) = [0 0 0; 0 1 0; 0 0 0];

nucVolume = ls_gen_intermediate_slices(nucVolume, zFactor);
cellVolume = ls_gen_intermediate_slices(cellVolume, zFactor);
prevCellVolume = ls_gen_intermediate_slices(prevCellVolume, zFactor);

seedCellVolume = and(prevCellVolume, cellVolume);
seedCellVolume(nucVolume(:,:,:) >0) = 1;

tPixCell = sum(sum(sum(cellVolume)));
tPixPrevCell = sum(sum(sum(prevCellVolume)));
tPixSeedCell = sum(sum(sum(seedCellVolume)));
if (tPixCell<tPixPrevCell*1.075)
    smoothCellVolume = ls_remove_intermediate_slices(cellVolume, zFactor);
else
    loopCount = 0;
    while tPixSeedCell <tPixPrevCell*1.02 && loopCount < maxIteration
        seedCellVolume = imdilate(seedCellVolume, morphSe);
        seedCellVolume(cellVolume(:,:,:) == 0) = 0;
        seedCellVolume(nucVolume(:,:,:) >0) = 1;
        
        tPixSeedCell = sum(sum(sum(seedCellVolume)));
        loopCount = loopCount + 1;
    end
    smoothCellVolume = ls_remove_intermediate_slices(seedCellVolume, zFactor);
end
end