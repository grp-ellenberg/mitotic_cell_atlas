function [avgBackInt, nucRegionSliceRim, maxZIndex] = ls_calculate_background_intensity(nucVolume, nuc, dil1, dil2)
%This function calculate the background intensity of nucleoplasm
%nucVolume: chromosome mass
%nuc: stack containing chromosome
%dil1: number of dilation needed to have outer part of the rim
%dil2: number of dilation needed to get inner part of the rim

%Author: M. Julius Hossain, EMBL Heidelberg
%Last update: 2017-04-07
lowerChopPor = 0.1; %Portion to be excluded from the lower part of the histogram
effPor = 0.5; % Effective portion of the histogram that taken for the background estimation
nucVolume(nucVolume>0) = 1;

sumZSlice = sum(sum(nucVolume));
[~, maxZIndex] = max(sumZSlice);

nucRegionSlice = nucVolume(:,:,maxZIndex);
nucRegionSliceDil1 = imdilate(nucRegionSlice, strel('diamond',dil1));
nucRegionSliceDil2 = imdilate(nucRegionSlice, strel('diamond',dil2));
nucRegionSliceRim = imsubtract(nucRegionSliceDil1, nucRegionSliceDil2);

nucImgSliceRim = nuc(:,:,maxZIndex);
nucImgSliceRim(nucRegionSliceRim(:,:) == 0 ) = 0;

% totalInt = sum(sum(nucImgSliceRim));
% totalPix = sum(sum(nucRegionSliceRim>0));
% avgBackInt = totalInt/totalPix;

[xSize, ySize] = size(nucImgSliceRim);
maxVal = max(max(nucImgSliceRim));
histN = round(maxVal)+1;
histogram = zeros(1,histN);

for i = 1:xSize
    for j = 1:ySize
        hIndex = round(nucImgSliceRim(i,j))+1;
        histogram(hIndex) = histogram(hIndex)+1;
    end
end
histogram(1) = 0;

totalPix = sum(histogram);
startIdx = 1;
for startIdx = 1:histN
    if(sum(histogram(1:startIdx))>= totalPix*lowerChopPor)
        break;
    end
end

for endIdx = histN:-1:1
    if(sum(histogram(endIdx:end))>= totalPix*effPor)
        break;
    end
end

totalPix = sum(histogram(startIdx:endIdx));
totalInt = 0;
for i = startIdx:endIdx
    totalInt = totalInt + histogram(i) *i;
end
avgBackInt = totalInt/totalPix;
end

