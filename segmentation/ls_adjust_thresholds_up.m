function [sRegion, thresh3D, thresh2D, threeDLabel]= ls_adjust_thresholds_up(stack, thresh3D, thresh2D, maxNoInitObj, bFactor2D, threeDLabel)
% This function adjusts threshold up so that number of connected component
% gets smaller than maxNoInitObj

Nz = size(stack, 3);
sRegion = zeros(size(stack));
inc = 0.01;

for thFactor = 1+inc: inc: 1.6
    thresh3D = thresh3D * thFactor;
    thresh2D = thresh2D * thFactor;
    for i = 1:Nz
        sRegion(:,:,i) = double(stack(:,:,i) >= (thresh3D * (1-bFactor2D) + thresh2D(i) * bFactor2D));
        sRegion(:,:,i)=imfill(sRegion(:,:,i),'holes');
    end
    
    threeDLabel = bwconncomp(sRegion,18);
    if threeDLabel.NumObjects < maxNoInitObj * 1.025
        break;
    end
end    
end