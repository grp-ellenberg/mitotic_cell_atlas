function [phase] = ls_annotate_mitotic_stages(minEigen, ncVolume, tfin, detCH)
%Annotate mitotic stages for cell axis  prediction
phase = zeros(tfin,1);
minEigenTemp = minEigen;
[minVal1, minIdx] = min(minEigenTemp);
minEigenTemp(minIdx) = 10000;
[minVal2, minIdx] = min(minEigenTemp);
minVal = (minVal1+minVal2)/2;
numCH = max(detCH);

if numCH == 2
    divLoc = find(detCH == 1, 1, 'last');
end

if minVal >2.5 && numCH == 1
    phase(:) = 3; %Cell neigther divided nor progessed to Metaphase 
else
    phase(minEigen(:) < minVal*2) = 4;
    if numCH == 2 && divLoc <tfin
        phase(divLoc+1:end) = 0;
    end
    phaseTemp = phase;
    idxFirst = find(phase == 4, 1,'first');
    idxLast = find(phase == 4, 1,'last');

    phaseTemp(idxFirst:idxLast) = 4;
    phaseTemp = phaseTemp - phase;
    if sum(phaseTemp) > 4
        idxTempLast = find(phaseTemp == 4, 1,'last');
        phase(1:idxTempLast) = 3;
    elseif sum(phaseTemp) == 4
        idxTempLast = find(phaseTemp == 4, 1,'last');
        phase(idxTempLast) = 4;
        if(idxFirst>1)
            phase(1:idxFirst-1) = 3;
        end
    else
        if(idxFirst>1)
            phase(1:idxFirst-1) = 3;
        end
    end

    if idxLast + 1 < tfin
        phase(idxLast+1: end) = 5;
    end

    if(idxLast+ 3 <=tfin)
        [minVal2, minIdx] = min(ncVolume(idxLast+1:end));
    end

    if idxLast + minIdx + 2 <=tfin
        phase(idxLast+minIdx+2:end) = 6;
    end
    for i = 1:tfin
        if phase(i) == 0
            if i==1
                phase(i) = phase(i+1);
            elseif i==tfin
                phase(i) = phase(i-1);
            else
                phase(i) = round((phase(i-1) + phase(i+1))/2);
            end
        end
    end
end
end

