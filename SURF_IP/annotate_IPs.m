function annotate_IPs(gen_opt, parameters)
%% ANNOTATE_IPs Annotate SURF IP using a bag of words dictionary
% ANNOTATE_IPs() Use default options and parameters
% ANNOTATE_IPs(gen_opt) Use paths as specified in struct gen_opt
% ANNOTATE_IPs(gen_opt, parameters) Use paths as specified in struct gen_opt and parametes as specified.
%
%   INPUT:
%       * gen_opt    - struct for the general paths
%       * parameters - struct giving the parameters of the feature extraction
%       See function get_default for more details
%   OUTPUT: 
%       * a bag-of-words feature vector is constructed for each image that have passed the QC
%
% Assigns all SURF interest points of each single image into a pre-defined SURF interest point dictionary.
%
% Author: Yin Cai, EMBL, Heidelberg, 2015.7.30
% Update 
% 2016.6.13 - Integration into the pipeline. Small change on
% background IP intensity level definition (ipsize and lastsize used as
% radius of the circle instead square). Added a part of
% ForJuliusAndJK_smooth.m (smoothing of feature vector over time)
% 2018.03.20 Struct parameters input

%% Load parameters
[gen_opt_default, parameters_default] = get_defaults();

if nargin < 1
    gen_opt = gen_opt_default;
end
if nargin < 2
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('Second argument parameters does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
end

if gen_opt.annotate_reprocess == -2
    return
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end
[gen_opt.mst_dir, status] = verifyDirectory(gen_opt.mst_dir, 'Specify MST model directory', true);
if ~status
    return
end
[gen_opt.surfdict_dir, status] = verifyDirectory(gen_opt.surfdict_dir, 'Specify SURF dictionary directory', true);
if ~status
    return
end

mstmodel_file = fullfile(gen_opt.mst_dir, gen_opt.mst_fname);
dictionary_file = fullfile(gen_opt.surfdict_dir, gen_opt.surfdict_fname);
if ~exist(mstmodel_file, 'file')
    error('No MST model file %s has been found in directory %s', gen_opt.mst_fname, gen_opt.mst_dir)
end
if ~exist(dictionary_file, 'file')
    error('No SURF dictionary file %s has been found in directory %s', gen_opt.surfdict_fname, gen_opt.surfdict_dir)
end


mst_model = load(mstmodel_file);
surf_dic = load(dictionary_file);
if isfield(surf_dic, 'FeatMat') && isfield(surf_dic, 'feats_centroids')
    if ~isfield(surf_dic, 'uuid')
        warning('Directory has no UUID!')
    end
else
    error('annotate_IPs: matfile %s is not a SURF dictionary file', dictionary_file);
end

%% Load database
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
if gen_opt.annotate_reprocess == 1
    vec_QC = -ones(length(summary_database.feature_vector),1);
else
    vec_QC = summary_database.feature_vector;
end
proc_history = (summary_database.manualQC > 0).*(summary_database.feature_extract>0);
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);


%% Load the mat file from the segmentation and registration
for cell_idx = vec_idx
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    features_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.poifeature_dir);
    annotate_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.annotate_dir);
    annotate_dir =  path_gen.getcelldir(gen_opt.out_dir, gen_opt.annotate_dir);
    mstalign_dir =  path_gen.getcelldir(gen_opt.out_dir, gen_opt.mstalign_dir);
    if ~exist(annotate_dir, 'dir')
        mkdir(annotate_dir);
    end
    % Check whether processed file was processed in the same way
    if gen_opt.annotate_reprocess == 0  && vec_QC(cell_idx) == 1
        if ~exist(annotate_matfiles{end}, 'file')
            warning('POI feature files are missing  in %s. Reprocess data ', annotate_dir);
            vec_QC(cell_idx) = -1;
        else
            previous = load(annotate_matfiles{end});
            % Usage of dictionary is based on UUID of dictionary
            if ~isfield(previous, 'uuid') || ~strcmp(previous.uuid, surf_dic.uuid)
                warning('Dictionary mismatching. Output will be overwritten for %s',  annotate_dir)
                vec_QC(cell_idx) = -1;
            end
        end
    end
    if vec_QC(cell_idx) ~= -1
        continue
    end
    fprintf('POI SURF IP assign to dictionary Cell_idx: %d\n',  cell_idx);
    
    celltime = load(fullfile(mstalign_dir, gen_opt.mstalign_fname));
    for tpoint = 1:length(celltime.mitotime)
        celltime.mitotime(tpoint, 2) = sum(mst_model.time_cluster.class_start <= celltime.mitotime(tpoint,1));
    end
    % Initiate framewise qc index
    qc_extra = 1;
    % Initiate metrices for smoothing and summarizing
    corrtrace = zeros(size(celltime.mitotime,1), 1); % initialte the correlation vector (correlation coefficient of the max image of two adjacent frames)
    M = load(features_matfiles{1});
    
    frame_t0 = M.maxProj_low(:); % load the image of the first frame
    f_ip = []; % cat SURF IP features
    f_glob = []; % cat global features - fraction of intensity > threshold in chromosome volume, fraction of intensity > threshold in putative spindle
    BG = []; % cat background calculation
    for tpoint = 1:size(celltime.mitotime, 1)
        fprintf('-');
        M = load(features_matfiles{tpoint});
        if ~isfield(M, 'parameters') % Legacy 
            try
                M.parameters = M.feature_parameters;
            catch
                M.parameters = M.options;
            end
        end
        if sum(M.flag)==0
            qc_extra = 0;
            continue;
        end
        
        % Clean up correlation features and merge the correlation to H2B
        % and to midzone by position check
        M.feature_corr(isnan(M.feature_corr)) = 0;
        M.feature_mid(isnan(M.feature_mid)) = 0;
        merged_corr = M.feature_corr.*sum(M.feature_pos(:,3:4) > 0, 2) + ...
            M.feature_mid.*sum(M.feature_pos(:,1:2) > 0 , 2);
        % Construct the feature matrix
        SurfFeats = [M.feature_LBP M.feature_spinImage merged_corr M.feature_pos];
        SurfFeats = SurfFeats(M.flag > 0,:);
        IP_idxvec = (1:length(M.flag))';
        IP_idxvec = IP_idxvec(M.flag > 0);
        clustering_featnum = size(M.feature_LBP, 2)+size(M.feature_spinImage, 2);
        
        % Determine the background intensity
        % In the dictionaty training, threholds were pre-set to
        % define what is bright IPs. Here, using a more
        % cellular relative way, a background of the averaged
        % IP intensity is defined such that threshold for
        % bright can be set as 2x background
        
        % Find the most contrastarm IP in the cytoplasm, or, if
        % there is no one, in the cell
        lastpos = find(M.feature_pos(:,2) > 0);
        if isempty(lastpos)
            lastpos = find(M.flag > 0);
        end
        lastpos = lastpos(end);
        % Calculate the mean intensity of the Ip with the
        % lowest SURF metric
        lastsize = 6*M.InterestPoints(lastpos).Scale;
        lastsize = pi*lastsize^2;
        background_int = M.tot_int(lastpos)/lastsize;
        
        % First level annotation based on the metric and
        % position threshold. Detailed comments see
        % generate_SURF_dictionary.m
        kd_clustering = zeros(size(SurfFeats,1),4);
        kd_clustering(:,4) = double(SurfFeats(:,end) <= surf_dic.metric_level1);
        kd_clustering(:,3) = 4*sum(SurfFeats(:,(end-2):(end-1)),2);
        metric_level2_idx = 2*kd_clustering(:,3) + kd_clustering(:,4) + 1;
        for midx = 1:4
            kd_clustering(metric_level2_idx == midx,2) = double(SurfFeats(metric_level2_idx == midx,end) <= surf_dic.metric_level2(midx));
        end
        kd_clustering(:,1) = 4*(SurfFeats(:,(end-1)) + SurfFeats(:,(end-3)));
        asscl = 8*kd_clustering(:,1) + 4*kd_clustering(:,2) + 2*kd_clustering(:,3) + kd_clustering(:,4);
        % Usually, the 16 first levels should all have IPs
        % represented in the dictionary. However, in case that
        % the dictionary is small and some classes are missing,
        % assign the IP to the most close metrical class
        for l1_idx = 1:length(asscl)
            if ~ismember(asscl(l1_idx), surf_dic.index_centroids(:,1))
                ant1 = 8*kd_clustering(l1_idx, 1) + 4*mod(kd_clustering(l1_idx, 2) + 1, 2) + ...
                    2*kd_clustering(l1_idx, 3) + kd_clustering(l1_idx, 4);
                if ismember(ant1, surf_dic.index_centroids(:,1))
                    asscl(l1_idx) = ant1;
                else
                    ant2 = 8*kd_clustering(l1_idx, 1) + 4*kd_clustering(l1_idx, 4) + ...
                        2*kd_clustering(l1_idx, 3) + mod(kd_clustering(l1_idx,4) + 1, 2);
                    if ismember(ant2, surf_dic.index_centroids(:,1))
                        asscl(l1_idx) = ant2;
                    else
                        asscl(l1_idx) = 8*kd_clustering(l1_idx,1) + 4*mod(kd_clustering(l1_idx,4) + 1,2)+ ...
                            2*kd_clustering(l1_idx, 3) + mod(kd_clustering(l1_idx, 4) + 1, 2);
                    end
                end
            end
        end
        
        % Second level annotation
        % Depending on how the second level clustering is
        % performed for the particular first level class, one,
        % two or three steps are performed of:
        % 1) Disition tree of correlation features
        % 2) dbscan (nearst neighbor assignment) or kNN
        % (closest center assignment)
        % 3) spinImage feature based intensity distribution
        % desition tree
        asscl2 = zeros(size(asscl));
        lib_idx = zeros(size(asscl));
        for l1_idx = 1:length(asscl)
            if size(surf_dic.clustering_meta{asscl(l1_idx)+1},1) == 1
                if isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.algorithm,'kmeans') % assign to the cluster with the closest center
                    if isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.distance,'sqEuclidean')
                        correspond_centers = surf_dic.feats_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),:);
                        correspond_centers(:,end) = [];
                        sqEuc_dist = sum((correspond_centers-repmat(SurfFeats(l1_idx,1:clustering_featnum),[size(correspond_centers,1) 1])).^2,2);
                        [~,asscl2(l1_idx)] = min(sqEuc_dist,[],1);
                        lib_idx(l1_idx) = find(surf_dic.index_centroids(:,1)==asscl(l1_idx),1) + asscl2(l1_idx) - 1;
                        correspond_index = surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2);
                        asscl2(l1_idx) = correspond_index(asscl2(l1_idx));
                    else
                        error('only Euclidean distance is implemented')
                    end
                elseif isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.algorithm,'dbscan') % dbscan, assign to the cluster of the closest training point
                    correspond_centers = surf_dic.FeatMat(surf_dic.devcl==asscl(l1_idx),1:clustering_featnum);
                    correspond_index = surf_dic.devcl2(surf_dic.devcl==asscl(l1_idx));
                    correspond_centers = correspond_centers*surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.pca_coeff(:,1:surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.dim);
                    dbscan_dist = sum((correspond_centers-repmat(SurfFeats(l1_idx,1:clustering_featnum)*surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.pca_coeff(:,1:surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.dim),[size(correspond_centers,1) 1])).^2,2);
                    [~,asscl2(l1_idx)] = min(dbscan_dist,[],1);
                    asscl2(l1_idx) = correspond_index(asscl2(l1_idx));
                    lib_idx(l1_idx) = find(surf_dic.index_centroids(:,1)==asscl(l1_idx),1) + find(surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2)==asscl2(l1_idx),1) - 1;
                elseif isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{1}.algorithm,'none')
                    asscl2(l1_idx) = 1;
                    lib_idx(l1_idx) = find(surf_dic.index_centroids(:,1)==asscl(l1_idx),1);
                end
            elseif size(surf_dic.clustering_meta{asscl(l1_idx)+1},1) > 1
                % get the correlation class
                correspond_centers = surf_dic.FeatMat(surf_dic.devcl==asscl(l1_idx),1:(clustering_featnum+1));
                [~,corr_idx] = min(abs(correspond_centers(:,end) - SurfFeats(l1_idx,clustering_featnum+1)),[],1);
                corr_val = correspond_centers(corr_idx,end);
                if ismember(asscl(l1_idx),[7 6 3 2]) % nuc boundary
                    corr_cl = 1 + double(corr_val>parameters.NE(1)) + double(corr_val>parameters.NE(2));
                elseif ismember(asscl(l1_idx),[15 14 11 10]) % pure nuc
                    corr_cl = 1 + double(corr_val>parameters.NUC(1)) + double(corr_val>parameters.NUC(2));
                elseif ismember(asscl(l1_idx),[13 12 9 8 5 4 1 0]) % cyto and cell boundary
                    corr_cl = 1 + double(corr_val>parameters.MID(1)) + double(corr_val>parameters.MID(2));
                end
                if isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.algorithm,'kmeans')
                    if isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.distance,'sqEuclidean')
                        % Get the index of all second level
                        % classes with the same correlation
                        % feature range
                        cl2min = 0;
                        jidx = 1;
                        while jidx < corr_cl
                            cl2min = surf_dic.clustering_meta{asscl(l1_idx)+1}{jidx}.parameter(1) + cl2min;
                            jidx = jidx + 1;
                        end
                        cl2max = cl2min + surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1);
                        if surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.refine == 0 % in case of low-middle metric
                            correspond_centers = surf_dic.feats_centroids(((surf_dic.index_centroids(:,1)==asscl(l1_idx)).*(surf_dic.index_centroids(:,2)<=cl2max).*(surf_dic.index_centroids(:,2)>cl2min))>0,:);
                            correspond_centers(:,end) = [];
                            sqEuc_dist = sum((correspond_centers-repmat(SurfFeats(l1_idx,1:clustering_featnum),[size(correspond_centers,1) 1])).^2,2);
                            [~,asscl2(l1_idx)] = min(sqEuc_dist,[],1);
                            asscl2(l1_idx) = cl2min + asscl2(l1_idx);
                        else
                            % Assign the kNN center
                            correspond_centers = surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.centroids;
                            sqEuc_dist = sum((correspond_centers-repmat(SurfFeats(l1_idx,1:clustering_featnum),[size(correspond_centers,1) 1])).^2,2);
                            [~,ass2_test] = min(sqEuc_dist,[],1);
                            % Assign the spinImage class
                            rsh = (reshape(SurfFeats(l1_idx,(size(M.feature_LBP,2)+1):clustering_featnum),[length(M.parameters.spinImage.is) length(M.parameters.spinImage.ds)]))';
                            for beta = length(M.parameters.spinImage.is):-1:2
                                rsh(:,beta) = sum(rsh(:,1:beta),2);
                            end
                            lsh = zeros(length(M.parameters.spinImage.ds),1);
                            for beta2 = 1:length(M.parameters.spinImage.ds)
                                lsh(beta2) = find(rsh(beta2,:)>(rsh(beta2,end)/2),1);
                            end
                            ipsize = 6*M.InterestPoints(IP_idxvec(l1_idx)).Scale;
                            ipsize = pi*ipsize^2;
                            meanint = M.tot_int(IP_idxvec(l1_idx))/ipsize;
                            lsh = mean(lsh)/std(lsh)*(meanint>(parameters.sIth_factor*background_int));
                            if isnan(lsh)
                                lsh = -1;
                            end
                            ass2_test = ass2_test + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3)*double(lsh>0) + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3)*double(lsh>parameters.sIth_high) + cl2min;
                            % In case that the assigned class
                            % is not represented in the
                            % dictionary, assign to the closest
                            % second level class
                            if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                if lsh <= 0 % low intensity
                                    ass2_test = ass2_test + 2*(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3); % prefer to merge to high intensity low structure class
                                    if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                        ass2_test = ass2_test - (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3);
                                    end
                                elseif lsh <= parameters.sIth_high % very structured high contrast pattern
                                    ass2_test = ass2_test + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3); % prefer to merge to high intensity low structure class
                                    if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                        ass2_test = ass2_test - 2*(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3);
                                    end
                                elseif lsh > parameters.sIth_high % high intensity homogeneous pattern
                                    ass2_test = ass2_test - 2*(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3); % prefer to merge to low intensity pattern class
                                    if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                        ass2_test = ass2_test + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(1)/3);
                                    end
                                end
                            end
                            asscl2(l1_idx) = ass2_test;
                        end
                    else
                        error('only Euclidean distance is implemented')
                    end
                elseif isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.algorithm, 'dbscan')  % dbscan, assign to the cluster of the closest training point
                    % get the index range
                    cl2min = 0;
                    jidx = 1;
                    while jidx < corr_cl
                        cl2min = surf_dic.clustering_meta{asscl(l1_idx)+1}{jidx}.parameter(end) + cl2min;
                        jidx = jidx + 1;
                    end
                    cl2max = cl2min + surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end);
                    correspond_centers = surf_dic.FeatMat(((surf_dic.devcl==asscl(l1_idx)).*(surf_dic.devcl2>cl2min).*(surf_dic.devcl2<=cl2max))>0,1:clustering_featnum);
                    correspond_centers = correspond_centers*surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.pca_coeff(:,1:surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.dim);
                    dbscan_dist = sum((correspond_centers-repmat(SurfFeats(l1_idx,1:clustering_featnum)*surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.pca_coeff(:,1:surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.dim),[size(correspond_centers,1) 1])).^2,2);
                    [~,ass2_test] = min(dbscan_dist,[],1);
                    if surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.refine == 0 % in case of low metric
                        correspond_index = surf_dic.devcl2(((surf_dic.devcl==asscl(l1_idx)).*(surf_dic.devcl2>cl2min).*(surf_dic.devcl2<=cl2max))>0);
                        asscl2(l1_idx) = correspond_index(ass2_test);
                    else
                        % Assign dbscan nearst neighbor
                        ass2_test = surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.assigned(ass2_test);
                        % assign the spinImage class
                        rsh = (reshape(SurfFeats(l1_idx,(size(M.feature_LBP,2)+1):clustering_featnum),[length(M.parameters.spinImage.is) length(M.parameters.spinImage.ds)]))';
                        for beta = length(M.parameters.spinImage.is):-1:2
                            rsh(:,beta) = sum(rsh(:,1:beta),2);
                        end
                        lsh = zeros(length(M.parameters.spinImage.ds),1);
                        for beta2 = 1:length(M.parameters.spinImage.ds)
                            lsh(beta2) = find(rsh(beta2,:)>(rsh(beta2,end)/2),1);
                        end
                        ipsize = 6*M.InterestPoints(IP_idxvec(l1_idx)).Scale;
                        ipsize = pi*ipsize^2;
                        meanint = M.tot_int(IP_idxvec(l1_idx))/ipsize;
                        lsh = mean(lsh)/std(lsh)*(meanint>(parameters.sIth_factor*background_int));
                        if isnan(lsh)
                            lsh = -1;
                        end
                        ass2_test = ass2_test + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3)*double(lsh>0) + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3)*double(lsh>parameters.sIth_high) + cl2min;
                        % In case that the assigned class
                        % is not represented in the
                        % dictionary, assign to the closest
                        % second level class
                        if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                            if lsh <= 0
                                ass2_test = ass2_test + 2*(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3);
                                if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                    ass2_test = ass2_test - (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3);
                                end
                            elseif lsh <= parameters.sIth_high
                                ass2_test = ass2_test + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3);
                                if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                    ass2_test = ass2_test - 2*(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3);
                                end
                            elseif lsh > parameters.sIth_high
                                ass2_test = ass2_test - 2*(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3);
                                if ~ismember(ass2_test,surf_dic.index_centroids(surf_dic.index_centroids(:,1)==asscl(l1_idx),2))
                                    ass2_test = ass2_test + (surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.parameter(end)/3);
                                end
                            end
                        end
                        asscl2(l1_idx) = ass2_test;
                    end
                elseif isequal(surf_dic.clustering_meta{asscl(l1_idx)+1}{corr_cl}.algorithm,'none')
                    % Similar as before
                    cl2min = 0;
                    jidx = 1;
                    while jidx < corr_cl
                        cl2min = surf_dic.clustering_meta{asscl(l1_idx)+1}{jidx}.parameter(end) + cl2min;
                        jidx = jidx + 1;
                    end
                    asscl2(l1_idx) = 1 + cl2min;
                end
                lib_idx(l1_idx) = find(((surf_dic.index_centroids(:,1) == asscl(l1_idx)).*(surf_dic.index_centroids(:,2) == asscl2(l1_idx)))>0);
            end
        end
        
        % Summarize and save the bag-of-words vector as the
        % total intensity in each dictionary class
        POI_Feats_int = zeros(size(surf_dic.index_centroids,1),1);
        vector_int = M.tot_int(M.flag>0);
        for fh_idx = 1:length(POI_Feats_int)
            POI_Feats_int(fh_idx) = sum(vector_int(lib_idx==fh_idx));
        end
        % save the mitotic standard time in reduced form with
        % the feature vector allowing simple analysis
        red_time = celltime.mitotime(tpoint,2);
        uuid = surf_dic.uuid;
        save(annotate_matfiles{tpoint}, 'uuid', 'red_time', 'POI_Feats_int', 'asscl', 'asscl2', 'lib_idx', 'dictionary_file', 'background_int');
        
        % Cat the feature vectors for all processed frames of one
        % cell (for smoothing later)
        frame_t1 = M.maxProj_low(:); % Projected image of the current frame
        frame_common = frame_t0+frame_t1; % Used for getting pixels with signal in at least one frame
        m = corrcoef(frame_t0(frame_common>0),frame_t1(frame_common>0)); % Calculate the correlation matrix of the two adjacent frame (intensity based)
        corrtrace(tpoint) = m(2,1); % Correlation coefficient. For frame 1 is always 1.
        frame_t0 = frame_t1; % Update "last frame"
        POI_Feats_int = POI_Feats_int/(sum(POI_Feats_int)+1*double(sum(POI_Feats_int)==0)); % Normalization of the feature vector to total intensity
        f_ip = cat(1,f_ip, POI_Feats_int');
        % global features
        f_glob = cat(1, f_glob, M.POI_Feats');
        BG = cat(1,BG,[M.frac_foreground M.frac_foreground2d]);
        
    end
    fprintf('|\n');
    
    % QC criteria under development
    if qc_extra == 1
        vec_QC(cell_idx) = 1;
        % Perform smoothing and save the feature matrix of the entire
        % image sequence in addition
        f_smooth = zeros(size(f_ip));
        tr = mean(corrtrace);
        % transform the correlation coefficients into weight
        fcorr = 1./(1+exp(-(corrtrace-tr) * parameters.annotate_smooth_fac));
        % Calculate the smoothing window for each frame as the product
        % of the adjacent weight vector: the higher the correlation,
        % the higher the similarity, the higher the averaging weight,
        % the wider the smoothing window
        for k = 1:size(celltime.mitotime,1)
            fcorr_k = zeros(size(celltime.mitotime,1),1);
            for p = 1:(k-1)
                fcorr_k(p) = prod(fcorr((p+1):k));
            end
            for p = (k+1):size(celltime.mitotime,1)
                fcorr_k(p) = prod(fcorr((k+1):p));
            end
            fcorr_k(k) = 1;
            fcorr_k = fcorr_k/sum(fcorr_k); % Normalization to 1
            f_smooth(k,:) = sum(f_ip.*repmat(fcorr_k, [1 size(f_ip,2)]),1);
        end
        
        fseqfile = fullfile(annotate_dir, 'complete_sequence.mat');
        mitotime = celltime.mitotime;
        save(fseqfile, 'f_ip', 'f_glob', 'f_smooth', 'mitotime', 'BG');
    else
        vec_QC(cell_idx) = 0;
    end
    
    
    summary_database.feature_vector = vec_QC;
    writetable(summary_database, exp_database, 'Delimiter','\t');
    
end
end


function [gen_opt_default, parameters_default] = get_defaults()

gen_opt_default = struct( ...
    'exp_dir', '', ...  % data directory
    'out_dir',  '',...  % directory to store the results
    'mst_dir', '', ...  % directory that contains the mst model
    'surfdict_dir', '', ... % directory of surf dictionary
    'exp_database', 'full_database.txt', ... % filename for experiment database text file (tab delimited file)
    'cellIdx', [1], ... % cell to process
    'poifeature_dir', 'Features_Poi', ...
    'annotate_dir', 'Feature_Poi_dbscan', ...
    'mst_fname', 'temporal_alignment.mat', ...
    'mstalign_dir', 'Temporal_Align', ...
    'mstalign_fname',  'MST_annotation.mat', ...
    'surfdict_fname', 'dictionary_Features_Poi_dbscan.mat', ...
    'annotate_reprocess', 1); % reprocess if 1 , pass if -2

NE_corr = 0.65;
NE_anticorr = 0.05;
NUC_corr = 0.7;
NUC_anticorr = 0.1;
MID_corr = 0.5;
MID_none = 0.2;
parameters_default = struct( ...
    'metric_cl', 0.03, ... % Threshold for the minimum metric value for a class to be further clustered
    'dict_method', 'dbscan', ... % 2.level clustering method, between 'dbscan' and 'kmeans'
    'meth', 'sqEuclidean', ... % Distance used for kNN clustering
    's_th', 0.5, ... % minimum Sihuette value for kNN algorithm to cluster the data further
    'pca_th', 85, ... % percentage of variance covered in PCA prior dbscan clustering
    'density_fac', 6, ... % DBSCAN: factor used for automatic estimation of the averaged density of the data
    'min_cluster', 3, ... % DBSCAN: minimum number of data points forming a cluster
    'NE', [NE_corr, NE_anticorr], ... %% Decision tree thresholds for the correlation feature. Nuclear envelope
    'NUC' , [NUC_corr, NUC_anticorr], ... % Nucleus
    'MID', [MID_corr, MID_none], ...  %  Midbody
    'sIth', 3, ... %% Decision tree thresholds for the spinImage feature based clustering. Minumum intensity level to reach for being bright, the higher the brighter.
    'sIth_factor', 2, ... % factor to the averaged intensity of the least contrasting IP, used to define the minumum intensity level for being bright, the higher the brighter
    'sIth_high', 4, ... % minimum mean/std factor for being homogeneous bright, the higher the more homogenoues and bright
    'subset_fraction', 0.05, ...% Fraction of images used to train the dictionary
    'minsize', 15, ... % Minimum size of a cluster to be further clustered in 2. level clustering
    'annotate_smooth_fac', 20); % smoothing factor when annotating SURF point with dictionary. The higher the factor the larger the smoothing window.
end
