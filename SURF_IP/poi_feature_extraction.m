function poi_feature_extraction(gen_opt, parameters)
%% POI_FEATURE_EXTRACTION Find interest points and extract global and SURF features
% POI_FEATURE_EXTRACTION() Use default options and parameters
% POI_FEATURE_EXTRACTION(gen_opt) Use paths as specified in struct gen_opt
% POI_FEATURE_EXTRACTION(gen_opt, parameters) Use paths as specified in struct gen_opt and parametes as specified.
%   INPUT:
%       * gen_opt    - struct for the general paths
%       * parameters - struct giving the parameters of the feature extraction
%       See function get_default for more details
%   OUTPUT:
%       Function saves a mat file for each time point in exp_dir/cell_dir/surfdir.
%       Matfile contains following variables:
%       frac_foreground     - Fraction of POI  in the foreground from 3D Otsu thresholded POI
%       frac_foreground2d   - Fraction of POI  in the foreground after  maximal projection. Same 3D threshold as frac_foreground
%       poi_foreground      - 2D maximal projection of preproc poi after thresholding.
%       POI_Feats           - [fraction of poi intensity above otsu thershold in chromosomal volume, fraction of poi intensity above otsu thershold in
%       estimated spindle volume]
%       parameters          - the input parameters
%       feature_name        - Name of features ?? 'frac_POI_nuc','frac_POI_SP','SURF_selected'
%       maxProj_low         - Maximal projection of preproc_poi. This is the image used to extract the features
%       InterestPoints      - The SURF points detected with detectSURFFeatures (compute Vision toolbox)
%       tot_int             - total intensity in the circular area around IP's center where the radius is defined by the SURF
%       detector. This is for each IP.
%       flag                - index whether an IP is selected. For each IP.
%       loc                 - pixel x-y localization of the IP center (resolution changed compare to original image). For each IP.
%       feature_spinImage   - 30 spinImage features - soft histogram bined at 6 intensity and 5 distance levels
%       feature_LBP         - 4 LBP (local binary patters) features summarized from LBP features
%       feature_corr        - Correlation to H2B signal for chromosomal localized IPs
%       feature_pos         - 4 position fatures
%       feature_mid         - correlation to midplane volume in cytoplamic localized IP
%       feature_detail      - 
%
% Two classes of features are extracted from each 3D image.
% Global features: two features are extracted in 3D
%   1. fraction of moleculeslocalized in the chromosomal volume and
%   2. the accumulation factor in the predited spindle volume which is defined as the convex hull volume of
%   the two intersession points of the predicted division axis on the cell
%   and the chromosomal volume
% SURF-based features: interest points are first detected on the maximally projected image.
% In order to avoid overlapping coverage of the interest points, a selection step is
% integrated. If two interest points overlap hugely, one is selected based
% on the metric, averaged intensity and size. The other one is then
% deleted. Afterwards, for each of the interest points, LBP, spin_image,
% h2b correlation, location, and metric based features are calculated.
%
% Author: Yin Cai, EMBL, 2015.02.03
% Update:
%   2015.03.13 Modified the correlation and the location features
%   2015.05.06 nucVolume turns into binary before using.
%   Added saving of 2D projected thresholded image and the calculation of 2D foreground fraction
%   2016.05.31  Comment and integration. Antonio Politi
%   2017.09.18 Default parameter values. more explanations. Fail save reading of mat file. Antonio Politi
%   2018.20.03 Struct input. Antonio Politi

%% Default parameter
[gen_opt_default, parameters_default] = get_default();

if nargin < 1
    gen_opt = gen_opt_default;
end

if nargin < 2
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('poi_feature_extraction: Second argument parameters does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

if gen_opt.poifeature_reprocess == -2
    return
end

%% Load database
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
if gen_opt.poifeature_reprocess == 1
    vec_QC = -ones(length(summary_database.feature_extract),1);
else
    vec_QC = summary_database.feature_extract;
end
proc_history = (summary_database.manualQC~=0).*(summary_database.time_alignment>0).*(summary_database.fcs_calibration>0);
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);

feature_name = {'frac_POI_nuc','frac_POI_SP','SURF_selected'};

%% Load the mat file from the segmentation and registration
for cell_idx = vec_idx
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    preproc_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.preproc_dir);
    seg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
    feature_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.poifeature_dir);
    feature_dir = path_gen.getcelldir(gen_opt.out_dir, gen_opt.poifeature_dir);
    if ~exist(feature_dir, 'dir')
        mkdir(feature_dir);
    end
    
    % Check whether processed file was processed in the same way
    if gen_opt.poifeature_reprocess == 0 && vec_QC(cell_idx) == 1
        display(['Check POI feature extraction cell_idx ' num2str(cell_idx)]) 
        if ~exist(feature_matfiles{end}, 'file')
            warning('POI feature files are missing  in %s. Reprocess data ', feature_dir);
            vec_QC(cell_idx) = -1;
        else
            previous = load(feature_matfiles{end});
            if ~isequal(feature_name, previous.feature_name)
                vec_QC(cell_idx) = -1;
                warning('POI feature mismatching. Output will be overwritten in directory %s ', feature_dir)
            end
        end
    end
    if vec_QC(cell_idx) ~= -1
        continue
    end
    fprintf('POI surf feature extraction Cell_idx: %d\n',  cell_idx);
    totint_over_time = 0;
    totN_ip = 0;
    for tpoint = 1:length(seg_matfiles)
        %% File in and out
        fprintf('-');
        seg = load(seg_matfiles{tpoint}, 'nuc', 'nucVolume', 'isectPointsPix', 'cellVolume', 'midPlane'); % load the result from the segmentation pipeline
        preproc = load(preproc_matfiles{tpoint}, 'intensities', 'proc_poi', 'normdists', 'satInt'); % load the result from the registration pipeline
        %%
        seg.nucVolume = double(seg.nucVolume>0);
        POI_Feats = [];
        tot_intensity = sum(preproc.intensities);
        totint_over_time = totint_over_time + tot_intensity;
        assert(tot_intensity > 0, 'Total intensity in preprocess image is below 0. Stop now!');
        %% FOREGROUND AND BACKGROUND DETERMINATION
        th_bw = parameters.bwth*max(preproc.proc_poi(:))*graythresh(preproc.proc_poi); % Otsu based threshold determination
        bw_proc_poi = preproc.proc_poi > th_bw; % 3D binary image
        poi_foreground = preproc.proc_poi.*bw_proc_poi; % Thresholded 3D image
        frac_foreground = sum(poi_foreground(:))/sum(preproc.proc_poi(:)); % Fraction of POI  in the foreground - 3D - for classification training later
        [maxProj_low, mPidx] = max(preproc.proc_poi,[],3); % Maximum projection, saving the coordinates
        poi_foreground = max(poi_foreground,[],3);
        frac_foreground2d = sum(poi_foreground(:))/sum(maxProj_low(:)); % Fraction of POI  in the foreground - 2D (from maximal projection)
        
        %% EXTRACTION OF GLOBAL FEATURES
        % 1. fraction poi over threshold in the chromosomal volume
        POI_Feats = cat(1, POI_Feats, sum(preproc.intensities(((preproc.normdists <= 0).*(preproc.intensities>th_bw)) > 0))...
            /sum(preproc.intensities(preproc.intensities > th_bw)));
        
        % 2. accumulation factor in the predicted spindle volume
        % Predict the spindle volume as the convex hull of
        % the chromosomal volume and the two isectPoints,
        % projected through z-direction
        spindle_volume = seg.nucVolume;
        spindle_volume(seg.isectPointsPix(1,1), seg.isectPointsPix(1,2), sum(sum(seg.nucVolume))>0) = 1;
        spindle_volume(seg.isectPointsPix(2,1), seg.isectPointsPix(2,2), sum(sum(seg.nucVolume))>0) = 1;
        for sp_idx = 1:size(spindle_volume,3)
            spindle_volume(:,:,sp_idx) = bwconvhull(spindle_volume(:,:,sp_idx),'union',4); % convex hull plane by plane
        end
        spindle_volume = spindle_volume.*seg.cellVolume; % Only inside of the cell
        outOFsp = seg.cellVolume - spindle_volume; % Non-spindle volume
        spindle_volume = spindle_volume - imfill(seg.nucVolume,'holes'); % Non-chromosomal spindle volume
        
        % Accumulation factor: mean in spindle/(mean outside of spindle + mean in spindle)
        sp_acc = mean(preproc.proc_poi(spindle_volume(:)>0))/...
            (mean(preproc.proc_poi(outOFsp(:)>0)) + mean(preproc.proc_poi(spindle_volume(:)>0)));
        if isnan(sp_acc)
            sp_acc = 0;
        end
        POI_Feats = cat(1 ,POI_Feats, sp_acc);
        
        %% EXTRACTION OF SURF INTEREST POINTS
        % Normalize the projected image to the theoretical saturation.
        % It conserves the relative intensity difference
        % between different frames within a image sequence
        % or between different image sequences, however,
        % the difference between genes is not as big as
        % normalizing to the concentration map
        maxProj_low = maxProj_low/preproc.satInt;
        
        nuc = seg.nuc.*seg.nucVolume; % H2B signal segmented by chromosomal volume
        % H2B signal 3D projected by taking the position of
        % POI maximum projection (corresponding H2B signal)
        nucProj = zeros(size(maxProj_low));
        for i = 1:size(nucProj,1)
            for j = 1:size(nucProj,2)
                nucProj(i,j) = nuc(i,j,mPidx(i,j))/preproc.satInt;
            end
        end
        
        % Midzone definition: the volume close to the
        % middle plane between two daughter chromosomal volumes
        midPlane = seg.midPlane;
        for zdx = 1:size(seg.midPlane,3)
            midPlane(:,:,zdx) = imdilate(seg.midPlane(:,:,zdx),ones(8));
        end
        
        % Extract SURF interest point at a right resolution
        maxProj = imresize(maxProj_low, parameters.factor);
        InterestPoints = detectSURFFeatures(maxProj, 'MetricThreshold', parameters.surfth); % threshold by metric
        
        %% REDUCTION OF INTEREST POINTS (IP)
        % Initialization
        ipidx_mask = cell(size(maxProj));
        mean_int = zeros(length(InterestPoints),1); % Initialized for: averaged intensity of 17x17 pixels around the IP center
        loc = repmat(mean_int,[1 2]); % Initialized for: pixel x-y localization of the IP center (resolution changed compare to original image)
        tot_int = mean_int; % Initialized for: the total intensity in the circular area around IP's center where the radius is defined by the SURF detector
        add_int = mean_int; % Initialized for: uncovered intensities that an IP brings in addition to selected IPs so far
        flag = mean_int; % Initialized for the index whether an IP is selected
        
        % Starting with the IP with the highest metric value, select the IP if
        % a) the center of the IP is uncovered by any selected IPs with higher metric values
        % b) the IP has the highest total intensity (incase equal, then the smaller size)
        for i = 1:length(InterestPoints)
            loc(i,:) = round(InterestPoints(i).Location);
            mean_int(i) = sum(sum((maxProj(loc(i,2)+(-8:8),loc(i,1)+(-8:8)))))/(17^2);
            if isempty(ipidx_mask{loc(i,2),loc(i,1)}) % select, IP center uncovered
                flag(i) = 1;
            else
                for j = 1:length(ipidx_mask{loc(i,2),loc(i,1)})
                    if flag(ipidx_mask{loc(i,2),loc(i,1)}(j)) > 0
                        if mean_int(i) > mean_int(ipidx_mask{loc(i,2),loc(i,1)}(j)) % select, averaged intensity larger
                            flag(ipidx_mask{loc(i,2),loc(i,1)}(j)) = 0; % de-select the previous one
                            flag(i) = 1;
                        elseif mean_int(i) == mean_int(ipidx_mask{loc(i,2),loc(i,1)}(j))
                            if InterestPoints(i).Scale < InterestPoints(ipidx_mask{loc(i,2),loc(i,1)}(j)).Scale % select, smaller size
                                flag(ipidx_mask{loc(i,2),loc(i,1)}(j)) = 0; % de-select the previous one
                                flag(i) = 1;
                            end
                        end
                    end
                end
            end
            % Examing the selection: keeping only if the IP has more than x% intensities uncovered
            if flag(i) == 1
                % Define the SURF circular surrounding area
                % 6 is defined by matlab as SURF IP scaled surrounding
                s = 6*InterestPoints(i).Scale;
                rs = round(s);
                [x,y] = meshgrid(-rs:rs,-rs:rs);
                pos = [x(:) y(:)];
                pos(sum(pos.^2,2)>s^2,:) = [];
                pos = pos + repmat(loc(i,:),[size(pos,1) 1]);
                pos(((pos(:,1)<1) + (pos(:,2)<1))>0,:) = [];
                pos(((pos(:,1)>size(maxProj,2))+(pos(:,2)>size(maxProj,1)))>0,:) = [];
                for iparea = 1:size(pos,1)
                    tot_int(i) = tot_int(i) + maxProj(pos(iparea,2),pos(iparea,1)); % total intensity Flip x-y image-matrix
                    existint = 0;
                    for j = 1:length(ipidx_mask{pos(iparea,2),pos(iparea,1)})
                        existint = existint+flag(ipidx_mask{pos(iparea,2),pos(iparea,1)}(j)); % Binary: whether a position is covered
                    end
                    add_int(i) = add_int(i) + double(existint==0)*maxProj(pos(iparea,2),pos(iparea,1));% uncovered intensity
                end
                if add_int(i) <= tot_int(i)*(1-parameters.ip_overlap)
                    flag(i) = 0;
                else % In case of selection: Mark all positions within the SURF IP
                    for iparea = 1:size(pos,1)
                        ipidx_mask{pos(iparea,2),pos(iparea,1)} = cat(1,i,ipidx_mask{pos(iparea,2),pos(iparea,1)});
                    end
                end
            end
        end
        
        %% FEATURE EXTRACTION FOR SELECTED IP
        % Five categories of features are extracted;
        % 30 spinImage features - soft histogram bined at 6
        % intensity and 5 distance levels
        feature_spinImage = zeros(length(InterestPoints),length(parameters.spinImage.ds)*length(parameters.spinImage.is));
        % 4 LBP features summarized from LBP features
        feature_LBP = zeros(length(InterestPoints),4);
        % 1 Correlation feature: correlation to H2B signal
        % for chromosomal localized IPs and correlation to
        % midplane volume in cytoplamic localized IP
        feature_corr = zeros(length(InterestPoints),1);
        feature_mid = zeros(length(InterestPoints),1);
        % 4 position fatures
        feature_pos = zeros(length(InterestPoints),5);
        feature_detail = zeros(length(InterestPoints),2);
        % Process through the IP list
        totN_ip = totN_ip + length(InterestPoints);
        for i = 1:length(InterestPoints)
            if flag(i) <= 0
                continue
            end
            s = 6*InterestPoints(i).Scale;
            rs = round(s);
            if min(loc(i,:)) <= rs
                rs = min(loc(i,:)-1);
            end
            if max(loc(i,:))+rs > min(size(maxProj))
                rs = min(size(maxProj))-max(loc(i,:));
            end
            % Crop the IP image
            I = (maxProj(loc(i,2)+(-rs:rs),loc(i,1)+(-rs:rs)));
            I(I<0) = 0;
            
            %% spin_Image features
            % this is similar as a 2D histogram on
            % distance-to-the-center and intensity with the
            % modification that intensities larger than the max
            % i-level will be counted in the max
            % i-level bin. It is a soft-histogram
            % because intensities/distance which don't
            % fall into the bins will contribute to the
            % bin's count depending on the difference
            % in intensity/distance.
            
            % resize the cropped image to the same size
            rs2 = ceil(parameters.spinImage.ds(end));
            I2 = imresize(I,length(-rs2:rs2)/size(I,1));
            I2 = I2(1:length(-rs2:rs2),1:length(-rs2:rs2));
            % create a distance map
            [x,y] = meshgrid(-rs2:rs2,-rs2:rs2);
            distmap = sqrt(x.^2+y.^2);
            distmap = distmap(:);
            % create the intensity vector
            I3 = I2(:);
            I3(I3>parameters.spinImage.is(end)) = parameters.spinImage.is(end);
            for d_i = 1:length(parameters.spinImage.ds)
                for i_i = 1:length(parameters.spinImage.is)-1
                    feature_spinImage(i,((d_i-1)*length(parameters.spinImage.is)+i_i)) = sum(exp(-(((distmap-parameters.spinImage.ds(d_i)).^2/2/parameters.spinImage.dI_weight(1)^2)+((I2(:)-parameters.spinImage.is(i_i)).^2/2/parameters.spinImage.dI_weight(2)^2))));
                end
                feature_spinImage(i,((d_i-1)*length(parameters.spinImage.is)+length(parameters.spinImage.is))) = sum(exp(-(((distmap-parameters.spinImage.ds(d_i)).^2/2/parameters.spinImage.dI_weight(1)^2)+((I3-parameters.spinImage.is(end)).^2/2/parameters.spinImage.dI_weight(2)^2))));
                feature_spinImage(i,((d_i-1)*length(parameters.spinImage.is)+(1:length(parameters.spinImage.is)))) = feature_spinImage(i,((d_i-1)*length(parameters.spinImage.is)+(1:length(parameters.spinImage.is))))/sum(exp(-(((distmap-parameters.spinImage.ds(d_i)).^2/2/parameters.spinImage.dI_weight(1)^2))));
            end
            
            %% LBP features
            % this is a modified version of LBP feature. In
            % principle, number of pixels with intensities
            % greater than the center pixel, number of
            % dark/bright switches and the major orientation of
            % the 5-by-5 field are calculated for each pixel.
            % Afterwards, the 18 dimensional vector is
            % summarized into 4 indicating the degree of
            % randomness, structured brightness, portion of
            % homegeneous field and orientation dominance
            % (rotational symmetric).
            
            % Initialization
            full_lbp = zeros(1,18);
            lbp_vec = -ones(size(I,1)*size(I,2),10);
            % binarize the surrounding circle based on
            % intensity comparison to the center pixel
            for x_i = 3:(size(I,1)-2)
                for y_i = 3:(size(I,2)-2)
                    v = I((x_i-2):2:(x_i+2),(y_i-2):2:(y_i+2));
                    v = v(:);
                    if v(5) < parameters.LBP.minsig % threshold for signal
                        v = 0.1*ones(9,1);
                    end
                    ref = v(5);
                    v(5) = [];
                    if ref == 0
                        if max(v) > 0 % just to avoid artefect of NaN, no influence on the output
                            ref = min(v(v>0))/5;
                        else
                            ref = 0.01;
                        end
                    end
                    v = [v(1:3);v(5);v(8:-1:6);v(4)]; % arrange into an anti-clockwise circle
                    v = v';
                    v(v<=ref) = 0;
                    lbp_vec(((y_i-1)*(size(I,2)-1)+x_i),:) = [v/ref 0 0]; % Relative LBP
                    v = double(v>0); % binarization
                    % Transition vector
                    for s_i = 1:7
                        v(s_i) = v(s_i)-v(s_i+1);
                    end
                    v(8) = 0;
                    % count the transitions
                    lbp_vec(((y_i-1)*(size(I,2)-1)+x_i),9) = sum(v==-1);
                    lbp_vec(((y_i-1)*(size(I,2)-1)+x_i),10) = sum(v==1);
                end
            end
            lbp_vec(sum(lbp_vec,2)<0,:) = [];
            totbox = size(lbp_vec,1); % total valid LBP reagions
            
            % Calculate the uLBP vector
            full_lbp(10) = sum(max(lbp_vec(:,9:10),[],2)>1); % count of multitransitions
            lbp_vec(max(lbp_vec(:,9:10),[],2)>1,:) = []; % Delete multitransition LBP regions
            full_lbp(8) = sum(sum(lbp_vec(:,1:8)>1,2)==8); % count of complete bright surround
            lbp_vec(sum(lbp_vec(:,1:8)>1,2)==8,:) = [];
            full_lbp(9) = sum(sum(lbp_vec(:,1:8),2)==0); % count of complete dark/homogeneous surround
            lbp_vec(sum(lbp_vec(:,1:8),2)==0,:) = [];
            sumvec_value = sum(lbp_vec(:,1:8),2);
            sumvec_count = sum(lbp_vec(:,1:8)>1,2);
            for p_i = 1:7 % count for N brighter surrounding neighbor pixels in single transition LBP regions
                full_lbp(p_i) = sum((sumvec_count==p_i));
            end
            lbp_vec(:,9:10) = [];
            
            % Calculate the orientation for sinlge transition LBP regions
            % orientation is the intensity weighted balance center
            orientation = zeros(size(lbp_vec,1),1);
            for idx = 1:size(lbp_vec,1)
                if lbp_vec(idx,1) == 0
                    for vdx = 1:8
                        v(vdx) = sum(lbp_vec(idx,1:vdx));
                    end
                    orientation(idx) = find(v >= (sumvec_value(idx)/2),1);
                else
                    start_idx = find(lbp_vec(idx,:)==0,1); % find the start of bright region (because of circular arrangement)
                    for vdx = 1:(8-start_idx+1)
                        v(vdx) = sum(lbp_vec(idx,start_idx:(vdx+start_idx-1)));
                    end
                    for vdx = (8-start_idx+2):8
                        v(vdx) = v(9-start_idx) + sum(lbp_vec(idx,1:(vdx-(8-start_idx+1))));
                    end
                    orientation(idx) = find(v >= (sumvec_value(idx)/2),1)+start_idx-1;
                    if orientation(idx) > 8
                        orientation(idx) = orientation(idx)-8;
                    end
                end
            end
            % Document the count of each direction with the uLBP together
            for o_i = 1:8
                full_lbp(o_i+10) = sum(orientation==o_i);
            end
            % Normalize the uLBP to 1
            full_lbp(1:10) = full_lbp(1:10)/totbox;
            % Normalize the orientation counts to 1
            if size(orientation,1) == 0
                orientation = 1;
            end
            full_lbp(11:end) = full_lbp(11:end)/size(orientation,1);
            % Summarize the LBP feature
            % 1) Distance to random: Based on the 18
            % dimensional feature vector of a simulated
            % completely random image and that of a
            % homogeneus image
            feature_LBP(i,1) = sum((full_lbp-parameters.LBP.randref).^2)/parameters.LBP.maxdist;
            % 2) Fraction of stripe- or bright area
            % similar reagions: based on the fraction
            % of LBP regions with 3-4 brighter
            % neighboring pixels within in the single
            % transition regions
            l = sum(full_lbp(1:7));
            if l > 0
                feature_LBP(i,2) = sum(full_lbp(3:4))/l;
            else
                feature_LBP(i,2) = 0;
            end
            % 3) Completely dark surrounding /
            % multitransisions indicating the
            % homogeneity of the image
            if full_lbp(10) > 0
                feature_LBP(i,3) = full_lbp(9)./sum(full_lbp(9:10));
            else
                feature_LBP(i,3) = 1;
            end
            % 4) Fraction of dominant direction
            feature_LBP(i,4) = max(full_lbp(11:14)+full_lbp(15:18),[],2);
            
            %% Correlation feature
            % Correlation between POI and H2B in
            % the square IP field
            rs_low = floor(rs/parameters.factor); % Transform to low resolution
            loc_low = round(loc(i,:)/parameters.factor);
            if min(loc_low) <= rs_low
                rs_low = min(loc_low-1);
            end
            if max(loc_low)+rs_low > min(size(maxProj_low))
                rs_low = min(size(maxProj_low))-max(loc_low);
            end
            % Linearize low resolution IP and H2B image
            I_low = maxProj_low(loc_low(2)+(-rs_low:rs_low),loc_low(1)+(-rs_low:rs_low));
            h2bsig = nucProj(loc_low(2)+(-rs_low:rs_low),loc_low(1)+(-rs_low:rs_low));
            % Calculate the correlation coefficient
            % between the two
            corrsig = corrcoef([I_low(:) h2bsig(:)]);
            feature_corr(i) = corrsig(2,1);
            
            %% Location features
            % location features categorize the IP into complete
            % chromosomal, half-nuc-half-cyt, complete cyt and
            % half-outside-of-cell. The position definition is
            % calculated based on the 3D position of each pixel
            % in the max projected POI image.
            
            % Crop the binary images of the landmarks
            % and predicted midplane in x-y to the size
            % of the IP
            cell_low = seg.cellVolume(loc_low(2)+(-rs_low:rs_low),loc_low(1)+(-rs_low:rs_low),:);
            nuc_low = seg.nucVolume(loc_low(2)+(-rs_low:rs_low),loc_low(1)+(-rs_low:rs_low),:);
            mid_low = midPlane(loc_low(2)+(-rs_low:rs_low),loc_low(1)+(-rs_low:rs_low),:);
            % Crop the z position of the POI projection
            % map to the IP size in xy
            mPidx_low = mPidx(loc_low(2)+(-rs_low:rs_low),loc_low(1)+(-rs_low:rs_low));
            % Binary matrix indicating the position
            % association for each IP pixel
            I_distnup = zeros(size(I_low));
            I_distcell = zeros(size(I_low));
            I_distmid = zeros(size(I_low));
            for xidx = 1:size(I_low,1)
                for yidx = 1:size(I_low,2)
                    I_distnup(xidx,yidx) = nuc_low(xidx,yidx,mPidx_low(xidx,yidx));
                    I_distcell(xidx,yidx) = cell_low(xidx,yidx,mPidx_low(xidx,yidx));
                    I_distmid(xidx,yidx) = mid_low(xidx,yidx,mPidx_low(xidx,yidx));
                end
            end
            I_distnup = I_distnup(:);
            I_distcell = I_distcell(:);
            I_distmid = I_distmid(:);
            pixnum = length(I_distnup);
            
            % Categorize the IP localization
            if sum(I_distcell > 0) < (pixnum*parameters.PosP.thcell) % partially outside of the cell
                feature_pos(i,1) = 0.25;
            else
                nuppart = sum(I_distnup.*I_low(:))/sum(I_low(:));
                if nuppart <= parameters.PosP.thnb % almost complete in cell
                    feature_pos(i,2) = 0.25;
                else
                    if nuppart > parameters.PosP.thchr % complete in nuc
                        feature_pos(i,4) = 0.25;
                    else % nuc boundary border
                        feature_pos(i,3) = 0.25;
                    end
                end
                feature_detail(i,1) = nuppart;
                feature_detail(i,2) = sum(I_distnup>0)/pixnum;
            end
            feature_pos(i,5) = log(InterestPoints(i).Metric)/(log(100))-1; % Normalized metric value
            % Intensity fraction in the midplane
            feature_mid(i) = sum(I_distmid.*I_low(:))/sum(I_low(:));
        end
        feature_spinImage = feature_spinImage * parameters.weight_parameters.spinImage;
        feature_LBP = feature_LBP * parameters.weight_parameters.LBP;
        feature_corr = feature_corr * parameters.weight_parameters.corr;
        feature_pos = feature_pos * parameters.weight_parameters.pos;
        feature_mid = feature_mid *  parameters.weight_parameters.corr;
        
        %% Saving and updating
        maxProj = imresize(maxProj,1/parameters.factor);
        
        save(feature_matfiles{tpoint}, 'frac_foreground', 'frac_foreground2d', 'poi_foreground', 'POI_Feats',...
            'parameters', 'feature_name', 'maxProj_low',...
            'InterestPoints', 'tot_int', 'flag', 'loc',...
            'feature_spinImage', 'feature_LBP',...
            'feature_corr', 'feature_pos', 'feature_mid', 'feature_detail');
    end
    fprintf('|\n');
    % QC criteria under development
    if (totint_over_time>0)+(totN_ip>0)==2
        vec_QC(cell_idx) = 1;
    else
        vec_QC(cell_idx) = 0;
    end
    summary_database.feature_extract(cell_idx) =  vec_QC(cell_idx);
    writetable(summary_database, exp_database,'Delimiter','\t');
end
end
function [gen_opt_default, parameters_default] = get_default()
gen_opt_default = struct( ...
    'exp_dir', '', ...  % data directory
    'out_dir',  '',...  % directory to store the results
    'exp_database', 'full_database.txt', ... % filename for experiment database text file (tab delimited file)
    'cellIdx', [1], ... % cell to process
    'out_dir_iso', '', ...  % directory to store the isotropic sampled results
    'segmentation_dir', 'Segmentation', ...
    'poifeature_dir', 'Features_Poi', ...
    'preproc_dir', 'Registration',  ... % name of subdirectory for segmentation
    'poifeature_reprocess', 1); % reprocess if 1 , pass if -2

% SURF descriptor: spinImage parameters
sI_a = 5; % 2D histogram, weighing the distance to measuremnet, the bigger the more bloring
sI_b = 0.05;  % 2D histogram, keep the weight between distance and intensity
d_level = [0 9 18 27 36]; % selected levels for distance counting
i_level = [0 0.1 0.2 0.3 0.4 0.5]; % selected levels for

% SURF descriptor: uLBP summary parameters
% uLBP values of a simulated random image
randref = [0.110564236111111,0.0315538194444444,0.0158420138888889,0.0127951388888889,0.0150086805555556,0.0313368055555555,0.110746527777778,0.111649305555556,0.111467013888889,0.449036458333333,0.124242463702357,0.125989111125535,0.122135936106053,0.124012963790673,0.125933435751646,0.128533487687166,0.126268195848040,0.122884405988530];
maxvalue = 1.15572717405279; % simulated value for defining random texture LBP summary
min_sig = 0.05; % minimum intensity for being a LBP center
% SURF descriptor: position feature parameters
thcell = 0.8; % minimum pixel fraction for being defined as a cellular localized IP
thchr = 0.85; % minimum intensity fraction for being defined as a chromosomal localized IP
thnb = 0.25; % minimum intensity fraction for being defined as a nuclear boundary localized IP

% SURF descriptor: parameters for adjusting the weight between different
weight_spinImage = 0.2;
weight_LBP = 0.5;
weight_corr = 1;
weight_pos = 1;

parameters_default = struct( ...
    'bwth',0.48, ...                     % Parameter for thresholding the BW image: factor multiplied on the intensity level for foreground found using otsu method
    'bit', 16, ...       % bit depth of image. TODO remove hardcoding and save bitdepth etc in segmented object
    'surfth', 100, ...   % Surf IP extraction: metric selection threshold
    'factor', 4, ...     % Surf IP extraction: resize image factor on intensity. A factor of 4 is a resolution/pixel size of 0.0625
    'ip_overlap', 0.4, ... % threshold: intensity fraction of two IP can overlap
    'spinImage', struct('dI_weight',[sI_a sI_b],'ds', d_level,'is',i_level), ...  % SURF descriptor: spinImage parameters
    'LBP', struct('randref',randref,'maxdist',maxvalue,'minsig',min_sig), ...  % SURF descriptor: uLBP summary parameters
    'PosP', struct('thcell',thcell,'thchr',thchr,'thnb',thnb), ...   % SURF descriptor: position feature parameters
    'weight_parameters', struct('global',1,'spinImage',weight_spinImage,'LBP',weight_LBP,'corr',weight_corr,'pos',weight_pos)); % SURF descriptor: parameters for adjusting the weight between different feature categories
end