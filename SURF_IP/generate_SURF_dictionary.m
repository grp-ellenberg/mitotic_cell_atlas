function generate_SURF_dictionary(gen_opt, parameters)
%% GENERATE_SURF_DICTIONARY This function is used to generate a general dictionary of all SURF textons.
% GENERATE_SURF_DICTIONARY() Use default options and parameters
% GENERATE_SURF_DICTIONARY(gen_opt) Use paths as specified in struct gen_opt
% GENERATE_SURF_DICTIONARY(gen_opt, parameters) Use paths as specified in struct gen_opt and parametes as specified.
%   INPUT:
%       * gen_opt    - struct for the general paths
%       * parameters - struct giving the parameters of the feature extraction
%       See function get_default for more details
%
%   OUTPUT:
%       Creates two matfiles in dictionary_dir
%       cells_in_dictionary.mat - contains name of cells and Feature matrix for all cells and IP index list. This is used to compute dictionary
%       dictionary_featuredirname_dictmethod.mat - contains the dictionary. This is the file to be used afterwards when putting IP to dictionary
%
%
% GENERATE_SURF_DICTIONARY uses the features generated with poi_feature_extraction.
% This function is used to generate a general dictionary of all SURF textons.
% First, a subset of the SURF interest points will be seleceted at random
% basis. Then, they will be clustered based on localization and metric using
% k-d tree similar method (threshold based separation). Furthermore, for
% each of the class, sub-clusters will be found based on spin-image, LBP
% etc. SURP IP features using dbscan/knn density based clustering method.
% Finally, for clusters with higher intensities, i.e. the potential high
% signal classes, an additional disition tree based clustering is used to
% perform further separation.

% Author: Yin Cai, EMBL, Heidelberg, 2015.02.16
% Update: 
%   2015.04.01: Added the correlation to midzone Parameters are set after having a check.
%   2016.06.10: Integrated into the general pipeline
%   2017.09.18: Comments. Option description. ModelId. Antonio Politi
%   2018.03.20: Struct for parameters

%% DEFAULT PARAMETERS
[gen_opt_default, parameters_default] = get_default();
if nargin < 1
    gen_opt = gen_opt_default;
end
if nargin < 2
    parameters = parameters_default;
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end
[gen_opt.surfdict_dir, status] = verifyDirectory(gen_opt.surfdict_dir, 'Specify SURF dictionary directory', true);
if ~status
    return
end


%% LOAD DATABASE
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database, 'Delimiter', '\t');

%% SUBSET OF SURF IPs AND FEATURE MATRIX
cellnamefile = fullfile(gen_opt.surfdict_dir, 'cells_in_dictionary.mat'); % stores the picked cells
poinamefile = fullfile(gen_opt.surfdict_dir, 'poi_to_use.txt');           % a list of names to set the POI to use for dictionary

if ~exist(cellnamefile,'file')
    % Choose specific POI only
    proc_history = (summary_database.feature_extract > 0).*...
        (summary_database.manualQC ~= 0).*(summary_database.time_alignment > 0);
    if exist(poinamefile, 'file')
        usepoi = readtable(poinamefile, 'Delimiter', '\t');
        idx_poi = cellfun(@(x)(ismember(x, usepoi.poi)), summary_database.poi);
        proc_history = proc_history.*idx_poi;
    end
    vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);
    
    
    % Generate the list of all cell mat files 
    cluster_filepaths = {};
    for cell_idx = vec_idx
        path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
        features_matfiles = path_gen.getmatpaths('', gen_opt.poifeature_dir);
        for tpoint = 1:length(features_matfiles)
            cluster_filepaths = cat(1, cluster_filepaths, features_matfiles{tpoint}); % save
        end
    end
    
    % Generate the subset cell list on random base
    cell_number = length(cluster_filepaths);
    selcellidx = cat(2, rand(cell_number,1), (1:cell_number)');
    selcellidx = sortrows(selcellidx,1);
    selcellidx = selcellidx(1:round(cell_number*parameters.subset_fraction),2);
    disp('cell list generated')
    
    % Generate the SURF IP feature list of the selected cells
    cell_number = length(selcellidx);
    selcellidx = cat(2,selcellidx,zeros(cell_number,1));
    FeatMat = [];
    IP_idx_list = {};
    for cellidx = 1:cell_number
        idx1 = size(FeatMat,1);
        M = load(fullfile(gen_opt.out_dir, cluster_filepaths{selcellidx(cellidx,1)})); % Load the feature file of the image frame
        % Clean up correlation features and merge the correlation to H2B
        % and to midzone by position check
        M.feature_corr(isnan(M.feature_corr)) = 0;
        M.feature_mid(isnan(M.feature_mid)) = 0;
        % chromosomal -> H2b, cellular -> midzone
        merged_corr = M.feature_corr.*sum(M.feature_pos(:,3:4)>0,2)+M.feature_mid.*sum(M.feature_pos(:,1:2)>0,2);
        % Cat the feature vectors
        SurfFeats = [M.feature_LBP M.feature_spinImage merged_corr M.feature_pos];
        % Select only valid SURF IPs
        SurfFeats = SurfFeats(M.flag > 0,:);
        % Cat and document
        FeatMat = cat(1,FeatMat, SurfFeats);
        selcellidx(cellidx,2) = size(SurfFeats,1);
        idx2 = size(FeatMat,1);
        IP_idx_list = cat(1,IP_idx_list,[cluster_filepaths{selcellidx(cellidx,1)}(1:(end-4)) '_' num2str(idx1+1) '_' num2str(idx2)]);
    end
    disp('training list generated')
    save(cellnamefile, 'cluster_filepaths', 'selcellidx', 'FeatMat', 'IP_idx_list');
else
    load(cellnamefile);
    % This loads the feature matrix
    % Check if features still exist!
    M = load(fullfile(gen_opt.out_dir, cluster_filepaths{selcellidx(1,1)}));
    disp('list of cells for training loaded')
end

% LEGACY format
if ~isfield(M, 'parameters')
    M.parameters = M.feature_parameters;
end
%% METRIC AND POSITION - DECISION TREE

% assign an unique identifier to model
uuid = char(java.util.UUID.randomUUID);

ipnumber = size(FeatMat,1);
featnumber = size(FeatMat,2);
% Adding index columns
FeatMat = cat(2,(1:ipnumber)',FeatMat);
FeatMat = cat(2,zeros(ipnumber,1),FeatMat);
% sort and split by median based on metric value
FeatMat = sortrows(FeatMat, featnumber+2);
splitidx = floor(ipnumber/2);
FeatMat(1:splitidx,1) = 1; % smaller metric
metric_level1 = FeatMat(splitidx,end);
% sort and split by localization: chromosomal or not?
FeatMat = cat(2,4*sum(FeatMat(:,(end-2):(end-1)),2),FeatMat);
devcl = 2*FeatMat(:,1)+FeatMat(:,2); % small metric = 1,3; nuc = 2,3;
FeatMat = cat(2,devcl,FeatMat);
% each cluster sort and split again by individual median of metric value
FeatMat = sortrows(FeatMat,[1 featnumber+4]);
FeatMat(:,1) = 0;
splitidx = 0;
metric_level2 = zeros(4,1);
for i = 0:3
    level1_number = sum(devcl==i);
    FeatMat(splitidx+(1:floor(level1_number/2)),1) = 1;
    metric_level2(i+1) = FeatMat(splitidx+floor(level1_number/2),end);
    splitidx = level1_number+splitidx;
end
% cluter the IPs based on pure/mixed chromosomal/cellular localizations
FeatMat = cat(2,4*(FeatMat(:,(end-1)) + FeatMat(:,(end-3))),FeatMat);
devcl = 8*FeatMat(:,1)+4*FeatMat(:,2) + 2*FeatMat(:,3) + FeatMat(:,4);
% 4 levels of metric, from low to high (15,13,7,5); (11,9,3,1); (14,12,6,4);(10,8,2,0);
% 4 localizations: cell boundary (5,4,1,0); cyto (13,12,9,8); nuc boundary (7,6,3,2); nuc pure (15,14,11,10);
disp('first-level decision tree generated')

%% EACH CLUSTER: SECOND LEVEL CLUSTERING (DBSCAN/KMEANS)
% Initialization
subfeats_number = size(M.feature_LBP,2) + size(M.feature_spinImage,2) + size(M.feature_corr,2);
level1_number = max(devcl);
subclass_meta = zeros(level1_number+1,2);
devcl2 = zeros(size(devcl));

FeatMat = cat(2,devcl,FeatMat);
% sort according to IP number (return FeatMat to its original state)
FeatMat = sortrows(FeatMat,6);
% store class number from previous assignment in devcl
devcl = FeatMat(:,1);
% FeatMat is now identical to initial value
FeatMat(:,1:6) = [];
clustering_meta = cell(level1_number,1);

% The clustering is performed in each level 1 cluster

% variable to store the number of level 2 cluster. Only important for reporting in paper
lev2_totclst = 0;
for i = 0:level1_number
    SubMat = FeatMat(devcl==i,:);
    ipnumber = size(SubMat,1);
    maxmetric = max(SubMat(:,end));
    % remove metric and sublocalization features
    SubMat = SubMat(:, 1:subfeats_number);
    % Perform clustering only if the minimum size of cluster is exeeded
    if ipnumber > parameters.minsize
        % Perform clustering only if the minimum metric value of a cluster is exeeded
        class_number = round(maxmetric/parameters.metric_cl);
        if class_number > 2 % high metric points
            subcl = zeros(size(SubMat,1),1);
            % Correlation threshold based decision tree clustering
            if ismember(i,[7 6 3 2])
                corr_cl = ones(size(SubMat,1),1) + double(SubMat(:,end)>parameters.NE(1)) + double(SubMat(:,end)>parameters.NE(2));
                corr_cl_number = 3;
            end
            if ismember(i,[15 14 11 10])
                corr_cl = ones(size(SubMat,1),1) + double(SubMat(:,end)>parameters.NUC(1)) + double(SubMat(:,end)>parameters.NUC(2));
                corr_cl_number = 3;
            end
            if ismember(i,[13 12 9 8 5 4 1 0])
                corr_cl = ones(size(SubMat,1),1) + double(SubMat(:,end)>parameters.MID(1)) + double(SubMat(:,end)>parameters.MID(2));
                corr_cl_number = 3;
            end
            % Initialization for 2. level clustering
            clustering_meta{i+1} = cell(corr_cl_number,1);
            class_number = 0;
            sub2_cl = zeros(size(corr_cl));
            % In each metric, position and correlation sub-cluster
            for j = 1:corr_cl_number
                sub2_ipnumber = sum(corr_cl==j);
                % If the size of the cluster is still too large
                if sub2_ipnumber > parameters.minsize
                    % delete the correlation feature
                    Sub2Mat = SubMat(corr_cl==j,1:(end-1));
                    sub2_cl(corr_cl==j) = 1;
                    % Use one of the clustering method (center/density
                    % based -> kmeans vs. dbscan)
                    % The algorithms has only been tested on dbscan
                    switch parameters.dict_method
                        case 'kmeans'
                            smax = parameters.s_th;
                            kupdate = 1;
                            kmax = round(maxmetric/parameters.metric_cl);
                            for k = 2:kmax
                                sub2_test = kmeans(Sub2Mat,k,'distance',parameters.meth,'emptyaction','singleton','replicates',4,'start','uniform');
                                s = silhouette(Sub2Mat,sub2_test,meth);
                                if mean(s) > smax
                                    smax = mean(s);
                                    sub2_cl(corr_cl==j) = sub2_test;
                                    kupdate = k;
                                end
                            end
                            clustering_meta{i+1}{j} = struct('algorithm', 'kmeans', 'distance',parameters.meth, 'parameter',[kupdate,4,smax], 'start','uniform','refine',0);
                            
                        case 'dbscan'
                            % Reduce the feature dimension by PCA
                            [coeff,~,latent,~,v_percent] = pca(Sub2Mat);
                            for kk = length(v_percent):-1:2
                                v_percent(kk) = sum(v_percent(1:kk));
                            end
                            num_feat = sum(v_percent<parameters.pca_th)+1;
                            if num_feat < 2
                                num_feat = 2;
                            end
                            % Composite the feature matrix in lower
                            % dimensional space.
                            S2 = Sub2Mat*coeff(:,1:num_feat);
                            % Calculate the density parameter
                            dbscan_env = 1/(sqrt(size(S2,1)/(max(S2(:,1))-min(S2(:,1)))/(max(S2(:,2))-min(S2(:,2)))))*parameters.density_fac;
                            [sub2_test,score] = dbscan(S2,dbscan_env,parameters.min_cluster);
                            % Documenting
                            [~,~,sub2_test] = unique(sub2_test);
                            kupdate = max(sub2_test);
                            % 2nd level tot clust
                            lev2_totclst = lev2_totclst + kupdate;
                            sub2_cl(corr_cl==j) = sub2_test;
                            clustering_meta{i+1}{j} = struct('algorithm','dbscan','pca_coeff',coeff,'dim',num_feat,'parameter',[parameters.density_fac;dbscan_env;parameters.min_cluster;kupdate],'refine',0);
                    end
                    %% THIRD LEVEL CLUSTERING: ONLY FOR STRONG SIGNALS
                    % Clustering based on spinImage matrix - distance to
                    % intensity center
                    if ismember(i,[0 2 8 10])
                        % Expand the cluster number for the subcluster
                        switch parameters.dict_method
                            case 'kmeans'
                                corr_cents = zeros(kupdate,size(Sub2Mat,2));
                                for kdx = 1:kupdate
                                    sub2_test = sub2_cl(corr_cl==j);
                                    corr_cents(kdx,:) = mean(Sub2Mat(sub2_test==kdx,:),1);
                                end
                                clustering_meta{i+1}{j} = struct('algorithm','kmeans','distance',parameters.meth,'parameter',[kupdate*3,4,smax],'start','uniform','refine',1,'centroids',corr_cents);
                                
                            case 'dbscan'
                                clustering_meta{i+1}{j} = struct('algorithm','dbscan','pca_coeff',coeff,'dim',num_feat,'parameter',[parameters.density_fac;dbscan_env;parameters.min_cluster;kupdate*3],'refine',1,'assigned',sub2_test);
                        end
                        
                        % Initialization
                        l = zeros(size(Sub2Mat,1),1);
                        for alpha = 1:length(l) % For each IP
                            % Reshape the linear spinImage feature vector
                            % into matrix
                            rsh = (reshape(Sub2Mat(alpha,(size(M.feature_LBP,2)+1):end),[length(M.parameters.spinImage.is) length(M.parameters.spinImage.ds)]))';
                            % Cummulative intensity from dim to bright
                            for beta = length(M.parameters.spinImage.is):-1:2
                                rsh(:,beta) = sum(rsh(:,1:beta),2);
                            end
                            % For each distance level, find the median
                            % intensity level
                            lsh = zeros(length(M.parameters.spinImage.ds),1);
                            for beta2 = 1:length(M.parameters.spinImage.ds)
                                lsh(beta2) = find(rsh(beta2,:)>(rsh(beta2,end)/2),1);
                            end
                            % The higher the lsh is, the brighter is the IP
                            % at a particular distance
                            % The more different between lsh at different
                            % distance is, the higher is the standard dev
                            % of lsh and the less homogen is the IP
                            l(alpha) = mean(lsh)/std(lsh)*(max(lsh)>parameters.sIth);
                            % Dark->l=0; bright homogen -> l high; bright
                            % inhomogen -> l low.
                            if isnan(l(alpha))
                                l(alpha) = -1;
                            end
                        end
                        sub2_test = sub2_test + kupdate*double(l>0) + kupdate*double(l>parameters.sIth_high);
                        % Update clustering index
                        kupdate = kupdate*3;
                        sub2_cl(corr_cl==j) = sub2_test;
                    end
                else % too low number of Ip, only first step of 2. level clustering
                    kupdate = 1;
                    lev2_totclst = lev2_totclst + kupdate;
                    sub2_cl(corr_cl==j) = 1;
                    clustering_meta{i+1}{j} = struct('algorithm','none','parameter',1);
                end
                % Update the metadata
                subcl = subcl + class_number*double(corr_cl==j) + sub2_cl.*double(corr_cl==j);
                class_number = class_number + kupdate;
            end
        else % very low metric: only second step of 2. level clustering. This is repeating the previous step
            clustering_meta{i+1} = cell(1);
            switch parameters.dict_method
                case 'kmeans'
                    subcl = kmeans(SubMat(:,1:(end-1)),class_number,'distance',parameters.meth,'replicates',4,'start','uniform');
                    clustering_meta{i+1}{1} = struct('algorithm','kmeans','distance',parameters.meth,'parameter',[class_number,4],'start','uniform');
                case 'dbscan'
                    [coeff,~,latent,~,v_percent] = pca(SubMat(:,1:(end-1)));
                    for j = length(v_percent):-1:2
                        v_percent(j) = sum(v_percent(1:j));
                    end
                    num_feat = sum(v_percent<parameters.pca_th)+1;
                    if num_feat < 2
                        num_feat = 2;
                    end
                    S2 = SubMat(:,1:(end-1))*coeff(:,1:num_feat);
                    dbscan_env = 1/(sqrt(size(S2,1)/(max(S2(:,1))-min(S2(:,1)))/(max(S2(:,2))-min(S2(:,2)))))*parameters.density_fac;
                    [subcl,score] = dbscan(S2,dbscan_env,parameters.min_cluster);
                    [~,~,subcl] = unique(subcl);
                    class_number = max(subcl);
                    lev2_totclst = lev2_totclst + class_number;
                    clustering_meta{i+1}{1} = struct('algorithm','dbscan','pca_coeff',coeff,'dim',num_feat,'parameter',[parameters.density_fac;dbscan_env;parameters.min_cluster]);
            end
        end
    else % too low number of IP from the begining: no 2. level clustering
        class_number = 1;
        lev2_totclst = lev2_totclst + class_number;
        subcl = ones(ipnumber,1);
        clustering_meta{i} = cell(1,1);
        clustering_meta{i}{1} = struct('algorithm','none','parameter',1);
    end
    devcl2(devcl==i) = subcl;
    subclass_meta(i+1,:) = [ipnumber class_number];
    disp(['clustering for cluster level 1 number ' num2str(i)])
end

%% Calculate the centroids for all classes
feats_centroids = [];
index_centroids = [];
for i = 0:level1_number
    for j = 1:subclass_meta(i+1,2)
        % not all subclasses are presrent in a cluster
        if sum((devcl==i).*(devcl2==j))>0
            index_centroids = cat(1,index_centroids,[i j]);
            feats_centroids = cat(1,feats_centroids,mean(FeatMat(((devcl==i).*(devcl2==j))>0,1:subfeats_number),1));
        else
            fprintf('SURF dictionary construction: class %d %d is empty\n', i, j)
        end
    end
end

%% save the results
dict_parameter = parameters;
savefile = fullfile(gen_opt.surfdict_dir, gen_opt.surfdict_fname);
totcluster_per_level = [16, 40, lev2_totclst, size(feats_centroids,1)];
save(savefile, 'uuid', 'cluster_filepaths','selcellidx','FeatMat','metric_level1','metric_level2','devcl','devcl2','subclass_meta','feats_centroids','index_centroids',...
    'clustering_meta','dict_parameter', 'totcluster_per_level', 'parameters');

end
function [gen_opt_default, parameters_default] = get_default()
gen_opt_default = struct( ...
    'exp_dir', '', ...  % data directory
    'out_dir',  '',...  % directory to store the results
    'exp_database', 'full_database.txt', ... % filename for experiment database text file (tab delimited file)
    'cellIdx', [1], ... % cell to process
    'poifeature_dir', 'Features_Poi', ...
    'surfdict_dir', '', ...
    'surfdict_fname','dictionary_Features_Poi_dbscan.mat', ...
    'poifeature_reprocess', 1); % reprocess if 1 , pass if -2

% Decision tree thresholds for the correlation feature and localization on Nuclear envelope, nucleus or midbody,
NE_corr = 0.65;
NE_anticorr = 0.05;
NUC_corr = 0.7;
NUC_anticorr = 0.1;
MID_corr = 0.5;
MID_none = 0.2;
parameters_default = struct( ...
    'metric_cl', 0.03, ...        % Threshold for the minimum metric value for a class to be further clustered
    'dict_method', 'dbscan', ...  % 2nd level clustering method, between 'dbscan' and 'kmeans'. Only dbscan has been tested
    'meth', 'sqEuclidean', ...    % Distance used for kNN clustering
    's_th', 0.5, ...              % minimum Sihuette value for kNN algorithm to cluster the data further
    'pca_th', 85, ...             % percentage of variance covered in PCA prior dbscan clustering
    'density_fac', 6, ...         % DBSCAN: factor used for automatic estimation of the averaged density of the data
    'min_cluster', 3, ...         % DBSCAN: minimum number of data points forming a cluster
    'NE', [NE_corr; NE_anticorr], ... % Decision tree thresholds nuclear envelope
    'NUC',[NUC_corr; NUC_anticorr], ... % Decision tree thresholds nucleus
    'MID',[MID_corr;MID_none], ... % Decision tree thresholds midbody
    'sIth', 3, ...                % minumum intensity level to reach for being bright, the higher the brighter. Decition tree thresholds spinImage feature based clustering.
    'sIth_factor',  2, ...        % factor to the averaged intensity of the least contrasting IP, used to define the minumum intensity level for being bright, the higher the brighter. ecition tree thresholds spinImage
    'sIth_high', 4, ...           % minimum mean/std factor for being homogeneous bright, the higher the more homogenoues and bright. ecition tree thresholds spinImage
    'subset_fraction', 0.05, ...  % Fraction of images used to train the dictionary
    'minsize', 15 ...             % Minimum size of a cluster to be further clustered in 2nd level clustering
    );

end
