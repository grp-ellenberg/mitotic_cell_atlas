# SURF_IP
Functions to extract the interest points, build the SURF dictionary, and classify the IPs.

### poi\_feature\_extraction
Two classes of features are extracted from each 3D image.

* **Global features** two features are extracted in 3D. These features are the last 2 features in the summary table created by *summarize\_featurevec.m*
	1. the fraction of molecules localized in the chromosomal volume and 
	2. the accumulation factor in the predicted spindle volume which is defined as the convex hull volume of
	the two intersection points between the predicted division axis of the cell and the chromosomal volume.


* **SURF-based features** (speeded up robust features) interest points are first detected on the maximum intensity projection image using the detectSURFFeatures function (MATLAB).  In order to avoid overlapping coverage of the interest points, a selection step is
integrated. If two interest points overlap significantly, one is selected based
on the metric, averaged intensity and size. The other one is then
deleted. Afterwards, for each of the interest points
	1. LBP (local binary pattern)
	2. spin\_image,
	3. h2b correlation
	4. location, 
	5. and metric based features are calculated.


### Annotation using dictionary (annotate_IPs)
Each feature represent the total intensity of IPs in the corresponding bag of words. In the file
complete_sequence.mat: 
* f_ip: is the fraction of intensity (from total intensity of all IPs) in a given word. This is not the number of IP.
* f_glob: [fraction POI intensity > Otsu threshold in chromosomal volume, fraction POI intensity > Otsu threshold in estimated spindle volume]

These feature are then exported using summarize_featvec.m

## Feature description
**FOREGROUND**
This is independent of SURF 2D detection. First threshold image in 3D using Otsu threshold -> *img_thr*

* foreground_1: Fraction of signal above threshold determined from Otsu method for each image from 3D image, i.e. sum(img_thr)/sum(img).
* foreground_2: Fraction of signal above threshold in 2D. This is the fraction of projected threshold image to projected 3D original image. sum(max_Z_projection(img_thr))/sum(max_Z_projection(img))