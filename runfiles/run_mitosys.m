%This function reads the options/parameters and runs the mitosys pipeline

% Get the root directory containing the 'main_and_parameters.m' file
[ini_dir,~,~] = fileparts(pwd);

% Get the root directory containing the tiff data and the Results folder
src_data_dir = uigetdir(ini_dir,'Select the main directory containing Data_tifs and Results');
if src_data_dir == 0
    return
end

src_root_dir = uigetdir(src_data_dir,'Select the directory containing the source code');
if src_root_dir == 0
    return
end

addpath(src_root_dir);
addpath(fullfile(src_root_dir, 'IO'));

% Read options and parameters
[opt, opt_seg, opt_mst, opt_mstalign, ...
    opt_preproc, opt_poifeature, opt_surfdict] = mitosys_opt(src_data_dir);

% Run the pipeline
main_and_parameters(opt, opt_seg, opt_mst, opt_mstalign, opt_preproc, opt_poifeature, opt_surfdict);
