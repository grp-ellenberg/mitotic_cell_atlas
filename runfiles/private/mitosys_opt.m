function [opt, seg, mst, mstalign, preproc, poifeature, surfdict] = mitosys_opt(mainpath)
% contains a list of options and directory locations for processing mitosys data
% The reprocess options are 
%   * -2 : Skip this pipeline step
%   * -1 : only process new generated files that have -1 entry in database file. Do not perform consistency check for model uuid etc. 
%   *  0 : only process new generated files that have -1 entry in database file. Eventually perform consistency check
%   *  1 : force reprocessing of all files

disp('Intializing I/O directories, variables and running options');
if nargin < 1
    mainpath = ''; % option to provide only subdirectories for the different part of the processing
end
%% Main directories to save data and 
opt.exp_dir = 'Data_tifs';      % default experimental directory
opt.out_dir = 'Results';        % default output directory
out_dir = regexprep(opt.out_dir, '(\\+|/+)$', ''); % remove backslashes 
opt.out_dir_iso = [out_dir '_Iso'];    % This will be overwritten in main_and_parameters
opt.out_dir_vis = [out_dir '_SegVis']; % This will be overwritten in main_and_parameters
opt.mst_dir = fullfile('Process_Settings', 'MST');  % directory stores the MST model
opt.surfdict_dir = fullfile('Process_Settings', 'SURF_dict'); % directory stores the SURF dictionary

% prepend a mainpath if available
opt.exp_dir = fullfile(mainpath, opt.exp_dir);
opt.out_dir = fullfile(mainpath, opt.out_dir);
opt.out_dir_iso = fullfile(mainpath, opt.out_dir_iso);
opt.out_dir_vis = fullfile(mainpath, opt.out_dir_vis);
opt.mst_dir = fullfile(mainpath, opt.mst_dir);
opt.surfdict_dir = fullfile(mainpath, opt.surfdict_dir);
%% 
opt.cellIdx = []; % idx of cell to process if empty all cells are processed. This is implemented for all functions as an internal option
% options for localization of exp directories and database file
opt.db_update = 0; % 0 or 1 if a database should be updated. if 0 database will only be created in the first round
opt.exp_database = 'full_database.txt'; % default name of data base
opt.poi_metadata = 'POI_annotation_database.txt'; % default name of protein annotation database

%% segmentation options
opt.segmentation_reprocess = 0;         % 0, segment only new data. 1, resegment data
opt.segmentation_dir = 'Segmentation';
seg = get_segopt();

%% Segementation visualization options
opt.segvis_reprocess = 0;               % Run the visualization again

%% landmarks feature extraction options
opt.lmfeatures_reprocess = 0;     % - 1, compute only for new data; 0, compute only for new data and check for consistency; 1, recompute all.
opt.lmfeatures_fname = 'Nuc_feature_3D.mat';      % Name of file of landmarks features
% this is saved in mstalign_dir
%% Mitotic standard time generation options
opt.mst_reprocess = 0;         % create a new MST
opt.mst_numcells = 300;         % number of cells to create MST model. These are selected randomly from dataset
opt.mst_fname = 'temporal_alignment.mat';      % default name for output of MST model
mst = get_mstopt();
% numeric parameters


%% MST alignement options
opt.mstalign_reprocess = 0; % Reprocess time alignment 
opt.mstalign_dir = 'Temporal_Align';
opt.mstalign_fname = 'MST_annotation.mat';      %
opt.mstalign_timeIndexFilename = 'MST_timeindex.mat';
mstalign = get_mstalignopt();



%% Preprocessing POI parameters
opt.preproc_reprocess = 0;        % 1, force reprocessing
opt.preproc_dir = 'Registration'; % Name of folder to save registered data
preproc = get_preprocopt();

%% Feature extraction SURF points
opt.poifeature_reprocess = 0;             % 1, force reprocessing
opt.poifeature_dir = 'Features_Poi';  % Saving directory for features. Publishing version name "Features_words_150507"
poifeature = get_poifeatureopt();

%% Dictionary generation
opt.surfdict_reprocess = 0;    % 1, redo dictionary
surfdict = get_surfdictopt();
opt.surfdict_fname = sprintf('dictionary_%s_%s.mat', opt.poifeature_dir, surfdict.dict_method);

%% Annotation of IP with dictionary
opt.annotate_reprocess = 0;
opt.annotate_dir = [opt.poifeature_dir '_' surfdict.dict_method]; % saving directory

%% Summarize
opt.annotation_summary_reprocess = 0;
opt.annotation_summary_file =  ['Summary_' opt.annotate_dir '.txt']; % this will be saved in the output directory

end

function [opt] = get_surfdictopt()
%% GET_SURFDICTOPT
% options to generate dictionary for SURF points
opt.metric_cl = 0.03; % Threshold for the minimum metric value for a class to be further clustered
opt.dict_method = 'dbscan'; % 2.level clustering method, between 'dbscan' and 'kmeans'
opt.meth = 'sqEuclidean'; % Distance used for kNN clustering
opt.s_th = 0.5; % minimum Sihuette value for kNN algorithm to cluster the data further
opt.pca_th = 85; % percentage of variance covered in PCA prior dbscan clustering
opt.density_fac = 6; % DBSCAN: factor used for automatic estimation of the averaged density of the data
opt.min_cluster = 3; % DBSCAN: minimum number of data points forming a cluster
% Decision tree thresholds for the correlation feature
NE_corr = 0.65;
NE_anticorr = 0.05;
opt.NE = [NE_corr, NE_anticorr];
NUC_corr = 0.7;
NUC_anticorr = 0.1;
opt.NUC = [NUC_corr;NUC_anticorr];
MID_corr = 0.5;
MID_none = 0.2;
opt.MID = [MID_corr;MID_none];
% Decision tree thresholds for the spinImage feature based clustering
opt.sIth = 3; % minumum intensity level to reach for being bright, the higher the brighter
opt.sIth_factor = 2; % factor to the averaged intensity of the least contrasting IP, used to define the minumum intensity level for being bright, the higher the brighter
opt.sIth_high = 4; % minimum mean/std factor for being homogeneous bright, the higher the more homogenoues and bright
% Fraction of images used to train the dictionary
opt.subset_fraction = 0.05;
% Minimum size of a cluster to be further clustered in 2. level clustering
opt.minsize = 15;
% smoothing factor when annotating SURF point with dictionary. In principle, the higher the factor, the larger is the smoothing window. 
opt.annotate_smooth_fac = 20;  
end

function [opt] = get_poifeatureopt()
%% GET_POIFEATUREOPT
% options for feature extraction of SURF points

preprocopt = get_preprocopt();
opt.bwth = 0.48;                    % Parameter for thresholding the BW image: factor multiplied on the intensity level for foreground found using otsu method
opt.bit = preprocopt.bitnum;      % bit depth of image. TODO remove hardcoding and save bitdepth etc in segmented object
% SURF detector parameters
opt.surfth = 100; % Surf IP extraction: metric selection threshold
opt.factor = 4; % Surf IP extraction: resize image factoron intensity
opt.ip_overlap = 0.4; % threshold: intensity fraction of two IP can overlap

% SURF descriptor: spinImage parameters
sI_a = 5; % 2D histogram, weighing the distance to measuremnet, the bigger the more bloring
sI_b = 0.05;  % 2D histogram, keep the weight between distance and intensity
d_level = [0 9 18 27 36]; % selected levels for distance counting
i_level = [0 0.1 0.2 0.3 0.4 0.5]; % selected levels for intensity
opt.spinImage = struct('dI_weight',[sI_a sI_b],'ds',d_level,'is',i_level);

% SURF descriptor: uLBP summary parameters
% uLBP values of a simulated random image
randref = [0.110564236111111,0.0315538194444444,0.0158420138888889,0.0127951388888889,0.0150086805555556,0.0313368055555555,0.110746527777778,0.111649305555556,0.111467013888889,0.449036458333333,0.124242463702357,0.125989111125535,0.122135936106053,0.124012963790673,0.125933435751646,0.128533487687166,0.126268195848040,0.122884405988530];
maxvalue = 1.15572717405279; % simulated value for defining random texture LBP summary
min_sig = 0.05; % minimum intensity for being a LBP center
opt.LBP = struct('randref',randref,'maxdist',maxvalue,'minsig',min_sig);

% SURF descriptor: position feature parameters
thcell = 0.8; % minimum pixel fraction for being defined as a cellular localized IP
thchr = 0.85; % minimum intensity fraction for being defined as a chromosomal localized IP
thnb = 0.25; % minimum intensity fraction for being defined as a nuclear boundary localized IP
opt.PosP = struct('thcell',thcell,'thchr',thchr,'thnb',thnb);

% SURF descriptor: parameters for adjusting the weight between different
% feature categories
weight_spinImage = 0.2;
weight_LBP = 0.5;
weight_corr = 1;
weight_pos = 1;
opt.weight_parameters = struct('global',1,'spinImage',weight_spinImage,'LBP',weight_LBP,'corr',weight_corr,'pos',weight_pos);
end

function [opt] = get_preprocopt()
%% GET_PREPROCOPT
% Options for the filtering and preprocessing of  the POI image
% TODO: remove hardcoding of image bitdepth and resolution
opt.usewt = 0;            % Use WT cells for background. This only make sense if it has been acquired
opt.bitnum = 16;           % bit depth of image
opt.filter_size = [3 3 1]; % Filter size for smoothing [3 3 1] indicates that there is no smoothing in Z
xy_resolution = 0.25;             % in um this should not be hardcoded
opt.z_resolution = 0.75;   % in um this should not be hardcoded
opt.z_resolution = opt.z_resolution/xy_resolution; % ratio of z_resolution
end

function [opt] = get_mstalignopt()
%% GET_MSTALIGNOPT
% options for alignement of cell sequence to mitotic standard time and
% derive MST
mstopt = get_mstopt();
opt.num_lmfeats = mstopt.num_lmfeats;   % number of landmarks features
opt.unit = mstopt.unit;                 % a.u., technical balancing for the end of the sequence alignment
opt.method = mstopt.method;             % selection between frame and time
opt.qcfactor = 3;       % factor to max distance during modelling. Defines how large the inter-sequence distance can be. The higher, the less QC
opt.fuse = [0; 360];    % penalty for fusing frames (e.g. 2-2;2-3) for the model/sequence to be aligned
opt.chop = [15; 540];   % penalty for chopping on the model/sequence to be aligned at the beginning and end
opt.gap = [36; 540];    % penalty for introducing unaligned frame in the model/sequence to be aligned
end

function [opt] = get_mstopt()
%%  GET_MSTOPT
% Options to generate the mitotic standard time reference
opt.cellpaths_fname =  'data_filepath_list.mat';  % List of cell paths used for generating the MST
opt.tmax = 40;                   % standard number of frames used for the modeling
opt.deltat = 1.5;                % time resolution of data in minutes
opt.num_lmfeats = 3;             % number of landmarks features
opt.method = 'time';             % selection between frame and time
opt.fuse = [0;0];                % penalty for fusing frames (e.g. 2-2;2-3) for the model/sequence to be aligned
opt.chop = [5;54];               % penalty for chopping on the model/sequence to be aligned at the beginning and end
opt.gap = [12;54];               % penalty for introducing unaligned frame in the model/sequence to be aligned
opt.unit = 0.3;                  % a.u., technical balancing for the end of the sequence alignment
opt.rounds = 4;                  % number of rounds (default 4)
tres = 0.25;                     % resolution of the mitotic standard time model, down sampled from the alignment, in minutes
opt.tres = tres/opt.deltat;      % resolution of the mitotic standard time model, down sampled from the alignment, in frame units
windowsize = 4.5;                % filtering size left and right for calculating the second derivative, in minutes
opt.windowsize = windowsize/opt.deltat; % filtering size left and right for calculating the second derivative, in frame units
nosplit = 12;                    % maximum duration of one mitotic standard stage, in minutes
opt.nosplit = nosplit/opt.deltat;% maximum duration of one mitotic standard stage, in frame units
minsplit = 1.5;                  % minimum duration of one mitotic standard stage, in minutes
opt.minsplit = minsplit/opt.deltat; % minimum duration of one mitotic standard stage, in frame units
opt.toptr = 0.7;                 % Threshold in second derivative value for being selected as a transition point
end

function [segopt] = get_segopt()
%% GETSEGOPT
% options for segmentation
segopt.nRemLowerSlices = 0;       % default slice to remove for cell segmentation. This is overwritten if a slicestart file is found.
segopt.overWriteMarker = 0;       % overwrite marker data and compute de novo segmentation of nucleus
segopt.saveIsotropicMatFiles = 1; % save isotropic sampled data
% directory to save isotropic sampled data

% segmentation options chromosomes
segopt.sigmaBlurChr = 3;    %Sigma for Gaussian filter
segopt.hSizeBlurChr = 5;    %Kernel size for Gaussian filter
segopt.bFactor2DChr = 0.33; %Contribution of 2D threshold
segopt.splitVolChr = 2000;  %Minimum volume for to be considered for splitting
segopt.mergeVolChr = 150;   %Maximum volume of a blob that is considered for merging
segopt.minProphaseVolChr = 910; % minimum volume of prophase chromatin
segopt.maxCentDispChr = 11;      % maximum displacement between centroids and 2 subsequent time points
segopt.maxNoInitObjChr = 450;    % maximum number of initial objects
% segmentation options negative staining
segopt.rCRegionNeg = 1;     % radius of disk structuring element
segopt.pairDistNeg = 5;     % distance used in ls_generate_referenc_points
segopt.bFactor2DNeg = 0.20; % Contribution of 2D threshold for negative staining
segopt.sigmaBlurNeg = 5;    % Sigma for negative staining channel
segopt.hSizeBlurNeg = 7;    % Kernel size for negative staining channel
segopt.sigmaRatioNeg = 3;   % Sigma for gaussian blur in ratio image.
segopt.hSizeRatioNeg = 5;   % Kernel size for gaussian blur in ratio image
segopt.minVolNeg = 500;     % Minimal volume of cell
segopt.smoothcontourNeg = 25; %number of points used in smoothing
end