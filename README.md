# Mitotic Cell Atlas

This repository contains the code used to produce the mitotic cell atlas described in:<br>
Cai Y, Hossain MJ, Hériché JK, Politi AZ, Walther N, Koch B, Wachsmuth M, Nijmeijer B, Kueblbeck M, Martinic Kavur M, Ladurner R, Alexander S, Peters JMP, Ellenberg J.
**Experimental and computational framework for a dynamic protein atlas of human cell division.** [bioRxiv, DOI: 10.1101/227751][4].
The paper has now been published in [Nature, DOI: 10.1038/s41586-018-0518-z](https://www.nature.com/articles/s41586-018-0518-z)

The project website is at [http://www.mitocheck.org/mitotic_cell_atlas/index.html](http://www.mitocheck.org/mitotic_cell_atlas/index.html).

### Overview

The pipeline is composed of a number of workflows:

* [Image processing from segmentation to interest points classification](#workflow2)
* [Computation of the canonical cell model and average protein distributions](#workflow3)
* [Unsupervised inference of sub-cellular compartments](#workflow4)
* [Prediction of the number of molecules in specific sub-cellular compartments](#workflow5)

### General software requirements

A MATLAB installation with the following toolboxes is required:

* Statistics and Machine Learning Toolbox
* Image Processing Toolbox
* Curve Fitting Toolbox
* Computer Vision System Toolbox

A R and RStudio installation is required. 

* Install R (latest version recommended) from [https://www.r-project.org/](https://www.r-project.org/).
* Install RStudio (latest version recommended) from [https://www.rstudio.com/products/rstudio/download/](https://www.rstudio.com/products/rstudio/download/).

The pipeline has been tested under Windows 7 with MATLAB2017a, R 3.4.4 and RStudio 1.1.442.


---- 
[1]: http://www.mitocheck.org/mitotic_cell_atlas/downloads/v1.0.1/mitotic_cell_atlas_v1.0.1_src.zip
[2]: http://www.mitocheck.org/mitotic_cell_atlas/downloads/v1.0.1/mitotic_cell_atlas_v1.0.1_exampledata.zip
[3]: http://www.mitocheck.org/mitotic_cell_atlas/downloads/v1.0.1/mitotic_cell_atlas_v1.0.1_fulldata.zip
[4]: https://www.biorxiv.org/content/early/2018/07/17/227751
[5]: http://www.mitocheck.org/mitotic_cell_atlas/downloads/v1.0.1/cell_features.txt
***

## Running the pipeline 

### I. Code and data setup

* **STEP1**: The most recent version of the code can be downloaded from:

	```git clone git@git.embl.de:grp-ellenberg/mitotic_cell_atlas.git  MCA_SRC```
    
	Alternatively download the source code v1.0.1 [mitotic_cell_atlas_v1.0.1_src.zip][1] and unzip the file 	to a directory.  Throughout this manual we denote the Mitotic Cell Atlas Source Code directory with 	*MCA_SRC*.

* **STEP2**: Download the example data set (4 cells, 2 proteins of interest) [mitotic_cell_atlas_v1.0.1_exampledata.zip][2]
and unzip the file to a data directory.  Throughout this manual we denote the Mitotic Cell Atlas Data and Results directory with *MCA_DATA*.

The *MCA_DATA* directory contains the following folders. See also the section [**Overall data structure for the pipeline**](#overalldata) for further details.

* *Data_tifs*: Contains the raw tif images and calibration parameters. 
* *Process_Settings*: Contains the MATLAB scripts to run the pipeline *run_mitosys.m* and *run_averaging.m*. Also contains option files *private/mitosys_opt.m* and *private/mss_averaging_opt.m*, the directory for the mitotic standard time model (*MST*), and the directory for the SURF interest points dictionary (*SURF_dict*).
* *Results*: Segmentation, time alignment and other results will be written to this directory.
* *Results_Iso*: Segmentation of nucleus/DNA and cell boundary with isotropic sampling will be written to this directory.
* *Result_SegVis*: Images and videos of the segmentation results will be written to this directory. They should be used to quality control the segmentation.
* *Results_spatial_avg*:  The canonical cell model and average protein distributions will be written to this directory.

The zip file [mitotic_cell_atlas_v1.0.1_fulldata.zip][3] contains the complete data set used for publication.


### <a name = 'workflow2'> </a> II. From Segmentation to interest point classification

This workflow performs several steps of the pipeline:

*	Segmentation of the landmarks (cell surface and chromatin mass(es)).
*	Computation of landmark features to be used for time alignment.
*	Creation of the mitotic standard time (MST) model (optional if model already exists).
*	Temporal alignment of data to MST model.
*	Calibration of the images using the FCS-calibration parameters.
*	Detection of interest points (IPs) using Speeded-Up Robust Features (SURF).
*	Computation of spatial and intensity-based point features.
*	Computation of IPs bag-of-words dictionary (optional if dictionary already exists)
*	Classification of IPs in bag-of-words classes using a SURF point dictionary. 

Processing the example data set takes about 2 hours on an Intel Xeon CPU running at 3.5GHz and using 16GB of RAMs.

#### How to run the workflow:


* **STEP1**: Execute the file *MCA_DATA/Process_Settings/run_mitosys.m* in MATLAB.

* **STEP2**: The software prompts for the location of the main folder, *MCA_DATA*, containing the data and results folders.

* **STEP3**: The software prompts for the location of the source code *MCA_SRC*.

At the end of the workflow the file *MCA_DATA/Results/Summary_Features_Poi_dbscan.txt* is generated. This file contains the features and protein numbers required form [workflow IV](#workflow4) and [V](#workflow5).

Edit the file *MCA_DATA/Process_Settings/private/mitosys_opt.m* to run the workflow with different options and parameters.


## <a name = 'workflow3'></a> III. Canonical cell model and average protein distributions

This workflow executes following steps:
* Registers individual cells based on the predicted cell axis.
* Represents cellular landmarks using a cylindrical coordinate system.
* Generates a canonical cell model for the landmarks at the different mitotic stages. 
* Generates averaged protein spatial distributions. 

Processing the example data set takes about 30 min on an Intel Xeon CPU running at 3.5GHz and using 16GB of RAM.

**Data requirements**: The workflow uses the isotropic segmentation and time alignment to the MST model from the workflow II as input. 

#### How to run the workflow:

* **STEP1**: Execute the file *MCA_DATA/Process_Settings/run_averaging.m* in MATLAB.

* **STEP2**: The software prompts for the location of the main folder, *MCA_DATA*, containing the data and results folders.

* **STEP3**: The software prompts for the location of the source code *MCA_SRC*.

The software automatically locates the directories for anisotropic segmentation (*MCA_Data/Results*), isotropic segmentation (*MCA_Data/Results_Iso*), MST model (*MCA_Data/Process_Settings/MST*) and canonical cells and average protein distributions (*MCA_Data/Results_spatial_avg*) if available. Otherwise, it prompts for those directories.

When the above steps are completed, the script executes the workflow for the dataset with default parameters and saves the results of average cells and POIs in their respective directories (e.g. *MCA_Data/Results_spatial_avg*).

Edit *MCA_DATA/Process_Settings/private/mss_averaging_opt.m* file to run the workflow with different options and parameters.


## <a name = 'workflow4'></a> IV. Unsupervised inference of sub-cellular compartments by non-negative tensor factorization.

This workflow infers sub-cellular compartments and their composition in an unsupervised way. The code is written in R and stored in the notebook *MCA_SRC/NTF2/mitotic_cell_atlas.ntf2.Rmd*.

The code uses the following R packages:

*	rTensor
*	NMF
*	psych
*	seriation
*	ggplot2
*	reshape2
*	grid
*	ggpubr
*	igraph
*	Matrix
*	rhdf5 (from Bioconductor, optional, this is only used to export hdf5 files, corresponding code can be commented out)


#### How to run the workflow:

* **STEP1**: Navigate to the directory *MCA_SRC/NTF2*

* **STEP2**: Open the notebook in RStudio and select Run > Run All.

The first part of the workflow will check if required packages are available and if not install them. In case of problems with the automatic 
software package installation, it may be preferable to install the packages manually using the procedure specific to the host computer system.
The second part of the code will download the input data file [*cell_features.txt*][5] into the data directory defined in the notebook by the variable data\_dir 
(by default this is set to the subdirectory data in the current working directory, i.e. *MCA_SRC/NTF2/data*). If this fails, download the file manually 
and copy it to the data directory. The file cell_features.txt contains the bag-of-words features for the different POIs as published in [Cai et al][4]. 
Alternatively, the file *MCA\_DATA/Results/Summary_Features_Poi_dbscan.txt* generated by [workflow II](#workflow2) can be used. 
Note however that it should contain enough data on multiple proteins to lead to meaningful results.

The workflow output will be in *MCA_SRC/NTF2/results/ntf2_output*


## <a name = 'workflow5'></a> V. Predict number of molecules in specific sub-cellular compartments by regression

This workflow predicts the number of molecules in selected subcellular compartments. The code is written in R and stored in the notebook 
*MCA_SRC/regression/mitotic_cell_atlas.regression.Rmd*.

The code uses the following R packages:

*  glmnet
*	ggplot2
*	reshape2
*	ggpubr
*	scales


#### How to run the workflow:

* **STEP1**: Navigate to the directory *MCA_SRC/regression*.

* **STEP2**: Open the notebook in RStudio and select Run > Run All.

The first part of the workflow will check if required packages are available and if not install them. 
In case of problem with the automatic software package installation, it may be preferable to install 
the packages manually using the procedure specific to the host computer system.
The second part of the code will download the input data file [*cell_features.txt*][5] 
into the data directory defined in the notebook by the variable data\_dir 
(by default this is set to the subdirectory data in the current working directory, i.e. *MCA_SRC/regression/data*). 
If this fails, download the file manually and copy it to the data directory.  
Alternatively, the file *MCA\_DATA/Results/Summary_Features_Poi_dbscan.txt* generated by [workflow II](#workflow2) can be used. 
Note however that it should contain enough data on multiple proteins to lead to meaningful results.

The workflow output will be in *MCA_SRC/regression/results/regression_output*

## VI. Generate a new MST model and/or a new SURF dictionary (optional)

The workflows to generate a new mitotic standard time model and SURF dictionary require a large data set (> 100 cells representing different POIs). 
For this reason, we suggest to process a large part or all of the whole data set contained in 
[mitotic_cell_atlas_v1.0.1_fulldata.zip](http://www.mitocheck.org/mitotic_cell_atlas/downloads/v1.0.1/mitotic_cell_atlas_v1.0.1_fulldata.zip).

To generate a new mitotic standard time model, set the value of *opt.mst_reprocess* to 1 in *MCA_DATA/Process_Settings/private/mitosys_opt.m*. 
Re-run [workflow II](#workflow2) by executing the script *MCA_Data/Process_Settings/run_mitosys.m*.

To generate a new SURF dictionary, set the value of *opt.surfdict_reprocess* to 1 in *MCA_DATA/Process_Settings/private/mitosys_opt.m*. 
Re-run [workflow II](#workflow2)  by executing the script *MCA_Data/Process_Settings/run_mitosys.m*.


## VII. Report protein numbers (optional)

This workflow reports the concentration and protein amounts in the whole cell, nuclear and cytoplasmic compartments.

#### How to run the workflow:

* **STEP1**: Run the function *MCA_SRC/report/protein_numbers_features/run_computeProteinIntensities*.

* **STEP2**: The function prompts for the image data folder, i.e. *MCA_DATA/Data_tifs*.

* **STEP3**: The function prompts for the results folder, i.e. *MCA_DATA/Results*.

The workflow output will be in *MCA_DATA/Results/proteinDistribution_YYMMDD.txt*. 
The file also contains the calibration parameters. Note that the protein numbers are also reported at the end of workflow II 
in the file *MCA_DATA/Results/Summary_Features_Poi_dbscan.txt*.

## <a name = 'overalldata'></a> Overall data structure for the pipeline

The pipeline expects the image data as OME-TIFF in directories that identify the experiment and the cell, e.g. 

	YYMMDD_POIname_microscopename/cellXXXX_RYYYY/rawtif/imagename_TZZZZ.tif

*YYMMDD* defines the date of acquisition, *POIname* identifies the protein that was imaged, microscopename is the name of the microscope 
used for acquisition, and *cellXXXX\_RYYYY* is a unique identifier for a cell. 
The image names must end with *_TZZZZ.tif* to specify the time point of acquisition (e.g. *TR1_W0001_P0007_T0010.tif* for time point 10).

The directory *YYMMDD_POIname_microscopename/Calibration* contains the parameters for calibrating the fluorescence intensities 
to concentrations and number of proteins. This file has been generated from the FCS calibrated imaging workflow described 
in “Quantitative mapping of fluorescently tagged cellular proteins using FCS-calibrated 4D imaging” Politi, et al. 
[bioRxiv DOI: 10.1011/188862](https://www.biorxiv.org/content/early/2018/02/22/188862) and the 
software [FCSAnalyze](https://git.embl.de/grp-ellenberg/FCSAnalyze/). The pipeline requires a calibration file to produce quantitative data. 
In the absence of this file a default calibration factor of 1 is assumed.

The file *MCA_Data/POI_annotation_database.txt* specifies the POIname and which images should be analyzed:


 
 poi\_name | segmodel |	poi\_channel | nuc\_channel | cell\_channel | tag | shortname | ...
 --- | --- |	--- | --- | --- | --- | --- | ---
 KIF11 | 1 | 1 | 2 | 3	| GFP |	KIF11 | ...

If additional POIs are added to the data set, this file must be modified accordingly. 
The column `segmodel` will be set to either 1 or 2 based on whether signal from DNA channel 
bleeds through the dextran (Dy481XL) channel.

The file *MCA_DATA/Results/full_database.txt* summarizes the cells to analyze (one row per cell) and the processing progress:

 idx | filepath | ... | manualQC | segmentation | seg_visualize | ...
----- | ---- | ---|---|---|---|---|
1 | 140411_KIF11_MitoSys1\cell0003_R0028 | ... | 1 | 1| -1 | ...

This file is continuously updated during the processing. Each processing step is a column whose values changes from -1 to 1 (successful processed) or 0 (failed) after the specific cell has been analyzed.

If additional data is added to *MCA_DATA/Data_tifs* the *full_database.txt* file is updated. For this the value of `opt.db_update` in *MCA_DATA/Process_Settings/private/mitosys_opt.m* must be equal 1. The user has to specify if the new data passes the manual QC and has FCS calibration data by setting the values of `manualQC` and `fcs_calibration` to 1.

## Code repository structure

**FCSCalibration** Functions to read in calibration data and apply it to the fluorescence data.

**IO** Functions for reading image data, generation of databases, and output of data.

**MST** Functions related to the mitotic standard model generation and time alignment.

**NTF2** RStudio notebook for non-negative tensor factorization of bag-of-words features.

**regression** RStudio notebook for regression on selected subcellular structures.

**report** Functions to report part of the results and create images for the visual QC of the segmentation.

**runfiles** Example script to run the main part of the workflow.

**spatial_avg** Functions to compute canonical model of the cell and average protein distributions.

**segmentation** Functions to perform segmentation of the landmarks.

**SURF_IP** Functions to extract the interest points, build the SURF dictionary, and classify the IPs.


## Authors

Yin Cai, EMBL, Heidelberg

Julius Hossain, EMBL, Heidelberg

Jean-Karim Hériché, EMBL, Heidelberg

Antonio Politi, EMBL, Heidelberg








