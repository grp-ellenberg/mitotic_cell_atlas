function mss_main_and_parameters(opt, opt_reg, opt_cyl, opt_avg, opt_mod)
%This scripts gets the option file and runs registration, modeling and
%averaging

%Author: M. Julius Hossain
%Last update: 2018_03_15

%Read the database file
expDatabase = readtable(fullfile(opt.ani_indir, opt.exp_database), 'Delimiter','\t');

%%
%Get the list of cells to process
numCells = size(expDatabase,1);
visitCells = 1:numCells;
if ~isempty(opt.cellIdx)
    if iscolumn(opt.cellIdx)
        opt.cellIdx = opt.cellIdx';
    end
    if ~all(ismember(opt.cellIdx, visitCells))
        warning('Some indexes of cellIdx option are not in table. Process the complete data set'); 
        opt.cellIdx = visitCells;
    end
else
    opt.cellIdx = visitCells;
end

%%
%Register landmarks for the selected number of cells
disp('Registration of isotropic data started...');
mss_register_landmarks(opt.ani_indir, opt.iso_indir, opt.exp_database, opt.cellIdx, opt.segdir, opt.mstdir, opt.mstalign_timeIndexFilename, opt.registration_reprocess, opt.regvis, opt_reg);
disp(['Registration of isotropic data is done' newline]);

%%
%Represents landmarks using cylindrical coordinate sytems
disp('Transformation into cylindrical coordinates started...');
mss_transform_reg_to_cyl(opt.ani_indir, opt.iso_indir, opt_reg.reg_dir, opt.exp_database, opt.cellIdx, opt.cylindrical_reprocess, opt_cyl);
disp(['Transformation into cylindrical coordinates is done' newline]);

%%
%Generation of mitotic standard space
disp('Generation of mitotic standard space started...');
mss_generate_average_landmarks(opt.ani_indir, opt.iso_indir, opt.mst_mod_dir, opt.avg_dir, opt_cyl.cyl_dir, opt.mstdir, opt.mstalign_timeIndexFilename, opt.exp_database, opt.mst_mod_filename, opt_cyl.num_samples, opt.model_reprocess, opt_mod);
disp(['Generation of mitotic standard space is done' newline]);

%%
%Generate average protein distribution maps
disp('Generation of average protein distributions started...');
mss_generate_poi_avg_dist(opt.ani_indir, opt.iso_indir,  opt.mst_mod_dir, opt.avg_dir, opt_mod.mss_dir, opt.exp_database, opt_reg.reg_dir, opt.ani_regdir, opt.mstdir, opt.mstalign_timeIndexFilename, opt.mst_mod_filename, opt_avg);
disp(['Generation of average protein distributions is done' newline]);
end
