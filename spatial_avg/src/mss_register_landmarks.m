function mss_register_landmarks(aniDirRoot, isoDirRoot, fnExpdatabase, cellsToProcess, isoSegDir, mstDir, mst_timeindex_file_name, reproc, regVis, options)
% This function register isotropic segmented data based on the predicted
% cell division axis
% Last update: 2018-02-15 (Performed clean up)
% Author M. Julius Hossain, EMBL Heidelberg: julius.hossain@embl.de

%% Input and output parameters
% aniDirRoot: root directory containing the results (anisotropic)
% isoDirRoot: root directory containing the isotropic data
% outDir: root directory containing the results of registration
% fnExpdatabase: filename of the experimental database
% cellsToProcess: list of cells to be registered
% isoSegDir: suffix for the directory string the segmentation results
% mst_timeindex_file_name: name of the file containing mitotic standard stage
% reproc: reprocess registration if it is set to 1
% options: options/parameters for registration

%%
%Default options/parameters for registration
if nargin < 10
    options = struct(...
        'reg_dir', 'Registration', ...
        'NzReg', 112, ...
        'start_mst', 7);
end

refMatInfo = {'avgBgInt', 'cellAxis' ,  'cellRegion', ...
    'chrCentMic', 'chrDistMic', 'chrRegion', 'chrVolMic', ...
    'eigValuesMic', 'eigVectors', 'isectPointsPix', 'mitoTime', ...
    'numChr', 'parameters', 'poi', 'regPointsMic', ...
    'voxelSizeX', 'voxelSizeY', 'voxelSizeZ', 'voxelSizeZ_Iso'}';
cellProcessed = 0;
%Reads the experimental database
expDatabase = readtable(fullfile(aniDirRoot, fnExpdatabase) , 'Delimiter','\t');
for cellIdx = cellsToProcess
    if expDatabase.manualQC(cellIdx) == 1 && (expDatabase.segmentation(cellIdx) == 1)
        %Set input and output directory
        %curIsoFilePart = expDatabase.filepath{cellIdx};
        %curIsoFilePart(curIsoFilePart == filesep) = '_';
        curIsoInDir  = fullfile(isoDirRoot, expDatabase.filepath{cellIdx}, isoSegDir, filesep);
        curIsoOutDir = fullfile(isoDirRoot, expDatabase.filepath{cellIdx}, options.reg_dir, filesep);
        
        fnMetaData = dir([curIsoInDir '*meta*.txt']);
        if isempty(fnMetaData)
            continue;
        end
        
        %Get the metadata
        metaData = readtable(fullfile(curIsoInDir, fnMetaData(1).name), 'Delimiter','\t');
        voxelSizeX = metaData.voxelSizeX;
        voxelSizeY = metaData.voxelSizeY;
        voxelSizeZ = metaData.voxelSizeZ;
        
        %Get the list of filenames
        fn = dir([curIsoInDir, '*.mat']);
        tfin = numel(fn);
        
        if tfin == 0
            disp('No input mat file is found');
            continue;
        end
        
        %Read the mst file and initialize the first timepoint to perform
        %registration
        mstFilename = fullfile(aniDirRoot, expDatabase.filepath{cellIdx}, mstDir, mst_timeindex_file_name);
        if ~exist(mstFilename)
            disp(['MST file is not found (CellID: ' num2str(cellIdx, '%03d') '). Perform time alignment before running registration']);
            continue;
        end
        
        mst = load(mstFilename);
        tinit = find(mst.mitotime(:,2) == options.start_mst, 1, 'first');
        matFileList = (length(dir([curIsoOutDir, '*.mat']))+tinit-1);
        if  isempty(matFileList) || (length(dir([curIsoOutDir, '*.mat']))+tinit-1) >= tfin && reproc == 0
            continue;
        end
        %Create output directory
        if ~exist(curIsoOutDir)
            mkdir(curIsoOutDir);
        end
        curIsoOutDirDisp = fullfile(curIsoOutDir, 'Reg_Vis', filesep);
        if ~exist(curIsoOutDirDisp) && regVis == 1
            mkdir(curIsoOutDirDisp);
        end
        cellProcessed = cellProcessed + 1;
        processStat = 1;
        for tpoint = tinit: tfin
            fsIdx = find(curIsoInDir == filesep, 4,'last');
            disp(['Currently proecessing - (CellID: ' num2str(cellIdx, '%03d') ') ' curIsoInDir(fsIdx(1)+1:fsIdx(3)-1) ', time point:' num2str(tpoint, '%03d')]);
            inMat = load(fullfile(curIsoInDir, fn(tpoint).name));
            
            curMatInfo = who('-file', fullfile(curIsoInDir, fn(tpoint).name));
            
            if ~isequal(refMatInfo, curMatInfo)
                disp('Structure of mat file is not compitable - please check whether you selected the right folder (Result_Iso)');
                cellProcessed = cellProcessed - 1;
                processStat = 0;
                return;
            end
            if tpoint == tinit
                [dx, dy, Nz] = size(inMat.cellRegion);
            end
            
            %Translate to bring the cell volume to centre of the image in xy
            curCentX = inMat.chrCentMic(1)/voxelSizeX;
            curCentY = inMat.chrCentMic(2)/voxelSizeY;
            xShift = dx/2 - curCentX;
            yShift = dy/2 - curCentY;
            
            cellRegion = mss_image_translate_xy(inMat.cellRegion, [xShift yShift]);
            chrRegion = mss_image_translate_xy(inMat.chrRegion, [xShift yShift]);
            chrRegion(chrRegion(:,:,:) == 2) = 1;
            poi = mss_image_translate_xy(inMat.poi, [xShift yShift]);
            
            %Rotate to allign all time points in xy based on predicted cell
            %division axis
            rotAngXY = -1*atan(inMat.cellAxis(2)/inMat.cellAxis(1))*180/pi;
            inImg = cellRegion(:,:,1);
            
            %Get the size of rotated image
            tImg = imrotate(inImg, rotAngXY, 'bicubic');
            tCellRegion = zeros([size(tImg) Nz]);
            tChrRegion = zeros([size(tImg) Nz]);
            tPoi = zeros([size(tImg) Nz]);
            % Perform rotation
            for i = 1: size(cellRegion, 3)
                tCellRegion(:,:,i) = imrotate(cellRegion(:,:,i), rotAngXY, 'bicubic');
                tChrRegion(:,:,i) = imrotate(chrRegion(:,:,i), rotAngXY, 'bicubic');
                tPoi(:,:,i) = imrotate(poi(:,:,i), rotAngXY, 'bicubic');
            end
            %Displying the results
            if regVis == 1
                disSliceCell = tCellRegion(:,:,round(inMat.chrCentMic(3)/voxelSizeZ));
                disSliceChr  = tChrRegion(:,:,round(inMat.chrCentMic(3)/voxelSizeZ));
                disSliceCell(disSliceChr(:,:) > 0.5) = 2;
                disSliceCell = cat(3, disSliceCell, disSliceCell, disSliceCell);
                disSliceCell = uint8(disSliceCell/max(max(max(disSliceCell)))*255);
                h = figure('Name','Displaying Segmentation Results','NumberTitle','off', 'Visible', 'off');
                imshow(disSliceCell);
                axis off; hold on;
                savefile = [curIsoOutDirDisp filesep fn(tpoint).name(1:end-8) '_xy.jpg'];
                print(savefile,'-djpeg','-r0');
            end
            %Change the dimension to apply rotation along xz
            tCellRegion = permute(tCellRegion, [3 1 2]);
            tChrRegion = permute(tChrRegion, [3 1 2]);
            tPoi = permute(tPoi, [3 1 2]);
            
            %Translate to bring the cell volume to centre of the image in zx
            tCentX = inMat.chrCentMic(3)/voxelSizeZ;
            txShift = size(tCellRegion,1)/2 - tCentX;
            tyShift = 0;
            
            tCellRegion = mss_image_translate_xy(tCellRegion, [txShift tyShift]);
            tChrRegion = mss_image_translate_xy(tChrRegion, [txShift tyShift]);
            tPoi = mss_image_translate_xy(tPoi, [txShift tyShift]);
            
            %Rotate to allign all time points in zx
            if inMat.cellAxis(1)>=0
                rotAngXZ = (asin(inMat.cellAxis(3)))*180/pi;
            else
                rotAngXZ = -1*(asin(inMat.cellAxis(3)))*180/pi;
            end
            %Get the size of rotated image
            inImg = tCellRegion(:,:,1);
            tImg = imrotate(inImg, rotAngXZ, 'bicubic');
            tzCellRegion = zeros([size(tImg) size(tCellRegion, 3)]);
            tzChrRegion = zeros([size(tImg) size(tCellRegion, 3)]);
            tzPoi = zeros([size(tImg) size(tCellRegion, 3)]);
            
            % Perform rotation
            for i = 1: size(tCellRegion, 3)
                tzCellRegion(:,:,i) = imrotate(tCellRegion(:,:,i), rotAngXZ, 'bicubic');
                tzChrRegion(:,:,i) = imrotate(tChrRegion(:,:,i), rotAngXZ, 'bicubic');
                tzPoi(:,:,i) = imrotate(tPoi(:,:,i), rotAngXZ, 'bicubic');
            end
            
            tzCellRegion = mss_image_crop_3d(tzCellRegion, [options.NzReg dx dy]);
            tzChrRegion = mss_image_crop_3d(tzChrRegion, [options.NzReg dx dy]);
            tzPoi = mss_image_crop_3d(tzPoi, [options.NzReg dx dy]);
            
            %Displying the results
            if regVis == 1
                disSliceCell = tzCellRegion(:,:,round(size(tzCellRegion, 3)/2));
                disSliceCell = flipud(disSliceCell);
                disSliceChr  = tzChrRegion(:,:,round(size(tzCellRegion, 3)/2));
                disSliceChr = flipud(disSliceChr);
                disSliceCell(disSliceChr(:,:) > 0.5) = 2;
                disSliceCell = cat(3, disSliceCell, disSliceCell, disSliceCell);
                disSliceCell = uint8(disSliceCell/max(max(max(disSliceCell)))*255);
                h = figure('Name','Displaying Segmentation Results','NumberTitle','off', 'Visible', 'off');
                imshow(disSliceCell);
                axis off; hold on;
                savefile = [curIsoOutDirDisp filesep fn(tpoint).name(1:end-8) '_zx.jpg'];
                print(savefile,'-djpeg','-r0');
            end
            
            %Get back to the original dimension
            cellRegion = permute(tzCellRegion, [2 3 1]);
            chrRegion = permute(tzChrRegion, [2 3 1]);
            cellRegion(cellRegion(:,:,:) >= 0.5) = 1;
            cellRegion(cellRegion(:,:,:) < 0.5) = 0;
            chrRegion(chrRegion(:,:,:) >= 0.5) = 1;
            chrRegion(chrRegion(:,:,:) < 0.5) = 0;
            poi = permute(tzPoi, [2 3 1]);
            
            %Save the results
            chrCentMic = [size(poi,1)/2*voxelSizeX size(poi,2)/2*voxelSizeY size(poi,3)/2*voxelSizeZ];
            eigValuesMic = inMat.eigValuesMic;
            chrVolMic = inMat.chrVolMic;
            chrDistMic = inMat.chrDistMic;
            numChr = inMat.numChr;
            mitoTime = inMat.mitoTime;
            savefile = fullfile(curIsoOutDir, fn(tpoint).name);
            save(savefile,'cellRegion', 'chrRegion','poi', 'chrCentMic',  'eigValuesMic', 'chrVolMic', 'chrDistMic', 'numChr', 'mitoTime');
            close all;
        end
        if processStat == 1
            metaData.dx = dx;
            metaData.dy = dy;
            metaData.Nz = options.NzReg;
            writetable(metaData, fullfile(curIsoOutDir, fnMetaData(1).name), 'Delimiter','\t');
        end
        clear tzCellRegion;
        clear tzChrRegion;
        clear tzPoi;
        clear tCellRegion;
        clear tChrRegion;
        clear tPoi;
    end
end
disp(['Total number of cells processed in this run: ' num2str(cellProcessed)]);
end