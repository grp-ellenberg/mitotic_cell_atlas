function mss_generate_average_landmarks(aniDirRoot, isoDirRoot, mst_mod_dir, outDirRoot, cylDir, mstDir, mstTimeindexFilename, fnExpdatabase, mst_mod_filename, num_samples, reproc, options)
%This function generate average cell from the cylindrical coordinate
%systems that were obtained from registered cell shapes.

%aniDirRoot: Root directory containing anisotropic data
%isoDirRoot: Root directory containing isotropic data
%mst_mod_dir: Directory containing mst model
%outDirRoot: Root directory to save average landmarks and proteins
%cylDir: Sub directory containing cylindrical data
%fnExpDatabase: filename of experimental database
%mstDir: director containing mst files
%mst_mod_filename: filename of the mst model
%mstTimeindexFilename: Filename containing clusters of mst
%num_samples: number of samples per slice used to represent cylindrical data
%options: runing options and parameters for landmarks averaging

%Author: M. Julius Hossain, EMBL, Heidelberg
%Last update 2016_01_27


%Read the experimental database and generate the list of cells to be used
%for modeling
expDatabase = readtable(fullfile(aniDirRoot, fnExpdatabase) , 'Delimiter','\t');
dbNumCells = size(expDatabase,1);
dbCellIdx = 1:dbNumCells;
protPos = zeros(dbNumCells, 1);

%Generate the list of poi to be included for averaging
for curProtIdx = 1:length(options.protList)
    curProtName = options.protList(curProtIdx).prot_name;
    if options.protList(curProtIdx).use_prot == 0
        continue;
    end
    strLoc = strfind(expDatabase.filepath, curProtName);
    %protPos(:) = 0;
    for curCellIdx = 1:dbNumCells
        if ~isempty(strLoc{curCellIdx})
            protPos(curCellIdx) = 1;
        end
    end
end

%Get the proteins of interest
protFilelist = expDatabase.filepath(protPos>0);
protDbPos = dbCellIdx(protPos>0);
protNumCells = length(protFilelist);

if exist(fullfile(mst_mod_dir, mst_mod_filename))
    modMatFile = load(fullfile(mst_mod_dir, mst_mod_filename));
    options.end_mst = size(modMatFile.time_cluster, 1);
end

%Getting the metadata
foundMeta = 0;
for dirIdx = 1:protNumCells
    %Generate current input data directory in isotropic registered folder
    curCellDir = fullfile(isoDirRoot, protFilelist{dirIdx}, cylDir, filesep);
    fnMetaData = dir([curCellDir '*meta*.txt']);
    if isempty(fnMetaData)
        continue;
    end
    
    %Get the metadata
    metaData = readtable(fullfile(curCellDir, fnMetaData(1).name), 'Delimiter','\t');
    voxelSize = metaData.voxelSizeX * metaData.voxelSizeY * metaData.voxelSizeZ;
    dx = metaData.dx;
    dy = metaData.dy;
    Nz = metaData.Nz;
    foundMeta = 1;
    break;
end

if foundMeta == 0
    disp('Cannonical cell construction - meta data missing');
    return;
else
    %Get backup of the current mst files
    curMssDir = fullfile(outDirRoot, options.mss_dir, filesep);
    if exist(curMssDir, 'dir') && reproc == 0
        mssFilelist = dir([curMssDir, 'mss*.mat']);
        if length(mssFilelist) == options.end_mst-options.start_mst + 1
            return;
        end
    elseif exist(curMssDir, 'dir') && reproc == 1
        backMssDir = [curMssDir '_back_' num2str(yyyymmdd(datetime('now')))];
        copyfile(curMssDir, backMssDir);
    end
    
end

%Allocate variables for averaging
avgCylCellZ  = zeros(num_samples * Nz,1);
avgCentCellZ = zeros(Nz+1,2);

avgCylCellP  = zeros(num_samples * dy,1);
avgCentCellP = zeros(dy+1,2);

avgCylChrZ1   = zeros(num_samples * Nz, 1);
avgCylChrZ2   = zeros(num_samples * Nz, 1);
avgCylChrZComb = zeros(num_samples * Nz, 1);

avgCentChrZ1  = zeros(Nz+1,2);
avgCentChrZ2  = zeros(Nz+1,2);
avgCentChrZComb = zeros(Nz+1,2);

avgCylChrP1   = zeros(num_samples * dy, 1);
avgCylChrP2   = zeros(num_samples * dy, 1);
avgCylChrPComb = zeros(num_samples * dy, 1);

avgCentChrP1  = zeros(dy+1,2);
avgCentChrP2  = zeros(dy+1,2);
avgCentChrPComb = zeros(dy+1,2);

for curStage = options.start_mst: options.end_mst
    for refUseFlag = 0:1
        %refUseFlag =1 uses existing mss to select a timepoint which is the
        %closest to the average model. if it is zero it uses all the
        % timepoints belonging to a particular mst
        
        nFrames = 0;
        nCells = 0;
        
        avgCylCellZ(:)  = 0;
        avgCentCellZ(:)  = 0;
        
        avgCylCellP(:)  = 0;
        avgCentCellP(:)  = 0;
        
        avgCylChrZ1(:)  = 0;
        avgCylChrZ2(:)  = 0;
        avgCylChrZComb(:)  = 0;
        
        avgCentChrZ1(:)  = 0;
        avgCentChrZ2(:)  = 0;
        avgCentChrZComb(:)  = 0;
        
        avgCylChrP1(:)  = 0;
        avgCylChrP2(:)  = 0;
        avgCylChrPComb(:)  = 0;
        
        avgCentChrP1(:)  = 0;
        avgCentChrP2(:)  = 0;
        avgCentChrPComb(:)  = 0;
        
        avgMitTime = 0;
        avgVolCell = 0;
        avgVolChr  = 0;
        avgVolChr1 = 0;
        avgVolChr2 = 0;
        nChrSingle = 0;
        nChrDouble = 0;
        avgMinDist = 0;
        if refUseFlag == 1
            initModFilename = fullfile(outDirRoot, options.mss_dir, [options.fn_prefix num2str(curStage, '%02.f') '.mat']);
            if ~exist(initModFilename)
                disp(['Initial model for MST Stage' num2str(curStage) ' is not found']);
                continue;
            end
            curRefMod = load(initModFilename);
            refCentCellP = sum(curRefMod.modCylCellP.*(1:length(curRefMod.modCylCellP))')/sum(curRefMod.modCylCellP);
            refCentChrP1 = sum(curRefMod.modCylChrP1.*(1:length(curRefMod.modCylChrP1))')/sum(curRefMod.modCylChrP1);
            if curRefMod.numChr == 2
                refCentChrP2 = sum(curRefMod.modCylChrP2.*(1:length(curRefMod.modCylChrP2))')/sum(curRefMod.modCylChrP2);
            end
        end
        
        for dirIdx = 1:protNumCells
            curCellDir = fullfile(isoDirRoot, protFilelist{dirIdx}, cylDir, filesep);
            cylFilename = dir([curCellDir '*.mat']);
            if isempty(cylFilename)
                continue;
            end
            
            curCellDbPos = protDbPos(dirIdx);
            disp(['Currently processing - (CellID: ' num2str(curCellDbPos, '%03d') ') ' protFilelist{dirIdx}]);
            
            %Check whether this cell is qced to be taken for averaging
            if expDatabase.segmentation(curCellDbPos) == 1 && expDatabase.time_alignment(curCellDbPos) == 1 && expDatabase.fcs_calibration(curCellDbPos) == 1
                mstFilename = fullfile(aniDirRoot, protFilelist{dirIdx}, mstDir, mstTimeindexFilename);
                
                if exist(mstFilename)
                    idxMat = load(mstFilename);
                    tarLoc = find(idxMat.mitotime(:,2) == curStage);
                    nTpoints = length(tarLoc);
                    if nTpoints >=1
                        nCells = nCells+1;
                    else
                        continue;
                    end
                else
                    continue;
                end
                
                fn = dir([curCellDir '*.mat']);
                if numel(fn) < 1
                    continue;
                end
                
                %Select the time point close to the average landmarks generated in the first round
                if refUseFlag == 1
                    minIdx = 1;
                    for curTpoint = 1: nTpoints
                        tarFname = fullfile(isoDirRoot,  protFilelist{dirIdx}, [fn(1).name(1:end-6) num2str(tarLoc(curTpoint), '%02.f') fn(1).name(end-3:end)]);
                        %disp(tarFname);
                        if exist(tarFname)
                            inData = load(tarFname);
                            if size(avgCylCellZ,1) ~= size(inData.cylCellZ,1) || size(avgCylCellP,1) ~= size(inData.cylCellP,1)
                                disp('Size of the cylindrical coordinates do not match');
                                continue;
                            end
                            
                            [inData.cylCellP] = mss_correct_drift_z_indiv(refCentCellP, inData.cylCellP);
                            if curStage ~= options.skip_drift_cor || inData.numChr == 2
                                [inData.cylChrP1] = mss_correct_drift_z_indiv(refCentChrP1, inData.cylChrP1);
                            end
                            if inData.numChr == 2
                                [inData.cylChrP2] = mss_correct_drift_z_indiv(refCentChrP2, inData.cylChrP2);
                            end
                        end
                        if inData.numChr == 1
                            curDist = sum(abs(curRefMod.modCylCellP - inData.cylCellP))*options.bFactPredAxis + sum(abs(curRefMod.modCylChrZ1 - inData.cylChrZ1))*(1-options.bFactPredAxis);
                        else
                            curDist = sum(abs(curRefMod.modCylCellP - inData.cylCellP))*options.bFactPredAxis + sum(abs(curRefMod.modCylChrZ1 - inData.cylChrZ1))*(1-options.bFactPredAxis)/2 + sum(abs(curRefMod.modCylChrZ2 - inData.cylChrZ2))*(1-options.bFactPredAxis)/2;
                        end
                        if curTpoint == 1
                            minDist = curDist;
                        else
                            if curDist < minDist
                                minDist = curDist;
                                minIdx = curTpoint;
                            end
                        end
                    end
                end
            end
            
            %initialize which timepoints are to used
            curTpointInit = 1;
            curTpointFin = nTpoints;
            
            %if refUseFlag == 1, use one time points per mst that is the
            %coloses to the average model already have
            if refUseFlag == 1
                curTpointInit = minIdx;
                curTpointFin  = minIdx;
            end
            
            for curTpoint = curTpointInit: curTpointFin
                listTarFilename = dir([fullfile(isoDirRoot, protFilelist{dirIdx}, cylDir, filesep) '*T' num2str(tarLoc(curTpoint), '%04.f') '*.mat']);
                tarFilename = fullfile(isoDirRoot, protFilelist{dirIdx}, cylDir, listTarFilename.name);
                %disp(tarFilename);
                if exist(tarFilename)
                    inData = load(tarFilename);
                    if size(avgCylCellZ,1) ~= size(inData.cylCellZ,1) || size(avgCylCellP,1) ~= size(inData.cylCellP,1)
                        disp('Sizes do not match');
                        continue;
                    end
                    
                    if refUseFlag == 1
                        [inData.cylCellP] = mss_correct_drift_z_indiv(refCentCellP, inData.cylCellP);
                        if curStage ~= options.skip_drift_cor || inData.numChr == 2
                            [inData.cylChrP1] = mss_correct_drift_z_indiv(refCentChrP1, inData.cylChrP1);
                        end
                        if inData.numChr == 2
                            [inData.cylChrP2] = mss_correct_drift_z_indiv(refCentChrP2, inData.cylChrP2);
                        end
                    end
                    avgCylCellZ  = avgCylCellZ  + inData.cylCellZ;
                    avgCentCellZ = avgCentCellZ + inData.centCellZ;
                    avgCylCellP  = avgCylCellP  + inData.cylCellP;
                    avgCentCellP = avgCentCellP + inData.centCellP;
                    avgVolCell   = avgVolCell   + inData.volCell;
                    
                    if inData.numChr == 1
                        avgCylChrZComb  = avgCylChrZComb  + inData.cylChrZ1;
                        avgCentChrZComb = avgCentChrZComb + inData.centChrZ1;
                        avgCylChrPComb  = avgCylChrPComb  + inData.cylChrP1;
                        avgCentChrPComb = avgCentChrPComb + inData.centChrP1;
                        nChrSingle = nChrSingle + 1;
                    else
                        avgCylChrZ1 = avgCylChrZ1 + inData.cylChrZ1;
                        avgCylChrZ2 = avgCylChrZ2 + inData.cylChrZ2;
                        
                        avgCentChrZ1 = avgCentChrZ1 + inData.centChrZ1;
                        avgCentChrZ2 = avgCentChrZ2 + inData.centChrZ2;
                        
                        avgCylChrP1 = avgCylChrP1 + inData.cylChrP1;
                        avgCylChrP2 = avgCylChrP2 + inData.cylChrP2;
                        
                        avgCentChrP1 = avgCentChrP1 + inData.centChrP1;
                        avgCentChrP2 = avgCentChrP2 + inData.centChrP2;
                        
                        nChrDouble = nChrDouble + 1;
                    end
                    avgVolChr  = avgVolChr  + inData.volChr;
                    avgVolChr1 = avgVolChr1 + inData.volChr1;
                    avgVolChr2 = avgVolChr2 + inData.volChr2;
                    avgMitTime = avgMitTime + tarLoc(curTpoint);
                    if refUseFlag == 1
                        avgMinDist = avgMinDist + minDist;
                    end
                    nFrames = nFrames + 1;
                end
            end
        end
        
        if nFrames < 1
            disp(['None of the cells have a time point with MST Stage:' num2str(curStage)]);
            break;
        end
        
        disp(['MST Stage:' num2str(curStage) ' Iteration: ' num2str((refUseFlag+1)) ': Cells analysed:' num2str(nCells) ', Frames analysed:' num2str(nFrames)]);
        
        avgVolCell  = avgVolCell/nFrames;
        avgVolChr   = avgVolChr/nFrames;
        avgVolChr1  = avgVolChr1/nFrames;
        avgVolChr2  = avgVolChr2/nFrames;
        avgMitTime  = avgMitTime/nFrames;
        if refUseFlag == 1
            avgMinDist  = avgMinDist/nFrames;
        end
        
        modCylCellZ  = avgCylCellZ/nFrames;
        modCentCellZ = avgCentCellZ/nFrames;
        avgCellRegionZ = mss_cyl_to_bw(modCylCellZ, dx, dy, modCentCellZ, Nz,num_samples,20);
        avgCellRegionZ = mss_fill_image_bw(avgCellRegionZ, 3);
        
        modCylCellP  = avgCylCellP/nFrames;
        modCentCellP = avgCentCellP/nFrames;
        avgCellRegionP = mss_cyl_to_bw(modCylCellP, Nz, dx, modCentCellP, dy,num_samples,20);
        avgCellRegionP = mss_fill_image_bw(avgCellRegionP, 3);
        
        modCell = double(and(avgCellRegionZ, permute(avgCellRegionP, [3 2 1])));
        modCellSpan = double(or(avgCellRegionZ, permute(avgCellRegionP, [3 2 1])));
        
        if nChrSingle > 0
            avgCylChrZComb  = avgCylChrZComb/nChrSingle;
            avgCentChrZComb = avgCentChrZComb/nChrSingle;
            avgCylChrPComb  = avgCylChrPComb/nChrSingle;
            avgCentChrPComb = avgCentChrPComb/nChrSingle;
            avgChrRegionZComb = mss_cyl_to_bw(avgCylChrZComb, dx, dy, avgCentChrZComb, Nz,num_samples,3);
            avgChrRegionPComb = mss_cyl_to_bw(avgCylChrPComb, Nz, dx, avgCentChrPComb, dy,num_samples,3);
            avgChrRegionZComb = mss_fill_image_bw(avgChrRegionZComb, 3);
            avgChrRegionPComb = mss_fill_image_bw(avgChrRegionPComb, 3);
        end
        
        if nChrDouble > 0
            avgCylChrZ1 = avgCylChrZ1/nChrDouble;
            avgCylChrZ2 = avgCylChrZ2/nChrDouble;
            avgCentChrZ1  = avgCentChrZ1/nChrDouble;
            avgCentChrZ2  = avgCentChrZ2/nChrDouble;
            avgChrRegionZ1 = mss_cyl_to_bw(avgCylChrZ1, dx, dy, avgCentChrZ1, Nz,num_samples,3);
            avgChrRegionZ2 = mss_cyl_to_bw(avgCylChrZ2, dx, dy, avgCentChrZ2, Nz,num_samples,3);
            avgChrRegionZ1 = mss_fill_image_bw(avgChrRegionZ1, 3);
            avgChrRegionZ2 = mss_fill_image_bw(avgChrRegionZ2, 3);
            
            avgCylChrP1 = avgCylChrP1/nChrDouble;
            avgCylChrP2 = avgCylChrP2/nChrDouble;
            avgCentChrP1  = avgCentChrP1/nChrDouble;
            avgCentChrP2  = avgCentChrP2/nChrDouble;
            avgChrRegionP1 = mss_cyl_to_bw(avgCylChrP1, Nz, dx, avgCentChrP1, dy, num_samples,3);
            avgChrRegionP2 = mss_cyl_to_bw(avgCylChrP2, Nz, dx, avgCentChrP2, dy, num_samples,3);
            avgChrRegionP1 = mss_fill_image_bw(avgChrRegionP1, 3);
            avgChrRegionP2 = mss_fill_image_bw(avgChrRegionP2, 3);
        end
        
        if nChrSingle > 0 && nChrDouble > 0
            avgChrRegionZComb1 = avgChrRegionZComb;
            avgChrRegionZComb2 = avgChrRegionZComb;
            avgChrRegionZComb1(round(dx/2)+1:end, :,:) = 0;
            avgChrRegionZComb2(1:round(dx/2),:,:) = 0;
            
            [cylChrZSpt1, centChrZSpt1] = mss_bw_to_cyl(avgChrRegionZComb1, dx, dy, Nz, num_samples,1);
            [cylChrZSpt2, centChrZSpt2] = mss_bw_to_cyl(avgChrRegionZComb2, dx, dy, Nz, num_samples,1);
            
            modCylChrZ1 = cylChrZSpt1 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCylChrZ1*(nChrDouble/(nChrSingle+nChrDouble));
            modCylChrZ2 = cylChrZSpt2 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCylChrZ2*(nChrDouble/(nChrSingle+nChrDouble));
            
            modCentChrZ1 = centChrZSpt1 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCentChrZ1*(nChrDouble/(nChrSingle+nChrDouble));
            modCentChrZ2 = centChrZSpt2 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCentChrZ2*(nChrDouble/(nChrSingle+nChrDouble));
            
            finAvgChrRegionZ1 = mss_cyl_to_bw(modCylChrZ1, dx, dy, modCentChrZ1, Nz, num_samples, 3);
            finAvgChrRegionZ2 = mss_cyl_to_bw(modCylChrZ2, dx, dy, modCentChrZ2, Nz, num_samples, 3);
            
            finAvgChrRegionZ1 = mss_fill_image_bw(finAvgChrRegionZ1, 3);
            finAvgChrRegionZ2 = mss_fill_image_bw(finAvgChrRegionZ2, 3);
            
            modChrZ1 = finAvgChrRegionZ1;
            modChrZ2 = finAvgChrRegionZ2;
            
            avgChrRegionPComb1 = avgChrRegionPComb;
            avgChrRegionPComb2 = avgChrRegionPComb;
            avgChrRegionPComb1(:, :, round(dx/2)+1:end) = 0; %dx is the z dimension in **P bwimage
            avgChrRegionPComb2(:, :, 1: round(dx/2)) = 0;
            
            [cylChrPSpt1, centChrPSpt1] = mss_bw_to_cyl(avgChrRegionPComb1, Nz, dx, dy, num_samples,1);
            [cylChrPSpt2, centChrPSpt2] = mss_bw_to_cyl(avgChrRegionPComb2, Nz, dx, dy, num_samples,1);
            
            [cylChrPSpt1, avgCylChrP1] = mss_correct_drift_z_blend(cylChrPSpt1, avgCylChrP1, (nChrSingle/(nChrSingle+nChrDouble)));
            [cylChrPSpt2, avgCylChrP2] = mss_correct_drift_z_blend(cylChrPSpt2, avgCylChrP2, (nChrSingle/(nChrSingle+nChrDouble)));
            
            modCylChrP1 = cylChrPSpt1 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCylChrP1*(nChrDouble/(nChrSingle+nChrDouble));
            modCylChrP2 = cylChrPSpt2 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCylChrP2*(nChrDouble/(nChrSingle+nChrDouble));
            
            modCentChrP1 = centChrPSpt1 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCentChrP1*(nChrDouble/(nChrSingle+nChrDouble));
            modCentChrP2 = centChrPSpt2 * (nChrSingle/(nChrSingle+nChrDouble)) + avgCentChrP2*(nChrDouble/(nChrSingle+nChrDouble));
            
            %finAvgCellRegion = mss_cyl_to_bw(avgCylCell, dx, dy, avgCentCell, Nz,num_samples,20);
            finAvgChrRegionP1 = mss_cyl_to_bw(modCylChrP1, Nz, dx, modCentChrP1, dy,num_samples,3);
            finAvgChrRegionP2 = mss_cyl_to_bw(modCylChrP2, Nz, dx, modCentChrP2, dy,num_samples,3);
            
            finAvgChrRegionP1 = mss_fill_image_bw(finAvgChrRegionP1, 3);
            finAvgChrRegionP2 = mss_fill_image_bw(finAvgChrRegionP2, 3);
            
            modChrP1 = finAvgChrRegionP1;
            modChrP2 = finAvgChrRegionP2;
            
            modChr1 = double (and(modChrZ1, permute(modChrP1, [3 2 1])));
            modChr1Span = double (or(modChrZ1, permute(modChrP1, [3 2 1])));
            modChr2 = double (and(modChrZ2, permute(modChrP2, [3 2 1])));
            modChr2Span = double (or(modChrZ2, permute(modChrP2, [3 2 1])));
            avgVolChr1 = avgVolChr/2;
            avgVolChr2 = avgVolChr/2;
        elseif nChrSingle > 0 && nChrDouble == 0
            modChr1 = double (and(avgChrRegionZComb, permute(avgChrRegionPComb, [3 2 1])));
            modChr1Span = double (or(avgChrRegionZComb, permute(avgChrRegionPComb, [3 2 1])));
            modChr2 = zeros(size(modChr1));
            modChr2Span = modChr2;
            
            modCylChrZ1 = avgCylChrZComb;
            modCentChrZ1 = avgCentChrZComb;
            modCylChrP1 = avgCylChrPComb;
            modCentChrP1 = avgCentChrPComb;
            
            modCylChrZ2 = zeros(size(modCylChrZ1));
            modCentChrZ2 = zeros(size(modCentChrZ1));
            modCylChrP2 = zeros(size(modCylChrP1));
            modCentChrP2 = zeros(size(modCentChrP1));
        elseif nChrSingle == 0 && nChrDouble > 0
            modChr1 = double(and(avgChrRegionZ1, permute(avgChrRegionP1, [3 2 1])));
            modChr2 = double(and(avgChrRegionZ2, permute(avgChrRegionP2, [3 2 1])));
            modChr1Span = double(or(avgChrRegionZ1, permute(avgChrRegionP1, [3 2 1])));
            modChr2Span = double(or(avgChrRegionZ2, permute(avgChrRegionP2, [3 2 1])));
            
            modCylChrZ1 = avgCylChrZ1;
            modCylChrZ2 = avgCylChrZ2;
            modCentChrZ1  = avgCentChrZ1;
            modCentChrZ2  = avgCentChrZ2;
            
            modCylChrP1 = avgCylChrP1;
            modCylChrP2 = avgCylChrP2;
            modCentChrP1  = avgCentChrP1;
            modCentChrP2  = avgCentChrP2;
        end
        
        modChrSpan = double(or(modChr1Span, modChr2Span));
        
        modChr1Span = imdilate(modChr1, ones(3,3,3));
        modChr2Span = imdilate(modChr2, ones(3,3,3));
        modChr1 = mss_smooth_and_equalize(modChr1, modChr1Span, avgVolChr1, voxelSize, 1, options.sm_size);
        if avgVolChr2 > 0
            modChr2 = mss_smooth_and_equalize(modChr2, modChr2Span, avgVolChr2, voxelSize, 1, options.sm_size);
        end
        modChr = double(or(modChr1, modChr2));
        
        modCellSpan = imdilate(modCellSpan, ones(3,3,3));
        modChrSpan = imdilate(modChrSpan, ones(3,3,3));
        
        modCell = mss_smooth_and_equalize(modCell, modCellSpan, avgVolCell, voxelSize, 0, options.sm_size);
        modCellBound = mss_detect_boundary_points_bw(modCell);
        
        %Visualize average cell and chromosome in 3d
        [X,Y,Z]=meshgrid((1:dy),(1:dx),(1:Nz));
        hVol = figure('Name', strcat('Model cell'),'Visible', 'off');
        isosurface(X,Y,Z,modCellBound,0.9);
        alpha(0.5)
        isosurface(X,Y,Z,modChr1,0.7);
        alpha(0.7)
        isosurface(X,Y,Z,modChr2,0.5);
        alpha(1.0)
        axis tight
        axis equal
        
        disp(['Average landmarks for stage_'  num2str(curStage, '%02.f') ' has been generated']);
        
        %Generate output directory to store average shapes
        curOutDirTif = fullfile(outDirRoot, options.mss_dir, 'Tif_stack',  filesep);
        curOutDir  = fullfile(outDirRoot, options.mss_dir, filesep);
        
        if ~exist(curOutDir)
            mkdir(curOutDir);
        end
        if ~exist(curOutDirTif) && options.save_tiff_stack == 1
            mkdir(curOutDirTif);
        end
        
        %%
        %Save average cell and chromosome in the tif files
        if options.save_tiff_stack == 1
            curOutFilename = ['Cell_' options.fn_prefix num2str(curStage, '%02.f') '.tif'];
            fn_write_tiff_stack(modCell, curOutDirTif, curOutFilename, 16);
            
            curOutFilename = ['Chr_' options.fn_prefix num2str(curStage, '%02.f') '.tif'];
            fn_write_tiff_stack(modChr, curOutDirTif, curOutFilename, 16);
        end
        %Generate numChr variable to store in the mat file
        if nChrDouble >0
            numChr = 2;
        else
            numChr = 1;
        end
        
        %Save mat files containing the average landmarks
        curOutFilename = fullfile(curOutDir,  [options.fn_prefix num2str(curStage, '%02.f') '.mat']);
        if refUseFlag == 1
            save(curOutFilename, 'modCell', 'modChr', 'modChr1', 'modChr2', 'modCylCellZ', 'modCylCellP', 'modCentCellZ', 'modCentCellP', 'modCylChrZ1', 'modCylChrZ2', 'modCentChrZ1', 'modCentChrZ2', 'modCylChrP1', 'modCylChrP2', 'modCentChrP1', 'modCentChrP2', 'avgMitTime', 'numChr', 'nFrames', 'avgVolCell', 'avgVolChr1', 'avgVolChr2', 'avgMinDist');
        else
            save(curOutFilename, 'modCell', 'modChr', 'modChr1', 'modChr2', 'modCylCellZ', 'modCylCellP', 'modCentCellZ', 'modCentCellP', 'modCylChrZ1', 'modCylChrZ2', 'modCentChrZ1', 'modCentChrZ2', 'modCylChrP1', 'modCylChrP2', 'modCentChrP1', 'modCentChrP2', 'avgMitTime', 'numChr', 'nFrames');
        end
        %Saver 3D reconstructed landmarks as tif files
        curOutFilename = fullfile(curOutDir, [options.fn_prefix num2str(curStage, '%02.f') '.tif']);
        saveas(hVol, curOutFilename, 'tif');
    end
end
end