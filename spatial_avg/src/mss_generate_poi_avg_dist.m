function mss_generate_poi_avg_dist(anisoDirRoot, isoDirRoot, mst_mod_dir, outDirRoot, mssSubDir, fnExpdatabase, isoRegDir, aniRegDir, mstDir, mstTimeindexFilename, mst_mod_filename, options)
%This function calculates average protein concentration in nanoMolecules
%per litre

%anisoDirRoot: Root directory containing anisotropic data
%isoDirRoot: Root directory containing isotropic data
%outDirRoot: Root directory to save average landmarks and proteins
%mst_mod_dir: directory of mst model
%isoRegDir: Directory postfix in isoDirRoot containg registered stack
%mssDir: Directory containing mitotic standard space
%fnExpDatabase: filename of experimental database
%isoRegDir: Sub directory containing isotropic registered data
%aniRegDir: Sub directory containing anistropic fcs calibrated images
%mstDir: director containing mst files
%mstTimeindexFilename: Filename containing clusters of mst
%mst_mod_filename: filename of mst model
%options: runing options and parameters for averaging

%Last modified 2018-07-07
%Author: Julius Hossain, EMBL Heidelberg (julius.hossain@embl.de)

%Set default parameters if number of parameters are not enough
if nargin < 12
    options = struct(...
        'regdir', 'Registration', ...
        'NzReg', 112, ...
        'start_mst', 7);
end

if exist(fullfile(mst_mod_dir, mst_mod_filename))
    modMatFile = load(fullfile(mst_mod_dir, mst_mod_filename));
    options.end_mst = size(modMatFile.time_cluster, 1);
end

numSmt = options.end_mst - options.start_mst + 1;

expDatabase = readtable(fullfile(anisoDirRoot, fnExpdatabase) , 'Delimiter','\t');
dbNumCells = size(expDatabase,1);
dbCellIdx = 1:dbNumCells;
curProtPos = zeros(dbNumCells, 1);

%Generate average protein distribution for each of the proteins one by one
for curProtIdx = 1:length(options.protList)
    curProtName = options.protList(curProtIdx).prot_name;
    strLoc = strfind(expDatabase.filepath, curProtName);
    curProtPos(:) = 0;
    for curCellIdx = 1:dbNumCells
        if isempty(strLoc{curCellIdx})
            curProtPos(curCellIdx) = 0;
        else
            curProtPos(curCellIdx) = 1;
        end
    end
    if sum(curProtPos) == 0
        continue;
    end
    %Get the proteins of interest
    curProtFilelist = expDatabase.filepath(curProtPos>0);
    curProtDbPos = dbCellIdx(curProtPos>0);
    curProtNumCells = length(curProtFilelist);
    
    %Look at all cells belonging to current proteins of interest
    firstDataDir = 1;
    for dirIdx = 1:curProtNumCells
        if firstDataDir == 1
            %flatCurProtDir = strrep(curProtFilelist{dirIdx},filesep,'_');
            %Generate current input data directory in isotropic registered folder
            curCellDir = fullfile(isoDirRoot, curProtFilelist{dirIdx}, isoRegDir, filesep);
            regStackFilename = dir([curCellDir '*.mat']);
            if length(regStackFilename) == 0
                disp([curProtFilelist{dirIdx} ': No mat file is found in the registration directory']);
                continue;
            end
            
            inMat = load(fullfile(curCellDir, regStackFilename(1).name), 'chrRegion');
            [dx, dy, Nz]  = size(inMat.chrRegion);
            
            fnMetaData = dir([curCellDir '*meta*.txt']);
            if isempty(fnMetaData)
                continue;
            end
            
            %Get the metadata
            metaData = readtable(fullfile(curCellDir, fnMetaData(1).name), 'Delimiter','\t');
            % LEGACY code
            if ~isfield(metaData, 'zFactor')
                metaData.zFactor = 3;
            end
            calVoxelSize = metaData.voxelSizeX * metaData.voxelSizeY * metaData.voxelSizeZ * metaData.zFactor;
            
            avgPoiSeries  = zeros(numSmt, dx, dy, Nz);
            totPoiCountSeries = zeros(numSmt, dx, dy, Nz);
            numFrames  = zeros(numSmt, 1);
            firstDataDir = 0;
        end
        
        %Get position of current cell in the experimental database
        curCellDbPos = curProtDbPos(dirIdx);
        disp(['Processing DBIDX:' num2str(curCellDbPos, '%03d') ' - ' curProtFilelist{dirIdx}]);
        
        %Check whether this cell is qced to be taken for averaging
        if expDatabase.segmentation(curCellDbPos) == 1 && expDatabase.time_alignment(curCellDbPos) == 1 && expDatabase.fcs_calibration(curCellDbPos) == 1 && expDatabase.feature_vector(curCellDbPos) == 1
            anisoRegDir = fullfile(anisoDirRoot, curProtFilelist{dirIdx}, aniRegDir, filesep);
            anisoRegFilename  = dir([anisoRegDir '*.mat']);
            if length(anisoRegFilename) == 0
                disp('Calbrated image (aniso registration) is not found');
                continue;
            end

            mstFilename = fullfile(anisoDirRoot, curProtFilelist{dirIdx}, mstDir, mstTimeindexFilename);
            idxMat = load(mstFilename);
            for tarClust = options.start_mst:options.end_mst
                curCellMitotime = round(idxMat.mitotime(:,2));
                tarLoc = find(curCellMitotime == tarClust);
                nTpoints = length(tarLoc);
                if nTpoints >=1
                    selTarLoc = round(mean(tarLoc));
                    curCellDir = fullfile(isoDirRoot, curProtFilelist{dirIdx}, isoRegDir, filesep);
                    regStackFilename = dir([curCellDir '*.mat']);
                    selTarLocIso =selTarLoc -(expDatabase.tmax(curCellDbPos)-length(regStackFilename));
                    if selTarLocIso < 1
                        continue;
                    end
                    
                    inMat = load(fullfile(curCellDir, regStackFilename(selTarLocIso).name), 'poi', 'cellRegion');
                    curPoi = inMat.poi;
                    for zplane = 1:Nz
                        curPoi(:,:,zplane) = imgaussian(curPoi(:,:,zplane), options.sigma, options.hsize);
                    end
                    anisoRegMat = load(fullfile(anisoRegDir, anisoRegFilename(selTarLoc).name), 'background_488_N_voxel', 'calibration_factor_N_voxel');
                    curPoi= curPoi * anisoRegMat.calibration_factor_N_voxel; % Concentration num molecules per voxel
                    curPoi = curPoi - anisoRegMat.background_488_N_voxel;
                    curPoi= curPoi /calVoxelSize/0.602214086; %Converting to nanoMoles per liter
                    
                    curPoi(curPoi<0) = 0;
                    curPoi(inMat.cellRegion(:,:,:) == 0) = 0;
                    avgPoiSeries(tarClust-options.start_mst + 1, :,:,:) = squeeze(avgPoiSeries(tarClust-options.start_mst + 1, :,:,:)) + curPoi;
                    totPoiCountSeries(tarClust-options.start_mst + 1, :,:,:) = squeeze(totPoiCountSeries(tarClust-options.start_mst + 1, :,:,:)) + double(curPoi(:,:,:) > 0);
                    numFrames(tarClust-options.start_mst + 1) = numFrames(tarClust-options.start_mst + 1) + 1;
                end
            end
        end
    end
    if firstDataDir == 1
        disp('Segmentation and time alignment has to be done before running this pipeline');
        continue;
    end
    for tarClust = options.start_mst:options.end_mst
        avgPoiSeries (tarClust-options.start_mst + 1, :,:,:) = avgPoiSeries(tarClust-options.start_mst + 1, :,:,:) ./totPoiCountSeries(tarClust-options.start_mst + 1, :,:,:);
    end
    
    %Create output directory to save results of averaging im mat files
    curProtOutDir = fullfile(outDirRoot, options.avg_dir, curProtName, filesep);
    if ~exist(curProtOutDir)
        mkdir(curProtOutDir);
    end
    curProtOutDirTif = fullfile(outDirRoot, options.avg_dir, 'Tif_stack', curProtName, filesep);
    if ~exist(curProtOutDirTif) && options.save_tiff_stack == 1
        mkdir(curProtOutDirTif);
    end
    
    %Use model to refine the results of averaging
    mssDir = fullfile(outDirRoot, mssSubDir, filesep);
    mssFilelist = dir([mssDir, 'mss*.mat']);
    [~, modFilename, ~] = fileparts(mssFilelist(1).name);
    modFilenamePref = modFilename(1:end-2);
    for tarClust = options.start_mst:options.end_mst
        avgPoi  = squeeze(avgPoiSeries(tarClust-options.start_mst + 1, :,:,:));
        curModFilename = fullfile(mssDir, [modFilenamePref num2str(tarClust, '%02.f') '.mat']);
        if ~exist(curModFilename)
            disp(['MST_stage' num2str(tarClust) ' is not found - averaging skipped for this stage']);
            continue;
        end
        modMat = load(curModFilename);
        avgPoi(modMat.modCell(:,:,:) == 0) = 0;
        avgPoi(isnan(avgPoi)) = 0;
        nFrames  = numFrames(tarClust-options.start_mst + 1);
        
        curOutFullPathname = fullfile(curProtOutDir, [options.fn_prefix num2str(tarClust, '%03.f')]);
        save(curOutFullPathname, 'avgPoi', 'nFrames');
        
        curOutFilename = [options.fn_prefix num2str(tarClust, '%03.f') '.tif'];

        if options.save_tiff_stack == 1
            fn_write_tiff_stack(avgPoi, curProtOutDirTif, curOutFilename, 16);
        end
    end
end
end