function mss_transform_reg_to_cyl(aniDirRoot, isoDirRoot, inRegDir, fnExpdatabase, cellsToProcess, reproc, options)
%This function converts registered landmarks using cylindrical coordinate
%sytems by taking both z axis and the predicted cell division axis as
%cylindrical axis

%aniDirRoot: root directory of anistropic results
%isoDirRoot: root directory of isotropic results
%inRegDir: Name of subdirectory within isoDirRoot containing registratered
%stack
%fnExpdatabase: filename of experimental database
%cellsToProcess: list of cells to be processed
%reproc: 1: if reprocess cydrical representation , 0 otherwise
%options: structure containing options/params for cylindrical
%representation

%Author: M. Julius Hossain, EMBL, Heidelberg
%Last update: 2018_03_15 : Documentation added

%Default options/parameters for registration
if nargin < 7
    options = struct(...
        'cyl_dir', 'Cylindrical_landmarks',...
        'num_samples', 360); %Number of samples per slice
end

cellProcessed = 0;
%Reads the experimental database
expDatabase = readtable(fullfile(aniDirRoot, fnExpdatabase) , 'Delimiter','\t');
for cellIdx = cellsToProcess
    if expDatabase.manualQC(cellIdx) == 1 && (expDatabase.segmentation(cellIdx) == 1)
        %Set input and output directory
        %curIsoFilePart = expDatabase.filepath{cellIdx};
        %curIsoFilePart(curIsoFilePart == filesep) = '_';
        curIsoInDir  = fullfile(isoDirRoot, expDatabase.filepath{cellIdx}, inRegDir, filesep);
        curIsoOutDir = fullfile(isoDirRoot, expDatabase.filepath{cellIdx}, options.cyl_dir, filesep);
        
        %Get the metadata
        fnMetaData = dir([curIsoInDir '*meta*.txt']);
        if isempty(fnMetaData)
            continue;
        end
        
        %Get the metadata
        metaData = readtable(fullfile(curIsoInDir, fnMetaData(1).name), 'Delimiter','\t');
        voxelSizeX = metaData.voxelSizeX;
        voxelSizeY = metaData.voxelSizeY;
        voxelSizeZ = metaData.voxelSizeZ;
        voxelSize = voxelSizeX*voxelSizeY*voxelSizeZ;
        %Get the list of filenames
        fn = dir([curIsoInDir, '*.mat']);
        %nFiles = numel(fn);
        
        if length(dir([curIsoOutDir, '*.mat'])) >= numel(fn) && reproc == 0
            continue;
        end
        %disp(['Currently processing - CellID: ' num2str(cellIdx, '%03d')]);
        %Create output directory
        if ~exist(curIsoOutDir)
            mkdir(curIsoOutDir);
        end
        cellProcessed = cellProcessed + 1;
        tinit = 1;
        tfin = numel(fn);
        %voxelSize = 0.25 * 0.25 * 0.25;
        for tpoint = tinit: tfin
            %disp(['Proecessing: ' curIsoInDir(fsIdx(1)+1:end) fn(tpoint).name]);
            fsIdx = find(curIsoInDir == filesep, 4,'last');
            disp(['Currently proecessing - (CellID: ' num2str(cellIdx, '%03d') ') ' curIsoInDir(fsIdx(1)+1:fsIdx(3)-1) ', time point:' num2str(tpoint, '%03d')]);
            inShape = load([curIsoInDir fn(tpoint).name]);
            
            if tpoint == tinit
                [dx, dy, Nz] = size(inShape.cellRegion);
                chr1 = zeros(dx, dy, Nz);
                chr2 = zeros(dx, dy, Nz);
            end
            
            volCell = sum(sum(sum((inShape.cellRegion(:,:,:) > 0)))) * voxelSize;
            volChr = sum(sum(sum((inShape.chrRegion(:,:,:) > 0)))) * voxelSize;
            
            
            [cylCellZ, centCellZ]= mss_bw_to_cyl(inShape.cellRegion, dx, dy, Nz, options.num_samples,0);
            cellRegionTemp = permute(inShape.cellRegion, [3 2 1]);
            [cylCellP, centCellP]= mss_bw_to_cyl(cellRegionTemp, Nz, dx, dy, options.num_samples,0);
            
            if inShape.numChr == 1
                chr1 = double(inShape.chrRegion(:,:,:) == 1);
                for i = 1:Nz
                    chr1(:,:,i) = imfill(chr1(:,:,i),'holes');
                end
                [cylChrZ1, centChrZ1] = mss_bw_to_cyl(chr1, dx, dy, Nz, options.num_samples,1);
                chr1temp = permute(chr1, [3 2 1]);
                [cylChrP1, centChrP1] = mss_bw_to_cyl(chr1temp, Nz, dx, dy, options.num_samples,1);
                cylChrZ2 = zeros(size(cylChrZ1));
                cylChrP2 = zeros(size(cylChrP1));
                centChrZ2 = zeros(size(centChrZ1));
                centChrP2 = zeros(size(centChrP1));
                volChr1 = volChr;
                volChr2 = 0;
            else
                threeDLabel = bwconncomp(inShape.chrRegion, 18);
                numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
                [biggest, idx] = max(numPixels);
                chr1(:,:,:) = 0;
                chr1(threeDLabel.PixelIdxList{idx}) = 1;
                [xCoord, yCoord, zCoord] = ind2sub([dx dy Nz], threeDLabel.PixelIdxList{idx});
                centX1 = round(mean(xCoord));
                numPixels(idx) = 0;
                [biggest, idx] = max(numPixels);
                chr2(:,:,:) = 0;
                chr2(threeDLabel.PixelIdxList{idx}) = 1;
                [xCoord, yCoord, zCoord] = ind2sub([dx dy Nz], threeDLabel.PixelIdxList{idx});
                centX2 = round(mean(xCoord));
                
                if centX1 > centX2
                    temp = chr1;
                    chr1 = chr2;
                    chr2 = temp;
                end
                
                volChr1 = sum(sum(sum((chr1)))) * voxelSize;
                volChr2 = sum(sum(sum((chr2)))) * voxelSize;
                
                for i = 1:Nz
                    chr1(:,:,i) = imfill(chr1(:,:,i),'holes');
                end
                [cylChrZ1, centChrZ1] = mss_bw_to_cyl(chr1, dx, dy, Nz, options.num_samples,1);
                chr1temp = permute(chr1, [3 2 1]);
                [cylChrP1, centChrP1] = mss_bw_to_cyl(chr1temp, Nz, dx, dy, options.num_samples,1);
                
                for i = 1:Nz
                    chr2(:,:,i) = imfill(chr2(:,:,i),'holes');
                end
                
                [cylChrZ2, centChrZ2] = mss_bw_to_cyl(chr2, dx, dy, Nz, options.num_samples,1);
                chr2temp = permute(chr2, [3 2 1]);
                [cylChrP2, centChrP2] = mss_bw_to_cyl(chr2temp, Nz, dx, dy, options.num_samples,1);
            end
            
            
            numChr = inShape.numChr;
            chrDistMic = inShape.chrDistMic;
            mitoTime = inShape.mitoTime;
            outFilename = fullfile(curIsoOutDir, fn(tpoint).name);
            save(outFilename,'cylCellZ', 'cylChrZ1', 'cylChrZ2', 'centCellZ', 'centChrZ1', 'centChrZ2', 'cylCellP', 'cylChrP1', 'cylChrP2', 'centCellP', 'centChrP1', 'centChrP2', 'numChr', 'chrDistMic', 'mitoTime', 'volCell', 'volChr', 'volChr1', 'volChr2');
        end
        writetable(metaData, fullfile(curIsoOutDir, fnMetaData(1).name), 'Delimiter','\t');
    end
end
disp(['Total number of cells processed in this run: ' num2str(cellProcessed)]);
end