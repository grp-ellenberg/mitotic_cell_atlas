function [opt, regopt, cylopt, avgopt, modopt] = mss_averaging_opt(mainpath)
%This function initializes parameters/options for averaging

%Author: Julius Hossain, EMBL, Heidelberg
%Last update: 2018_03_15

if nargin < 1
    mainpath = ''; % option to provide only subdirectories for the different part of the processing
end

%Directory containing isotropic data used for shape modeling and poi averaging
opt.iso_indir = 'Results_Iso';   
%Directory storing non-isotropic data used for mst and poi analysis
opt.ani_indir = 'Results'; 
%Directory containing MST model
opt.mst_mod_dir = fullfile('Process_Settings', 'MST');
%Output directory containing results for averaging both landmarks and proteins
opt.avg_dir = 'Results_spatial_avg';
opt.iso_indir = fullfile(mainpath, opt.iso_indir);
opt.ani_indir = fullfile(mainpath, opt.ani_indir);
opt.avg_dir = fullfile(mainpath, opt.avg_dir);
opt.mst_mod_dir = fullfile(mainpath, opt.mst_mod_dir);


opt.registration_reprocess = 0; %1: reprocess registration, 0 otherwise
opt.cylindrical_reprocess = 0;  %1: reprocess cyl representation, 0 otherwise
opt.model_reprocess = 0; %1: reprocess shape model, 0 otherwise

%Index of the list of cells to process registration and cylindrical representation
%if empty all cells are processed.
opt.cellIdx = []; 
opt.exp_database = 'full_database.txt'; % default name of data base
opt.mstalign_timeIndexFilename = 'MST_timeindex.mat'; %Filename storing mst cluster
opt.segdir = 'Segmentation'; %Subdirectory containing isotropic segmented results
opt.regvis = 0; %1: visualize registration results, 0 otherwise
opt.ani_regdir = 'Registration'; %Subdirectory storing anisotropic calibrated images
opt.mstdir = 'Temporal_Align'; %Subdirectory stroing mst files 
opt.mst_mod_filename = 'temporal_alignment.mat'; %filename of the mst model 
regopt = get_regopt(); %Get registration options
cylopt = get_cylopt(); %Get otions for cylindrical representation
avgopt = get_avgopt(); %Get options for protein averaging
modopt = get_modopt(); %Get options for landmarks averaging
end

function [regopt] = get_regopt()
%Options for registration
regopt.reg_dir = 'Registration'; %Subdirectory storing registered results
regopt.NzReg = 112; %Number of z slices to save registered stack
regopt.start_mst = 7; % starting mst for registration
end

function [cylopt] = get_cylopt()
%Options for representing landmakrs using cylindrical coordinates
cylopt.cyl_dir = 'Cylindrical_landmarks';
cylopt.num_samples = 360; %Number of samples per slice
end

function [modopt] = get_modopt()
%Options for landmarks modeling
modopt.mss_dir = 'Avg_cell';
modopt.start_mst = 7; %Starting mst for averaging
modopt.end_mst = 20; %Last mst for registration
modopt.bFactPredAxis = 0.4; %Contribution of cyl coord with predicted axis
modopt.fn_prefix = 'mss_stage_'; %Filename containing avg landmarks.
modopt.skip_drift_cor = 13; %Stage that skips drift correction in z
modopt.save_tiff_stack = 0; % 1: to save tiff stack of average shape, 0 otherwise
modopt.sm_size = 7; %size of the smooth and equalize 
%List of poi: 
%set use_prot to 1 to include and 0 to skip the poi for averaging. Setting
%all of them to 1 will use entire dataset for averaging
modopt.protList = [... 
    struct('prot_name', 'NEDD1', 'use_prot', 1); ...
    struct('prot_name', 'KIF11', 'use_prot', 1); ...
    struct('prot_name', 'MIS12', 'use_prot', 1); ...
    struct('prot_name', 'TUBB2C', 'use_prot', 1); ...
    struct('prot_name', 'RACGAP1', 'use_prot', 1); ...
    struct('prot_name', 'CDCA8', 'use_prot', 1); ...
    struct('prot_name', 'AURKB', 'use_prot', 1); ...
    struct('prot_name', 'NUP214', 'use_prot', 1); ...
    struct('prot_name', 'PLK1', 'use_prot', 1); ...
    struct('prot_name', 'CENPA', 'use_prot', 1); ...
    struct('prot_name', 'AURKBZFN', 'use_prot', 1); ...
    struct('prot_name', 'BUB1', 'use_prot', 1); ...
    struct('prot_name', 'NES', 'use_prot', 1); ...
    struct('prot_name', 'gfpNUP107z26z31', 'use_prot', 1); ...
    struct('prot_name', 'gfpRANBP2c97', 'use_prot', 1); ...
    struct('prot_name', 'gfpNUP214c2-12', 'use_prot', 1); ...
    struct('prot_name', 'TPRgfpc171', 'use_prot', 1); ...
    struct('prot_name', 'STAG2gfpcf2', 'use_prot', 1); ...
    struct('prot_name', 'gfpBUBR1cM04-A03', 'use_prot', 1); ...
    struct('prot_name', 'gfpAPC2cM21-P1-A11', 'use_prot', 1); ...
    struct('prot_name', 'SCC1gfpc', 'use_prot', 1); ...
    struct('prot_name', 'STAG1gfpcH8', 'use_prot', 1); ...
    struct('prot_name', 'CEP192gfpz15',  'use_prot', 1); ...
    struct('prot_name', 'CTCFgfpcF2', 'use_prot', 1); ...
    struct('prot_name', 'CEP250gfpc1A-142', 'use_prot', 1); ...
    struct('prot_name', 'WAPLgfpc', 'use_prot', 1); ...
    struct('prot_name', 'MAD2L1gfpcM11-F11', 'use_prot', 1); ...
    struct('prot_name', 'MAD2L1gfpcM11-B11', 'use_prot', 1); ...
    struct('prot_name', 'gfpNCAPHz9', 'use_prot', 1); ...
    struct('prot_name', 'SMC4gfpz82z68', 'use_prot', 1); ... 
    struct('prot_name', 'NCAPD3gfpc16', 'use_prot', 1); ... 
    struct('prot_name', 'gfpNCAPH2c1', 'use_prot', 1); ...
    struct('prot_name', 'NCAPD2gfpc272c78', 'use_prot', 1); ...
    struct('prot_name', 'NCAPHgfpc86', 'use_prot', 1); ...
    struct('prot_name', 'NCAPH2gfpc67', 'use_prot', 1); ...
    struct('prot_name', 'gfpTOP2Ac102', 'use_prot', 1); ...
    struct('prot_name', 'gfpKIF4Ac173', 'use_prot', 1)];
end

function [avgopt] = get_avgopt()
%Options for proteins averaging
avgopt.start_mst = 7; %Starting mst for averaging
avgopt.end_mst = 20; %Last mst for registration
avgopt.sigma = 2; %Sigma for Gaussian filter
avgopt.hsize = 2; %size of the kernel
avgopt.avg_dir = 'Avg_poi'; %Directory to save average protein distributions in mat files
%avgopt.avg_dir_tif = 'Avg_prot_dist_tif'; %Directory to save average protein distributions in tif stacks
avgopt.fn_prefix = 'Avg_poi_stage_'; %Filename containing avg protein dist.
avgopt.save_tiff_stack = 0; % 1: to save tiff stack of average poi, 0 otherwise
%List of proteins and reproc flag (1 reproc, 0 no)
avgopt.protList = [... 
    struct('prot_name', 'NEDD1', 'reproc', 1); ...
    struct('prot_name', 'KIF11', 'reproc', 1); ...
    struct('prot_name', 'MIS12', 'reproc', 1); ...
    struct('prot_name', 'TUBB2C', 'reproc', 1); ...
    struct('prot_name', 'RACGAP1', 'reproc', 1); ...
    struct('prot_name', 'CDCA8', 'reproc', 1); ...
    struct('prot_name', 'AURKB', 'reproc', 1); ...
    struct('prot_name', 'NUP214', 'reproc', 1); ...
    struct('prot_name', 'PLK1', 'reproc', 1); ...
    struct('prot_name', 'CENPA', 'reproc', 1); ...
    struct('prot_name', 'AURKBZFN', 'reproc', 1); ...
    struct('prot_name', 'BUB1', 'reproc', 1); ...
    struct('prot_name', 'NES', 'reproc', 1); ...
    struct('prot_name', 'gfpNUP107z26z31', 'reproc', 1); ...
    struct('prot_name', 'gfpRANBP2c97', 'reproc', 1); ...
    struct('prot_name', 'gfpNUP214c2-12', 'reproc', 1); ...
    struct('prot_name', 'TPRgfpc171', 'reproc', 1); ...
    struct('prot_name', 'STAG2gfpcf2', 'reproc', 1); ...
    struct('prot_name', 'gfpBUBR1cM04-A03', 'reproc', 1); ...
    struct('prot_name', 'gfpAPC2cM21-P1-A11', 'reproc', 1); ...
    struct('prot_name', 'SCC1gfpc', 'reproc', 1); ...
    struct('prot_name', 'STAG1gfpcH8', 'reproc', 1); ...
    struct('prot_name', 'CEP192gfpz15',  'reproc', 1); ...
    struct('prot_name', 'CTCFgfpcF2', 'reproc', 1); ...
    struct('prot_name', 'CEP250gfpc1A-142', 'reproc', 1); ...
    struct('prot_name', 'WAPLgfpc', 'reproc', 1); ...
    struct('prot_name', 'MAD2L1gfpcM11-F11', 'reproc', 1); ...
    struct('prot_name', 'MAD2L1gfpcM11-B11', 'reproc', 1); ...
    struct('prot_name', 'gfpNCAPHz9', 'reproc', 1); ...
    struct('prot_name', 'SMC4gfpz82z68', 'reproc', 1); ... 
    struct('prot_name', 'NCAPD3gfpc16', 'reproc', 1); ... 
    struct('prot_name', 'gfpNCAPH2c1', 'reproc', 1); ...
    struct('prot_name', 'NCAPD2gfpc272c78', 'reproc', 1); ...
    struct('prot_name', 'NCAPHgfpc86', 'reproc', 1); ...
    struct('prot_name', 'NCAPH2gfpc67', 'reproc', 1); ...
    struct('prot_name', 'gfpTOP2Ac102', 'reproc', 1); ...
    struct('prot_name', 'gfpKIF4Ac173', 'reproc', 1)];
end