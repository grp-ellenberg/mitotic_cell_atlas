% This scripts reads the options/parameters and creates a canonical cell model and spatial average POI distribution


% Get the root directory containing the 'main_and_parameters.m' file
[ini_dir,~,~] = fileparts(pwd);


% Get the root directory containing the tiff data and the Results folder
src_data_dir = uigetdir(ini_dir,'Select the main directory containing Data_tifs and Results');
if src_data_dir == 0
    return
end

src_root_dir = uigetdir(src_data_dir,'Select the directory containing source code');
if src_root_dir == 0
    return
end

%% 
addpath(src_root_dir);
addpath(fullfile(src_root_dir, 'IO'));
addpath(fullfile(src_root_dir, 'spatial_avg'));
addpath(fullfile(src_root_dir, 'spatial_avg', 'src'));
addpath(fullfile(src_root_dir, 'spatial_avg', 'src_extern'));
addpath(fullfile(src_root_dir, 'spatial_avg', 'src_utils'));
%% 
[opt, opt_reg, opt_cyl, opt_avg, opt_mod] = mss_averaging_opt(src_data_dir);



%% Select the directory containing anisotropic results
[opt.ani_indir, status] = verifyDirectory(opt.ani_indir, 'Specify the directory containing segmented anisotropic data, i.e. ..\Results', false);
if ~status
    return;
end


%% Select the directory containing isotropic version of segmented results
[opt.iso_indir, status] = verifyDirectory(opt.iso_indir, ...
    'Specify the directory containing segmented isotropic data, i.e. ..\Results_Iso', false);
if ~status
    return;
end

%% Select the directory containing MST model
[opt.mst_mod_dir, status] = verifyDirectory(opt.mst_mod_dir, ...
    'Specify the directory containing MST model, i.e. ..\MST', false);
if ~status
    return;
end


%% Select the directory to save the output of average cells and pois
[opt.avg_dir, status] = verifyDirectory(opt.avg_dir, 'Specify the directory to save averaging data (average cells and pois)', true);
if ~status
    return;
end
%%
%Run the pipeline
mss_main_and_parameters(opt, opt_reg, opt_cyl, opt_avg, opt_mod);