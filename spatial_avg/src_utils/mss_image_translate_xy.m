function [tImgVol] = mss_image_translate_xy(imgVol, tVect)
%This fuction translates an image stack by given shift
%ingVol: input image volue to shifted
%tVect : translation vector

%Version date: 2015-02-16
%Author: Julius Hossain, EMBL, Heidelberg

tImgVol = zeros(size(imgVol));
xShift = round(tVect(1));
yShift = round(tVect(2));

%Apply shift along x
if xShift <0
    tImgVol(1:end+xShift, :, :) = imgVol(1-xShift:end,:,:);
elseif xShift >0
    tImgVol(xShift+1:end, :, :) = imgVol(1:end-xShift,:,:);
end

%Copy the shifted data to original volume
if xShift ~= 0
    imgVol = tImgVol;
    tImgVol(:,:,:) = 0;
end

%Apply shift along y
if yShift <0
    tImgVol(:, 1:end+yShift,:) = imgVol(:,1-yShift:end,:);
elseif yShift >0
    tImgVol(:, yShift+1:end,:) = imgVol(:, 1:end-yShift,:);
end

%Initialize output stack if y shift is zero
if yShift == 0 
    tImgVol = imgVol;
end
end