function [outStack] = mss_smooth_and_equalize(bwStack, stkSpan, refVol, voxelSize, useSpan, smSize)
%This function smooth 3D bwStack and equalize to a given reference volume

%bwStack: input stack
%stkSpan: bounding region to retrict output region
%refVol: volume of the input stack
%voxelSize: size of voxel
%useSpan: 1: use stkSpan, 0 otherwise

smoothStack  = smooth3(bwStack(:,:,:) , 'box', smSize);
outStack = double(smoothStack(:,:,:) >= 0.5);
outVol = sum(sum(sum(outStack))) *voxelSize;
thDist = 0.49;
while outVol <= refVol *0.995 && thDist >= 0.01
    if useSpan == 1
        outStack = double(and((smoothStack(:,:,:) >= thDist), stkSpan));
    else
        outStack = double(smoothStack(:,:,:) >= thDist);
    end
    outVol = sum(sum(sum(outStack))) *voxelSize;
    thDist = thDist -0.01;
end
end

