function [outCyl1, outCyl2] = mss_correct_drift_z_blend(inCyl1, inCyl2, bFact)
%This function corrects the drift in drift in Z. It is used for blend two
%models

%refCent: reference center that is used for shift correction
%inCyl: input cylindrical coordinates
%outCly: output corrected cylindrical coords

zCent1 = sum(inCyl1.*(1:length(inCyl1))')/sum(inCyl1);
zCent2 = sum(inCyl2.*(1:length(inCyl2))')/sum(inCyl2);
outCyl1 = zeros(size(inCyl1));
outCyl2 = zeros(size(inCyl2));
avgCent = round(zCent1 * bFact + zCent2 * (1- bFact));
zCent1 = round(zCent1);
zCent2 = round(zCent2);
if avgCent>zCent1
    outCyl1(avgCent-zCent1+1:end) = inCyl1(1:end-(avgCent-zCent1));
    outCyl2(1:end-(zCent2-avgCent)) = inCyl2(zCent2-avgCent+1:end);
else
    outCyl1(1:end-(zCent1-avgCent)) = inCyl1(zCent1-avgCent+1:end);
    outCyl2(avgCent-zCent2+1:end) = inCyl2(1:end-(avgCent-zCent2));
end
end

