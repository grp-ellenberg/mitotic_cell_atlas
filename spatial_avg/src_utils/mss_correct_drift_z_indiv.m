function [outCyl] = mss_correct_drift_z_indiv(refCent, inCyl)
%This function corrects the drift in Z  and it is used for individual cells

%refCent: reference center that is used for shift correction
%inCyl: input cylindrical coordinates
%outCly: output corrected cylindrical coords

inCent = sum(inCyl.*(1:length(inCyl))')/sum(inCyl);

outCyl = zeros(size(inCyl));
refCent = round(refCent);
inCent  = round(inCent);

if refCent>inCent
    outCyl(refCent-inCent+1:end) = inCyl(1:end-(refCent-inCent));
else
    outCyl(1:end-(inCent-refCent)) = inCyl(inCent-refCent+1:end);
end
end