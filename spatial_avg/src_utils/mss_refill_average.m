function [stdCyl] = mss_refill_average(cylCoord, maxSep, nSamp, chrFlag)
%This function fills the missing points up in cylindrical representation

%cylCoord: cylindrical representations
%maxSep: maximum separation/max number of missing points
%nSamp: number of points per slice used to represent cylindrical coords
%chrFlag: 1 if it input shape is chromosome

%Author: Julius Hossain, EMBL Heidelberg
%Last update: 2015_11_22

stdCyl = zeros(nSamp,1);

i = 1;
while i <= size(cylCoord,1)
    j = i+1;
    sum = cylCoord(i,2);
    dCount = 1;
    maxVal = cylCoord(i,2);
    while j <= size(cylCoord,1) && cylCoord(j,1) == cylCoord(i,1)
        sum = sum+cylCoord(j,2);
        dCount = dCount + 1;
        if cylCoord(j,2)>maxVal
            maxVal = cylCoord(j,2);
        end
        j = j + 1;
    end
    if dCount > 1
        if chrFlag ==1
            avg = maxVal;
        else
            avg = sum/dCount;
        end
        cylCoord(i:i+dCount-1,2) = avg;
    end
    i = j;
end
cylCoord = unique(cylCoord, 'rows');
cylCoord(1,:) = int16(cylCoord(1,:));
for i = 1: size(cylCoord,1)-1
    val = int16(min(cylCoord(i,1)+1,nSamp));
    stdCyl(val) = cylCoord(i,2);
    if (cylCoord(i+1,1)-cylCoord(i,1) >1) && (cylCoord(i+1,1)-cylCoord(i,1) <=maxSep)
        rVals = linspace(cylCoord(i,2), cylCoord(i+1,2), cylCoord(i+1,1)-cylCoord(i,1)-1);
        stdCyl(val+1:val+length(rVals))= rVals; 
    end
end

if (cylCoord(end,1)-cylCoord(1,1) >nSamp-maxSep) && (cylCoord(i+1,1)-cylCoord(i,1) <nSamp-1)
    rVals = linspace(cylCoord(end,2), cylCoord(1,2), nSamp-cylCoord(end,1)+ cylCoord(1,1));
    if cylCoord(end,1)+1<nSamp
       stdCyl(int16(cylCoord(end,1)+1): nSamp) = rVals(1:length(cylCoord(end,1)+1: nSamp));
    end
    if cylCoord(1,1) > 0
       stdCyl(1:int16(cylCoord(1,1)+1)) = rVals(end -length(1:cylCoord(1,1)+1)+1:end); 
    end 
end

end
