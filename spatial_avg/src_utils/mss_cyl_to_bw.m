function [bwStack] = mss_cyl_to_bw(cylCoords, dx, dy, cent, Nz,nSamp,nSmooth)
%This fuction generate binary image stack from cylindrical representation
%of a 3D shape 

%cylCellCoords: cylindrical coordinates
%dx, dy, Nz are the size of image stack used to generate cylindrical coords
%nSamp: number of samples per slice used to represent cylindrical coords
%nSmooth: size of smoothing box
%cent: center of the cylinder

%Author: Julius Hossain, EMBL Heidelberg
%Last update: 2015_01_25

bwStack = zeros(dx,dy,Nz);
cartCoord = zeros(size(cylCoords,1),3);
cylCoords = smooth(cylCoords,nSmooth);
for i = 1: size(cylCoords,1)
    cartCoord(i, 1) = mod(i,nSamp);
    if cartCoord(i,1) == 0
        cartCoord(i,1) = nSamp;
    end
    cartCoord(i,2)= cylCoords(i);
    cartCoord(i,3) = ceil(i/nSamp);
end

[x, y, z]=cyl2cart(cartCoord);
for i = 1:length(z)
    x(i) = round(x(i) + cent(z(i),1));
    y(i) = round(y(i) + cent(z(i),2));
end

for i = 1:length(cent)-1
bwStack(round(cent(i,1)),round(cent(i,2)),i) = 0;
end

for i=1:length(x)
    bwStack(x(i),y(i),z(i))=1;
end

portion = 0.1:0.1:0.9;
for i=1:length(x)-1
    xIdx = round(x(i)+(x(i+1)-x(i))*portion);
    yIdx = round(y(i)+(y(i+1)-y(i))*portion);
    zIdx = round(z(i)+0*portion);
    bwStack(xIdx,yIdx,zIdx)=1;
end
for i = 1:length(cent)-1
bwStack(round(cent(i,1)),round(cent(i,2)),i) = 0;
end

