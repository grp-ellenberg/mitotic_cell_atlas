function filledImage = mss_fill_image_bw(bwImage, span)
%This function filles the holes in bw image. In general it is used to
%process binary image generated from cylindrical coordinates.
%bwImage: binary image

Nz = size(bwImage, 3);
filledImage = imdilate(bwImage, ones(span,span,span));
for i = 1:Nz
    filledImage(:,:,i) = imfill(filledImage(:,:,i), 'holes');
end
filledImage = imerode(filledImage, ones(span,span,span));
end