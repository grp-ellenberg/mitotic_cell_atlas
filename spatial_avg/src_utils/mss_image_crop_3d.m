function [cImg] = mss_image_crop_3d(img, sizeVol)
%This function crops image volume with a predifined size around the centre of the volume
%img: input image stack
%sizeVol: size of the output stack
%Version date: 2016_02_22
%Author: M. Julius Hossain, EMBL, Heidelberg

cImg = zeros(sizeVol);
% Getting the centre of the stack
orgCentX = floor(size(img,1)/2);
orgCentY = floor(size(img,2)/2);
orgCentZ = floor(size(img,3)/2);
if size(img,1)>=sizeVol(1) && size(img,2)>=sizeVol(2) && size(img,3)>=sizeVol(3)
    cImg(:,:,:) = img(orgCentX-sizeVol(1)/2+1: orgCentX+sizeVol(1)/2, orgCentY-sizeVol(2)/2+1: orgCentY+sizeVol(2)/2, orgCentZ-sizeVol(3)/2+1: orgCentZ+sizeVol(3)/2);
elseif size(img,1)< sizeVol(1)
    cImg(sizeVol(1)/2-orgCentX+1: sizeVol(1)/2+orgCentX,:,:) = img(1:2*orgCentX, orgCentY-sizeVol(2)/2+1: orgCentY+sizeVol(2)/2, orgCentZ-sizeVol(3)/2+1: orgCentZ+sizeVol(3)/2);
elseif size(img,2)< sizeVol(2)
    cImg(:, sizeVol(2)/2-orgCentY+1: sizeVol(2)/2+orgCentY, :) = img(orgCentX-sizeVol(1)/2+1: orgCentX+sizeVol(1)/2, 1:2*orgCentY, orgCentZ-sizeVol(3)/2+1: orgCentZ+sizeVol(3)/2);
end