function [cylRep, cent] = mss_bw_to_cyl(bwImage, dx, dy, Nz, nSamp, chrFlag)
%This function represents a 3D shape from binary image using cylindrical
%coordinate system.

%bwImage: input binary image containg the shape to be represented using
%cylindrical coordinate system
%nSamp: number of points per slice for cylindrical representation
%chrFlag: 1 if input shape is chromosome/ 0 otherwise
%cylRep: output cylindrical coordinates
%cent: center of the cylinder
%Author: M. Julius Hossain, EMBL, Heidelberg
%Created: 2015-09-20

maxSep = 10; %maximum separation/max number of missing points

cylRep= zeros(nSamp*Nz,1);
cent = zeros(Nz+1,2);
if chrFlag == 0
    [xCoord, yCoord, zCoord] = ind2sub(size(bwImage), find(bwImage>0));
    cent(:,1) = round(mean(xCoord));
    cent(:,2) = round(mean(yCoord));
else
    cent(:,1) = dx/2;
    cent(:,2) = dy/2;
end

for zplane = 1: Nz
    [xCoord, yCoord] = ind2sub(size(bwImage(:,:,zplane)), find(bwImage(:,:,zplane)>0));
    if length(xCoord)<10
        continue;
    else
        cent(zplane,1) = round(mean(xCoord));
        cent(zplane,2) = round(mean(yCoord));
    end
    
    [B,L] = bwboundaries(bwImage(:,:,zplane),'noholes');
    boundPoints = [];
    for k = 1:length(B)
        if k == 1
            boundPoints = B{k};
        else
            boundPoints = [boundPoints; B{k}];
        end
    end
    if size(boundPoints,1)>1
        boundPoints(:,1)=boundPoints(:,1)-cent(zplane,1);
        boundPoints(:,2)=boundPoints(:,2)-cent(zplane,2);
        boundPoints(:,3) = zplane;
        cylCoord = cart2cyl(boundPoints);
        cylCoord = sortrows(cylCoord);
        cylCoord(:,1) = floor(cylCoord(:,1));
        stdCoord = mss_refill_average(cylCoord, maxSep, nSamp, chrFlag);
        cylRep(nSamp*(zplane-1)+1:nSamp*zplane) = stdCoord;
    end
end
end