# Mitotic standard stage (MSS)

This package generates mitotic standard stage (MSS) and align individual 
cells into it to create average protein distributions. The pipeline uses
segmented results (isotropic version) as input and registers individual 
cells based on the predicted cell axis and represents it using cylindrical
coordinate system. This also uses standard mitotic time (MST) as input to 
bin individual time points to the corresponding stages.

Data requirements: Segmentation of landmarks and temporal alignment (MST) 
has to be done before running this pipeline.
 
How to run the pipeline:

**STEP1**: Go to the subdirectory 'MSS\runfiles' and execute 'run_averaging.m' 
file 

**STEP2:** Select the root directory of 'mss_main_and_parameters.m' files. 
However, it will be done automatically if the directory structure of the 
source code as provided remains

**STEP3:** Select the root directory of isotropic segmented volume 
('Results_Iso'). 

**STEP4:** Select the root directory of anisotropic segmented volume 
('Results')containing standard mitotic time. It will be done automatically 
if it finds a sub directory 'Results' under the same directory containg 
anisotropic data (Results_Iso). 

When above 4 steps are done this script will execute the pipeline for 
the dataset with default parameters and to save the results of MSS and 
average pois in a sub directory called 'Results_mss_avgpoi' located 
under the same directory containg anisotropic data (Results_Iso). 

**TIPS:** To avoid step 2-4 mentioned above specify the directory of the 
isotropic segmented data in 'MSS\runfiles\mss_averaging_opt.m' file 
(variable 'opt.iso_indir' in line 10)

Edit 'mss_averaging_opt.m' files to run the pipeline with different options
and parameters.
