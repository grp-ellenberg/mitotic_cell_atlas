    function [dirname, status] = verifyDirectory(dirname, guitext, mk_dir)
        % CHECKDIRECTORY verify if a directory exist and eventually create one
        % CHECKDIRECTORY(dirname, guitext, mk_dir)
        % INPUT:
        %   dirname: Path to directory. Can be empty 
        %   guitext: A text to show in the GUI when selecting the path
        %   mk_dir: logical to force making a directory
        status = false;
        if exist(dirname,'dir')
            status = true;
            return;
        end
        if ~isempty(dirname) && mk_dir
            status = mkdir(dirname);
        end
        if ~status
            dirname = uigetdir(dirname, guitext);
        end
        if dirname
            status = true;
        end
            
    end