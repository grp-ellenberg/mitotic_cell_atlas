function [success, gen_opt] = gen_database(gen_opt, pattern)
%% GEN_DATABASE generate a tab delimited file containing a list of highres image files  and QC
% Browse a directory and find the corresponding high_res files according to a pattern
%
% [success] = GEN_DATABASE() use default settings and ask for experiment and result folder
% [success] = GEN_DATABASE(gen_opt) use gen_opt struct to obtain the parameters  
% [success] = GEN_DATABASE(gen_opt, pattern) specify regular expression patterns to identify the paths
%
% Generates  exp_database file for all imaged high-resolution experiments. 
% Only folders that match entry poi_name in metadata_file are searched. 
% For each QC there is a number
%   -1 : unprocessed
%    0 : failed
%    1, or above: successful 
% INPUTS: 
%   * gen_opt: A stuct with directories and file names
%        'exp_dir', 'your data directory', ...
%        'out_dir', 'your directory to save the results', ...
%        'exp_database',  'full_database.txt', ...              
%        'poi_metadata', 'POI_annotation_database.txt');
%   * pattern: a struct with regular expression string to identify folder, well, and time infos 
%       'exp',  '(?<date>\d+)_(?<poi>.+)_(?<system>.+)', ... Find the experiment, poi/clone name and microscope system
%        'wellRnd', 'cell(?<well>\d+)_R(?<round>\d+)', ...   Find well and round of experiment
%        'positionTime', '.+_P(?<position>\d+)_T(?<time>\d+).tif$' find position and time 
% -----
% Format of poi_metadata
% poi_name      segmodel	poi_channel	nuc_channel	cell_channel	tag	shortname	tmax	tres
% NUP96gfpc195	2           1           2           3               GFP	NUP96       40      1.5
% ------
%   poi_name: name of poi that matches folder name YYMMDD_poi_name	
%   segmodel: The model used to compute the segmentation. Currently 1 for acqusition with H2BmCherry, and 2 for
%             acquisition with SiR-DNA
%   poi_channel:    index of channel that contains POI data (1 in rawtif)
%   cell_channel:   index of channel to segment cell boundary landmark (2 in rawtif)
%   nuc_channel:    index of channel to segment nucleus landmark (3 in rawtif)
%   tag: FP tag
%   shortname: short name of protein. Allows to pool different clones
%   tmax: Maximal number of time frames
%   tres: time resolution in minutes
%
% Author: Yin Cai, EMBL Heidelberg
% Updates: Antonio Z. Politi, M. Julius Hossain, EMBL Heidelberg
%   16.05.18 - pipeline integration and comments
%   16.03.20 - POI_annotation_database.txt
%   17.09.11 - Merge AQ and DE data type. Code simplified use getAllFolders
%   to search for correct folder. Updated documentation.
%   18.03.12 - Data base for raw tifs processing. New metadata_format, struct as argument

if nargin < 1
    gen_opt = struct( ...
        'exp_dir', '', ... 
        'out_dir', '', ...
        'exp_database',  'full_database.txt', ...
        'poi_metadata', 'POI_annotation_database.txt');
end

if nargin < 2
    pattern = struct('exp',  '(?<date>\d+)_(?<poi>.+)_(?<system>.+)', ...
                    'wellRnd', 'cell(?<well>\d+)_R(?<round>\d+)', ...
                    'positionTime', '.+_P(?<position>\d+)_T(?<time>\d+).tif$');
end

expected_gen_opt = {'exp_dir', 'out_dir', 'exp_database', 'poi_metadata'};
expected_pattern = {'exp', 'wellRnd', 'positionTime'};

if ~all(isfield(gen_opt, expected_gen_opt))
    fprintf('First argument gen_opt does not contains the expected fields\n%s \n', strjoin(expected_gen_opt));
    return
end

if ~all(isfield(pattern, expected_pattern))
    fprintf('First argument gen_opt does not contains the expected fields\n%s \n', strjoin(expected_pattern));
    return
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

if strcmp(gen_opt.exp_dir, gen_opt.out_dir)
    error('Experiment and output directory must be different');
end
%% Load the poi label and localization definitions
metadata_variable_name = {'poi_name', 'segmodel', 'poi_channel', 'nuc_channel', 'cell_channel', ...
    'tag', 'shortname', 'tmax', 'tres'};
metadata = fullfile(gen_opt.exp_dir, gen_opt.poi_metadata);
if ~exist(metadata, 'file')
    fprintf('Generate metadata file prior processing experimental directory. Processing stops now\n');
    help gen_database
    return
end
annotation = readtable(metadata, 'Delimiter','\t');

if ~isequal(annotation.Properties.VariableNames, metadata_variable_name)
    error('gen_database:wrongformat', 'Format of metadata file %s does not comply with expected format', metadata);
end

%% load and/or check data base file
variable_name = {'idx', 'filepath', 'imgexp', 'poi', 'system', ...
    'segmodel', 'tmax', 'tres', ...
    'Cpoi','Cdna','Ccell','tag', ...
    'well','round', 'position','manualQC'...
    'segmentation', 'seg_visualize', 'fcs_calibration','lm_features','time_alignment',...
    'poiprocess','feature_extract','feature_vector'};
output_txt = fullfile(gen_opt.out_dir, gen_opt.exp_database);
success = 0;

if exist(output_txt,'file')
    % if exist a database file with the same variables, load the file. Else, overwrite
    processed_database_loc = readtable(output_txt,'Delimiter','\t');
    if ~isequal(processed_database_loc.Properties.VariableNames, variable_name)
        qdlg = questdlg(sprintf('The current database %s has a different format than expected!\nDo you want to proceed and overwrite the file?', output_txt));
        if ~strcmp(qdlg, 'Yes')
            return
        end
    else
        processed_database = processed_database_loc;
    end
end

%% get directories
[list_of_dir, dir_id] = get_mitosys_dirlist(gen_opt.exp_dir, pattern.exp);
if isempty(list_of_dir)
    warning('No images in %s that fit the expected pattern %s', exp_dir, pattern);
    return
end

num_of_data = length(list_of_dir);
for date_idx = 1:num_of_data
    poi = dir_id{date_idx}.poi;
    annotation_idx = find(strcmp(annotation.poi_name, poi),1);
    if isempty(annotation_idx)
        fprintf('POI directory %s is not in metadata file %s\n', poi, gen_opt.poi_metadata); 
        continue
    end
    Cpoi = annotation.poi_channel(annotation_idx);
    Cdna = annotation.nuc_channel(annotation_idx);
    Ccell = annotation.cell_channel(annotation_idx);
    tmax = annotation.tmax(annotation_idx);
    segmodel = annotation.segmodel(annotation_idx);
    tres = annotation.tres(annotation_idx);
    tag = annotation.tag{annotation_idx};
    system = dir_id{date_idx}.system;
    cell_list = getAllFolders(list_of_dir{date_idx}, 'cell\d+_R\d+');
    
    for icell = 1:length(cell_list)
        %% Read well, round, position and time (this is for keeping track of original images)
       
        wellRnd = regexp(cell_list{icell}, pattern.wellRnd , 'names');
        wellRnd = [str2double(wellRnd.well), str2double(wellRnd.round)];
        imglist = getAllFiles(cell_list{icell}, 'tif');
        
        % check that the number of time points is conform
        if length(imglist) ~= annotation.tmax(annotation_idx)  % QC based on image sequence length
            imglist = getAllFiles(fullfile(cell_list{icell}, 'rawtif'),  'tif'); % format with rawtif in front
            if length(imglist) ~= annotation.tmax(annotation_idx)
                fprintf('For %s number of images is %d expected is %d\n', cell_list{icell} , length(imglist), annotation.tmax(annotation_idx))
                continue
            end
        end
        positionTime = cell2mat(regexp(imglist, pattern.positionTime, 'names'));
        positionTime = [arrayfun(@(x) str2double(x.position), positionTime), arrayfun(@(x) str2double(x.time), positionTime)];
        idx_lastimage = find(positionTime(:,2) == annotation.tmax(annotation_idx));
        
        
        filepath = strrep(cell_list{icell}, [gen_opt.exp_dir '\'],  '');
        % remove rawtif in case it is in path
        filepath = strrep(filepath, ['rawtif\'],  '');
        
        imgexp = strrep(imglist{idx_lastimage}, [gen_opt.exp_dir '\'], '');
        newtab = cell2table({icell, filepath, imgexp, poi, system, ...
                segmodel, tmax, tres, ...
                Cpoi, Cdna, Ccell, tag, ...
                wellRnd(1), wellRnd(2), positionTime(1,1),...
                -1, -1, -1, -1, -1, -1, -1, -1, -1});
        newtab.Properties.VariableNames = variable_name;
        if ~exist('processed_database', 'var') 
            processed_database = newtab;
        elseif ~ismember(filepath, processed_database.filepath)
            processed_database = cat(1, processed_database, newtab);
        end
    end
end
processed_database.idx = [1:length(processed_database.idx)]';
writetable(processed_database, output_txt, 'Delimiter','\t');
success = 1;
end