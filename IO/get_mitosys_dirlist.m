function [list_of_dir, dir_id] = get_mitosys_dirlist(expdir, varargin)
% GET_MITOSYS_DIRLIST Read all directories below expdir that match a certain pattern
%   GET_MITOSYS_DIRLIST(expdir) Find directories with default pattern 
%   GET_MITOSYS_DIRLIST(expdir, 'pattern', your_pattern) Find directories with pattern as specified (regex)
%   
% Default pattern is '(?<date>\d+)_(?<poi>.+)_(?<system>.+)', i.e. YYMMDD_poiname_systemname
%
% Author: Antonio Politi, March 2018, EMBL 

%% read parameters
if nargin < 1
    expdir = uigetdir('', 'Select experimental directory');
end

defaultPattern = '(?<date>\d+)_(?<poi>.+)_(?<system>.+)';
p = inputParser;
addRequired(p, 'expdir', @ischar);
addOptional(p, 'pattern', defaultPattern, @ischar);
parse(p, expdir, varargin{:});
par = p.Results;
assert(exist(par.expdir, 'dir') > 0 , 'Experimental directory does not exist');

%% Get all experiments
list_of_dir = getAllFolders(par.expdir, par.pattern, 0);
if isempty(list_of_dir)
    dir_id = {};
    return
end
for i = 1:length(list_of_dir)
    [tmp, dirname{i}] = fileparts(list_of_dir{i});
end
dir_id = regexp(dirname, par.pattern, 'names');
idxs = find(~cellfun(@isempty, dir_id));
dir_id = dir_id(idxs);
list_of_dir = list_of_dir(idxs);
end
