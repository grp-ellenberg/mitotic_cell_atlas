classdef pathgen_mitosys < handle
    %% PATHGEN_MITOSYS Utility class to generate paths for mitosys loading and saving data
    properties (Access = public)
        indir = '';
        imgfile = '180222_NUP96gfpc195_MitoSys2\cell0002_R0001\rawtif\TR1_2_W0001_P0001_T0040.tif'
        pattern = '(?<poi>.+)(\\|/)(.+|)(?<cell>cell\d+_R\d+)(.+|)(\\|/)(?<name>.+T\d+.tif$)';
        pattern_cellID = '(?<date>\d+)_(?<poiname>.+)_Mito(s|S)ys(?<system>\d)(\\|/)(.+|)cell(?<well>\d+)_R(?<round>\d+)(.+|)(\\|/).+P(?<position>\d+)_T\d+.tif$'
        %(?<name>.+T\d+.tif$)'
        tmax = 40;
        id = [];
        imgpaths = {};
        matfiles = {};
    end
    
    properties (Access = private)
        imgfiles = {};
        jpgfiles = {};
        mp4file = [];
        reportfiles = struct('res', '', 'meta', '', 'par', '');
    end
    methods
        function obj = pathgen_mitosys(indir, imgfile, tmax)
            % PATHGEN_MITOSYS(indir, imgfile, tmax) Constructor
            %   indir:   directory of image data
            %   imgfile: example path to image. Full path is indir\imgfile
            %   tmax:    maximal number of time points
            if nargin >= 1
                obj.indir = indir;
            end
            if nargin >= 2
                obj.imgfile = imgfile;
            end
            
            if nargin >= 23
                obj.tmax = tmax;
            end
            % replace \\ with / for system compatibility
            obj.imgfile = strrep(obj.imgfile, '\', '/');
            
            if ~obj.computeid(obj.imgfile)
                error('No id could be generated');
            end
            obj.computefilenames(obj.imgfile);
        end
        
        function [caldir] = calibrationdir(obj)
            % CALIBRATIONDIR returns path to calibration directory
            caldir = fullfile(obj.indir, obj.id.poi, 'Calibration');
        end
        function [calfile] = calibrationfile(obj)
            % CALIBRATIONFILE returns path to calibration file
            calfile = fullfile(obj.calibrationdir, 'calibration.mat');
        end
        
        function id = getcellid(obj, imgfile)
            % COMPUTEID(imgfile) parse imgfile for date_poi_system, cellname, imgname
            assert(ischar(imgfile), 'imgfile must be a string');
            id = regexp(imgfile, obj.pattern_cellID, 'names');
            if ~isempty(id)
                id.system = str2num(id.system);
                id.well = str2num(id.well);
                id.round = str2num(id.round);
                id.position = str2num(id.position);
            else
                id = [];
                return
            end
            id = sprintf('%s_%d_%d_%d_%d', id.date, id.system, id.well, id.round, id.position);

        end
        
        function test_getcellid(obj)
            % TEST_COMPUTEID test the pattern matching
            imgfile = '180222_NUP96gfpc195_MitoSys2\cell0002_R0001\rawtif\TR1_2_W0001_P0001_T0040.tif';
            id = regexp(imgfile, obj.pattern_cellID, 'names');
                        status = 1;
            if isempty(id)
                status = 0;
            end

            if ~strcmp(id.date, '180222')
                fprintf('test compute_cellid failed date\n');
                status = 0;
            end
            if ~strcmp(id.well, '0002')
                fprintf('test compute_cellid failed well\n');
                status = 0;
            end
            if ~strcmp(id.round, '0001')
                fprintf('test compute_cellid failed round\n');
                status = 0;
            end
            if ~strcmp(id.name, 'TR1_2_W0001_P0001_T0040.tif')
                fprintf('test compute_cellid failed image name\n');
                status = 0;
            end
            
            if status
                fprintf('test compute_cellid success\n')
            end
            
        end
        
        function status = computeid(obj, imgfile)
            % COMPUTECELLID(imgfile) parse imgfile for date_poi_system, cellname, imgname
            assert(ischar(imgfile), 'imgfile must be a string');
            obj.id = regexp(imgfile, obj.pattern, 'names');
            
            if isempty(obj.id)
                status = 0;
            else
                status = 1;
            end
        end
        
        function test_computeid(obj)
            % TEST_COMPUTEID test the pattern matching
            imgfile = '180222_NUP96gfpc195_MitoSys2\cell0002_R0001\rawtif\TR1_2_W0001_P0001_T0040.tif';
            id = regexp(imgfile, obj.pattern, 'names');
            status = 1;
            if ~strcmp(id.cell, 'cell0002_R0001')
                fprintf('test ocmputeid failed cell name\n');
                status = 0
            end
            if ~strcmp(id.name, 'TR1_2_W0001_P0001_T0040.tif')
                fprintf('test computeid failed image name\n');
                status = 0
            end
            if status
                fprintf('test computeid success\n');
             end
        end
        function status = computefilenames(obj, imgfile)
            % COMPUTEFILENAMES(imgfile) Compute name of .mat, .jpg files
            if ~obj.computeid(imgfile)
                status = 0;
                warning('Was not able to parse %s\n', imgfile);
                return;
            end
            
            %  the names for every time point
            for t = 1:obj.tmax
                obj.imgfiles{t,1} = regexprep(obj.id.name, 'T\d+', num2str(t,'T%04.f'));
                obj.matfiles{t,1} = regexprep(obj.id.name, 'T\d+.+', num2str(t,'T%04.f.mat'));
                obj.jpgfiles{t,1} = regexprep(obj.id.name, 'T\d+.+', num2str(t,'T%04.f.jpg'));
            end
            
            % three text file can be genreated a resfile containing some or all of the results
            % a metadata file containing metadata of the experiment
            % a parfile containings parameters of the processing
            obj.mp4file = [obj.id.poi '_' obj.id.cell '.mp4'];
            obj.reportfiles.res = [obj.id.cell '_res.txt'];
            obj.reportfiles.meta = [obj.id.cell '_meta.txt'];
            obj.reportfiles.par = [obj.id.cell '_par.txt'];
            
            obj.imgpaths = cellfun(@(x) regexprep(fullfile(obj.indir, imgfile), obj.id.name, x),  obj.imgfiles, 'UniformOutput', false);
        end
        
        function status = verifypath(obj, filepaths)
            % VERIFYPATH(filepaths) Verify if files in cellarray filepaths exist
            status = 0;
            for i = 1:length(filepaths)
                if (~exist(filepaths{i},'file'))
                    return
                end
            end
            status = 1;
        end
        
        function filepaths = getfilepaths(obj, celldir, filenames)
            % GETFILEPATHS(celldir, filenames) concatenate celldir to filenames
            % filenames is a cell array or a struct
            if iscell(filenames)
                filepaths = cellfun(@(x) fullfile(celldir, x),  filenames, 'UniformOutput', false);
            elseif isstruct(filenames)
                filepaths = filenames;
                filepaths = structfun(@(x) fullfile(celldir, x), filepaths, 'UniformOutput', false);
            end
        end
        
        
        function celldir = getcelldir(obj, maindir, subdir)
            % GETCELLDIR(maindir, subdir)
            % combine cell directory name given a maindir and subdir (e.g. Segmentation)
            celldir = fullfile(maindir, obj.id.poi, obj.id.cell, subdir);
        end
        
        function poidir = getpoidir(obj, maindir)
            % GETPOIDIR(maindir)
            % combine poi ID  with maindir
            poidir = fullfile(maindir, obj.id.poi);
        end
        
        function celldir = getcelldir_short(obj, maindir)
            % combine cell directory name given a maindir without subdir (e.g. Segmentation)
            celldir = fullfile(maindir, [obj.id.poi '_' obj.id.cell]);
        end
        
        function filepaths = getmatpaths(obj, maindir, subdir)
            % GETMATPATHS(maindir, subdir) Returns cell array of .mat file paths.
            % to be stored in subdir.
            celldir = obj.getcelldir(maindir, subdir);
            filepaths = obj.getfilepaths(celldir, obj.matfiles);
        end
        
        function filepaths = getjpgpaths(obj, maindir)
            % GETJPGPATHS(maindir) Returns array of jpg files
            celldir = obj.getcelldir_short(maindir);
            filepaths = obj.getfilepaths(celldir, obj.jpgfiles);
        end
        
        function filepath = getmp4path(obj, maindir)
            % GETMP4PATHS(maindir) Returns path to mp4 for a cell
            filepath = fullfile(maindir, obj.mp4file);
        end
        
        function filepaths = getreportpaths(obj, maindir, subdir)
            % GETREPORTPATHS(maindir, subdir) Returns struct with path to report files
            celldir = obj.getcelldir(maindir, subdir);
            filepaths = obj.getfilepaths(celldir, obj.reportfiles);
        end
        
    end
end
