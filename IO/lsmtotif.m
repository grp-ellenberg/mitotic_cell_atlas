function lsmtotif(indir, outdir, varargin)
% LSMTOTIF Converts lsm files found in indir to ome.tif files and save them to outdir
% The files are saved according to Cha_lsm struct. This sets which channel will be the POI, DNA, or Cell 
% channel. i.e. the 1st, 2nd and 3rd channel of the ome.tif.
% LSMTOTIF Script will prompt for input and output directory
% LSMTOTIF(indir, outdir) Specify input and output directory. Use default settings
% LSMTOTIF(indir, outdir, 'Cha_lsm', struc('Cpoi', a, 'Cdna', b, 'Ccell', c)) Specify which channel of
% lsm file to map to POI, DNA and cell.
% LSMTOTIF(indir, outdir, 'Dt', afloat) The Dt to write in the metadata in sec
% LSMTOTIF(indir, outdir, 'scopes', {'microscopename1', 'microscopename2'}) Specify name of microscope for pattern matching.
% LSMTOTIF(indir, outdir, 'force', 0) Force reprocessing
% LSMTOTIF(indir, outdir, 'manual', 0) 0: Search for lsm files named '(TR1_2|TR1)_W0001.+'
% LSMTOTIF(indir, outdir, 'manual', 1) 0: Search for lsm files named '(cell\d+)_DE_2_W0001.+'

% INPUT:
%   indir:  the input directory for the lsm file. This is a directory of an experiment of the type YYMMDD_poiname
%   The subdirectory structure is 
%       microscopename, e.g. MitoSys1|2
%           LSM/...contains the triggered experiment time lapse
%           Result/
%               Calibration/
%       The data must already have been processed with the FCS calibration pipeline and QCd
%   outdir: the output main directory where to move the data. The data will be saved in
%       outdir/YYMMDD_poiname/cellXXXX_RYYYY/rawtif
% OUTPUT:
%   write out converted lsm files matching a pattern to an output directory 

if nargin < 1
   indir = uigetdir('V:\MitoSys_data\Data_U2OS\180517_NUP96gfpc195', 'Specify input directory');
end
if ~indir
    return
end
if nargin < 2
    outdir = uigetdir('W:\Antonio_t2_2\U2OS_MitoSys\Data_tif\test', 'Specify output directory');
end

if ~outdir
    return
end
defaultScopes = {'MitoSys\d', 'LSM880'};
defaultCha_lsm = struct('Cpoi',1, 'Cdna', 3, 'Ccell', 2);
% Default Deltat of time lapse
defaultDt = 90;
% default value if data should be reprocessed
defaultForce = 0; 
% default if data has been acquired without full mitosys pipeline
defaultManual = 0; 

p = inputParser();
validate_struct = @(x) isstruct(x) && all(cellfun(@strcmp, fieldnames(x), {'Cpoi', 'Cdna', 'Ccell'}'));
addRequired(p, 'indir', @ischar);
addRequired(p, 'outdir', @ischar);
addParameter(p, 'exp_database', '', @ischar);
addParameter(p, 'Cha_lsm', defaultCha_lsm, validate_struct);
addParameter(p, 'Dt', defaultDt,  @isfloat);
addParameter(p, 'scopes', defaultScopes, @iscell);
addParameter(p, 'force', defaultForce, @isinteger);
addParameter(p, 'manual', defaultManual, @isinteger);

parse(p, indir, outdir, varargin{:});
par = p.Results;

%% Sanity check for mitosys data structure
% check that indir is conforms to expected directory naming 
for i = 1:length(par.scopes)
    scopefolders = getAllFolders(indir, par.scopes{i});
    if length(scopefolders) >= 1
         break
    end
end

assert(length(scopefolders) >= 1, sprintf('%s\n%s\n%s', 'The input directory does not conform to the Mitosys pipeline convention YYMMDD_poiname/scopename.',...
    'You should choose the directory YYMMDD_poiname', ...
    'Specify in scopes option the name of your microscope'));

for ascopefolder = scopefolders'
    calibfolder = getAllFolders(fullfile(ascopefolder{1}, 'Result'), 'Calibration');
    assert(length(calibfolder) >= 1, 'No FCS-calibration was found in %s', ascopefolder{1});
end

% patterns to identify directories
patterns = {'(DE_2|AQ)_W(?<well>\d+)_P0001_T(?<time>\d+)', ...
    'cell(?<well>\d+)_DE_2_W\d+_P\d+_T\d+'};
allpatterns = {};
for i = 1:length(par.scopes)
    for j = 1:length(patterns)
        allpatterns{length(allpatterns)+1} = sprintf('(?<scope>%s).+%s', par.scopes{i}, patterns{j});
    end
end

% files with triggered images
allfiles = getAllFiles(indir, 'lsm', '(TR1_2|TR1)_W0001.+', 1);

if par.manual 
    allfiles =  getAllFiles(indir, 'lsm', '(cell\d+)_DE_2_W0001.+', 1);
end

fprintf('lsmtotif Processing directory %s.\nWriting data to %s\n', indir, outdir)
for afile = allfiles'
    lsmfile = afile{1};
    %% check if there are 40 time points for the corresponding file
    % check directory name pattern
    for ipatt = 1:length(allpatterns)
        tokenNames = regexp(lsmfile, allpatterns{ipatt}, 'names');
        if length(tokenNames) == 1
            break
        end
    end
    if par.manual 
         tokenNames = regexp(lsmfile, allpatterns{2}, 'names');
         tokenNames.well = sprintf('%04s', tokenNames.well );
         tokenNames.time = sprintf('%04d', 1);   
    end
    
    if length(tokenNames) == 0
        warning('Could not find a matching pattern for %s\n Verify in source code the patterns to recognize the file', afile{1})
        continue
    end
    
    cellname = sprintf('cell%s_R%s', tokenNames.well, tokenNames.time);
    [tmp, dirname] = fileparts(indir);
    
    %% create missing directories and copy files
    outdir_raw = fullfile(outdir, [dirname '_' tokenNames.scope]);
    if ~exist(outdir_raw, 'dir')
        mkdir(outdir_raw);
    end
    
    if ~exist(fullfile(outdir_raw, 'Calibration'), 'dir')
        copyfile(fullfile(indir, tokenNames.scope, 'Result', 'Calibration'), ...
                fullfile(outdir_raw, 'Calibration'));
    end
    slicestart_in  = fullfile(indir, tokenNames.scope, 'Result', 'slicestart.txt');
    slicestart_out = fullfile(outdir_raw, 'slicestart.txt');
    if ~exist(slicestart_out, 'file') 
        if exist(slicestart_in, 'file')
            copyfile(slicestart_in, ...
                    slicestart_out);
        else
            fid = fopen(slicestart_out, 'w');
            fprintf(fid, '1\n');
            fclose(fid);
        end
           
    end
    
    outdir_cell = fullfile(outdir_raw, cellname, 'rawtif');
    if ~exist(outdir_cell, 'dir')
        mkdir(outdir_cell);
    end
    %% convert and save files to destination
    [tmp, fname] = fileparts(lsmfile);
    if par.manual
        fname = regexprep(fname, 'cell(\d+)_DE_2_W\d+_P\d+', 'TR1_2_W0001_P000$1');
    end
    tiffile = fullfile(outdir_cell, [fname '.tif']);
    disp_tiffile = displayname(tiffile, 100);
        
    if ~exist(tiffile, 'file') || par.force
        fprintf('Processing %s\n', disp_tiffile);
        convertlsm2tif(lsmfile, tiffile, par.Cha_lsm, par.Dt);
    else
        fprintf('output file exists %s\n',  disp_tiffile);
    end
end
end

function outname = displayname(name, nchar)
    % reduce size of string to display
    if length(name) > nchar
        outname = ['...' name(end-nchar:end)];
    else
        outname = name;
    end
end

function convertlsm2tif(lsmfile, tiffile, ch, Dt)
% lsmfile: path to lsmfile
% tiffile: path to outputfile
% Dt: time interval in sec
% ch: A struct with index of Cpoi, Cdna and Ccell for the lsm file

[reader, dim] = getBioformatsReader(lsmfile);
% read the stack
poi  = getTPointBioFormats(reader, dim, 1, ch.Cpoi);
nuc  = getTPointBioFormats(reader, dim, 1, ch.Cdna);
neg = getTPointBioFormats(reader, dim, 1, ch.Ccell);

height = size(poi,1);
width = size(poi,2);
depth = size(poi,3);
stack = zeros(height, width, depth, 3, 'uint16');
stack(:,:,:,1) = poi;
stack(:,:,:,2) = nuc;
stack(:,:,:,3) = neg;

%% create metadata
metadata = createMinimalOMEXMLMetadata(stack);
pixelSizeXY = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(1)), ome.units.UNITS.MICROMETER);
pixelSizeZ = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(3)), ome.units.UNITS.MICROMETER);
metadata.setPixelsPhysicalSizeX(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeY(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeZ(pixelSizeZ, 0);
metadata.setChannelName('POI', 0, 0);
metadata.setChannelName('NUC', 0, 1);
metadata.setChannelName('NEG_Dextran', 0, 2);
metadata.setPixelsTimeIncrement(ome.units.quantity.Time(java.lang.Double(Dt), ome.units.UNITS.SECOND), 0);

% this is very slow
%bfsave(stack, files_out.img, 'metadata', metadata);
data = reshape(stack, width, height, 3*depth);
options = struct('overwrite' , true);
saveastiff(data, tiffile, options);
swapomexml(tiffile, metadata)
end

function swapomexml(filename, metadata)
% swap metadata of file tiff file. Write out to tagstruct.ImageDescription
filein = loci.common.RandomAccessInputStream(filename);
fileout = loci.common.RandomAccessOutputStream(filename);
saver = loci.formats.tiff.TiffSaver(fileout, filename);
saver.overwriteComment(filein, metadata.dumpXML);
saver.close();
filein.close();
fileout.close();
end