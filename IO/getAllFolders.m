function [folderList, level] = getAllFolders(dirName, file_pattern_matching, recursive)
% GETALLFOLDERS Recursively finds all folders in a folder and its subfolder
% that match/cotain file_pattern_matching
%     dirName - root directory name
%     file_pattern_matching - a string contained in filename
%     recursive - 1,0. Loop through all subdirectories
% Antonio Politi, EMBL, January 2017 added file_patter_matching

if nargin < 3
    recursive = 1;
end
dirData   = dir(dirName);                                             % Get the data for the current directory
dirIndex  = [dirData.isdir].*(~ismember({dirData.name}, {'.','..'}));   % index of valid subdirectories 
dirData = dirData(find(dirIndex));                                          

dirList = {dirData.name}';  % name of valid subdirectorues
% perform pattern matching
idx = [];
for i=1:length(dirList)
    if ~isempty(regexp(dirList{i}, file_pattern_matching, 'match'))
        idx = [idx;i];
    end
end
folderList = dirList(idx);
if ~isempty(folderList)
    folderList = cellfun(@(x) fullfile(dirName,x),...  %# Prepend path to files
        folderList,'UniformOutput',false);
end

if recursive
    for i = 1:length(dirList)                  %# Loop over valid subdirectories
        nextDir = fullfile(dirName, dirList{i});    %# Get the subdirectory path
        folderList = [folderList; getAllFolders(nextDir, file_pattern_matching)];  %# Recursively call getAllFiles
    end
end
end