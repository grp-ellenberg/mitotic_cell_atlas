# IO 
Directory contains function to read in and out images parse directories to create a list of images to process. 

Read in images using the [bioformat](http://downloads.openmicroscopy.org/bio-formats/5.3.4/) API. Requires bioformat >= 5.3.4 (February 2017) 