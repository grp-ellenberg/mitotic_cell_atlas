function [dx, dy, NzOrig, Nz, Nc, voxelSizeX, voxelSizeY, voxelSizeZ, voxelSizeZ_Iso,  voxelVolume_Iso, zFactor] = get_image_dimensions_bf(imgfile, zinit)
%% GET_IMAGE_DIMESIONS_BF get image-file dimensions using bioformat reader
% INPUT: 
%   * imgfile: fullpath to image file
%   * zinit: set higher than one if some slices will be removed afterwards
% OUTPUT:
%   * dx: Image height in pixels
%   * dy: Image width in pixels
%   * Nzorig: numer of Z stacks
%   * Nz: number of Z stack isotropic sampling
%   * Nc: Numer of channels
%   * voxelSizeX: voxel size in X direction micrometer
%   * voxelSizeY: voxel size in Y direction micrometer
%   * voxelSizeZ: voxel size in Z direction micrometer for isotropic sampling
%   * voxelVolumeIso: Volume isotropic
%   * zFactor: zFactor-1 intermediate slice(s) will be generated

if nargin < 2
    zinit = 1;
end
%% read image using bioformats
[reader, dim] = getBioformatsReader(imgfile);
% extract image dimensions
dx = dim.Nx; %Image height
dy = dim.Ny; % Image width
NzOrig = dim.Nz; %Number of Z slices in lsm file
Nc = dim.Nc; %Number of channels

%Size of voxels in x, y and z in micro meter
voxelSizeX = dim.voxelSize(1);
voxelSizeY =  dim.voxelSize(2);
voxelSizeZ =  dim.voxelSize(3);

zFactor = round(voxelSizeZ/voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZ_Iso = voxelSizeZ/zFactor; % Update voxelSizeZ based on the number of intermediate slices
voxelVolume_Iso = voxelSizeX * voxelSizeY * voxelSizeZ_Iso; %Volume of a voxel in cubic micro meter
Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1; %Number of z slice after intepolation
reader.close();
end