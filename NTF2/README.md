### Unsupervised inferrence of sub-cellular compartments by non-negative tensor factorization.

The code is written in R and stored in the notebook mitotic_cell_atlas.ntf2.Rmd.

To run the analysis:

1. Install R (latest version recommended) from https://www.r-project.org/

2. Install RStudio (latest version recommended) from https://www.rstudio.com/products/rstudio/download/

3. Download the R notebook mitotic_cell_atlas.ntf2.Rmd and the file find_elbow.R into the same directory, for example under ~/mitotic_cell_atlas.

4. Open the notebook in RStudio and select Run > Run All.  
The first code chunk will check if required packages are available and if not install them. In case of problem with package installation, it may be preferable to install the packages manually using the procedure specific to the host system.  
The second code chunk will download the input data file cell_features.txt (from http://www.mitocheck.org/mitotic_cell_atlas/downloads/cell_features.txt) into the data directory defined in the notebook by the variable data_dir (by default this is set to ~/mitotic_cell_atlas/data). If this fails, download the file manually and copy it to the data directory.


The code uses the following R packages:  

  * rTensor
  * NMF
  * psych
  * seriation
  * ggplot2
  * reshape2
  * grid
  * ggpubr
  * igraph
  * Matrix
  * rhdf5 (from Bioconductor, optional, this is only used to export hdf5 files, corresponding code can be commented out)





