function main_and_parameters(opt, opt_seg, opt_mst, opt_mstalign, opt_preproc, opt_poifeature, opt_surfdict)
%% MAIN_AND_PARAMETERS runs segmentation, time alignment, interest point extraction, and classification
% MAIN_AND_PROCESS(opt, opt_seg, opt_mst, opt_mstalign, opt_preproc, opt_poifeature, opt_surfdict) Specify several options
% as defined in option file mitosys_opt.m. The task are performed for all the files found in a defined input directory.
%   INPUT: 
%       opt: general options such as input(experiment), output directories and file names
%       opt_seg: options for the segmentation pipeline
%       opt_mstalign: options for the mitotic standard time alignment
%       opt_proproc: options for preprocessing of the files
%       opt_poifeature: options for the poi feature extraction
%       opt_surfdict: options for the SURF dictionary
%   EXAMPLE:
%       See example in runfiles\run_mitosys.m for calling the function.
%       [opt, opt_seg, opt_mst, opt_mstalign, opt_preproc, opt_poifeature, opt_surfdict] = mitosys_opt();
%       main_and_parameters(opt, opt_seg, opt_mst, opt_mstalign, opt_preproc, opt_poifeature, opt_surfdict);
%
% The input data must conform following format
%   * YYMMDD_poiname_microscopename -- e.g. 170203_gfpBUBR1cM04-A03_MitoSys1. Do not use '_' within each of the elements!
%       * rawtif             -- a directory containig the cell image data
%           * cellXXXX_RYYYY -- data for a cell in ome.tif format named imgname_TZZZZ.lsm. One time point per file. 
%             Each file is a multipage tiff with 3 channels (poi, nucleus, cell) and several planes per channel.
%       * Calibration        -- directoy that constaincalibration coefficient as obtained from FCS-calibration workflow. 
% 
% Author: Yin Cai
% Modified: Julius Hossain, Antonio Z. Politi, EMBL, Heidelberg
% Last modification: March 2018

%% ADD ALL SOURCE CODE PATHS
try
    % remove annoying warning when loading tables
    warning('OFF', 'MATLAB:table:ModifiedAndSavedVarnames')
end
%% Verify licenses
if ~license('test', 'Statistics_Toolbox')
    warning('Statistics and Machine Learning Toolbox is required! Not all steps will be performed.')
end
if ~license('test', 'Image_Toolbox')
    warning('Image Processing Toolbox is required! Not all steps will be performed.')
end

if ~license('test', 'Curve_Fitting_Toolbox')
    warning('Curve Fitting Toolbox is required! Not all steps will be performed.')
end

if ~license('test', 'video_and_image_blockset')
    warning('Computer Vision System Toolbox is required! Not all steps will be performed.')
end
%%
addpath(genpath_loc(fileparts(mfilename('fullpath'))), '-end');

if nargin < 1
    error('No options specified. Use mitosys_opt to specify options')
end

%% INPUT and OUTPUT directories
% Interactive selection of experiment directory
[opt.exp_dir, status] = verifyDirectory(opt.exp_dir, 'Specify data directory', false);
if ~status 
    return;
end

[opt.out_dir, status] = verifyDirectory(opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

% Sanity check
if strcmp(opt.out_dir, opt.exp_dir)
    error('Root directory of data and output directory for the results must be different')
end

[opt.mst_dir, status] = verifyDirectory(opt.mst_dir, 'Specify/create directory for MST model', true);
if ~status
    return
end

[opt.surfdict_dir, status] = verifyDirectory(opt.surfdict_dir, 'Specify/create directory for SURF IP dictionary', true);
if ~status
    return
end


% Check whether the required localization definition existis
if ~exist(fullfile(opt.exp_dir, 'POI_annotation_database.txt'),'file')
    error('Please define an annotation file POI_annotation_database.txt.');
end

% set output directory for Isotropic sampling data. This has name of out_dir with _Iso appended
opt.out_dir = regexprep(opt.out_dir, '(\\+|/+)$', ''); 
opt.out_dir_iso = [opt.out_dir '_Iso'];
opt.out_dir_vis = [opt.out_dir '_SegVis'];


fprintf('Root directory of data is %s\n', opt.exp_dir);

%% DATABASE generation
% The analysis pipeline monitors the processings via a database file
% generated as ./exp_dir/full_database.txt. It includes the information
% whether a cell has been procesed through a particular step and whether
% the processing quality passes the controls.

% automatically create a backup copy
db_filename = fullfile(opt.out_dir, opt.exp_database);
if exist(db_filename , 'file') && opt.db_update
    db_process = questdlg(sprintf('File %s exists!\nYes: Update file (old file will be backed up).\nNo: Proceed without update.',db_filename), ...
        sprintf('Experiments database %s.txt file', opt.exp_database));
    % create alternative file
    switch db_process
        case 'Cancel'
            return
        case 'Yes'
            FileInfo = dir(db_filename);
            [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
            [path, name, ext] = fileparts(db_filename);
            copyfile(db_filename, fullfile(path, sprintf('%s_%02d%02d%02d.txt', name, Y-2000, M, D)));
            opt.db_update = 1;
        case 'No'
            FileInfo = dir(db_filename);
            [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
            [path, name, ext] = fileparts(db_filename);
            if ~exist(fullfile(path, sprintf('%s_%02d%02d%02d.txt', name, Y-2000, M, D)), 'file')
                copyfile(db_filename, fullfile(path, sprintf('%s_%02d%02d%02d.txt', name, Y-2000, M, D)));
            end
            opt.db_update = 0;           
    end
end

display('Database generation/loading start')
if ~exist(db_filename , 'file') || opt.db_update 
    [status, opt] = gen_database(opt);
    if(~status) % do not proceed if no database has been generated
        return
    end
end
display('Database generation/loading is done')

%% Check if some QC has been performed
tab = readtable(db_filename, 'Delimiter','\t');

if all(tab.manualQC == -1)
    errordlg('No manual QC has been performed. Open full_database.txt file and change the values in the column manualQC accordingly!')
    return
end
if (any(tab.manualQC==-1))
    fprintf('Manual QC has not been performed for some of the cells. Change in table %s the manualQC parameter from -1 to 0 (failed) or 1 (passed)!', ...
        db_filename);
end
if (all(tab.manualQC==0))
    fprintf('No cell passes the manual QC. Nothing to do')
    return
end

%% CELLS to process
% Currently this is only used for segmentation part
numCells = size(tab,1);
visitCells = [1:numCells];
if ~isempty(opt.cellIdx)
    if iscolumn(opt.cellIdx)
        opt.cellIdx = opt.cellIdx';
    end
    if ~all(ismember(opt.cellIdx, visitCells))
        warning('Some indexes of cellIdx option are not in table. Process the complete data set'); 
        opt.cellIdx = visitCells;
    end
else
    opt.cellIdx = visitCells;
end

%% SEGMENTATION
% Please consider possibilities: just process unprocessed data or force
% re-processing. You can check one of the following code as example.
% QC: in addition to the QC you have implemented, dirProcStat.txt 1/2 are
% both valid cells where in other cases, QC = 0;

disp('Segmentation start');
ls_segment_landmarks(opt, opt_seg);
disp('Segmentation is done');


%% Visualize segmentation results
disp('Segmentation visualization start')
visualize_landmarks_3D(opt);
disp('Segmentation visualization done')

%% EXTRACTION OF LANDMARK'S FEATURES
% this will create a merged copy (all time points) of 3D segmentation features in result folder for correspoding cell
disp('Shape feature start');
LM_shape_feature(opt);
disp('shape feature done')

%% GENERATION OF MITOTIC STANDARD TIME MODEL (OPTIONAL)
% This step is not essential if a model has already been trained. However, if
% you would like to regenerate a model, you can run this function.

if opt.mst_reprocess
    disp('Creation of MST model start')
    % update mst directory if required
    opt.mst_dir = mst_setup(opt, opt_mst);   
    generate_MST(opt, opt_mst);
    mstmodel_file = fullfile(opt.mst_dir, opt.mst_fname)
    disp('Creation of MST model is done')
else
    mstmodel_file = fullfile(opt.mst_dir, opt.mst_fname);
    if ~exist(mstmodel_file, 'file') || ~exist(opt.mst_dir, 'dir')
        [opt.mst_fname, opt.mst_dir] = uigetfile(opt.mst_dir, 'Select the file for the MST model');
        if opt.mst_fname
            mstmodel_file = fullfile(opt.mst_dir, opt.mst_fname);
        end
    end
    % check if model is MST model
    tmp_mstmodel = load(mstmodel_file);
    %check for some entries 
    assert(all(isfield(tmp_mstmodel, {'uuid', 'timeline', 'feat_normalized'})), sprintf('File %s is not a MST model file', mstmodel_file))
        
    disp('Loading of MST model is done')
end

%% TEMPORAL ALIGNMENT
% The image sequence will be aligned to the mitotic standard time model
% generated before using this step. This will create the MST_timeindex and
% MST_annotation. MST_annotation is used for further computations
%disp('successfully run');
%return;
disp('Alignment to MST start')
annotate_time(mstmodel_file, opt, opt_mstalign);
disp('Alignment to MST is done')

%% PREPROCESS POI CHANNEL
% Basic pre-processing for the POI channel including smoothing and
% background subtraction and FCS calibration . Yields a file that contains
% proc_poi matrix
disp('Preprocessing of POI start')
status = preprocess_poi(opt, opt_preproc);
if ~status
    return
end
disp('Preprocessing of POI is done')

% FEATURE EXTRACTION POI CHANNEL
% This step extracts glabal feature (fraction POin in chromosomal volume,
% accumulation factor of POIn in the predicted spindle volume) and SURF
% interest points out of each image. For all extracted SURF interest points
% using a standard SURF detector, a filtering step and a self-developed
% descriptor are performed to select and featureing the SURF interest
% points.
disp('POI surf feature extraction start')
poi_feature_extraction(opt, opt_poifeature);
disp('POI surf feature extraction is done')

%% GENERATION OF SURF INTEREST POINT DICTIONARY (OPTIONAL)
% This step is used to generate a SURF interest point dictionary by taking
% a subset of the images and their IPs and perform decision tree and KNN or
% DBSCAN clustering in order to define characteristic classes within all
% possible interest points within the data. The dictionary can be reused if
% new data are added to the existing set.

if opt.surfdict_reprocess == 1
    opt.surfdict_dir = uigetdir(opt.surfdict_dir,'Select the saving directory for the SURF dictionary');
    if ~opt.surfdict_dir
        return
    end
    surfdict_filename = fullfile(opt.surfdict_dir, opt.surfdict_fname);
    % dictionary file exists
    if exist(surfdict_filename, 'file')
        surfdict_process = questdlg(sprintf('File Dictionary %s exists!\n Press Yes if you want to update! (Old file will be backed up.)\nPress No if you want to proceed without update',surfdict_filename));
        switch surfdict_process
            case 'Cancel'
                return
            case 'Yes'
                FileInfo = dir(surfdict_filename);
                [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
                [path, name, ext] = fileparts(surfdict_filename);
                copyfile(surfdict_filename, fullfile(path, sprintf('%s_%02d%02d%02d.mat', name, Y-2000, M, D)));
            case 'No'
                return;
        end
    end
    generate_SURF_dictionary(opt, opt_surfdict);
    disp('SURF dictionary generation is done')
else
    if ~exist(fullfile(opt.surfdict_dir, opt.surfdict_fname), 'file') || isempty(opt.surfdict_dir)
        [opt.surfdict_fname, opt.surfdict_dir] = uigetfile({'dictionary*.mat','(dictionray*.mat) Dictionary files';...
          '*.mat','Mat Files'; '*.*', 'All Files'},'Select the SURF dictionary mat file',  opt.surfdict_dir);
    end
    
end

%% ASSIGN SURF INTEREST POINTS INTO BAG OF WORDS FEATURES
% This step assign for all images the extracted SURF interest points into
% pre-defined SURF IP dictionary and perform a correlation dependent
% smoothing step for the feature vectors over time.
disp('Start IPs annotation')
mstmodel_file = fullfile(opt.mst_dir, opt.mst_fname);
dictionary_file = fullfile(opt.surfdict_dir, opt.surfdict_fname);
if ~strcmp(opt.surfdict_fname, sprintf('dictionary_%s_%s.mat', opt.poifeature_dir, opt_surfdict.dict_method))
    warning('Expected dictionary name matching the SURF features is dictionary_%s_%s.mat!\nThe results may not be consistent', opt.poifeature_dir, opt_surfdict.dict_method)
end
annotate_IPs(opt, opt_surfdict);
disp('IPs annotation is done')

%% GENERATION OF DATA FEATURE MATRIX
% This step generate a txt file including the feature vectors for all cells
% afer all QC steps in order to make further analysis more flexible and
% easy. This is used by JK (unsmoothed for JK)

% Parameter: which feature used
disp('Start generation of data feature matrix')
BoW_smooth = 0; % 1: using smoothed SURF feature; 0: using un-smoothed SURF IP feature
global_f = 1; % 1: using global fature as well; 0: only use the SURF features
summarize_featurevec(opt,  BoW_smooth, global_f);
% 
% BoW_smooth = 1; % 1: using smoothed SURF feature; 0: using un-smoothed SURF IP feature
% global_f = 1; % 1: using global fature as well; 0: only use the SURF features
% summarize_featurevec(opt,  BoW_smooth, global_f);
% disp('Creation of data feature matrix is done')

end
