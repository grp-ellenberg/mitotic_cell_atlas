function project_image(celldir, savedir, mitotime, varargin)
% PROJECT_IMAGE create projected 2D images of 3D stack from
% poi, chromatin, and external dye signal
%   PROJECT_IMAGE(celldir, savedir) - read images in celldir and save output in savedir
%   PROJECT_IMAGE(celldir, savedir, 'option', optionval) -
%   read images in celldir and save output in savedir. specify some dafult
%   optional values

% OPTIONS:
%   segdir  - the sub-directory celldir/Preprocessing/segdir that
%             contains segmentation. Default 'Segmentation'
%   poidir  - the sub-directory celldir/Preprocessing/poidir that
%             contains pre-processed POI channel in number of N per voxel. Default 'Registration'
%   addname - an additional prefix for the filename. e.g. genename. Default ''
%   timevec - a vector of all timepoints (in frame) which should be projected. Default [1:40]
%   poiproj - type of Z-projection to perform.
%             meanN, meanC, meanC: mean Z-projection of N, Concentration in nM, and intensity
%             maxN, maxC, maxI: max Z-projection of N, Concentration in nM, and intensity
%             for the SURF extraction an image identical to maxN has been used.
%             for plots of concentrations meanC has been used.
%   fact    - factor to multiply protein numbers when using 16 bit representation. 
%             This allows to visualize also decimal quantities. Default is 1.
% This function use the pre-processing output the images (segmentation and
% POI pre-processing) and project the image in z in the way that both
% landmarks can be clearly seen and a smoothed averaged projection towards 
% z of the POI in the cell of interest are saved separately as tif images.
% The user can later use softwares such as FiJi to generate RGB images
% showing the merged image in a most natural way. Example of the output can
% be seen in the Figure 1 of the manuscript Cai et al:
% An experimental and computational framework to build a dynamic protein 
% atlas of human cell division. (2016)
% The saving is done so to create a movie of the cell division for the
% different cell stages according to the mitotic standard phases.
% Author: Yin Cai, modified Antonio Politi (arguments passing and optional
% different projections), save as bioformat tiff
% Last modification date: 24.11.2017

%% default parameter values
p = inputParser;
defaultTimevec = [1:40];
defaultSegdir = 'Segmentation';
defaultPoidir = 'Registration';
defaultAddname = '';
defaultPoiproj = 'meanN';
expectedPoiproj = {'meanN', 'meanC', 'meanI',  'maxN',  'maxC', 'maxI', 'sumN'};
defaultFact = 1; 

%% prompt for missing input arguments
if nargin < 1
    celldir = uigetdir('Select directory containing Result for a cell');
    if celldir == 0
        return
    end
end

if nargin < 2
    savedir = uigetdir('Select directory to save the results');
    if savedir == 0
        return
    end
end

%% parse parameter inputs and check for consistency
addRequired(p, 'celldir', @ischar);
addRequired(p, 'savedir', @ischar);
addParameter(p, 'timevec', defaultTimevec, @isnumeric);
addParameter(p, 'segdir', defaultSegdir, @ischar);
addParameter(p, 'poidir', defaultPoidir, @ischar);
addParameter(p, 'addname', defaultAddname, @ischar);
addParameter(p, 'poiproj', defaultPoiproj,  @(x) any(validatestring(x,expectedPoiproj)));
addParameter(p, 'fact', defaultFact, @isnumeric); 
parse(p,celldir, savedir, varargin{:});
par = p.Results;

%% some default values for the directories
seg_dir = fullfile(celldir, 'Preprocessing', par.segdir);
reg_dir = fullfile(celldir, 'Preprocessing', par.poidir);
cell_seq = dir(fullfile(reg_dir, '*T0*.mat'));

savepoi = fullfile(savedir, [par.addname '.ome.tif']);
savemitotime =  fullfile(savedir, [par.addname '_mitotime.txt']);
% blank image when a time point is missing
reg = load(fullfile(reg_dir, cell_seq(1).name), 'proc_poi');
blank = mean(reg.proc_poi,3)*0;

if exist(reg_dir, 'dir')
    outimage = zeros(size(reg.proc_poi,2), size(reg.proc_poi,1), 1, 3, length(par.timevec), 'uint16');
    
    % write txt file with time infos
    fid = fopen(savemitotime, 'w');
    fprintf(fid, '%s\t%s\t%s\r\n', 'frame', 'MST_stage', 'MST_time');
    for tdx = 1:length(par.timevec)
        fprintf(fid,'%d\t%d\t%d\r\n', mitotime(par.timevec(tdx),1),  mitotime(par.timevec(tdx),2), mitotime(par.timevec(tdx),3));
    end
    fclose(fid);
    
    for tdx = 1:length(par.timevec)
        % create matrix to store ome image data
        if isnan(par.timevec(tdx))
            % load first image and set to 0
            idx = par.timevec(tdx);
            avepoi = blank;
            neg = blank;
            h2b = blank;
        else 
            idx = par.timevec(tdx);
            reg = load(fullfile(reg_dir, cell_seq(idx).name), 'proc_poi', 'proc_nuc', 'background_488_N_voxel', 'calibration_factor_N_voxel');
            seg = load(fullfile(seg_dir, cell_seq(idx).name), 'chrCentMic', 'poi', 'cellVolume', 'neg', 'isectPointsPix', 'cellVolume');
            chrcent = round(mean(seg.chrCentMic(3,1:end)));
            zrange = [chrcent-3, chrcent+3];
            zrange(zrange<1) = 1;
            zrange(zrange>31) = 31; 
            zrange =[1:31];
            switch par.poiproj 
                case 'meanN'
                    avepoi = mean(reg.proc_poi(:,:,zrange), 3)*par.fact; % projection is mean projection and contrasting. 
                case 'maxN'
                    avepoi = max(reg.proc_poi,[], 3)*par.fact;
                case 'sumN'
                    avepoi = sum(reg.proc_poi,[], 3)*par.fact;
                case 'maxC'
                    avepoi = max(reg.proc_poi, [], 3)*par.fact/(0.2516^2*0.75)/0.6023;
                case 'maxI'
                    % this is kernel [3,3,1] and sd 0.65
                    avepoi = max(smooth3(seg.poi, 'gaussian', [3, 3,1]).*seg.cellVolume, [], 3);          
                case 'meanC'
                    avepoi = mean(reg.proc_poi(:,:,zrange), 3)*par.fact/(0.2516^2*0.75)/0.6023;
                case 'meanI'
                    avepoi = mean(smooth3(seg.poi(:,:,zrange), 'gaussian', [3, 3,1]).*seg.cellVolume(:,:,zrange),  3);          
            end
            % For feature extraction max projection is used
            neg = mean(seg.neg(:,:,min(seg.isectPointsPix(:,3)):max(seg.isectPointsPix(:,3))),3);
            h2b = reg.proc_nuc;
            h2b = max(h2b,[],3);
            outimage(:,:, 1, 1, tdx) = avepoi;
            outimage(:,:, 1, 2, tdx) = h2b;
            outimage(:,:, 1, 3, tdx) = neg;
        end
    end
    metadata = createMinimalOMEXMLMetadata(outimage);
    pixelSize = ome.units.quantity.Length(java.lang.Double(.251), ome.units.UNITS.MICROMETER);
    %metadata.setChannelColor(ome.xml.model.primitives.Color(java.lang.Integer(1)), 1, 0)
    metadata.setPixelsPhysicalSizeX(pixelSize, 0);
    metadata.setPixelsPhysicalSizeY(pixelSize, 0);
    bfsave(outimage, savepoi, 'metadata', metadata);
else
    error('pre-processing for the cell of interest not finished')
end
end