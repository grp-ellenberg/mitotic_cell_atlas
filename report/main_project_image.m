function main_project_image(cellOI, varargin)
% MAIN_PROJECT_IMAGE
% this creates the projected image for figure1 c. We use average projection
%   cellOI - is a cell array containing the path to cells that need to be processed.
%             default is empty -> process all cells.
%           e.g. cellOI = {'140506_NEDD1\MitoSys1\Result\cell0013_R0010'};
% OPTIONS (see also project_image):
%   timevec - a vector of time points to process
%   poiproj - type of projection. 
%             meanN, meanC, meanC: mean Z-projection of N, Concentration in nM, and intensity
%             maxN, maxC, maxI: max Z-projection of N, Concentration in nM, and intensity
%             for the SURF extraction an image identical to maxN has been used.
%             for plots of concentrations meanC has been used. 
%   fact    -
% Author: Antonio Politi, EMBL, November 2017

if nargin < 1
    cellOI = {'140506_NEDD1\MitoSys1\Result\cell0013_R0010'};
end

%% read default parameter
% read in parameters (see also project_image)
p = inputParser;
defaultTimevec = [1:40];
defaultPoiproj = 'meanI';
expectedPoiproj = {'meanN', 'meanC', 'meanI',  'maxN',  'maxC', 'maxI', 'sumN'};
defaultFact = 1; % use 100 is referring to protein numbers. This avoids cutting the number < 1
addParameter(p, 'timevec', defaultTimevec, @isnumeric);
addParameter(p, 'poiproj', defaultPoiproj,  @(x) any(validatestring(x,expectedPoiproj)));
addParameter(p, 'fact', defaultFact, @isnumeric);
parse(p, varargin{:});
par = p.Results;

mitosysdir = fileparts(fileparts(mfilename('fullpath')));
% add path
addpath(genpath_loc(mitosysdir), '-end')
% add java library

% LOAD DATABASE
[database, exp_dir] = uigetfile({'*.txt', 'databse file (*.txt)'},'Specify database file', ...
    'V:\MitoSys_data\Data\full_database.txt');

if database == 0
    return
end


% get outputdir
outdir = uigetdir( 'V:\MitoSys_data\Results\Cell_Vis', ...
    'Specify output folder');

exp_database = fullfile(exp_dir,database);
summary_database = readtable(exp_database,'Delimiter','\t');

%restrict cells to process to cells that pass all
summary_database = summary_database(1:size(summary_database,1), :);
vec_QC =  (summary_database.manualQC > 0).*(summary_database.time_alignment > 0).*(summary_database.feature_vector > 0).*(summary_database.segmentation > 0);
vec_idx = (1:length(summary_database.feature_vector))';
vec_idx = vec_idx(vec_QC>0);

cell_dirlist = summary_database.filepath(vec_idx);
cell_total = length(cell_dirlist);

% the cell of interest used for Figure1 is
if ~isempty(cellOI)
    idx = [];
    for i = 1:length(cellOI)
        tmp = find(strcmp(cell_dirlist, cellOI{i}));
        assert(~isempty(tmp), 'cellOI %s has not been found or did not pass all quality controls', cellOI{1})
        idx = [idx, tmp];
    end
    cell_dirlist = cell_dirlist(idx);
    cell_total = length(cell_dirlist);
end

%% Proces all cells
for icell = 1:cell_total
    % read the MST stage
    timeidx = load(fullfile(exp_dir, cell_dirlist{icell}, 'Temporal_Align', 'MST_timeindex.mat'));
    % read MST in min
    timeidx2 = load(fullfile(exp_dir, cell_dirlist{icell}, 'Temporal_Align', 'MST_annotation.mat'));

    timevec = frameMitotime(timeidx.mitotime);
    
    if ~isempty(timevec) % process cells that do have stages
        strA = strsplit(cell_dirlist{icell}, '\');
        outputdir = fullfile(outdir, getoutname(par.poiproj), strjoin({strA{1}, strA{2}},'_'));
        if ~exist(outputdir)
            mkdir(outputdir);
        end
        fprintf('project_image Processing %s\n', cell_dirlist{icell})
        % skip if file already exists
        % if exist(fullfile(outputdir{idir}, [strA{4} '.ome.tif']))
        %    continue
        % end
        project_image(fullfile(exp_dir, cell_dirlist{icell}),...
            outputdir, [timeidx.mitotime timeidx2.mitotime(:,2)],  'addname',  strA{4}, ...
            'timevec', par.timevec, 'poiproj', par.poiproj, 'fact', par.fact);    
    end
 
end
end

function [outname] = getoutname(poiproj)
% GETOUTNAME returns name of folder to be used for output
switch poiproj
    case 'meanN'
        outname = 'meanProjN';
    case 'maxN'
        outname ='maxProjN';
    case 'sumN'
        outname = 'sumProjN';
    case 'maxC'
        outname = 'maxProjC';
    case 'maxI'
        outname = 'maxProjI';
    case 'meanC'
        outname = 'meanProjC';
    case 'meanI'
        outname = 'meanProjI';
end
end


function frame = frameMitotime(mitotime)
% FRAMEMITOTIME return frames that correspond to mitotic time.
% stages that do not exist are skipped
%     if length(unique(mitotime(:,2))) < 15
%         frame = [];
%         return;
%     end
for i = 1:20
    frame(i) = round(mean(find(mitotime(:,2) == i)));
end
end