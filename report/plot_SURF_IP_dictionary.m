function plot_SURF_IP_dictionary(dictionary_dir,surfdir,method,savedir,exp_dir)

% Author: Yin Cai
% Date: 28.06.2016

% This is a functio to plot the feature vectors and the cropped images of
% 84 randomly sellected IPs for each dictionary cluster. It is used for
% checking the quality of the dictionary during the training stage.

% dictionary_dir: the directory saved the dictionary
% surfdir: the directory name saving the poi_feature_extraction results
% method: 'dbscan' or 'kmeans'
% savedir: saving the output

%% Load the dictionary
dictfile = fullfile(dictionary_dir,['dictionary_' surfdir '_' method '.mat']);
load(dictfile);

%% Construct the index vector as [cell index, IP index, 1st level clustering, 2nd level clustering, random index, vector index]
ipidx_vec = [];
for cidx = 1:size(selcellidx,1);
    vec = cat(2,repmat(selcellidx(cidx,1),[selcellidx(cidx,2) 1]),(1:selcellidx(cidx,2))');
    ipidx_vec = cat(1,ipidx_vec,vec);
end

randvec = rand(length(ipidx_vec),1);
mergevec = [ipidx_vec devcl devcl2 randvec (1:length(ipidx_vec))'];

%% For each IP cluster, plot 84 randomly selected IPs

total_class = size(index_centroids,1);
for cl = 1:total_class;
    i = index_centroids(cl,1); % 1st level clustering index
    j = index_centroids(cl,2); % 2nd level clustering index

    ip_in_class = mergevec(((mergevec(:,3)==i).*(mergevec(:,4)==j))>0,:); % crop the index vector for IP in class
    if size(ip_in_class,1) > 84; % random selection of 84 IPs
        ip_in_class = sortrows(ip_in_class,5);
        ip_in_class = ip_in_class(1:84,:);
    end
    number_ip_in_class = size(ip_in_class,1);
    
    feature_heatmap = []; % initialize the feature matrix for selected IPs
    for idx = 1:number_ip_in_class; % Load and plot IP by IP
        
        cellpath = fullfile(exp_dir,cluster_filepaths{ip_in_class(idx,1)});
        if ~exist(cellpath,'file');
            error('Please map the exp_dir to the same directory as training the dictionary')
        else
            M = load(cellpath);
            
            maxProj = imresize(M.maxProj_low,M.feature_parameters.factor);
            
            % Index of the flag
            flagvec = (1:length(M.flag))';
            flagvec = flagvec(M.flag>0);
            plot_ipidx = flagvec(ip_in_class(idx,2)); % index of the IP in the cell
            
            % Crop the IP from the image
            s = 6*M.InterestPoints(plot_ipidx).Scale;
            rs = round(s);
            I = maxProj(M.loc(plot_ipidx,2)+(-rs:rs),M.loc(plot_ipidx,1)+(-rs:rs));
            figure(1)
            subplot(7,12,idx)
            imshow(I)
            path = strsplit(cellpath,'\MitoSys','CollapseDelimiters',true);
            path = strsplit(path{1},'\','CollapseDelimiters',true);
            title(path{end-1})
            
            % Cat the feature vector
            feature_heatmap = cat(1,feature_heatmap,FeatMat(ip_in_class(idx,6),:));
        end
    end
    
    % Output
    figname = fullfile(savedir,['Dictionary_class_' num2str(cl,'%02.f') '_' num2str(i,'%02.f') '_' num2str(j,'%02.f') '.fig']);
    savefig(1,figname)
    close(gcf)
    close all
    
    savefile = fullfile(savedir,['Dictionary_class_' num2str(cl,'%02.f') '_' num2str(i,'%02.f') '_' num2str(j,'%02.f') '.mat']);
    save(savefile,'feature_heatmap')
    feature_heatmap(feature_heatmap>0.5) = 0.5;
    feature_heatmap = feature_heatmap*2;
    savefile = fullfile(savedir,['Dictionary_class_' num2str(cl,'%02.f') '_' num2str(i,'%02.f') '_' num2str(j,'%02.f') '.tif']);
    imwrite(feature_heatmap,savefile)
end
end
    
    
    