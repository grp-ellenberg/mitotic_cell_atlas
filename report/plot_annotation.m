function plot_annotation(fullname,poilist,feature_summary,annotation_file,timevec,featurevec,savedir)

% Author: Yin Cai
% Date: 28.06.2016

% This function can be used to generate figures illustrating statistics of
% the BoW features and the supervised pattern annotation results in the
% registered time. It allows the generation of following graphics:

% 1. bar plot of the averaged BoW feature values with standard deviation
% for each protein each mitotic standard stage (similar Figure 5a)
% 2. area plot of the averaged annotated pattern profile for each protein
% in mitotic standard time (as Figure S13)
% 3. single cell annotation for each pattern over time for each protein
% 4. averaged kinetics for each pattern and many protins (Figure 5b)

% fullname: 0/1, whether fullname of the poi will be used for grouping
% poilist: {'p1','p2'...}; list of protein of interest need to be plotted. Use the corresponding fullname or shortname
% feature_summary: the summary txt file of the feature
% annotation_file: the mat file of the supervised pattern annotation
% timevec: selected mitotic standard stage to plot the feature value
% featurevec: selected feature index to be plotted in barplot
% savedir: saving directory for the figures.

%% Load the summary feature table, the annotation and the time model for the timeline
Data = readtable(feature_summary,'Delimiter','\t');
load(annotation_file);
load(mst_model);
timeline = timeline((outputtime(1)):(outputtime(length(outputtime))));
timeline = timeline * 1.5; %Added by Julius
cm = colormap(jet(length(poilist)));

%% Check whether the processing was correctly done for the plotting
if fullname == useshort;
    error('please annotate the data and plot the data with the same protein name')
end

if min(ismember(timevec,timeclass))==0;
    error('not all mitotic stages to be plotted are processed')
end

if min(ismember(poilist,poi_all)) == 0;
    error('not all proteins to be plotted are processed')
end

switch fullname
    case 1
        poiname = Data.fullname;
    case 0
        poiname = Data.poi;
end

%% For each protein
for i = 1:length(poilist);
    pidx = strcmp(poiname,poilist{i});
    meanmat = [];
    stdmat = [];
    % Calculate the mean and std of the feature vectors for selected time
    % points
    for j = 1:length(timevec);
        tidx = pidx.*(Data.time_2==timevec(j));
        Q = table2array(Data(tidx>0,9:end));
        fdx = ismember(1:size(Q,2),featurevec);
        Q = Q(:,fdx>0);
        meanmat = cat(2,meanmat,(mean(Q,1))');
        stdmat = cat(2,stdmat,(std(Q,[],1))');
    end
    % If only one feature is selected, bar plot for this feature over time.
    % Else, generate for each time point a bar plot
    if size(meanmat,1) == 1;
        meanmat = meanmat';
        stdmat = stdmat';
        figure(1)
        bar(timevec,meanmat,'y');
        errorbar(timevec,meanmat,stdmat,'k.');
        xlabel('Mitotic Standard Stage')
        ylabel('Fraction of intensities')
        figname = fullfile(savedir,[poilist{i} '_BoW_feature_' num2str(featurevec(1),'%02.f') '.fig']);
        savefig(1,figname)
        close(gcf)
    else
        for j = 1:length(timevec);
            figure(1)
            bar(featurevec,meanmat(:,j),'y');
            errorbar(featurevec,meanmat(:,j),stdmat(:,j),'k.');
            xlabel('BoW SURF IP cluster')
            ylabel('Fraction of intensities')
            figname = fullfile(savedir,[poilist{i} '_BoW_feature_MSS_' num2str(timevec(j),'%02.f') '.fig']);
            savefig(1,figname)
            figure(1)
            close(gcf)
        end
    end
    
    pidx = find(ismember(poi_all,poilist{i}));
    pidx = pidx(1);
    
    % Area plot for this protein's localization profile
    figure(2)
    subplot(3,ceil(length(poilist)/3),i);
    area((timeline)',Predict_Ave(:,:,pidx))
    title(poilist{i})
    xlabel('Mitotic Standard Time [min]') %Added by Julius
    ylabel('Pred. num. molecules') %Added by Julius
    
    for j = 1:length(pattern_name)
        % for each pattern, fraction of this poi at single cell level over
        % time
        figure(3)
        subplot(2,ceil(length(pattern_name)/2),j)
        hold on
        for k = 1:size(poi_distribute_patterns_MST{pidx},3);
            plot((timeline)',poi_distribute_patterns_MST{pidx}(:,j,k),'-')
        end
        title(pattern_name{j})
        xlabel('Mitotic Standard Time [min]')
        ylabel('Number of molecules')
        
        % for each pattern, the mean fraction over time of this protein
        figure(4)
        subplot(2,ceil(length(pattern_name)/2),j)
        hold on
        plot((timeline)',Predict_Ave(:,j,pidx),'LineStyle','-','Color',cm(i,:))
        title(pattern_name{j})
        xlabel('Mitotic Standard Time [min]')
        ylabel('Number of molecules')
    end
    figname = fullfile(savedir,[poilist{i} '_single_cell_annotation.fig']);
    savefig(3,figname)
    figure(3)
    close(gcf)
end
figure(2)
legend(pattern_name) %Added by Julius
figname = fullfile(savedir,'Localization_profile.fig');
savefig(2,figname)
figure(2)
close(gcf)

figure(4)
legend(poilist)
figname = fullfile(savedir,'Kinetics_annotated.fig');
savefig(4,figname)
figure(4)
close(gcf)

end