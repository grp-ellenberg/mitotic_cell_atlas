function summarize_featurevec(gen_opt, BoW_smooth, global_features)
%% SUMMARIZE_FEATUREVEC write to file for each cell and time point the fraction of POI SURF IP in a bag of word
% INPUT
%   gen_opt    - struct with path options
%   BoW_smooth - generate an in time smoothed features (not uised anymore)
%   global_features   - 1: using global fature as well; 0: only use the SURF features
% OUTPUT
%   A text file featuretxt containing the features per protein and time point
gen_opt_default = struct( ...
    'exp_dir', '', ...  % data directory
    'out_dir',  '',...  % directory to store the results
    'cellIdx', [1], ... % Cell to process
    'poi_metadata', 'POI_annotation_database.txt', ...
    'exp_database', 'full_database.txt', ...  % filename for experiment database text file (tab delimited file)
    'segmentation_dir', 'Segmentation', ...   % Segmentation directory
    'preproc_dir' ,'Registration', ...        % preprocess and registration directory
    'annotate_dir', 'Features_Poi_1709_dbscan', ... % Annotation directory
    'annotation_summary_file', 'Summary_features_Feature_Poi_dbscan.txt', ... % name of file to save
    'annotation_summary_reprocess', 1); % reprocess if 1 , pass if -2


if nargin < 1
    gen_opt = gen_opt_default;
end
if nargin < 2
    BoW_smooth = 0;
end

if nargin < 3
    global_features = 1;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if gen_opt.annotation_summary_reprocess == -2
    return
end



[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end

[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end


if BoW_smooth
    [tmp, fname, ext] = fileparts(gen_opt.annotation_summary_file);
    featuretxt = fullfile(gen_opt.out_dir, sprintf('%s_smooth%s', fname, ext));
else
    featuretxt = fullfile(gen_opt.out_dir, gen_opt.annotation_summary_file);
end
% process the whole data set if no file exists
if ~exist(featuretxt)
    gen_opt.annotation_summary_reprocess = 1;
end



%% Load database files
POI_metadata = readtable(fullfile(gen_opt.exp_dir, gen_opt.poi_metadata), 'Delimiter','\t');
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
proc_history = (summary_database.manualQC > 0).*(summary_database.feature_vector > 0).*...
    (summary_database.time_alignment > 0).*(summary_database.segmentation > 0).*(summary_database.fcs_calibration > 0);
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);


cell_dirlist = summary_database.filepath(proc_history > 0);
poi_list = summary_database.poi(proc_history > 0);
tmaxlist = summary_database.tmax(proc_history > 0);
cell_total = length(cell_dirlist);

%% Check formats and load old table
if ~gen_opt.annotation_summary_reprocess
    features_table_old = readtable(featuretxt,'Delimiter','\t');
    FileInfo = dir(featuretxt);
    [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
    [path, name, ext] = fileparts(featuretxt);
    featuretxt_backup = fullfile(path, sprintf('%s_%02d%02d%02d.txt', name, Y-2000, M, D));
    if ~testFormat(gen_opt, vec_idx, summary_database, BoW_smooth, global_features, features_table_old)
        warning('Format of summary files do not match reprocess the whole data set!')
        gen_opt.annotation_summary_reprocess = 1;
    end
end

%% Initialize the columns of the table to be generated
dir_list = {};
poi_name = {};
poi_namefull = {};
time_vec = [];
fmat_all = [];
foreground = [];
idx_vec = [];

% Cat the feature data for all cells
protNumAll = [];
protNumNames = {};

%% Process each cell

for cell_idx = vec_idx
    
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    celldir =  path_gen.getcelldir('', '');    
    if ~gen_opt.annotation_summary_reprocess 
        if any(strcmp(celldir, features_table_old.path))
            fprintf('Feature vector already summarized Cell_idx: %d\n',  cell_idx);
            continue
        end
    end
    fprintf('Summarize feature vector Cell_idx: %d\n',  cell_idx);
    seg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
    reg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.preproc_dir);
    calfile = path_gen.calibrationfile;
    [protNum, protNumNames] = getProteinnumbers(path_gen.imgfile, 1, ...
        seg_matfiles, reg_matfiles, calfile);
    protNumAll = [protNumAll; protNum];
    annotate_dir = path_gen.getcelldir(gen_opt.out_dir, gen_opt.annotate_dir);

    poi = summary_database.poi(cell_idx);
    tmax = summary_database.tmax(cell_idx);
    
    annotation = load(fullfile(annotate_dir, 'complete_sequence.mat'));
    
    dir_list = cat(1, dir_list, repmat({celldir},[tmax 1]));
    poi_namefull = cat(1, poi_namefull, repmat(poi,[tmax 1]));
    metadata_idx = find(strcmp(POI_metadata.poi_name, poi),1);
    poi_name = cat(1, poi_name, repmat(POI_metadata.shortname(metadata_idx), [tmax 1]));
    foreground = cat(1, foreground, annotation.BG);
    time_vec = cat(1,time_vec, annotation.mitotime);
    idx_vec = cat(1, idx_vec, (1:tmax)');
    switch BoW_smooth
        case 1
            FA = annotation.f_smooth;
        case 0
            FA = annotation.f_ip;
    end
    
    switch global_features
        case 1
            FA = cat(2, FA, annotation.f_glob);
    end
    
    fmat_all = cat(1, fmat_all, FA);
end

%%
featuremat = dataset({dir_list, 'path'}, {poi_name,'poi'}, {poi_namefull,'fullname'}, ...
    {time_vec,'time'}, {idx_vec,'index'}, ...
    {foreground,'foreground'});

for i = 1:size(fmat_all,2)
    featuremat = [featuremat dataset({fmat_all(:,i), ['f_' num2str(i)]})];
end

for i = 1:length(protNumNames)
    featuremat = [featuremat dataset({protNumAll(:,i), protNumNames{i}})];
end

if isempty(featuremat)
    return
end

if exist(featuretxt, 'file') && ~gen_opt.annotation_summary_reprocess 
    copyfile(featuretxt, featuretxt_backup);
    export(featuremat, 'file', featuretxt);
    features_table = readtable(featuretxt,'Delimiter','\t');
    try
        features_table = [features_table_old; features_table];
        writetable(features_table, featuretxt,  'Delimiter','\t');
    catch
        warning('Format of summary files do not match!')
    end
else
    export(featuremat,'file',featuretxt);
end

end

function [status] = testFormat(gen_opt, vec_idx, summary_database, BoW_smooth, global_features, features_table)
    status = 0;
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{vec_idx(1)}, summary_database.tmax(vec_idx(1)));
    seg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
    reg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.preproc_dir);
    calfile = path_gen.calibrationfile;
    [protNum, protNumNames] = getProteinnumbers(path_gen.imgfile, 1, ...
        {seg_matfiles{1}}, {reg_matfiles{1}}, calfile);
    annotate_dir = path_gen.getcelldir(gen_opt.out_dir, gen_opt.annotate_dir);
    annotation = load(fullfile(annotate_dir, 'complete_sequence.mat'));
    
    switch BoW_smooth
        case 1
            FA = annotation.f_smooth;
        case 0
            FA = annotation.f_ip;
    end
    switch global_features
        case 1
            FA = cat(2, FA, annotation.f_glob);
    end
    old_field_names =  fieldnames(features_table);
    old_feat_num = sum(cell2mat(cellfun(@(x) regexp(x,  'f_\d+'), old_field_names, 'UniformOutput', false)));
    if length(FA) ~= old_feat_num
        return
    end
    idxName = find(cellfun(@(x) strcmp(x,  protNumNames{1}), old_field_names));
    if isempty(idxName)
        return
    end
    for idx = 1:length(protNumNames)
        if ~strcmp(protNumNames{idx}, old_field_names(idx+idxName-1))
            return
        end
    end
    status = 1;
end


function [ov, ovnames] = getProteinnumbers(imgfile, iC, seg_matfiles, reg_matfiles, calfile)
       ov = zeros(length(seg_matfiles), 10);
       ovnames = {'POI_cell12_N',	'POI_cyt12_N',	'POI_nuc12_N',...
           'POI_cell12_nM',	'POI_cyt12_nM',	'POI_nuc12_nM',	'POI_cell12_I',...
           'Vol_cell12_um3', 'Vol_cyt12_um3',	'Vol_nuc12_um3'};
       
       for imat = 1:length(seg_matfiles)
            fprintf('-');
            matfile = seg_matfiles{imat};
            matfilereg = reg_matfiles{imat};
            
            matfile_names = {matfile, matfilereg};
            op = computeProteinIntensities(imgfile, matfile_names, calfile, iC, 1);
            %% compute different variables from  total intensity
            names = {'cell', 'cyt', 'nuc12',};

            % compute total number of proteins from  total intensity
            for i = 1:length(names)
                ov(imat, i) =  (getfield(op, ['GFP' names{i}]) - getfield(op, ['V' names{i}])*op.cal(1))*op.cal(2)*op.Vvoxel*op.Na;
            end
            
            % compute average concentration from total intensity
            for i = 1:length(names)
                ov(imat, i + 3) = (getfield(op, ['GFP' names{i}])/getfield(op, ['V' names{i}]) - op.cal(1))*op.cal(2);
            end
            % total fluorescence intensity
            ov(imat, 7) = getfield(op, ['GFPcell']);
            % compute Volume in um3
            for i = 1:length(names)
                ov(imat, i + 7) = getfield(op, ['V' names{i}])*op.Vvoxel;
            end
       end
       fprintf('|\n');
end