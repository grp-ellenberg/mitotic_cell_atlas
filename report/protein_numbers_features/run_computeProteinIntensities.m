function run_computeProteinIntensities(proteins, gen_opt)
%% RUN_COMPUTEPROTEININTENSITIES
% This runs the code computeProteinIntensities and then convert protein intensties in the cytoplasmic and
% nucleus/chromatin compartments to concentrations and protein numbers. The data can then be processed with a R script for
% further analysis. The cells to process are extracted from the mitosys database file (e.g. fulldatabase.txt)
% INPUT:
%   proteins - a cell array that contains the full name of protein to use.
%   This compares to the POI entry of the database. Use '' for process all POI or
%   {'MAD2L1gfpcM11-B11', 'CTCFgfpcF2'} to process several POIs only
%   outdir - the output directory for the file
%
% OUTPUT:
%   the scripts write out two files containing the protein amounts for each time point and explanation of the column values.
%   The files are named
%       proteinDistribution_DATE.txt (protein amounts)
%       proteinDistribution_DATE_doc.txt (explanation of column usage)
%
%
%   Antonio Politi, EMBL, May 2017, December 2017 added more comments

gen_opt_default = struct( ...
    'exp_dir', '', ...                       % data directory
    'out_dir',  '',...                       % directory to store the results
    'cellIdx', [], ...                     % cell_idx to process
    'exp_database', 'full_database.txt', ... % filename for experiment database text file (tab delimited file)
    'segmentation_dir', 'Segmentation', ...  % subdirectory for segmentation results
    'mstalign_dir', 'Temporal_Align', ...
    'mstalign_fname', 'MST_annotation.mat', ...     %
    'mstalign_timeIndexFilename', 'MST_timeindex.mat', ...
    'preproc_dir', 'Registration');    % subdirectory for preprocessing

if nargin < 1
    % process only specific proteins to speed up analysis
    proteins = [];
end

if nargin < 2
    gen_opt = gen_opt_default;
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

%% Read database
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database, 'Delimiter','\t');
% find idx of cells
numCells = size(summary_database,1);
visitCells = [1:numCells];
if ~isempty(gen_opt.cellIdx)
    if iscolumn(gen_opt.cellIdx)
        gen_opt.cellIdx = gen_opt.cellIdx';
    end
    if ~all(ismember(gen_opt.cellIdx, visitCells))
        warning('Some indexes of cellIdx option are not in table. Process the complete data set');
        gen_opt.cellIdx = visitCells;
    end
else
    gen_opt.cellIdx = visitCells;
end
%% extract information of the data

elemtotest = {'manualQC', 'segmentation', 'fcs_calibration', 'lm_features', ...
    'time_alignment', 'poiprocess', 'feature_extract', 'feature_vector'};
proc_history = ones(length(summary_database.filepath), 1);
for elem = elemtotest
    proc_history = proc_history.*(table2array(summary_database(:, elem) ) > 0);
end
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);

%%
idxProt = zeros(length(vec_idx), 1);
if ~isempty(proteins)
    for iprot = 1:length(proteins)
        idxProt = idxProt + cellfun(@(x) strcmp(proteins{iprot}, x), summary_database.poi(vec_idx));
    end
    vec_idx = vec_idx(find(idxProt));
end

%% some processing options
iT = 1; % only one time point
namefile = 'proteinDistribution_';
tres = 0.25; % time resolution of mitotic standard time in min

%% header for output
% first entry is header name second entry is explanation
outputNames = getoutputNames();
fidAll = fopen(fullfile(gen_opt.out_dir, [namefile char(datetime('now','Format', 'yyMMdd')) '.txt']), 'w');
for i=1:size(outputNames,1)-1
    fprintf(fidAll, '%s\t', outputNames{i,1});
end
fprintf(fidAll, '%s\n', outputNames{end,1});
% create a file with an explanation of the entries
fidDoc = fopen(fullfile(gen_opt.out_dir, [namefile char(datetime('now','Format', 'yyMMdd')) '_doc.txt']), 'w');
for i=1:size(outputNames,1)-1
    fprintf(fidDoc, '%s\t', outputNames{i,1});
end
fprintf(fidDoc, '%s\n', outputNames{end,1});

for i=1:size(outputNames,1)-1
    fprintf(fidDoc, '%s\t', outputNames{i,2});
end
fprintf(fidDoc, '%s\n', outputNames{end,2});
fclose(fidDoc);
% log file
fidlog = fopen(fullfile(gen_opt.out_dir, [namefile char(datetime('now','Format', 'yyMMdd')) '.log']), 'w');


%% Run for each cell
for cell_idx = vec_idx
    fprintf('Protein quantification in compartments Cell_idx: %d\n',  cell_idx);
    try
        genH2B = contains(summary_database.filepath{cell_idx}, 'genH2B');
        path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
        iC = summary_database.Cpoi(cell_idx); % Channel of the POI
        %% define matfile files and output directories
        
        calfile = path_gen.calibrationfile;
        
        % the calibration file is in an old format
        if ~exist(calfile, 'file')
            error('No calibration file found!');
        end
        Volpar = load(calfile, 'Veff', 'kappa_lsm','w0');
        seg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
        reg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.preproc_dir);
        mstalign_dir = path_gen.getcelldir(gen_opt.out_dir, gen_opt.mstalign_dir);

        timeAlfile1 = fullfile(mstalign_dir, gen_opt.mstalign_fname);
        timeAlfile2 = fullfile(mstalign_dir, gen_opt.mstalign_timeIndexFilename);
        if(exist(timeAlfile1, 'file') && exist(timeAlfile2, 'file'))
            timemin = load(timeAlfile1, 'mitotime');
            timemin = timemin.mitotime;
            timestage = load(timeAlfile2,'mitotime');
            timestage = timestage.mitotime;
        else
            continue
        end
        
        %% iterate for each segmented lsm file
        for imat = 1:length(seg_matfiles)
            fprintf('-');
            imgfile = fullfile(path_gen.imgpaths{imat});
            
            matfile = seg_matfiles{imat};
            matfilereg = reg_matfiles{imat};
            
            matfile_names = {matfile, matfilereg};
            op = computeProteinIntensities(imgfile, matfile_names, calfile, iC, iT, genH2B);
            
            %% compute different variables from  total intensity
            names = {'cell', 'cyt',  ...
                'nuc12', 'nuc1', 'nuc2'};
            ov = [];
            
            % compute total number of proteins from  total intensity
            for i = 1:length(names)
                ov =  [ov , (getfield(op, ['GFP' names{i}]) - getfield(op, ['V' names{i}])*op.cal(1))*op.cal(2)*op.Vvoxel*op.Na];
            end
            
            
            % compute average concentration from total intensity
            for i = 1:length(names)
                ov =  [ov , (getfield(op, ['GFP' names{i}])/getfield(op, ['V' names{i}]) - op.cal(1))*op.cal(2)];
            end
            
            % compute Volume in um3
            for i = 1:length(names)
                ov =  [ov , getfield(op, ['V' names{i}])*op.Vvoxel];
            end
            ov = [ov, double(op.cal(1)), double(op.cal(2)), double(op.cal(3)), double(Volpar.Veff(1)), double(Volpar.w0(1)), double(Volpar.kappa_lsm(1))];
            
            % total fluorescence intensity
            for i = 1:length(names)
                ov = [ov , getfield(op, ['GFP' names{i}])];
            end
            
            % total pixel number oer object of interest
            for i = 1:length(names)
                ov =  [ov , getfield(op, ['V' names{i}])];
            end
            if genH2B
                tok = regexp(summary_database.filepath(idx,:), '.+cell(\d+)_R(\d+)', 'tokens');
                id = sprintf('%s_%d_%d_%d_%d', summary_database.filepath(idx,1:6) , summary_database.system(idx,:), str2num(tok{1}{1}), ...
                    str2num(tok{1}{2}), idx);
            else
                id = path_gen.getcellid(path_gen.imgfile);
            end
            fprintf(fidAll, '%s\t%s\t%s\t%s\t%d\t%.2f\t%s\t%.4f\t%.4f\t%d',  strtrim(summary_database.poi{cell_idx}), id, ...
                strtrim(summary_database.filepath{cell_idx}), op.imgfile, ...
                timestage(imat,1),  timestage(imat,1)*1.5, op.mitoTime, ...
                timemin(imat, 1)*tres, timestage(imat,2), op.numChr);
            for ival=1:length(ov)
                fprintf(fidAll, '\t%.5f', ov(ival));
            end
            fprintf(fidAll,'\n');
        end
        fprintf('|\n');
    catch ME
        getReport(ME, 'extended', 'hyperlinks', 'off')
        fprintf(fidlog, 'Error for file %s idx %d\n%s\n', matfile, cell_idx, ME.message);
    end
end
fclose(fidAll);
end


function outputNames = getoutputNames()
% cell array of variables to print out to the output file
outputNames = {...
    'POI',           'name of POI',
    'CellID',        'Cell ID',
    'cellpath',      'relative filepath to result cell directory',
    'filepath',      'absolute filepath to image',
    'frame',         'imaging frame',
    'timeMin',       'imaging time',
    'mitoTimeJH',    'classification from Julius Hossain pipeline. Geometry change in chromatin',
    'mitoTimeMin',   'mitotic standard time',
    'mitoTimeStage', 'mitotic stage',
    'numChr',        'number of chromosomes from Julius Hossain pipeline',
    'POI_cell12_N',  'Total number of proteins in cell          POI_cell12_nM*Vol_cell12*Na*Vvoxel',
    'POI_cyt12_N',   'Total number of proteins in cytoplasm     POI_cyt12_nM*Vol_cyt12*Na*Vvoxel',
    'POI_nuc12_N',   'Total number of proteins in nuclei (1+2)  POI_nuc12_nM*Vol_nuc12*Na*Vvoxel',
    'POI_nuc1_N',    'Total number of proteins in nucleus 1     POI_nuc1_nM*Vol_nuc1*Na*Vvoxel',
    'POI_nuc2_N',            'Total number of proteins in nucleus 2     POI_nuc1_nM*Vol_nuc1*Na*Vvoxel',
    'POI_cell12_nM',         'Average concentration of POI in cell',
    'POI_cyt12_nM',          'Average concentration of POI in cytoplasm',
    'POI_nuc12_nM',          'Average concentration of POI on nucleus 1 and 2',
    'POI_nuc1_nM',           'Average concentration of POI on nucleus1 ',
    'POI_nuc2_nM',           'Average concentration of POI on nucleus2',
    'Vol_cell12_um3',        'Volume of both cells in um3',
    'Vol_cyt12_um3',         'Volume cytoplasmic of both cells in um3',
    'Vol_nuc12_um3',         'Volume of total nuclei in um3 (= nuc1 + nuc2 or nuc1 if not yet divided)',
    'Vol_nuc1_um3',          'Volume of nucleus1 in um3',
    'Vol_nuc2_um3',          'Volume of nucleus2 in um3',
    'calibration_P1',        'calibration factor P1 (baseline)',
    'calibration_P2',        'calibration factor P2 (slope)',
    'calibration_P1_wt',     'calibration factor P1 (baseline) estimated from WT measurement',
    'Veff_fl',               'Effective confocal volume in fl',
    'w0_um',                 'Effective confocal radius in um',
    'kappa',                 'Structural parameter of effective confocal volume',
    'POI_cell12_I',          'Total fluorescence intensity cell 1 and 2',
    'POI_cyt12_I',           'Total fluorescence intensity cyt 1 and 2',
    'POI_nuc12_I',           'Total fluorescence intensity nucleus 1 + 2',
    'POI_nuc1_I',            'Total fluorescence intensity nucleus 1',
    'POI_nuc2_I',            'Total fluorescence intensity nucleus 2',
    'Vol_cell12_px',         'Volume cell 1 + 2 in pixels',
    'Vol_cyt12_px',          'Volume cytoplasm 1 + 2 in pixels',
    'Vol_nuc12_px',          'Volume nucleus 1 + 2 in pixels',
    'Vol_nuc1_px',           'Volume nucleus 1 in pixels',
    'Vol_nuc2_px',           'Volume nucleus 2 in pixels'};

end
