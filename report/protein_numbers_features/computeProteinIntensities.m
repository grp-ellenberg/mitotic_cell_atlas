function output = computeProteinIntensities(imgfile, matfile_names, calfile, iC, iT, genH2B)

% COMPUTEPROTEININTENSITIES compute the sum intensity in compartments
% found from nuclei and cell boundary segmentation
%   INPUT:
%       gen_opt - A struct
% 
%       imgfile - a bioformat compatible image. 
%       matfile_names - a cell containing the path to 3 matfiles 
%           {1} = binary/segmentation mask (e.g. from M.J. Hossain pipeline) of the nucleus and
%       outer boundary. The binary mask are denoted nucVolume (levels 1-2
%       depending on number of nuclei). The cell mask is denoted
%       cellVolume. 
%           {2} = The registration file containing the calibration data.
%       calfile - a calibration file obtained from FCS calibration
%       iC - channel of POI. Default is 1
%       iT - time point. Typically the images do not contain a time stack. Only applies if images are a time stack
%            Default is 1
%       oldformat - legacy code if 1
%       genH2B  - 0, 1 special processing if artifically generated H2B data. Only used to test background subtraction
%                 from chromatin mass
%   Antonio Politi, EMBL, modified May 2017, June 2017 added processing of
%   refined segmentation


if nargin < 1
    % a test path
    imgfile =  'O:\MitoSys_data\HeLaKyoto_MitoSys\Data_tifs\160516_gfpNCAPH2c1_MitoSys1\cell0006_R0001\rawtif\TR1_2_W0001_P0001_T0020.tif';
end

if nargin < 2
    % a test path
    maindir = 'O:\MitoSys_data\HeLaKyoto_MitoSys\Results\160516_gfpNCAPH2c1_MitoSys1\cell0006_R0001\';
    matfile = fullfile(maindir, 'Segmentation', 'TR1_2_W0001_P0001_T0020.mat');
    matfilereg = fullfile(maindir, 'Registration', 'TR1_2_W0001_P0001_T0020.mat');
    matfile_names =  {matfile, matfilereg};
end

if nargin < 3
    calfile = 'O:\MitoSys_data\HeLaKyoto_MitoSys\Data_tifs\170406_gfpTOP2Ac102_MitoSys1\Calibration\calibration.mat';
end

if nargin < 4
    iC = 1;
end

if nargin < 5
    iT = 1;
end



if nargin < 6
    genH2B = 0; 
end

% This is used for nM and um3 units
Na = 0.6022140; %*10^24
Vvoxel = 0.2516*0.2516*0.75; % TODO!! remove hardcoding
%% load data nucVolume and cellVolume are segmented nucleus and whole cell
assert(length(matfile_names) == 2, 'Length of matfiles path cell array must be 2. Segmentation, registration');
segmatfile = matfile_names{1};
regmatfile = matfile_names{2};

if isempty(segmatfile) || ~exist(segmatfile, 'file')
    error('No valid path for segmentation matfile');
end

if isempty(regmatfile) || ~exist(regmatfile, 'file')
    error('No valid path for registration matfile');
end


%% read in the calibration 
% using the bgValue for calibration
% compute back the value we need here which is C_nM!
reg = load(regmatfile, 'calibration_factor_N_voxel', 'background_488_N_voxel');

% reg.calibration_factor is in N per voxel convert again in nM (alternatively one could directly read it from the calibration file)
calibration(2) = reg.calibration_factor_N_voxel/Vvoxel/Na;

% the background
calibration(1) = reg.background_488_N_voxel/reg.calibration_factor_N_voxel;


calibration_C_nM = load(calfile, 'calibration_factor_C_nM', 'voxel_V_um3');
% calibration with background computed from wt or by linear interpolation from FCS traces (old data set from Yin Cai)
calibration(3) = calibration_C_nM.calibration_factor_C_nM(1);


try
    seg = load(segmatfile, 'cellVolume', 'nucVolume', 'poi', 'numChr', 'mitoTime', 'nuc');
catch
    seg = load(segmatfile);
end

%%
%%
% check for alias 
if ~isfield(seg, 'nucVolume')
    seg.nucVolume = seg.chrMass;
    seg.mitoTime = '';
end

if ~isfield(seg, 'cellVolume')
    seg.cellVolume = seg.cellMass;
    seg.mitoTime = {''};
end


% a ball 3D structural element
erodFil = zeros(3,3,3);
erodFil(:,:,1) = [0 0 0; 0 1 0; 0 0 0];
erodFil(:,:,2) = [0 1 0; 1 1 1; 0 1 0]; 
erodFil(:,:,3) = [0 0 0; 0 1 0; 0 0 0];

%% load image
if genH2B
    % dummy variable 
    GFP = seg.nuc;
else
    if ~isfield(seg, 'poi')
        [reader, dim] = getBioformatsReader(imgfile);
        %% get first channel, the GFP channel (typically images contain only one time point)
        GFP = getTPointBioFormats(reader, dim, iT, iC);
        Vvoxel = dim.voxelSize(1)*dim.voxelSize(2)*dim.voxelSize(3);
    else
        %Vvoxel = calibration_C_nM.voxel_V_um3;
        GFP = seg.poi;
    end
end

%% A struct that constains the volumes  
Volumes = struct('nuc12',  seg.nucVolume > 0, 'nuc1', seg.nucVolume == 1, 'nuc2', seg.nucVolume == seg.numChr, ...
    'cell', seg.cellVolume,  'cyt', (seg.nucVolume == 0).*seg.cellVolume);

fnames = fieldnames(Volumes);

%% Create default structures

%matrix containing the fluorescence intensities
GFPI = struct;
for i = 1:length(fnames)
    % intensity 
    GFPI = setfield(GFPI, fnames{i}, []);
end
% sum of the fluorescence intensities
sGFPI = GFPI;
% sum of volumes
sVolumes = GFPI;
GFPI.w = GFP; 




% fluorescence intensities in the different compartments
for i=1:length(fnames)
    GFPI =  setfield(GFPI, fnames{i}, GFPI.w.*getfield(Volumes, fnames{i}));
end

% compute the sums of the different entries
for i = 1:length(fnames)
    l = getfield(GFPI, fnames{i});
    sGFPI = setfield(sGFPI, fnames{i}, sum(l(:))); %sum of intensity
    l = getfield(Volumes,  fnames{i});
    sVolumes = setfield(sVolumes, fnames{i}, sum(l(:)));
end

% chromosomes have not been yet splitted but anaphase has occured
if strcmp(seg.mitoTime{1}, 'Ana') && seg.numChr == 1
   sGFPI.nuc1 = sGFPI.nuc1/2;
   sGFPI.nuc2 = sGFPI.nuc2/2;
   sVolumes.nuc1 = sVolumes.nuc1/2;
   sVolumes.nuc2 = sVolumes.nuc2/2;
end
output = struct('imgfile', imgfile, 'cal', calibration, 'Na', Na, 'numChr', seg.numChr, 'mitoTime', seg.mitoTime{1}, 'Vvoxel', Vvoxel, ...
    'Vnuc12', sVolumes.nuc12, 'Vnuc1', sVolumes.nuc1, 'Vnuc2', sVolumes.nuc2, ...
    'Vcyt',  sVolumes.cyt, 'Vcell', sVolumes.cell, ...
    'GFPnuc12', sGFPI.nuc12,  'GFPnuc1', sGFPI.nuc1, 'GFPnuc2', sGFPI.nuc2, ...
    'GFPcyt', sGFPI.cyt, 'GFPcell', sGFPI.cell);
if ~isfield(seg, 'poi')
    reader.close;
end
end