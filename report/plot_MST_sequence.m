function plot_MST_sequence(mst_modeldir,savedir,segdir,contrast)

% Author: Yin Cai
% Date: 27.06.2016

% This function can be used for plotting all figures relatid to the Mitotic
% Standard Time, more precisely: Figure 2e, Figure S7, and gallery of
% multiple cells of the same stage during the training.

% mst_modeldir: directory of the MST model
% savedir: directory for saving the output images
% segdir: e.g. "Segmentation", folder name to save the segmentation output
% contrast: parameter defining the saturation of landmark's channel.
% Recommanded starting point would be 45000. 

%% Load the MST model and the list of training cells
model_file = fullfile(mst_modeldir,'temporal_alignment.mat');
load(model_file);
filepathfile = fullfile(mst_modeldir,'data_filepath_list.mat');
load(filepathfile);
timeline = timeline/p.deltat;

F = [feat_normalized;feat_derivative];
clear feat_normalized
clear feat_derivative
class_number = size(time_cluster,1);

%% Find cell frames for each mitotic stage and their feature vector
cellname_cl = {};
celltime_cl = [];
for i = 1:class_number;
    allcells = [];
    allidx = [];
    subalign = aligned_full(:,(sum(align_original_model.timeline < timeline(time_cluster.class_start(i)))+1):sum(align_original_model.timeline <= timeline(time_cluster.class_end(i))));
    if isempty(subalign)
        warning(['no image in the training set assigned to mitotic stage ' num2str(i)])
        subalign = aligned_full(:,(sum(align_original_model.timeline < timeline(time_cluster.class_start(i-1)))+1):sum(align_original_model.timeline <= timeline(time_cluster.class_end(i+1))));
    end
    for j = 1:size(subalign,1);
        subframe = unique(subalign(j,:)); % Single frame can be aligned to multiple frames in the model
        subframe(subframe==0) = [];
        if ~isempty(subframe)
            allidx = cat(2,allidx,cat(1,repmat(j,[1 length(subframe)]),subframe));
            for k = 1:length(subframe);
                allcells = cat(2,allcells,F(:,subframe(k),j)); % Cat the feature vector of all frames
            end
        end
    end
    
    %% Generate the gallery of 24 randomly chosen cell image examples
    cmax = min(size(allcells,2),24);
    crand = rand(1,size(allcells,2)); % random vecotr
    for j = 1:cmax;
        [~,clidx] = max(crand); % random selection of the cell
        crand(clidx) = 0;
        % load the segmentation output
        cellpath = strsplit(feat_filelist{allidx(1,clidx)},{'Temporal'},'CollapseDelimiters',true);
        cellpath = fullfile(cellpath{1}(1:end-1),'Preprocessing',segdir);
        if ~exist(cellpath,'dir')
            error('exp_dir mapping is wrong. Please check that the network mapping is exactly the same as training the MST model (Y, Z etc.)')
        end
        cell_seq = dir(fullfile(cellpath,'*T0*.mat'));
        load(fullfile(cellpath,cell_seq(allidx(2,clidx)).name));
        % 3D max projection of the chromosomal image and the segmented cell
        % volume
        maxNuc = max(nuc.*(nucVolume>0),[],3);
        maxNuc = maxNuc/max(maxNuc(:));
        projCell = max(cellVolume,[],3);
        img = projCell*0.3 + maxNuc/1.3; % Balance the grey level for the output image
        figure(1)
        subplot(4,6,j)
        imshow(img)
    end 
    % Generate and save the image
    figname = fullfile(savedir,['cluster' num2str(i,'%02.f') '_image_examples.fig']);
    savefig(1,figname)
    close(gcf)
    close all

    %% Projection of the cell closest to the center of the stage in the feature space
    dist_classmean = sum((allcells-repmat((time_cluster.class_mean(i,:))',[1 size(allcells,2)])).^2,1);
    [~,clidx] = min(dist_classmean);
    cellpath_cl = strsplit(feat_filelist{allidx(1,clidx)},{'Temporal'},'CollapseDelimiters',true);
    cellpath_cl = cellpath_cl{1}(1:end-1);
    addname = ['cluster' num2str(i,'%02.f')];
    plot_lm_registered(cellpath_cl,allidx(2,clidx),savedir,addname,segdir,contrast); % Projection and rotate the landmarks 
    cellname_cl = cat(1,cellname_cl,cellpath_cl); % Document the selected cell
    celltime_cl = cat(1,celltime_cl,allidx(2,clidx));
    
    %% Generate the RGB image showing the selection procedure
    % Reduce the feature vectors of all cells and the center of the stage
    % into a 2-D space using multi-dimensional scaling
    Y = cat(2,allcells,(time_cluster.class_mean(i,:))');
    D = pdist(Y');
    [Y,e] = mdscale(D,2);
    % Rescale the reduced feature vectors into 1600x900
    map = ones(1600,900);
    Y(:,1) = Y(:,1)-min(Y(:,1));
    Y(:,2) = Y(:,2)-min(Y(:,2));
    factor = min(1550/max(Y(:,1)),850/max(Y(:,2)));
    Y = Y*factor;
    Y = round(Y);
    Y = Y+1;
    % Cat all projected images assigned to this stage and centralize them
    img = zeros(256,256,length(dist_classmean));
    pos = [];
    for j = 1:length(dist_classmean);
        % The larger the distance to the center, the more closer to the
        % background layer.
        [~,clidx] = max(dist_classmean);
        dist_classmean(clidx) = -inf;
        pos = cat(1,pos,Y(clidx,:));
        % Load the segmentation image
        cellpath = strsplit(feat_filelist{allidx(1,clidx)},{'Temporal'},'CollapseDelimiters',true);
        cellpath = fullfile(cellpath{1}(1:end-1),'Preprocessing',segdir);
        cell_seq = dir(fullfile(cellpath,'*T0*.mat'));
        load(fullfile(cellpath,cell_seq(allidx(2,clidx)).name));
        % 3D maximum projection of the chromosomal image and the cell
        % volume (segmented) 
        maxNuc = max(nuc.*(nucVolume>0),[],3);
        maxNuc = maxNuc/max(maxNuc(:));
        projCell = max(cellVolume,[],3);
        % Centralize the image
        x1 = find(sum(projCell,2)>0,1,'first');
        xend = find(sum(projCell,2)>0,1,'last');
        y1 = find(sum(projCell,1)>0,1,'first');
        yend = find(sum(projCell,1)>0,1,'last');
        xstart = 128-floor((xend-x1+1)/2);
        ystart = 128-floor((yend-y1+1)/2);
        img(xstart:(xstart+xend-x1),ystart:(ystart+yend-y1),j)= (projCell(x1:xend,y1:yend)*0.3 + maxNuc(x1:xend,y1:yend))/1.3;
    end
    % Crop the projected images
    I = sum(img,3);
    Ix = sum(I,2);
    Iy = sum(I,1);
    crop_start = min(find(Ix,1,'first'),find(Iy,1,'first'));
    crop_end = max(find(Ix,1,'last'),find(Iy,1,'last'));
    img = img(crop_start:crop_end,crop_start:crop_end,:);
    % Position the images into the big image with white border
    for j = 1:size(img,3);
        small = imresize(img(:,:,j),[50,50]);
        small(1,:) = 1;
        small(end,:) = 1;
        small(:,1) = 1;
        small(:,end) = 1;
        map(pos(j,1):(pos(j,1)+49),pos(j,2):(pos(j,2)+49)) = small;
    end
    % Dark the border of the last image in G and B channel
    map2 = map;
    small(1,:) = 0;
    small(end,:) = 0;
    small(:,1) = 0;
    small(:,end) = 0;
    map2(pos(j,1):(pos(j,1)+49),pos(j,2):(pos(j,2)+49)) = small; % G and B
    % Mark the center of the stage in red in the feature space
    map2((Y(end,1)-3):(Y(end,1)+3),(Y(end,2)-3):(Y(end,2)+3)) = 0;
    map((Y(end,1)-3):(Y(end,1)+3),(Y(end,2)-3):(Y(end,2)+3)) = 1;
    % Write the images channel by channel
    saveimg = fullfile(savedir,['cluster' num2str(i,'%02.f') '_mds_image_selection_R.tif']);
    imwrite(map,saveimg);
    saveimg = fullfile(savedir,['cluster' num2str(i,'%02.f') '_mds_image_selection_GB.tif']);
    imwrite(map2,saveimg);
end
% Save the selected cells
savefile_cl = fullfile(savedir,'representative_MST_sequence.mat');
save(savefile_cl,'cellname_cl','celltime_cl');

end