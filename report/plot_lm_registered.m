function plot_lm_registered(celldir,timevec,savedir,addname,segdir,contrast)

% Author: Yin Cai
% Date: 24.6.2016

% This function use the pre-processing output the images (segmentation and
% POI pre-processing) and project the image in z in the way that both
% landmarks can be clearly seen and rotated to one axis.
% The user can later use softwares such as FiJi to generate RGB images
% showing the merged image in a most natural way. Example of the output can
% be seen in the Figure 2 of the manuscript Cai et al:
% An experimental and computational framework to build a dynamic protein 
% atlas of human cell division. (2016)

% celldir: the Result directory of the cell of interest
% timevec: a vector of all timepoints (in frame) which should be plotted
% contrast: parameter vector defining the saturation of both channel.
% Recommanded starting point would be [45000;45000]. 
% segdir: name of the directory for saving the segmentation result, e.g.
% 'Segmentation';
% savedir: output directory
% addname: a string for unifying the name of the output through multiple
% plotting circles (e.g. using different contrast values etc.)

seg_dir = fullfile(celldir,'Preprocessing',segdir);
cell_seq = dir(fullfile(seg_dir,'*T0*.mat'));

if exist(seg_dir,'dir');
    for tdx = 1:length(timevec);
        
        load(fullfile(seg_dir,cell_seq(timevec(tdx)).name));
        
        %% Calculating the retational angle based on intersetion axis-cell locations
        rotation_xy = 180*atan2(isectPointsPix(1,2)-isectPointsPix(2,2),isectPointsPix(1,1)-isectPointsPix(2,1))/pi;
        %% Caculating the center of rotation, i.e. the center of chromosomal volume
        nucdata = bwconncomp(nucVolume>0);
        size_nuc = cellfun(@numel,nucdata.PixelIdxList);
        nucnumber = length(size_nuc);
        centronuc = regionprops(nucdata,'Centroid');
        if nucnumber == 1;
            nuccent = centronuc(1).Centroid;
        else % we only consider the biggest two parts of the nucleus, i.e. two divided doughters
            [val1,nucidx1] = max(size_nuc);
            size_nuc(nucidx1) = 0;
            [val2,nucidx2] = max(size_nuc);
            % Weighted center of the two chromosomal volumes
            nuccent = (centronuc(nucidx1).Centroid*val1+centronuc(nucidx2).Centroid*val2)/(val1+val2);
        end
        if length(nuccent) < 3;
            nuccent = cat(2,nuccent,1);
        end
        % x-y switch in matlab
        nuccent = round([nuccent(2) nuccent(1) nuccent(3)]);
        
        %% Rotate the cellVolume and filtered nuc
        [cellV,cellcent,~] = yc_imrotate3d(neg,-rotation_xy,nuccent,[]); % rotate the negative staining image and export the new center
        proc_nuc = nuc.*(nucVolume>0);
        [h2b,~,~] = yc_imrotate3d(proc_nuc,-rotation_xy,nuccent,cellcent); % rotate the H2B channel
        
        cellV = mean(cellV(:,:,min(isectPointsPix(:,3)):max(isectPointsPix(:,3))),3)/contrast(2);
        cellV(1,1) = 1;
        savecell = fullfile(savedir,[addname cell_seq(timevec(tdx)).name(1:(end-4)) '_C01_T' num2str(timevec(tdx),'%02.f') '.tif']);
        imwrite(cellV,savecell);

        h2b = max(h2b,[],3)/contrast(1);
        h2b(1,1) = 1;
        saveh2b = fullfile(savedir,[addname cell_seq(timevec(tdx)).name(1:(end-4)) '_C02_T' num2str(timevec(tdx),'%02.f') '.tif']);
        imwrite(h2b,saveh2b);
    end
else
    error('pre-processing for the cell of interest not finished')
end
end