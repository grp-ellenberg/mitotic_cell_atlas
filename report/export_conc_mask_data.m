function export_conc_mask_data(img_dir, res_dir, db_file, force_reprocess)
% EXPORT_CONC_MASK_DATA Export concentration and mask maps of library to 
% multipage tif. Only exports cells that passed all QCs according to db_file
%   EXPORT_CONC_MASK_DATA read default full_database and export to default directory
%   EXPORT_CONC_MASK_DATA(exp_dir) read exp\full_database.txt and export to default directory
%   EXPORT_CONC_MASK_DATA(exp_dir, db_file): read db_file and export to default directory
%   EXPORT_CONC_MASK_DATA(exp_dir, db_file, out_dir): read db_file and export to outdir directory
%   EXPORT_CONC_MASK_DATA(exp_dir, db_file, out_dir force_reprocess): read db_file and export to outdir directory. Set force_reprocess
%   INPUT:
%       exp_dir: Path to lsm data
%       db_file: name of database file. 
%       out_dir: fullpath to output directory
%       force_reprocess: 0 or 1 if already processed data should be reprocessed. This is done cell wise. 
%   OUTPUT:
%       Data is saved to outdir\date_POIname_system\cellname\
%           conctif: ome  tif, 32 bit (floats) of concentrations in nM. This are the images processed with a 3D filter_gauss
%           masktif: ome tif multilabel, multipage 8 bit. Ch1: Segmented nuclear landmark; Ch2: Segmented cellular landmark

% Antonio Politi, EMBL, December, 2017
%   add doc and error handling February, 2018
if nargin < 1
    img_dir = 'O:\MitoSys_data\HeLaKyoto_MitoSys\Data_tifs';
end
if nargin < 2
   res_dir =  'O:\MitoSys_data\HeLaKyoto_MitoSys\Results';
end
if nargin < 3
    db_file = 'full_database.txt';
end
if nargin < 4
    force_reprocess = 1;
end
dataB = readtable(fullfile(res_dir, db_file),'Delimiter','\t');
proc_history = (dataB.manualQC~=0).*(dataB.time_alignment>0).*(dataB.fcs_calibration>0).*(dataB.segmentation>0);
vec_idx = find(proc_history>0);
dataB = dataB(vec_idx, :);
for idx = 1:length(vec_idx)
    try
        % skip genH2B folders
        if contains(dataB.filepath(idx), 'genH2B')
            continue
        end
        fprintf('Process %d idx %s\n', idx, dataB.filepath{idx})
        
        [files_in, files_out, cellout] = assign_folders(img_dir, res_dir, strtrim(dataB.filepath{idx,:}));
        
        % only add files thet are missing        
        idxfiles = [];

        for ifile = 1:size(files_in,1)
            % if one of the files is missing add them all 
            if ~exist(files_out(ifile).seg, 'file') || force_reprocess 
                idxfiles = 1:size(files_in,1);  
                break;
            end
        end
        if isempty(idxfiles)
            continue
        end
        
        [reader, dim] = getBioformatsReader(files_in(1).img);
        reader.close();
        for ifile = idxfiles 
            savemask(files_in(ifile), files_out(ifile), dim);
            saveconc(files_in(ifile), files_out(ifile), dim);      
        end

    catch ME
        getReport(ME, 'extended', 'hyperlinks', 'off')
    end
end
end

function saveconc(files_in, files_out, dim)
reg = load(files_in.reg, 'proc_poi');
height = size(reg.proc_poi,1);
width = size(reg.proc_poi,2);
depth = size(reg.proc_poi,3);
stack = zeros(height, width, depth, 1, 'single');
Na = 0.6022140;
stack(:,:,:,1) = reg.proc_poi/(dim.voxelSize(1)*dim.voxelSize(2)*dim.voxelSize(3))/Na;


%% create metadata
metadata = createMinimalOMEXMLMetadata(stack);
pixelSizeXY = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(1)), ome.units.UNITS.MICROMETER);
pixelSizeZ = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(3)), ome.units.UNITS.MICROMETER);
metadata.setPixelsPhysicalSizeX(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeY(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeZ(pixelSizeZ, 0);
metadata.setChannelName('POI_conc', 0, 0);
metadata.setPixelsTimeIncrement(ome.units.quantity.Time(java.lang.Double(90.0), ome.units.UNITS.SECOND), 0);

% this is very slow
%bfsave(stack, files_out.img, 'metadata', metadata);
data = reshape(stack, width, height, 1*depth);
options = struct('overwrite' , true);
saveastiff(data, files_out.reg, options);
swapomexml(files_out.reg, metadata)
end

function saveraw(files_in, files_out, dim)
seg = load(files_in.seg, 'poi', 'nuc', 'neg');
height = size(seg.poi,1);
width = size(seg.poi,2);
depth = size(seg.poi,3);
stack = zeros(height, width, depth, 3, 'uint16');
stack(:,:,:,1) = seg.poi;
stack(:,:,:,2) = seg.nuc;
stack(:,:,:,3) = seg.neg;

%% create metadata
metadata = createMinimalOMEXMLMetadata(stack);
pixelSizeXY = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(1)), ome.units.UNITS.MICROMETER);
pixelSizeZ = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(3)), ome.units.UNITS.MICROMETER);
metadata.setPixelsPhysicalSizeX(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeY(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeZ(pixelSizeZ, 0);
metadata.setChannelName('POI', 0, 0);
metadata.setChannelName('NUC', 0, 1);
metadata.setChannelName('NEG_Dextran', 0, 2);
metadata.setPixelsTimeIncrement(ome.units.quantity.Time(java.lang.Double(90.0), ome.units.UNITS.SECOND), 0);

% this is very slow
%bfsave(stack, files_out.img, 'metadata', metadata);
data = reshape(stack, width, height, 3*depth);
options = struct('overwrite' , true);
saveastiff(data, files_out.img, options);
swapomexml(files_out.img, metadata)
end

function savemask(files_in, files_out, dim)
seg = load(files_in.seg, 'cellVolume', 'nucVolume');
height = size(seg.cellVolume,1);
width = size(seg.cellVolume,2);
depth = size(seg.cellVolume,3);

stack = zeros(height, width, depth, 2, 'int8');
stack(:,:,:,1) = seg.nucVolume;
stack(:,:,:,2) = seg.cellVolume;
metadata = createMinimalOMEXMLMetadata(stack);

pixelSizeXY = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(1)), ome.units.UNITS.MICROMETER);
pixelSizeZ = ome.units.quantity.Length(java.lang.Double(dim.voxelSize(3)), ome.units.UNITS.MICROMETER);
metadata.setPixelsPhysicalSizeX(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeY(pixelSizeXY, 0);
metadata.setPixelsPhysicalSizeZ(pixelSizeZ, 0);
metadata.setChannelName('NUC', 0, 0);
metadata.setChannelName('CELL', 0, 1);
metadata.setPixelsTimeIncrement(ome.units.quantity.Time(java.lang.Double(90.0), ome.units.UNITS.SECOND), 0);
options = struct('overwrite' , true);
data = reshape(stack, width, height, 2*depth);
saveastiff(data, files_out.seg, options);
swapomexml(files_out.seg, metadata)

end

function swapomexml(filename, metadata)
% swap metadata of file tiff file. Write out to tagstruct.ImageDescription
filein = loci.common.RandomAccessInputStream(filename);
fileout = loci.common.RandomAccessOutputStream(filename);
saver = loci.formats.tiff.TiffSaver(fileout, filename);
saver.overwriteComment(filein, metadata.dumpXML);
saver.close();
filein.close();
fileout.close();
end

function [files_in, files_out, celldir_out]  = assign_folders(img_dir, res_dir,  cellfolder)
% create a list of files for input and output 

segfolder = fullfile(res_dir, cellfolder, 'Segmentation');
regfolder = fullfile(res_dir, cellfolder,  'Registration'); %contains the processed poi channel converted in number of molecules
imgfolder = fullfile(img_dir, cellfolder, 'rawtif');
if ~exist(imgfolder)
    error('No imaging folder has been found')
end

[pathname, fname] = fileparts(cellfolder);
cellfolder  = fullfile(strrep(pathname, '\', '_'), fname);
celldir_out = cellfolder;
segfolder_out = fullfile(img_dir, cellfolder, 'masktif');
concfolder_out = fullfile(img_dir, cellfolder, 'conctif');


if ~exist(segfolder_out)
    mkdir(segfolder_out);
end
if ~exist(concfolder_out)
    mkdir(concfolder_out);
end

segfiles_tmp = dir(fullfile(segfolder, '*.mat'));
nrtp = length(segfiles_tmp);
files_in = struct('img', cell(nrtp, 1), 'seg',cell(nrtp,1), 'reg', cell(nrtp,1));
files_out = struct( 'seg', cell(nrtp,1), 'reg', cell(nrtp,1));
for ifile = 1:length(segfiles_tmp)
    files_in(ifile).seg = fullfile(segfolder, segfiles_tmp(ifile).name);
    files_in(ifile).reg = fullfile(regfolder, segfiles_tmp(ifile).name);
    files_in(ifile).img = fullfile(imgfolder, [segfiles_tmp(ifile).name(1:end-4) '.tif']);
    files_out(ifile).seg = fullfile(segfolder_out, [segfiles_tmp(ifile).name(1:end-4) '.tif']);
    files_out(ifile).reg =  fullfile(concfolder_out, [segfiles_tmp(ifile).name(1:end-4) '.tif']);  
end
end