function create_tif_standardmodel(wdir, multipagedir, zprojdir, dim)
% CREATE_TIF_STANDARDMODEL cretes a 32-bit multi page tif, and Z-max,
% Z-mean projection and midslice of the standard mitoic model 
% CREATE_TIF_STANDARDMODEL(wdir, multipagedir, zprojdir) read files in wdir, and write multipage
% tiff to multipagedir and Z-projections to zprojdir
% CREATE_TIF_STANDARDMODEL(wdir, multipagedir, zprojdir, dim) specify dim = [dX, dY, dZ] to convert
% to protein number. Default is [0.25, 0.25, 0.25]
%   INPUT:
%       wdir - main directory data of the standard model in mat format
%            POINAME1/AvgPOI01.mat, AvgPOI02.mat, ...
%       multipagedir - output directory for multipage tif
%       zprojdir     - output directory for Z-projected images
%       dim          - expected dimension of image. This is required to write out the number of proteins
%
% Author: Antonio Politi, EMBL, November 2017
%   modified January 2018 use saveastiff function from  YoonOh Tak, MATLAB Central 
%   https://de.mathworks.com/matlabcentral/fileexchange/35684-multipage-tiff-stack

%% input and directory creation
addpath(genpath_loc(fileparts(mfilename('fullpath'))), '-end')
if nargin < 1
    wdir = uigetdir('./', 'Specify directory for input data mat files');
end
assert(exist(wdir, 'dir') > 0, 'Working directory does not exist!')

if nargin < 2
    multipagedir = uigetdir('./', 'Specify directory for output of multipage tif');
end

if nargin < 3
    zprojdir = uigetdir('./', 'Specify directory for output of z-projected tif');
end
if ~exist(multipagedir, 'dir')
    mkdir(multipagedir);
end
if ~exist(zprojdir, 'dir')
    mkdir(zprojdir);
end
if nargin < 4
    dim = [0.25, 0.25, 0.25];
end


%% Processing part
dnames = dir(wdir);
Na = 0.602214086;
isub = [dnames(:).isdir];
nameFolds = {dnames(isub).name};
for name = nameFolds
    celldir = fullfile(wdir, name{1});
    locmultipagedir = fullfile(multipagedir, name{1});
    loczprojdir = fullfile(zprojdir, name{1});
    fnames = dir(fullfile(celldir, 'AvgPoi*.mat'));
    if ~isempty(fnames)
        if ~exist(locmultipagedir, 'dir')
            mkdir(locmultipagedir);
        end
        if ~exist(loczprojdir, 'dir')
            mkdir(loczprojdir);
        end
        for i = 1:length(fnames)
            matfile_stage = fullfile(celldir, fnames(i).name);
            [path, basename] = fileparts(matfile_stage);
            model = load(matfile_stage);
            options = struct('overwrite' , true);
            saveastiff(single(model.avgPoi), fullfile(locmultipagedir, [basename '.tif']), options);
            avgPoi_N = model.avgPoi*dim(1)*dim(2)*dim(3)*Na;
            
            % concentrations
            maxAvgPoi = max(model.avgPoi, [], 3);
            sumAvgPoi = sum(model.avgPoi, 3);
            meanAvgPoi = mean(model.avgPoi, 3);
            midAvgPoi = model.avgPoi(:,:,round(size(model.avgPoi,3)/2));
            saveastiff(single(maxAvgPoi), fullfile(loczprojdir, [basename '_max_C_nM.tif']), options);
            saveastiff(single(sumAvgPoi), fullfile(loczprojdir, [basename '_sum_C_nM.tif']), options);
            saveastiff(single(meanAvgPoi), fullfile(loczprojdir, [basename '_mean_C_nM.tif']), options);
            saveastiff(single(midAvgPoi), fullfile(loczprojdir, [basename '_mid_C_nM.tif']), options);
            
            % protein number
            maxAvgPoi = max(avgPoi_N, [], 3);
            meanAvgPoi = mean(avgPoi_N, 3);
            sumAvgPoi = sum(avgPoi_N, 3);
            midAvgPoi = avgPoi_N(:,:,round(size(model.avgPoi,3)/2));
            
            saveastiff(single(maxAvgPoi), fullfile(loczprojdir, [basename '_max_N_voxel.tif']), options);
            saveastiff(single(sumAvgPoi), fullfile(loczprojdir, [basename '_sum_N_voxel.tif']), options);
            saveastiff(single(meanAvgPoi), fullfile(loczprojdir, [basename '_mean_N_voxel.tif']), options);
            saveastiff(single(midAvgPoi), fullfile(loczprojdir, [basename '_mid_N_voxel.tif']), options);
            
        end
    end
end
end



