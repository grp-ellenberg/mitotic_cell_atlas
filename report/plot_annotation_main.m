clear all;
close all;
fullname = 0;
poilistAllSacs = {'APC2', 'BUBR1' 'CEP192', 'CEP250'};
poilistAllNups = {'NUP107', 'NUP214', 'RANBP2', 'TPR'};
poilistAllCohs = {'CTCF', 'SCC1', 'STAG1', 'STAG2', 'WAPL'};
poilistAll = {'APC2', 'BUBR1', 'CEP192', 'CEP250', 'CTCF', 'NUP107', 'NUP214', 'RANBP2', 'SCC1', 'STAG1', 'STAG2', 'TPR',  'WAPL'};
poilistMixed = {'BUBR1', 'STAG1', 'NUP107'};

feature_summary = 'Z:\AntonioP_elltier1\CelllinesImaging\MitoSysPipelines\Summary_feature_150507july_smooth.txt';
annotation_file = 'Z:\AntonioP_elltier1\CelllinesImaging\MitoSysPipelines\Pattern_model\poi_subcellular_distribution.mat';
timevec = 1:20;
featurevec = 1:87;
savedirAll = 'C:\Research Materials\Presentation\Results_Common\Figures_AntonioDataset\All\';
savedirAllSacs = 'C:\Research Materials\Presentation\Results_Common\Figures_AntonioDataset\AllSacs\';
savedirAllNups = 'C:\Research Materials\Presentation\Results_Common\Figures_AntonioDataset\AllNups\';
savedirAllCohs = 'C:\Research Materials\Presentation\Results_Common\Figures_AntonioDataset\AllCohs\';
savedirMixed = 'C:\Research Materials\Presentation\Results_Common\Figures_AntonioDataset\Mixed\';

plot_annotation(fullname,poilistAllSacs,feature_summary,annotation_file,timevec,featurevec,savedirAllSacs);
% plot_annotation(fullname,poilistAllNups,feature_summary,annotation_file,timevec,featurevec,savedirAllNups);
% plot_annotation(fullname,poilistAllCohs,feature_summary,annotation_file,timevec,featurevec,savedirAllCohs);
% plot_annotation(fullname,poilistAll,feature_summary,annotation_file,timevec,featurevec,savedirAll);
%plot_annotation(fullname,poilistMixed,feature_summary,annotation_file,timevec,featurevec,savedirMixed);