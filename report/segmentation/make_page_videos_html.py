#!/usr/bin/env python
#
# make_page_segvis_html.py
# Create a html page with jpg/tif images found recursively in a directory
#
# Copyright A. Politi, 11.11.2016, modified 23.01.2017
# originally from a code from F. Nedelec

"""
Synopsis:

    Generates an HTML page linking images & movies found in sub-directories

Usage:
    
    make_page_segvis_html.py

"""

import sys, os, subprocess
import tkFileDialog
import csv

name_images   = ['index_images.html', 'index_images_toreprocess.html']
name_movies   = ['index_movies.html', 'index_movies_toreprocess.html']
outimg    = 0
outmov = 0
indx   = 1
imsize = ''
table  = 0
videowidth = 500
videoheight = videowidth*0.75

def writeHeader():
    global outimg, outmov
    for outi in outimg:
        outi.write('<!DOCTYPE html>')
        outi.write('<html>\n')
        outi.write('<head>\n')
        outi.write('<meta charset="utf-8">\n')
        outi.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">\n')
        outi.write('<meta name="viewport" content="width=device-width, initial-scale=1">\n')
        outi.write('<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n')
        outi.write('<title>ImageViewer</title>\n')
        outi.write('<!-- Bootstrap -->\n')
        outi.write('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">\n')
        outi.write('<style> .bottomMargin { margin-bottom: 50 px} </style>\n')     
        outi.write('</head>\n')
        outi.write('<body>\n')
        outi.write('<div class="container" id="main">\n')
        outi.write('<div class="panel-group" id = "accordion">\n')
    # Write header for movie file
    for outm in outmov:
        outm.write('<!DOCTYPE html>')
        outm.write('<html>\n')
        outm.write('<head>\n')
        outm.write('<meta charset="utf-8">\n')
        outm.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">\n')
        outm.write('<meta name="viewport" content="width=device-width, initial-scale=1">\n')
        outm.write('<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n')
        outm.write('<title>Mitosys Segmentation</title>\n')
        outm.write('<!-- Bootstrap -->\n')
        outm.write('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">\n')
        outm.write('<style> body { width:80%; margin-left:auto; margin-right:auto;}\n .bottomMargin { margin-bottom: 50 px}')
        outm.write('html {text-align: center;}\n</style>'); 
        outm.write('</head>\n')
        outm.write('<body>\n')


def writeFooter():
    global outimg, outmov
    for outi in outimg:
        outi.write('</div>\n')
        outi.write('</div>\n')
        outi.write('<!---- Footer ---->\n')
        outi.write("""<script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type='text/javascript' src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <!-- JavaScript jQuery code from Bootply.com editor     -->
        <script type='text/javascript'>
        $(document).ready(function() {
        $.getScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js',
        function(){$('#main .img-responsive').lazyload({});});})
        </script>\n""")
        outi.write('</body>\n')
        outi.write('</html>\n')
    # movie html
    for outm in outmov:
        outm.write('</body>\n')
        outm.write('</html>\n')


def getImageSize(file):
    """
    Call ffprobe, which is a tool that comes with ffmpeg,
    and parse output to extract size of videos or images
    """
    res = [256, 256];
    proc = subprocess.Popen(['ffprobe', '-v', 'quiet', '-show_streams', file], stdout=subprocess.PIPE)
    if not proc.wait():
        for line in proc.stdout:
            [key, equal, value] = line.partition('=')
            if key=="width":
                res[0] = int(value)
            elif key=="height":
                res[1] = int(value)
    return res


def writeImageLinks(paths):
    global outimg, imsize
    for outi in outimg:
        outi.write('<div class="panel-body">\n')
        spaths = sorted(paths)
        for path in sorted(paths):
            outi.write('<div class="col-md-6">\n')
            #out.write(' <img class="img-responsive" alt=".\%s"     data-original=".\%s"  width="1201" height="901">\n' % (path, path))
            outi.write(' <img class="img-responsive bottomMargin" alt=".\%s"     data-original=".\%s"  width="1284" height="1027">\n' % (path, path))
            outi.write('</div>\n')
        outi.write('</div>\n')

def writeMovieLinks(paths, movfile, qc = [-2]):
    global movsize, db, videowidth, videoheight
    spaths = sorted(paths)
    pathname = ['_'.join(ob[1].split('\\')) + '.mp4' for ob in db]
    for path in sorted(paths):
        idx_db = pathname.index(path[2:])
        if (int(db[idx_db][16]) > 0 and int(db[idx_db][17]) in qc):
            print 'add img'
            imgpath = os.path.dirname(db[idx_db][2]).replace('\\', '/')
            cellpath = db[idx_db][1].replace('\\', '/')
            movfile.write('<a href="../../Data/%s" target = "_blank" > %s </a> <a href = "../../Data/%s" target = "_blank" >   mat-files </a> <br>\n' % (imgpath, path[2:], cellpath))
            movfile.write('<video width="%d" height = "%d" controls preload="none">\n' % (videowidth, videoheight) )                
            #movfile.write('<object class="embed-responsive-item">\n<video width="%d" controls>\n' % videowidth)
            movfile.write('<source src="%s" type="video/mp4">\nYour browser does not support the video tag.' % path)
            movfile.write('</video>\n')
            #movfile.write('</object>\n')
            movfile.write('<br>\n')



def selectFiles(dirpath, directories, files):
    """
    Return files worth showing
    """
    images = []
    movies = []
    for f in files:
        path = os.path.join(dirpath, f)
        if f.endswith('.jpg'):
       # if f.endswith('.png') or f.endswith('.jpg') or f.endswith('.tif') or f.endswith('.jpg'):
            images.append(path)
        elif f.endswith('.mp4'):
            movies.append(path)
    return [images, movies]


def process(dirpath, directories, files):
    """
    Write HTML code for given directory
    """
    global outimg, indx, table, outmov
    dirname = dirpath.lstrip('./')
    [images, movies] = selectFiles(dirpath, directories, files)
    if len(movies) > 0:
        writeMovieLinks(movies, outmov[0], qc = [1,2])
        writeMovieLinks(movies, outmov[1], qc = [-2])
    if len(images) > 0:
        outimg.write('<div class="panel panel-default">\n')
        outimg.write('<div class="panel-heading">\n    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse%d">%s</a></h4>\n</div>\n' %(indx, dirname))
        outimg.write('<div id="collapse%d" class="panel-collapse collapse">\n' %(indx))
        writeImageLinks(images)
        outimg.write('</div>\n</div>\n')
        indx += 1



def process_dir(dirpath):
    """call process() with appropriate arguments"""
    global process_movies
    files = []
    directories = []
    dirList = os.listdir(dirpath)

    for f in os.listdir(dirpath):
        if os.path.isdir(os.path.join(dirpath, f)):
            directories.append(f)
        else:
            files.append(f)
    process(dirpath, directories, files)

#------------------------------------------------------------------------

def command(args):
    """generates HTML page"""
    global name, outimg, outmov, imsize, table, db
    dir_opt = {}
    dir_opt['title'] = 'Select directory to process'
    #paths = tkFileDialog.askdirectory(**dir_opt)
    
    paths = 'W:\\Antonio_t2_2\\U2OS_MitoSys\\Results_SegVis'
    database = 'W:\\Antonio_t2_2\\U2OS_MitoSys\\Results\\full_database.txt'
    db = list(csv.reader(open(database), delimiter = '\t'))
     
    os.chdir(paths)
    try:
        outimg = [open(name_images[0], 'w'), open(name_images[1], 'w')] 
        outmov = [open(name_movies[0], 'w'), open(name_movies[1], 'w')]
    except Exception as e:
        err.write("Error creating file `%s': %s\n" % (path, e));
    
    writeHeader()
    dirNames = os.listdir('.')
    # sort names alphabetically
    dirNames.sort()
    process_dir('.')
    #for dirN in dirNames:
    #    if os.path.isdir(dirN):
    #        process_dir(dirN)
    writeFooter()
    for outi in outimg:
        outi.close()
    for outm in outmov:
        outm.close()
    print("generated '%s' with %i entries" % (name_images, indx-1))
    print("generated '%s'" % (name_movies))



#------------------------------------------------------------------------

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1]=='help':
        print(__doc__)
    else:
        command(sys.argv[1:])




