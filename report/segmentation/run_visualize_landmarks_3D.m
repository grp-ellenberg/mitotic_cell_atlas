%% run visualize_landmarks_3D data in database

opt = struct( ...
    'outdir', 'V:\MitoSys_data\Results\Seg_Vis', ...
    'reprocess', 0, ... % force reprocessing
    'framerate', 4);  % frame rate of video

exp_dir = 'V:\MitoSys_data\Data';
exp_database = 'full_database.txt';
voxelSize = [0.25, 0.25, 0.75];
cellsIdx = [1563:1573];
data_base = readtable(fullfile(exp_dir, exp_database), 'Delimiter','\t');
idx_cells = (data_base.manualQC > 0).*(data_base.segmentation > 0).*(~strcmp(data_base.poi, 'genH2B'));
idx_cells = cellsIdx(idx_cells(cellsIdx)>0);


for idx = idx_cells
    pathtocell = fullfile(exp_dir, data_base.filepath{idx});
    display(sprintf('Visualize 3D %s', pathtocell));
    visualize_landmarks_3D(pathtocell, voxelSize, opt);
end


%% create file for cells that have been only partially segmented
opt = struct( ...
    'outdir', 'V:\MitoSys_data\Results\Seg_Vis_PartiallyPassed', ...
    'framerate', 4, ...
    'reprocess', 0); % force reprocessing

idx_cells = (data_base.manualQC > 0).*(data_base.segmentation <= 0).*(~strcmp(data_base.poi, 'genH2B'));

idx_cells = cellsIdx(idx_cells(cellsIdx)>0);

for idx = idx_cells
    pathtocell = fullfile(exp_dir, data_base.filepath{idx});
    display(sprintf('Visualize 3D %s', pathtocell));
    visualize_landmarks_3D(pathtocell, voxelSize, opt);
end