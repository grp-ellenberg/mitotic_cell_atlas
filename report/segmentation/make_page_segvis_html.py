#!/usr/bin/env python
#
# make_page_segvis_html.py
# Create a html page with jpg/tif images found recursively in a directory
#
# Copyright A. Politi, 11.11.2016, modified 23.01.2017
# originally from a code from F. Nedelec

"""
Synopsis:

    Generates an HTML page linking images & movies found in sub-directories

Usage:
    
    make_page_segvis_html.py

"""

import sys, os, subprocess
import tkFileDialog

name   = 'page.html'
out    = 0
indx   = 1
imsize = ''
table  = 0


def writeHeader():
    global out;
    out.write('<html>\n')
    out.write('<head>\n')
    out.write('<meta charset="utf-8">\n')
    out.write('<meta http-equiv="X-UA-Compatible" content="IE=edge">\n')
    out.write('<meta name="viewport" content="width=device-width, initial-scale=1">\n')
    out.write('<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->\n')
    out.write('<title>ImageViewer</title>\n')
    out.write('<!-- Bootstrap -->\n')
    out.write('<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">\n')
    out.write('<style> .bottomMargin { margin-bottom: 50 px} </style>\n')    
    out.write('</head>\n')
    out.write('<body>\n')
    out.write('<div class="container" id="main">\n')
    out.write('<div class="panel-group" id = "accordion">\n')

def writeFooter():
    global out;
    out.write('</div>\n')
    out.write('</div>\n')
    out.write('<!---- Footer ---->\n')
    out.write("""<script type='text/javascript' src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type='text/javascript' src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- JavaScript jQuery code from Bootply.com editor  -->
    <script type='text/javascript'>
    $(document).ready(function() {
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js',
    function(){$('#main .img-responsive').lazyload({});});})
    </script>\n""")
    out.write('</body>\n')
    out.write('</html>\n')


def getImageSize(file):
    """
    Call ffprobe, which is a tool that comes with ffmpeg,
    and parse output to extract size of videos or images
    """
    res = [256, 256];
    proc = subprocess.Popen(['ffprobe', '-v', 'quiet', '-show_streams', file], stdout=subprocess.PIPE)
    if not proc.wait():
        for line in proc.stdout:
            [key, equal, value] = line.partition('=')
            if key=="width":
                res[0] = int(value)
            elif key=="height":
                res[1] = int(value)
    return res


def writeImageLinks(paths):
    global out, imsize
    out.write('<div class="panel-body">\n')
    spaths = sorted(paths)
    for path in sorted(paths):
        out.write('<div class="col-md-6">\n')
        
        #out.write(' <img class="img-responsive" alt=".\%s"  data-original=".\%s"  width="1201" height="901">\n' % (path, path))
        out.write(' <img class="img-responsive bottomMargin" alt=".\%s"  data-original=".\%s"  width="1284" height="1027">\n' % (path, path))
        out.write('</div>\n')
    out.write('</div>\n')


def writeMovieLinks(paths):
    global out
    for path in sorted(paths):
        size = getImageSize(path)
        out.write('<video controls="controls" width="%s" height="%s" loop="true" alt="%s">\n' % (size[0], size[1], path));
        out.write('  <source src="%s" type="video/mp4">\n' % path);
        out.write('  Your browser does not support the HTML5 <video> element.\n');
        out.write('</video>\n');
    if paths:
        out.write('<br>\n')


def selectFiles(dirpath, directories, files):
    """
    Return files worth showing
    """
    images = []
    movies = []
    for f in files:
        path = os.path.join(dirpath, f)
        if f.endswith('.jpg'):
       # if f.endswith('.png') or f.endswith('.jpg') or f.endswith('.tif') or f.endswith('.jpg'):
            images.append(path)
        elif f.endswith('.mp4'):
            movies.append(path)
    if 'images' in directories:
        path = os.path.join(dirpath, 'images')
        [i, m] = selectFiles(path, [], os.listdir(path))
        images.extend(i)
    return [images, movies]


def process(dirpath, directories, files):
    """
    Write HTML code for given directory
    """
    global out, indx, table
    dirname = dirpath.lstrip('./')
    [images, movies] = selectFiles(dirpath, directories, files)
    if len(images) > 0:
        out.write('<div class="panel panel-default">\n')
        out.write('<div class="panel-heading">\n    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapse%d">%s</a></h4>\n</div>\n' %(indx, dirname))
        [images, movies] = selectFiles(dirpath, directories, files)
        out.write('<div id="collapse%d" class="panel-collapse collapse">\n' %(indx))
        writeImageLinks(images)
        out.write('</div>\n</div>\n')
        indx += 1
    


def process_dir(dirpath):
    """call process() with appropriate arguments"""
    files = []
    directories = []
    dirList = os.listdir(dirpath)
    for f in os.listdir(dirpath):
        if os.path.isdir(os.path.join(dirpath, f)):
            directories.append(f)
        else:
            files.append(f)
    process(dirpath, directories, files)

#------------------------------------------------------------------------

def command(args):
    """generates HTML page"""
    global name, out, imsize, table
    dir_opt = {}
    dir_opt['title'] = 'Select Seg_Vis directory'
    paths = tkFileDialog.askdirectory(**dir_opt)
    os.chdir(paths)
    try:
        out = open(name, 'w')
    except Exception as e:
        err.write("Error creating file `%s': %s\n" % (path, e));
    
    writeHeader()
    dirNames = os.listdir('.')
    # sort names alphabetically
    dirNames.sort()
    for dirN in dirNames:
        if os.path.isdir(dirN):
            process_dir(dirN)
    writeFooter()
    out.close()
    print("generated '%s' with %i entries" % (name, indx-1))



#------------------------------------------------------------------------

if __name__ == "__main__":
    if len(sys.argv) > 1 and sys.argv[1]=='help':
        print(__doc__)
    else:
        command(sys.argv[1:])




