
expDatabaseFilename = 'full_database.txt';
expDir = 'Z:\Nike\MitoSysCondensins\';
outRootDir = 'Z:\Nike\MitoSysCondensins\';
expDatabase = readtable([expDir filesep expDatabaseFilename],'Delimiter','\t');
numCells = size(expDatabase,1);
processFlag = 3; %1:xy, 2:yz and 3:both
%for cellIdx = 34:-1:32
for cellIdx = numCells:-1:1
    if expDatabase.segmentation(cellIdx) == 1
        curProcessedSystem = [expDatabase.filepath{cellIdx} num2str(expDatabase.system(1))];
        if processFlag == 1 || processFlag == 3
            lv_visualize_segmentation_results_xy([expDir expDatabase.filepath{cellIdx} '\Preprocessing\Segmentation\'],  outRootDir, expDir, 0, 80);
        end
        if processFlag == 2|| processFlag == 3
            lv_visualize_segmentation_results_yz([expDir expDatabase.filepath{cellIdx} '\Preprocessing\Segmentation\'],  outRootDir, expDir, 0, 80);
        end   
    end
end


