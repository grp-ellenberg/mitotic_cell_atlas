function lv_visualize_segmentation_results_yz(curCellDir, fpOutRoot, expDir, overWrite, tMax)
% This function displays the results of segmentation for validation yz
% section
% Julius Hossain 
% Created on 2015_01_16
% last update 2017_01_19 make figure display in background


fpOut = curCellDir(length(expDir)+1:end-27);
fpOut(fpOut == filesep) = '_';
fpOut = [fpOutRoot 'Seg_Vis\' fpOut];
fpOut = [fpOut(1:end-22) fpOut(end-14:end)];
fpOut(end) = '\';
disp(['Processing:' fpOut]);
if ~exist(curCellDir, 'dir')
    disp('Input directory is missing');
    return
else
    if ~exist(fpOut, 'dir')
        mkdir(fpOut);
    else
        fn = dir([fpOut '*.tif']);
        if overWrite == 0 && length(fn) == tMax
            return;
        end
    end
end

fn = dir([curCellDir '*.mat']);
if length(fn) == 0
    return;
end

voxelSizeX = 0.25;
voxelSizeY = 0.25;
voxelSizeZ = 0.75;
try
    tpoint = 1;
    while tpoint <=length(fn)
        curMat = load([curCellDir fn(tpoint).name]);
        xIdx = round(curMat.chrCentMic(1)/voxelSizeX);
        yIdx = round(curMat.chrCentMic(2)/voxelSizeY);
        %zIdx = round(curMat.chrCentMic(3)/voxelSizeZ);
        xShift = 128 - xIdx;
        yShift = 128 - yIdx;
        
        nucVolume = fn_RG_ImageTranslate_XY(curMat.nucVolume, [xShift yShift]);
        cellVolume = fn_RG_ImageTranslate_XY(curMat.cellVolume, [xShift yShift]);
        nuc = fn_RG_ImageTranslate_XY(curMat.nuc, [xShift yShift]);
        neg = fn_RG_ImageTranslate_XY(curMat.neg, [xShift yShift]);
        
        rot = -1*atan(curMat.cellAxis(2)/curMat.cellAxis(1))*180/pi ;
        
        for i = 1: size(curMat.nuc, 3)
            %chr = curMat.nucVolume(:,:,i);
            nucVolume(:,:,i) = imrotate(nucVolume(:,:,i), rot, 'bicubic', 'crop');
            cellVolume(:,:,i) = imrotate(cellVolume(:,:,i), rot, 'bicubic', 'crop');
            nuc(:,:,i) = imrotate(nuc(:,:,i), rot, 'bicubic', 'crop');
            neg(:,:,i) = imrotate(neg(:,:,i), rot, 'bicubic', 'crop');
        end
        
        nucVolume = permute(nucVolume, [3 1 2]);
        cellVolume = permute(cellVolume, [3 1 2]);
        nuc = permute(nuc, [3 1 2]);
        neg = permute(neg, [3 1 2]);
        
        zIdx = 128;
        chr = nucVolume(:,:,zIdx);
        chr = flipud(chr);
        cell = cellVolume(:,:,zIdx);
        cell = flipud(cell);
        maxImage = max(nuc(:,:,zIdx), neg(:,:,zIdx)*3);
        maxImage = flipud(maxImage);
        figHand = figure('Name','Displaying Segmentation Results','NumberTitle','off', 'Visible', 'off');
        imagesc(maxImage)
        chrBoun = bwboundaries(chr);
        
        hold on
        for k = 1:length(chrBoun)
            boundary = chrBoun{k};
            plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
        end
        
        cellBoun = bwboundaries(cell);
        
        hold on
        for k = 1:length(cellBoun)
            boundary = cellBoun{k};
            plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2);
        end
        
        axis image
        saveas(figHand, [fpOut fn(tpoint).name(1:end-4) '_yz.jpg'], 'jpg');
        close all;
        tpoint = tpoint + 1;
    end
catch
    disp('Neg Problem');
end
end