function lv_visualize_segmentation_results_xy(curCellDir, fpOutRoot, expDir, overWrite, tMax)
% This function displays the results of segmentation for validation xy
% section
% Julius Hossain 
% Created on 2015_01_16
% last update 2017_01_19 make figure display in background
% curCellDir = 'C:\Research Materials\Data\Mitosys\ProofOFConcept\140423_MIS12\MitoSys2\Result3\cell0015_R0019\';
disp(curCellDir);
fpOut = curCellDir(length(expDir)+1:end-27);
fpOut(fpOut == filesep) = '_';
fpOut = [fpOutRoot 'Seg_Vis\' fpOut];
fpOut = [fpOut(1:end-22) fpOut(end-14:end)];
fpOut(end) = '\';

if ~exist(curCellDir, 'dir')
    disp('Input directory is missing');
    return
else
    if ~exist(fpOut, 'dir')
        mkdir(fpOut);
    else
        fn = dir([fpOut '*.tif']);
        if overWrite == 0 && length(fn) == tMax
            return;
        end
    end
end
    
fn = dir([curCellDir '*.mat']);
if length(fn) == 0
    return;
end
voxelSizeZ = 0.75;
try
tpoint = 1;
while tpoint <=length(fn)
    curMat = load([curCellDir fn(tpoint).name]);
%     s  = regionprops(curMat.nucVolume, 'centroid');
%     centroids = cat(1, s.Centroid);
%     if size(centroids,1) == 1   
%         zIdx = floor(centroids(1,3));
%     else
%         zIdx = floor((centroids(1,3) + centroids(2,3))/2);
%     end
    zIdx = round(curMat.chrCentMic(3)/voxelSizeZ);
    chr = curMat.nucVolume(:,:,zIdx);
    cell = curMat.cellVolume(:,:,zIdx);
    
    maxImage = max(curMat.nuc(:,:,zIdx), curMat.neg(:,:,zIdx)*3);

    figHand = figure('Name','Displaying Segmentation Results','NumberTitle','off', 'Visible', 'off');
    imagesc(maxImage)
    chrBoun = bwboundaries(chr);

    hold on
    for k = 1:length(chrBoun)
        boundary = chrBoun{k};
        plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
    end

    cellBoun = bwboundaries(cell);

    hold on
    for k = 1:length(cellBoun)
        boundary = cellBoun{k};
        plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 2)
    end

    hold on
    plot([curMat.isectPointsPix(1,2) curMat.isectPointsPix(2,2)], [curMat.isectPointsPix(1,1) curMat.isectPointsPix(2,1)], 'r', 'LineWidth', 2);

    midBoun = bwboundaries(curMat.midPlane(:,:,zIdx));
    hold on
    for k = 1:length(midBoun)
        boundary = midBoun{k};
        plot(boundary(:,2), boundary(:,1), 'y', 'LineWidth', 1)
    end
    
    saveas(figHand, [fpOut fn(tpoint).name(1:end-4) '_xy.jpg'], 'jpg');
    close all;    
%     cRegionProj = sum(curMat.cellVolume, 3);
%     imwrite(uint16(cRegionProj),[fpOut 'Proj1_' fn(tpoint).name(1:end-4) '.tif'], 'tif');
    tpoint = tpoint + 1;
end
catch
    disp('Neg Problem');
end
end

