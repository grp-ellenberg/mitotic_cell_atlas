function [Rotated newcenter center_input] = yc_imrotate3d(A,angle,center_input,center_output)

% Using this function, images can be rotated conterclockwise for "angle"
% degree with "center_input" as fixed point in xy plane. In the output image,
% "center_input" will located on the position "center_output". The output
% image will have the same size as the input image. If the "center_output" 
% is not given the function will try to find the best location where the
% signal is minimum cut out and image as close to the center as possible.

imagesize = size(A);
if length(imagesize) < 3;
    imagesize = [imagesize 1];
end

% Extend the image so that the center_input is located at the image center
if center_input(1) < (imagesize(1)+1)/2;
    num_addrow = imagesize(1)-2*center_input(1)+1;
    A = [zeros(num_addrow,imagesize(2),imagesize(3));A];
    center_input(1) = center_input(1)+num_addrow;
else
    num_addrow = -imagesize(1)+2*center_input(1)-1;
    A = [A;zeros(num_addrow,imagesize(2),imagesize(3))];
end

temp_imagesize = size(A);
if length(temp_imagesize)<3;
    temp_imagesize = [temp_imagesize 1];
end
if center_input(2) < (imagesize(2)+1)/2;
    num_addcol = imagesize(2)-2*center_input(2)+1;
    A = [zeros(temp_imagesize(1),num_addcol,temp_imagesize(3)) A];
    center_input(2) = center_input(2)+num_addcol;
else
    num_addcol = -imagesize(2)+2*center_input(2)-1;
    A = [A zeros(temp_imagesize(1),num_addcol,temp_imagesize(3))];
end

% Rotate the image using imrotate

Rot_A = imrotate(A,angle,'bilinear','loose');
temp_imagesize = size(Rot_A);
if length(temp_imagesize)<3;
    temp_imagesize = [temp_imagesize 1];
end
temp_cent = round((temp_imagesize+1)/2);

% In the case that center_output is given, move the center of the image to
% the center_output and crop the image into the size of the original image

if length(center_output) == 3;
    Rot_A_org = sum(sum(sum(Rot_A)));
    topdelete = temp_cent(1)-center_output(1);
    bottemdelete = temp_imagesize(1)-temp_cent(1)-imagesize(1)+center_output(1);
    leftdelete = temp_cent(2)-center_output(2);
    rightdelete = temp_imagesize(2)-temp_cent(2)-imagesize(2)+center_output(2);
    if topdelete >= 0;
        Rot_A(1:topdelete,:,:) = [];
    else
        Rot_A = [zeros(abs(topdelete),temp_imagesize(2),temp_imagesize(3));Rot_A];
    end
    center_input(1) = center_input(1)-topdelete;
    if bottemdelete >= 0;
        Rot_A(end-bottemdelete+1:end,:,:) = [];
    else
        Rot_A = [Rot_A;zeros(abs(bottemdelete),temp_imagesize(2),temp_imagesize(3))];
    end
    if leftdelete >= 0;
        Rot_A(:,1:leftdelete,:) = [];
    else
        Rot_A = [zeros(imagesize(1),abs(leftdelete),imagesize(3)) Rot_A];
    end
    center_input(2) = center_input(2)-leftdelete;
    if rightdelete >= 0;
        Rot_A(:,end-rightdelete+1:end,:) = [];
    else
        Rot_A = [Rot_A zeros(imagesize(1),abs(rightdelete),imagesize(3))];
    end
    if sum(sum(sum(Rot_A)))~=Rot_A_org;
        warning('rotated image extended the size of the original image')
    end
    if center_output(3)~=center_input(3);
        error('the center of input and output has to be the same in z-direction')
    end
elseif isempty(center_output); % Automatic searching of best cent
    center_output = [0 0 center_input(3)];
    Rot_A_mid = Rot_A(:,:,center_input(3));
    signalrows = find(sum(Rot_A_mid,2)>0);
    signalrows = [signalrows(1) signalrows(end)];
    num_rows = signalrows(2)-signalrows(1)+1;
    signalcols = find(sum(Rot_A_mid,1)>0);
    signalcols = [signalcols(1) signalcols(end)];
    num_cols = signalcols(2) - signalcols(1)+1;
    if num_rows <= imagesize(1);
        addrows_top = round((imagesize(1)-num_rows)/2);
        addrows_buttom = imagesize(1)-num_rows-addrows_top;
        row1 = signalrows(1)-addrows_top;
        rowend = signalrows(2)+addrows_buttom;
        if row1>=1 && rowend <= temp_imagesize(1);
            Rot_A = Rot_A(row1:rowend,:,:);
            center_input(1) = center_input(1)-row1+1;
        elseif row1 < 1;
            Rot_A = Rot_A(1:imagesize(1),:,:);
        elseif rowend > temp_imagesize(1);
            Rot_A = Rot_A(end-imagesize(1)+1:end,:,:);
            center_input(1) = center_input(1)-size(Rot_A,1)+imagesize(1);
        end
        center_output(1) = temp_cent(1)-signalrows(1)+addrows_top+1;
    else
        warning('rotated image extended the size of the original image')
        delrows_top = round((num_rows-imagesize(1))/2);
        delrows_bottum = num_rows-delrows_top-imagesize(1);
        Rot_A = Rot_A(signalrows(1)+delrows_top:signalrows(2)-delrows_bottum,:,:);
        center_output(1) = temp_cent(1)-signalrows(1)-delrows_top+1;
    end
    if num_cols <= imagesize(2);
        addcols_top = round((imagesize(2)-num_cols)/2);
        addcols_buttom = imagesize(2)-num_cols-addcols_top;
        col1 = signalcols(1)-addcols_top;
        colend = signalcols(2)+addcols_buttom;
        if col1>=1 && colend <= temp_imagesize(2);
            Rot_A = Rot_A(:,col1:colend,:);
            center_input(2) = center_input(2)-col1+1;
        elseif col1<1
            Rot_A = Rot_A(:,1:imagesize(2),:);
        elseif colend > temp_imagesize(2);
            Rot_A = Rot_A(:,end-imagesize(2)+1:end,:);
            center_input(2) = center_input(2)-size(Rot_A,2)+imagesize(2);
        end
        center_output(2) = temp_cent(2)-signalcols(1)+addcols_top+1;
    else
        warning('rotated image extended the size of the original image')
        delcols_top = round((num_cols-imagesize(2))/2);
        delcols_bottum = num_cols-delcols_top-imagesize(2);
        Rot_A = Rot_A(:,signalcols(1)+delcols_top:signalcols(2)-delcols_bottum,:);
        center_output(2) = temp_cent(2)-signalcols(1)-delcols_top+1;
    end
    
elseif ~isempty(center_output);
    error('center_output must be empty or a 3-dimension coordinate')
end

Rotated = Rot_A;
newcenter = center_output;

end