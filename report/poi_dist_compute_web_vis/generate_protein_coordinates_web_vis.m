%This fucntion converts the probability map to coordinates with intensity
%for visualization on the webpage
%Create 2016-02-16
%Modified: 2018-01-24
%Units: concentration in nanoMolar

clc;
clear all;
close all;
%fp = 'C:\Research Materials\Presentation\Results_Common\ProbMap_AntonioDataset_Conc_nanoMoles_per_liter\';
fp = 'W:\MitoSys_data\HeLaKyoto_MitoSys\Results_avg_poi_web_2018_07_09\Avg_con_nM_pL_3d_stack_mat\';
outDirRoot = 'W:\MitoSys_data\HeLaKyoto_MitoSys\Results_avg_poi_web_2018_07_09\Avg_con_nM_pL_coordinates_tab_delimited\';
protDir = dir(fp);
for pIdx = 3:length(protDir)
    curProtDir = [fp protDir(pIdx).name filesep];
    idx = find(curProtDir == filesep, 2, 'last');
    outDir = [outDirRoot curProtDir(idx(1)+1:idx(2))];
    if ~exist(outDir)
        mkdir(outDir);
    end
    fn = dir([curProtDir '*.mat']);
    for i=1:length(fn)
        curProtMat = load([curProtDir fn(i).name]);
        avgPoi = curProtMat.avgPoi; % 200(saved multiple)
        avgPoiRegion = double(avgPoi>0);

        threeDLabel = bwconncomp(avgPoiRegion,18);
        numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
        [x,y,z] = ind2sub(size(avgPoi),threeDLabel.PixelIdxList{1});
        int = avgPoi(threeDLabel.PixelIdxList{1});
        savefile = [outDir fn(i).name];
        numPix = length(x);
        %save(savefile, 'x', 'y', 'z', 'int', 'numPix');
        avg_conc_nM_pL = int;
        savefile = [outDir fn(i).name(1:end-4) '.txt'];
        tableParams = table(x, y, z, avg_conc_nM_pL);
        writetable(tableParams,savefile, 'Delimiter','\t');
    end
end