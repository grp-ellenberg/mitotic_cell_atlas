%function change_dir_sturcture(root_dir, sub_dir_name)
%This function converts the isotropic segmenated/registered data from 
%previous format (which is mainly directory structure) to current format of
%the data that could be processed by the current version of the manuscript.
in_root_dir  = 'W:\MitoSys_data\HeLaKyoto_MitoSys\Results_Iso';
%out_root_dir = 'W:\MitoSys_data\HeLaKyoto_MitoSys\Results_Iso';
in_root_dir = fullfile(in_root_dir, filesep);
%sub_dir_name = 'Registration'; %This should be either Segmentation or Registration
dir_file_list = dir(in_root_dir);
dir_list_all = dir_file_list([dir_file_list.isdir]);
dir_list = dir_list_all(3:end);



%Get flat directory names of all cells and organize it on the prescribed
%directory structure needed for the current pipeline
for exp_idx = 1:length(dir_list)
    cur_dir_name = dir_list(exp_idx).name;
    
    dir_list_seg_reg = dir(fullfile(in_root_dir, cur_dir_name));
    dir_list_seg_reg = dir_list_seg_reg([dir_list_seg_reg.isdir]);
    dir_list_seg_reg = dir_list_seg_reg(3:end);
    
    for seg_reg_idx = 1:length(dir_list_seg_reg)
        
        dir_list_cell = dir(fullfile(in_root_dir, cur_dir_name,dir_list_seg_reg(seg_reg_idx).name));
        dir_list_cell = dir_list_cell([dir_list_cell.isdir]);
        dir_list_cell = dir_list_cell(3:end);
        
        for cell_idx = 1:length(dir_list_cell)
            new_cell_dir = fullfile(in_root_dir, cur_dir_name, dir_list_cell(cell_idx).name, dir_list_seg_reg(seg_reg_idx).name);
            if ~exist (new_cell_dir)
                mkdir(new_cell_dir);
            end
            
            prev_cell_dir = fullfile(in_root_dir, cur_dir_name, dir_list_seg_reg(seg_reg_idx).name, dir_list_cell(cell_idx).name);
            
            file_dir_list = dir(prev_cell_dir);
            
            for file_idx = 3:length(file_dir_list)
                indiv_dir_file_fp = fullfile(prev_cell_dir, file_dir_list(file_idx).name);
                movefile(indiv_dir_file_fp,new_cell_dir);
            end
            rmdir(prev_cell_dir);
        end
        rmdir(fullfile(in_root_dir, cur_dir_name, dir_list_seg_reg(seg_reg_idx).name));
    end
end
