%function change_dir_sturcture(root_dir, sub_dir_name)
%This function converts the isotropic segmenated/registered data from 
%previous format (which is mainly directory structure) to current format of
%the data that could be processed by the current version of the manuscript.
in_root_dir  = 'Y:\Julius\Data\NewPipelineIsoSegmentation_Nike\Virtual_Registration';
out_root_dir = 'Y:\Julius\Data\NewPipelineIsoSegmentation_Nike\Results_iso';
in_root_dir = fullfile(in_root_dir, filesep);
sub_dir_name = 'Registration'; %This should be either Segmentation or Registration
dir_file_list = dir(in_root_dir);
dir_list_all = dir_file_list([dir_file_list.isdir]);
dir_list = dir_list_all(3:end);

%Read the metadata to copy it in subdirectory for ceach cell
fnMetaData = dir([in_root_dir '*meta*.txt']);
if isempty(fnMetaData)
    return;
end
%Get the metadata
metaData = readtable(fullfile(in_root_dir, fnMetaData(1).name), 'Delimiter','\t');

%Get flat directory names of all cells and organize it on the prescribed
%directory structure needed for the current pipeline
for i = 1:length(dir_list)
    cur_dir_name = dir_list(i).name;
    
    prev_cell_dir_name = fullfile(in_root_dir,cur_dir_name);
    fs_idx = find(cur_dir_name == '_', 4, 'last');
    if length(fs_idx)<4
        continue;
    end
    cur_cell_name = cur_dir_name(fs_idx(3)+1:end);
    cur_cell_dir_name = fullfile(out_root_dir, cur_dir_name(1:fs_idx(3)-1), sub_dir_name, cur_cell_name);
    
    if ~exist(cur_cell_dir_name)
        mkdir(cur_cell_dir_name);
    end
    
    file_dir_list = dir(prev_cell_dir_name);
    
    for file_idx = 3:length(file_dir_list)
        indiv_dir_file_fp = fullfile(prev_cell_dir_name, file_dir_list(file_idx).name);
        movefile(indiv_dir_file_fp,cur_cell_dir_name);
    end
    writetable(metaData, fullfile(cur_cell_dir_name, [cur_cell_name '_meta.txt']), 'Delimiter','\t');
    file_dir_list = dir(prev_cell_dir_name);
    if length(file_dir_list) < 3
        rmdir(prev_cell_dir_name);
    end
end
