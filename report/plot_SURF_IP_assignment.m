function plot_SURF_IP_assignment(celldir,timevec,surfdir,assigndir,savedir)

% Author: Yin Cai
% Date: 27.06.2016

% This function can be used to generate figures for illustrating the
% feature extraction for a particular cell at multiple time points given in
% "timevec". Three output images are plotted for each image frame:

% 1. The SURF detector before and after IP selection step (Figure 4a)
% 2. The position and the zoomed and croped image of the first 18 selected
% SURF IPs, can be used for e.g. Figure 4a and S10
% 3. The Bag of Words (final feature vector of the cell) vector for the
% cell as bar plot (Figure 5a).

% celldir: the result directory of the cell of interest
% timevec: a vector of all time points need to be plotted
% surfdir: the name of the directory saving the poi_feature_extraction
% results
% assigndir: the name of the directory saving the results of the SURF IP
% assignments
% savedir: output dir to save the images

surf_dir = fullfile(celldir,surfdir);
BoW_dir = fullfile(celldir,assigndir);
cell_seq = dir(fullfile(surf_dir,'*T0*.mat'));

if exist(BoW_dir,'dir');
    for tdx = 1:length(timevec);
        
        %% Load the IP data
        N = load(fullfile(BoW_dir,cell_seq(timevec(tdx)).name));
        M = load(fullfile(surf_dir,cell_seq(timevec(tdx)).name));
        
        maxProj = imresize(M.maxProj_low,M.feature_parameters.factor);
        
        %% Plot the SURF detection, reduced
        figure(1)
        subplot(1,2,1)
        imshow(maxProj)
        hold on
        plot(M.InterestPoints)
        title('SURF detected')
        subplot(1,2,2)
        imshow(maxProj)
        hold on
        plot(M.InterestPoints(M.flag>0))
        title('SURF reduced')
        
        figname = fullfile(savedir,['SURF_detection_T' num2str(timevec(tdx),'%02.f') '.fig']);
        savefig(1,figname)
        close(gcf)
        close all
        
        %% Plot the 18 SURF interest points in detail with the highest metric value
        ipidx = find(M.flag>0);
        if length(ipidx) > 18;
            l = 18;
        else
            l = length(ipidx);
        end
        
        for i = 1:l;
            plot_ipidx = ipidx(i);
            px = ceil(i/9);
            py = i-(px-1)*9;
            
            figure(2)
            subplot(4,9,py+9*2*(px==2))
            imshow(maxProj)
            hold on
            plot(M.InterestPoints(plot_ipidx))
            
            s = 6*M.InterestPoints(plot_ipidx).Scale;
            rs = round(s);
            I = maxProj(M.loc(plot_ipidx,2)+(-rs:rs),M.loc(plot_ipidx,1)+(-rs:rs));
            subplot(4,9,py+9+9*2*(px==2))
            imshow(I)
            title(['cl:' num2str(N.lib_idx(i)) ':' num2str(N.asscl(i)) ':' num2str(N.asscl2(i))])
        end
        figname = fullfile(savedir,['SURF_top18_T' num2str(timevec(tdx),'%02.f') '.fig']);
        savefig(2,figname)
        close(gcf)
        close all
        
        %% Plot the bag of words feature vector
        figure(3)
        bar(N.POI_Feats_int/(sum(N.POI_Feats_int)))
        figname = fullfile(savedir,['BoW_feature_T' num2str(timevec(tdx),'%02.f') '.fig']);
        savefig(3,figname)
        close(gcf)
        close all
        
    end
end

end