function plot_alignment(featmat,alignmat,modelseq,timeline,deviate,name,savefigdir)
% Author: Yin Cai
% Date: 2016.07
% This function is called during the generation of the mitotic time for
% plotting the alignment progression.

afilename = fullfile(savefigdir,['iter_' num2str(name) '.mat']);
save(afilename,'alignmat','modelseq','timeline');
close all

for cellidx = 1:size(alignmat,1);
    if sum(alignmat(cellidx,:)) > 0;
        plotseq = nan(size(featmat,1),size(alignmat,2));
        for atidx = 1:size(alignmat,2);
            if alignmat(cellidx,atidx) >= 1;
                plotseq(:,atidx) = featmat(:,alignmat(cellidx,atidx),cellidx);
            end
        end
        figure(1)
        hold on
        for pidx = 1:size(featmat,1);
            subplot(2,size(featmat,1)/2,pidx)
            hold on
            plot(timeline,plotseq(pidx,:)','LineStyle','-','Color',[224/255 224/255 224/255],'LineWidth',0.5)
        end
    end
end

figure(1)
for pidx = 1:size(featmat,1);
    subplot(2,size(featmat,1)/2,pidx)
    hold on
    m_deviate = modelseq(pidx,:)'-deviate(pidx,:)';
    p_deviate = modelseq(pidx,:)'+deviate(pidx,:)';
    plot(timeline,modelseq(pidx,:)','LineStyle','-','Color',[0 0 0],'LineWidth',3);
    plot(timeline,m_deviate,'LineStyle','--','Color',[0 0 0],'LineWidth',0.5);
    plot(timeline,p_deviate,'LineStyle','--','Color',[0 0 0],'LineWidth',0.5);
    if sum(alignmat(:,end)>0)==size(alignmat,1);
        xlabel('Imaging time [frame]');
    else
        xlabel('Aligned time [minute/time resolution]');
    end
    ylabel(['normalized feature no. ' num2str(pidx)]);
end
figname = fullfile(savefigdir,['iter_' num2str(name) '.fig']);
savefig(1,figname)
close(gcf)

end