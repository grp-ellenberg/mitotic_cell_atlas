# Jython ImageJ macro 
# make_project_image_montages imageJ jython plugin
# Macro recursively finds tif images in directory (generated by main_project_image.m maximal projection of stack 4D stack)
# tif files are expected to be 3 Channels single plane multi time point stacks 
# Macro finds best contrast for GFP using maximal time projection, generates 2 montages (with and without landmarks),  and save them as jpg
#	 filename_ch1.jpg: Ch1 (green, GFP)
#	filename_merged.jpg: Ch1 (green, GFP), Ch2 (magenta, Dextran), Ch3 (magenta, DNA)and save as jpg
# Project: MitoSys
# Author: Antonio Politi, EMBL, 2017

# imports 
import ij
import os
from ij import IJ
from glob import glob
from ij.plugin import HyperStackConverter
from ij.plugin import ZProjector, ChannelSplitter, MontageMaker


# directory to process
adirs = [u'V:\MitoSys_data\Results\Cell_Vis\maxProj_allTP']


ZP = ZProjector()
for adir in adirs:
	for dirName, subdirList, fileList in os.walk(adir):
		# find tif files
		tiffiles = glob(os.path.join(dirName, '*.tif'))
		if len(tiffiles) == 0:
			continue
		for tiffile in tiffiles:
			IJ.run("Close All", "")
			# load and convert to hyperstack channel, time
			imp = IJ.openImage(tiffile)
			imp = HyperStackConverter.toHyperStack(imp, 3, 1, int(imp.getDimensions()[3]/3), "default", "Color")
			# split channels
			chSp = ChannelSplitter()
			imageC = chSp.split(imp)
			# maximal projection of GFP channel in time to find best contrast
			ZP.setImage(imageC[0])
			ZP.setMethod(ZProjector.MAX_METHOD)
			ZP.doProjection()
			imgP = ZP.getProjection()
			IJ.run(imgP, "Enhance Contrast", "saturated=0.35")
			ZP.setImage(imageC[1])
			ZP.doProjection()
			imgD = ZP.getProjection()
			IJ.run(imgD, "Enhance Contrast", "saturated=0.35")
			print('%d\t%d\n'% (imgD.getDisplayRangeMin(),imgD.getDisplayRangeMax()))
			ZP.setImage(imageC[2])
			ZP.doProjection()
			imgO = ZP.getProjection()
			IJ.run(imgO, "Enhance Contrast", "saturated=0.35")
			# adapt contrast and color of original images
			imp.setC(1)
			IJ.run(imp, "Green", "")
			imp.setDisplayRange(imgP.getDisplayRangeMin(), imgP.getDisplayRangeMax())
			imp.setC(2)
			IJ.run(imp, "Magenta", "")
			imp.setDisplayRange(imgD.getDisplayRangeMin(), imgD.getDisplayRangeMax())
			imp.setC(3)
			IJ.run(imp, "Magenta", "")
			imp.setDisplayRange(imgO.getDisplayRangeMin(), imgO.getDisplayRangeMax())
			# create a montage
			imp.show()
			if imp.getNFrames() == 20:
				IJ.run(imp, "Make Montage...", "columns=5 rows=4 scale=1 border=1 font=20 label")
			else:
				IJ.run(imp, "Make Montage...", "columns=8 rows=5 scale=1 border=1 font=20 label")
			mont = IJ.getImage()
			mont.hide()
			IJ.run(mont, "Set Scale...", "distance=1 known=0.25 unit=um");
			IJ.run(mont, "Scale Bar...", "width=10 height=5 font=18 color=White background=None location=[Lower Right] bold overlay");
			
			# save montages
			mont.setActiveChannels("100")
			IJ.saveAs(mont, "Jpeg", os.path.splitext(tiffile)[0] + '_ch1.jpg')
			mont.setActiveChannels("110")
			IJ.saveAs(mont, "Jpeg",  os.path.splitext(tiffile)[0] + '_merged.jpg')
