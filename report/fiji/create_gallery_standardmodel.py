''' ImageJ/FiJi jython script
The function makegallery reads Z-projected standard model tif and creates a gallery from different MST
stages. The projected Z-standard model for each stage is a single page tif.
If several proteins are plotted it scales all images to the same max and min
value.
Projection have been generated with the matlab script create_tif_standardmodel.m
Usage:
    makegallery(cellNames = ['celllinename1', 'celllinename2'],
        wdir = 'YOUR INPUT DIRECTORY'
        stages = [9,11],
        minV = 25,
        maxV = 700,
        colorbarname = 'colorbar_NUPs.tif'
        outdir = 'YOUR OUTPUT DIRECTORY')
Author: Antonio Politi, November 2017
        modified January 2018
'''

import ij
from ij import IJ
import os
import sys
from loci.plugins import BF

def makegallery(cellNames, # list of POI names as in the wdir directory
         stages,    # list of stages from which  to make a montage
         wdir,      # Working directory that contains z-projected files
         outdir,    # output directory
         minV,         # minmal value for display
         maxV,         # maximal value for display
         colorbarname, # name of colorbar
         projtype = 'max_C_nM',  # suffix of file name representing the projection type
         LookUpTable = 'Fire'): # Look up table 
 
    wdirs = ['%s%s' % (wdir, cell) for cell in cellNames]
    # open one image to test
    fname = os.path.join(wdirs[0], 'AvgPoi_%03d_%s.tif' % (stages[0], projtype))
    img = IJ.openImage(fname)
    imgSize = img.getWidth()
    # The montage is made of smaller ROI
    roiwidth = 140
    # roi position
    roipos = int(imgSize/2-roiwidth/2)

    # load all images and find minima and then maxima
    minMax = getMinMaxImages(wdirs, stages, projtype)
    print(max([x[1] for x in minMax]))
    print(sum([x[0] for x in minMax])/float(len(minMax)))
    # create colorbar
    cb = IJ.createImage("Untitled", "32-bit black", 50, 145, 1)
    IJ.setMinAndMax(cb, minV, maxV);
    IJ.run(cb, LookUpTable, "");
    cb.setRoi(0,0,1,1)
    IJ.run(cb, "Calibration Bar...", "location=[At Selection] fill=White label=Black number=5 decimal=0 font=10 zoom=1 bold");
    cb = IJ.getImage()
    cb.deleteRoi()
    cb.show()
    IJ.saveAs(cb, "Tiff", os.path.join(outdir, colorbarname))
    # Create montage for each image
    for wdir in wdirs:
        IJ.run("Close All", "")
        # list of all images
        img = list()
        if os.path.exists(wdir):
            for ist, stage in enumerate(stages):
                fname = os.path.join(wdir, 'AvgPoi_%03d_%s.tif' % (stage, projtype))
                img.append(IJ.openImage(fname))
                IJ.run(img[ist], LookUpTable, "")
                IJ.setMinAndMax(img[ist], minV, maxV);
                img[ist].show()
            IJ.run(img[1], "Images to Stack", "name=Stack title=[] use")
            imp = IJ.getImage()
            IJ.run(imp, "Gamma...", "value=1 stack")
            IJ.run(imp, "Rotate 90 Degrees Left", "")
            imp.setRoi(roipos, roipos, roiwidth, roiwidth);
            imp2 = imp.duplicate();
            imp2.show()
            IJ.run(imp2, "Make Montage...", "columns=%d rows=1 scale=1 border=3" % len(stages));
            imp_montage = IJ.getImage()
            IJ.run(imp_montage, "Set Scale...", "distance=4 known=1 pixel=1 unit=um")
            IJ.run(imp_montage, "Scale Bar...", "width=10 height=4 font=14 color=White background=None location=[Lower Right] bold hide")
            outname = os.path.join(outdir, "%s_%s.tif" %(os.path.basename(wdir),'_'.join(str(stg) for stg in stages)))
            outname = os.path.join(outdir, "%s.tif" %(os.path.basename(wdir)))
            IJ.run(imp_montage, "RGB Color", "")
            IJ.saveAs(imp_montage, "Tiff", outname)

def getMinMaxImage(img):
    '''
    get the min and max value for an image. img is imagePlus class
    '''
    stats = img.getStatistics();
    arr = img.getProcessor().getFloatArray()
    minV = 10000
    for i in range(0, img.getWidth()):
        for j in range(0, img.getHeight()):
            # want to have value higher than 0 stats.min considers also value = 0
            if ( arr[i][j] > 0 and arr[i][j] < minV ):
                minV = arr[i][j]

    return([minV, stats.max])


def getMinMaxImages(wdirs, stages, projtype):
    # find the range of min values and maximal values
    minMax = list()
    for wdir in wdirs:
        if os.path.exists(wdir):
            for ist, stage in enumerate(stages):
                fname = os.path.join(wdir, 'AvgPoi_%03d_%s.tif' % (stage, projtype))
                img = IJ.openImage(fname)
                minMax.append(getMinMaxImage(img))
    return minMax



