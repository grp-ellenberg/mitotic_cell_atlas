function generate_MST(gen_opt, parameters)
%% GENERATE_MST generates a new model for the mitotic standard time.
% GENERATE_MST() Use default options for paths and parameters
% GENERATE_MST(gen_opt) Specify in struct gen_opt the paths. Use default parameters
% GENERATE_MST(gen_opt, parameters) Spcify in struct options the processing parameters
% 
% Uses a number of mitotic high resolution image sequences, and aligns 
% the landmark feature profiles in time. Calculates the
% averaged mitotic progression in the corresponding feature space.
% Afterwards, the function further sample the averaged profile with
% homogeneous temporal distribution and identify mitotic transitions.
% 
% INPUT:
%   * gen_opt - A struct
%   * parameters - A struct
% OUTPUT: 
%  
% Author: Yin Cai, EMBL, 2016.5.25
% Updates: 
%   2017.09.13: A. Politi, variable uuid for unique identifier. Comments. Default options. Reusage of old filepaths. 
%   2018-03-19: A. Politi, structure as options for file paths

%% Default options 
[gen_opt_default, parameters_default] = get_defaults();


if nargin < 1
    gen_opt = gen_opt_default;
end

if nargin < 2
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('generate_MST: First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('generate_MST: Second argument options does not contain the expected field(s)s\n%s \n', ...
        strjoin(fieldnames(parameters_default)));
end

[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

%% check if data_filepath list exist and contains all relevant entries
file_cellpaths = fullfile(gen_opt.mst_dir, parameters.cellpaths_fname);
assert(exist(file_cellpaths, 'file') > 0, 'No files of cells to be used for creating MST. Run mst_setup to create a list of cells')
cellpaths = load(file_cellpaths);
assert(all(isfield(cellpaths, {'cell_filelist', 'gen_opt'})), 'Mat file for cells to be used for MST should contain fields cell_filelist and gen_opt')

if ~strcmp(cellpaths.gen_opt.out_dir, gen_opt.out_dir)
    fprintf('out_dir from MST cells to be used is not the same with current out_dir');
end
if ~strcmp(cellpaths.gen_opt.exp_database, gen_opt.exp_database)
    fprintf('experiment database from MST cells to be used is not the same with current database');
end

% create file list
feat_filelist = cellfun(@(x) fullfile(gen_opt.out_dir, x, gen_opt.mstalign_dir, gen_opt.lmfeatures_fname), ...
    cellpaths.cell_filelist, 'UniformOutput', false);
cell_total = length(feat_filelist);       
% create a unique identifier for MST model
uuid = char(java.util.UUID.randomUUID);

% name of output file for MST model
savefile = fullfile(gen_opt.mst_dir, gen_opt.mst_fname);

%% LOAD THE FEATURE MATRIX
feat_original = zeros(parameters.num_lmfeats, parameters.tmax, cell_total);
for cdx = 1:cell_total
    if exist(feat_filelist{cdx},'file')
        load(feat_filelist{cdx});
        if isequal(size(LM_feats),[parameters.num_lmfeats,parameters.tmax])
            feat_original(:, :, cdx) = LM_feats;
        else
            warning(['Feature dimension mismatch ' feat_filelist{cdx}])
        end
    else
        warning(['Missing file ' feat_filelist{cdx}])
    end
end
if sum(feat_original(:)) == 0
    error('No features have been loaded. Check for the paths or force generation of a new list of cells for MST')
end
% delete sequences without any non-zero data and update the filelist
zero_idx = max(max(feat_original,[],1),[],2);
feat_original(:, :, zero_idx==0) = [];
zero_idx = reshape(zero_idx,[cell_total 1 1]);
cellpaths.cell_filelist(zero_idx == 0) = [];
cell_filelist = cellpaths.cell_filelist;
cell_total = size(feat_original,3);
save(file_cellpaths, 'cell_filelist', 'gen_opt');



%% NORMALIZATION OF THE FEATURE TO ZERO-MEAN-UNIT-STD
% the mean and standard deviation are calculated for all measures across
% the entire imaging time and data set
feat_mean = mean(reshape(feat_original, [parameters.num_lmfeats, parameters.tmax*cell_total]),2);
feat_std = std(reshape(feat_original, [parameters.num_lmfeats, parameters.tmax*cell_total]),[],2);
feat_normalized = (feat_original - repmat(feat_mean, [1, parameters.tmax, cell_total]))./repmat(feat_std, [1, parameters.tmax, cell_total]);

%% FIRST DERIVATIVE OF THE FEATURES
% piecewise smoothing fit the feature profiles and calculae the derivative
% from the fitting curves
% USE CURVE FITTING TOOLBOX
t_aq = 1:parameters.tmax;
feat_derivative = zeros(size(feat_normalized));
for fdx = 1:parameters.num_lmfeats
    for cdx = 1:cell_total
        [smooth_seq, stat] = fit(t_aq',feat_normalized(fdx,:,cdx)','smoothingspline');
        feat_derivative(fdx,:,cdx) = (differentiate(smooth_seq,t_aq'))';
    end
end

% Normalize all derivative features using mean and std
dev_mean = mean(reshape(feat_derivative,[parameters.num_lmfeats,parameters.tmax*cell_total]),2);
dev_std = std(reshape(feat_derivative,[parameters.num_lmfeats,parameters.tmax*cell_total]),[],2);
feat_derivative = (feat_derivative-repmat(dev_mean,[1,parameters.tmax,cell_total]))./repmat(dev_std,[1,parameters.tmax,cell_total]);

p_normalize = struct('mean_0_deriv', feat_mean, 'std_0_deriv', feat_std, 'mean_1_deriv', dev_mean, 'std_1_deriv', dev_std);

%% Plot the un-aligned data
F = cat(1, feat_normalized, feat_derivative);
F(isnan(F)) = 0;
std_ground = std(F,[],3);
std_over_time = [sum(std_ground(~isnan(std_ground))) 0];
plot_alignment(F, repmat(1:parameters.tmax,[cell_total 1]), mean(F,3), t_aq', std_ground, 0, gen_opt.mst_dir);

% 1st time save of temporal alignment file
save(savefile, 'feat_original', 'feat_normalized', 'feat_derivative', 'p_normalize', 'parameters', 'uuid');

%% INITIALTION OF THE ALIGNMENT
% Calculate the Euclidic distance between all sequences in imaging time
dist_matrix = pdist((reshape(F,[2*parameters.num_lmfeats*parameters.tmax cell_total 1]))');
dist_matrix = squareform(dist_matrix);
% find the cell having the smallest total distance to all other cells
align_ranking = zeros(cell_total,1);
[~,align_ranking(1)] = min(sum(dist_matrix,2)); 
dist_matrix(:,align_ranking(1)) = inf;
% find the nearst cell to the selected cell
[~,align_ranking(2)] = min(dist_matrix(align_ranking(1),:));
dist_matrix(:,align_ranking(2)) = inf;

%% ALIGNMENT 1ST ROUND
% Align the first two sequences against each other
align_TD = zeros(cell_total,1); % Alignment quality: alignemtn distance
align_model = F(:,:,align_ranking(1)); % master sequence
align_seq = F(:,:,align_ranking(2)); % sequence to be aligned
[align_TD(align_ranking(2)), matching_vector] = multidim_DDTW(align_model,t_aq,align_seq,t_aq,parameters.method,parameters.fuse,parameters.chop,parameters.gap,parameters.unit);

% Assign the alignment of the two first sequences into the alignment matrix
aligned_full = zeros(cell_total, parameters.tmax);
aligned_full(align_ranking(1),:) = t_aq;
aligned_full = AssAligned(aligned_full, matching_vector, cell_total,align_ranking(2));
% Update the master sequence and the timeline
[master_seq, timeline, StD] = GetMaster(F, aligned_full);

% Align the remaining data sequence by sequence
for align_cdx = 3:cell_total
    % find the next processing sequence: the nearst of the last aligned sequence
    [~,align_ranking(align_cdx)] = min(dist_matrix(align_ranking(align_cdx-1),:));
    dist_matrix(:,align_ranking(align_cdx)) = inf;
    % align the sequence to the model
    align_seq = F(:,:,align_ranking(align_cdx));
    [align_TD(align_ranking(align_cdx)), matching_vector]=multidim_DDTW(master_seq, timeline, align_seq, t_aq, parameters.method, parameters.fuse, parameters.chop, parameters.gap, parameters.unit);
    % Assign the alignment
    aligned_full = AssAligned(aligned_full,matching_vector,cell_total,align_ranking(align_cdx));
    % Update the master sequence and the timeline
    [master_seq, timeline, StD] = GetMaster(F, aligned_full);
end

% Quality control sorting the worst aligned sequences (alignment distance > mean+std) out
[aligned_full, qc_idx] = qc_masterseq(align_TD,aligned_full);
% Update the master_sequence after the QC
[master_seq,timeline, StD] = GetMaster(F,aligned_full);


%% Plot the result after aligment
norm_StD = StD.*repmat((diff([0;timeline]))',[size(StD,1) 1]);
std_over_time = cat(1,std_over_time,[sum(norm_StD(~isnan(norm_StD))) sum(align_TD)/cell_total]);
plot_alignment(F, aligned_full, master_seq, timeline, StD, 1, gen_opt.mst_dir);
% 2nd time save of temporal alignment file
save(savefile, 'master_seq', 'aligned_full', 'timeline', 'std_over_time', 'StD','align_TD', 'qc_idx', '-append');

%% REFINED ALIGNMENT (FURTHER ROUNDS)
al_round = 1;
while al_round < parameters.rounds
    % Ranking of the alignment: first the best aligned in the last round, then the worst
    align_ranking = [align_TD (1:cell_total)'];
    align_ranking = sortrows(align_ranking);
    align_ranking = align_ranking(:,2);
    for cidx = 1:cell_total
        align_seq = F(:,:,align_ranking(cidx));
        % Calculate the master_sequence of the alignment without the to-be-aligned sequence (-1)
        aligned_full(align_ranking(align_cdx),:) = 0;
        [master_seq, timeline, StD] = GetMaster(F,aligned_full);
        % Align the sequence to the updated master_sequence
        [align_TD(align_ranking(cidx)),matching_vector] = multidim_DDTW(master_seq, timeline, align_seq, t_aq, ...
            parameters.method, parameters.fuse, parameters.chop, parameters.gap, parameters.unit);
        aligned_full = AssAligned(aligned_full,matching_vector,cell_total,align_ranking(align_cdx));
        [master_seq, timeline, StD] = GetMaster(F,aligned_full);
    end
    % QC step after the round
    [aligned_full, qc_idx] = qc_masterseq(align_TD,aligned_full);
    [master_seq, timeline, StD] = GetMaster(F,aligned_full);
    % Save and update
    al_round = al_round + 1;   
    norm_StD = StD.*repmat((diff([0;timeline]))',[size(StD,1) 1]);
    std_over_time = cat(1,std_over_time,[sum(norm_StD(~isnan(norm_StD))) sum(align_TD)/cell_total]);
    plot_alignment(F, aligned_full, master_seq, timeline, StD, al_round, gen_opt.mst_dir);
    % round time save of temporal alignment file
    save(savefile, 'master_seq', 'aligned_full', 'timeline', ...
        'std_over_time', 'StD', 'align_TD', 'qc_idx', '-append');
end

%% EQUALIZE THE TEMPORAL RESOLUTION
% Calculate the new timeline with homogenous distribution
eq_timeline = timeline(1):parameters.tres:parameters.tmax;
% Calculate the homogenous resolved master sequence and standard deviation
% by linear interpolation between two adjacent data points in the current
% master sequence
eq_master_seq = zeros(size(master_seq,1),length(eq_timeline));
eq_StD = eq_master_seq;
for i = 1:length(eq_timeline)
    idx_timeline = sum(timeline<=eq_timeline(i));
    dt1 = eq_timeline(i)-timeline(idx_timeline); % duration to the adjacent master_sequence frame before
    if idx_timeline < length(timeline)
        dt2 = timeline(idx_timeline+1)-eq_timeline(i); % duration to the adjacent master_sequence frame after
        eq_master_seq(:,i) = master_seq(:,idx_timeline) + (master_seq(:,idx_timeline+1)-master_seq(:,idx_timeline))*(dt1/(dt1+dt2)); % linear interpolation
        eq_StD(:,i) = StD(:,idx_timeline) + (StD(:,idx_timeline+1)-StD(:,idx_timeline))*(dt1/(dt1+dt2));
    else
        eq_master_seq(:,i) = master_seq(:,idx_timeline); % boundary condition
        eq_StD(:,i) = StD(:,idx_timeline);
    end
end
align_original_model = struct('master_seq', master_seq, 'StD', StD, 'timeline', timeline); % results from the alignment
master_seq = eq_master_seq;
StD = eq_StD;
timeline = eq_timeline;

save(savefile, 'master_seq', 'aligned_full', 'timeline', 'align_original_model', ...
    'std_over_time', 'StD', 'align_TD', 'qc_idx', '-append');

%% IDENTIFICATION OF MAJOR MITOTIC TRANSITIONS
% Calculate the smoothed 2. derivative with moving window
% Boundary conditions: copy the first and last frames
frame_start = sum(timeline<=(parameters.windowsize+timeline(1)))+1;
frame_end = sum(timeline<=(timeline(end)-parameters.windowsize));
add_start = frame_start-1;
add_end = size(master_seq,2)-frame_end;
frame_end = length(timeline)+add_start;
master_seq = cat(2,repmat(master_seq(:,1),[1 add_start]),master_seq);
master_seq = cat(2,master_seq,repmat(master_seq(:,end),[1 add_end]));
StD = cat(2,repmat(StD(:,1),[1 add_start]),StD);
StD = cat(2,StD,repmat(StD(:,end),[1 add_end]));
timeline = [-parameters.tres*(add_start:-1:1) timeline parameters.tmax+parameters.tres*(1:add_end)];

% Calculate the 2. derivative
tangentseq = zeros(size(master_seq,2),size(master_seq,1));
for i = frame_start:frame_end
    i_minus = sum(timeline<=(timeline(i)-parameters.windowsize));
    i_plus = sum(timeline<(timeline(i)+parameters.windowsize))+1;
    if i_minus == i
        i_minus = i-1;
    end
    if i_plus == i
        i_plus = i+1;
    end
    tangentseq(i,:) = abs(mean(master_seq(:,i_minus:i-1)-repmat(master_seq(:,i),[1 i-i_minus]),2)+mean(master_seq(:,i+1:i_plus)-repmat(master_seq(:,i),[1 i_plus-i]),2)); % 2. derivative
end

% Select transition points with large differences in feature behavior
breakpoints = zeros(size(tangentseq,1),1);
tangentseq_original = tangentseq; % back up

% Select transitions based on the single feature 2. derivatives 
break_max_single = zeros(size(tangentseq)); %Corrected Julius 2018-03-08
for i = 1:size(tangentseq,2)
    val = 1000; % initialization
    while val > parameters.toptr % pre-set threshold
        % find the maximum
        [val,idx] = max(tangentseq(:,i));
        if val > parameters.toptr
            breakpoints(idx) = 1;
            break_max_single(idx,i) = 1;
            % Find within the smoothing windowsize the lowest derivative
            % positions and suppress the selection inbetween (only select
            % derivative peaks not high values)
            i_minus = sum(timeline<=(timeline(idx)-parameters.windowsize));
            i_plus = sum(timeline<(timeline(idx)+parameters.windowsize))+1;
            if i_minus == idx
                i_minus = idx-1;
            end
            if i_plus == idx
                i_plus = idx+1;
            end
            [~,idx_min] = min(tangentseq((idx-1):-1:i_minus,i));
            tangentseq((idx-idx_min):idx,i) = 0;
            [~,idx_min] = min(tangentseq((idx+1):1:i_plus,i));
            tangentseq(idx:(idx+idx_min),i) = 0;
        end
    end
end

%% POST-PROCESSING: to avoid too long or short phases
tangentseq = tangentseq_original; % Reset
% Normalize the threshold and 2. derivatives allowing cross-feature comparison
th_tan = parameters.toptr*ones(1,size(tangentseq,2));
th_tan = (th_tan-mean(tangentseq(frame_start:frame_end,:),1))./std(tangentseq(frame_start:frame_end,:),[],1);
tangentseq(frame_start:frame_end,:) = (tangentseq(frame_start:frame_end,:) - repmat(mean(tangentseq(frame_start:frame_end,:),1),[frame_end-frame_start+1,1]))./repmat(std(tangentseq(frame_start:frame_end,:),[],1),[frame_end-frame_start+1,1]);

% Adding selected points for too long phases without any transitions
frame_end0 = frame_end;
breakpoints(frame_start) = 1;
for i = 1:sum(timeline<=timeline(end)-parameters.nosplit)
     frame_end = sum(timeline<=(timeline(i)+parameters.nosplit));
     if sum(breakpoints(i:frame_end)) == 0
         % Select the point within the upper limit of period of time with
         % the highest maximum 2. derivative through all feature dimensions
         [~,idx] = max(max(tangentseq(i:frame_end,:),[],2),[],1);
         breakpoints(idx+i-1) = 1;
         [~,fdx] = max(tangentseq(idx+i-1,:),[],2);
         break_max_single(idx+i-1,fdx(1)) = 2;
     end
end

% Delete transition points with very close timing 
i = 1;
while i <= sum(timeline<=timeline(end)-parameters.minsplit)
     frame_end = sum(timeline<=(timeline(i)+parameters.minsplit));
     if sum(breakpoints(i:frame_end)) > 1
         % Only keep the point within the lowest limit of period of time
         % with the highest sum of 2. derivative through all feature
         % dimensions
         s = sum(tangentseq(i:frame_end,1:(size(tangentseq,2)/2)),2);
         s(breakpoints(i:frame_end)==0) = -inf;
         [~,idx] = max(s,[],1); 
         breakpoints(i:frame_end) = 0;
         break_max_single(i:frame_end,:) = -break_max_single(i:frame_end,:);
         breakpoints(idx+i-1) = 1;
         break_max_single(idx+i-1,:) = break_max_single(idx+i-1,:)+4;
         i = idx+i;
     else
         i = i+1;
     end
end
breakpoints(frame_start) = 1;

% Summarize
master_seq = master_seq(:,frame_start:frame_end0);
StD = StD(:,frame_start:frame_end0);
% convert to minutes
timeline = timeline(frame_start:frame_end0)*parameters.deltat;
tangentseq = tangentseq(frame_start:frame_end0,:);
breakpoints = breakpoints(frame_start:frame_end0);
break_max_single = break_max_single(frame_start:frame_end0,:);

%% OUTPUT
class_start = find(breakpoints==1);
class_label = (1:length(class_start))';
class_end = cat(1,class_start(2:end)-1,length(breakpoints));
class_mean = zeros(length(class_start),size(master_seq,1)); % the mean of the master sequence frames belonging to this stage -> not the mean of all cells assigned to this stage
class_creteria = zeros(length(class_start),2);
for i = 1:length(class_start)
    class_mean(i,:) = (mean(master_seq(:,class_start(i):class_end(i)),2))';
    [class_creteria(i,2),class_creteria(i,1)] = max(break_max_single(class_start(i),:),[],2);
end

close all
figure(1)
hold on
for i = 1:size(master_seq,1)
    subplot(2,size(master_seq,1)/2,i);
    hold on;
    plot(timeline,master_seq(i,:)','b-');
    plot(timeline(breakpoints==1),master_seq(i,breakpoints'==1)','bo');
    xlabel('Mitotic standard time [min]');
    ylabel('Normalized feature [a.u.]');
end
figname = fullfile(gen_opt.mst_dir, 'master_sequence.fig');
savefig(1,figname)
close(gcf)

close all
figure(1)
hold on
for i = 1:size(master_seq,1)
    subplot(2,size(master_seq,1)/2,i)
    hold on
    plot(timeline,tangentseq(:,i),'b-')
    plot([timeline(1);timeline(end)], [th_tan(i); th_tan(i)],'b--')
    h1 = plot(timeline(break_max_single(:,i) == 1), tangentseq(break_max_single(:,i) == 1, i),'ro');
    plot(timeline(break_max_single(:,i) == 3), tangentseq(break_max_single(:,i) == 3, i),'ro');
    h2 = plot(timeline(break_max_single(:,i) == 2), tangentseq(break_max_single(:,i) == 2, i),'go');
    h3 = plot(timeline(break_max_single(:,i) < 0), tangentseq(break_max_single(:,i) < 0, i),'yo');
    xlabel('Mitotic standard time [min]')
    ylabel('Normalized feature [a.u.]')
%     if i == 1;
%         hleg = legend([h1,h2,h3],'Selected peaks','Added: avoid long stages','Deleted: aviud short stages');
%         set(hleg,'Position','NorthWest')
%     end
end
figname = fullfile(gen_opt.mst_dir, 'select_transitions.fig');
savefig(1,figname)
close(gcf)

time_cluster = table(class_label,class_start,class_end, class_mean, class_creteria);
save(savefile, 'master_seq', 'aligned_full', 'timeline', 'align_original_model', ...
    'std_over_time','StD', 'align_TD', 'qc_idx', ...
    'time_cluster', 'tangentseq', 'breakpoints', 'break_max_single', 'parameters', '-append');

end

function [gen_opt_default, parameters_default] = get_defaults()
gen_opt_default = struct( ...
    'out_dir', 'W:\Antonio_t2_2\HeLaKyoto_MitoSys\Results', ...      % main directory for results
    'exp_database', 'full_database.txt', ...         % name of experiment database found in out_dir\exp_database
    'lmfeatures_fname', 'Nuc_feature_3D.mat', ...    % name file LM features
    'mst_dir', 'W:\Antonio_t2_2\HeLaKyoto_MitoSys\Process_Settings\MST\', ... % Name of directory to save MST
    'mst_fname', 'temporal_alignment.mat',...
    'mstalign_dir', 'Temporal_Align');

parameters_default = struct( ...
    'cellpaths_fname', 'data_filepath_list.mat', ...  % List of cell paths used for generating the MST
    'tmax', 40, ...                   % standard number of frames used for the modeling
    'deltat', 1.5, ...                % time resolution of data in minutes
    'num_lmfeats', 3, ...             % number of landmarks features
    'method', 'time', ...             % selection between frame and time
    'fuse', [0;0], ...                % penalty for fusing frames (e.g. 2-2;2-3) for the model/sequence to be aligned
    'chop', [5;54], ...               % penalty for chopping on the model/sequence to be aligned at the beginning and end
    'gap', [12;54], ...               % penalty for introducing unaligned frame in the model/sequence to be aligned
    'unit', 0.3, ...                  % a.u., technical balancing for the end of the sequence alignment
    'rounds', 4, ...                  % number of rounds (default 4)
    'tres', 0.25/1.5, ...             % resolution of the mitotic standard time model, down sampled from the alignment, in frame units
    'windowsize', 4.5/1.5, ...        % filtering size left and right for calculating the second derivative, in frame units
    'nosplit',  12/1.5, ...           % maximum duration of one mitotic standard stage, in frame units
    'minsplit', 1, ...                % minimum duration of one mitotic standard stage, in frame units
    'toptr', 0.7);                    % Threshold in second derivative value for being selected as a transition point
end