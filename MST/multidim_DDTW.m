function [TD,w]=multidim_DDTW(sm,tm,ss,ts,pen_mode,pen_fuse,pen_chop,pen_gap,unit_t)
% Author: Yin Cai
% Version July 2014
% Modifications based on discussion with Wolfgang/Bernd on July 2014
% Update 15.4.3 pen_fuse error correction in mode frame, added in mode time
% Update 16.5.31 comments

% sm and ss are the model and the sequence to be aligned
% tm and ts are the timeline of the model and the sequence

% pen_mode: Two alignment mothods are allowed.
% Mode "frame" aligns the sequence in the classical discrete dynamic time
% warping way, only the distance between frames is calculated and summed up
% over the entire sequence.
% Mode "time" has a modification which weights the distance between aligned
% frames by the duration over which the frames are aligned during the
% summing up step. The distance between two aligned sequences, i.e. the
% objective function, can be understood as an approximation of the area
% between the two sequences.

% pen_fuse is the penalty parameter that is used when multiple frames
% of one sequence are aligned to a single frame of the other sequence

% pen_chop is the penalty parameter for chopping the starting or ending
% frames away for alignment

% pen_gap is the penalty parameter for jumps (up to one frame) in alignment.

% unit_t is the unit time for solving the end duration in 'time' mode

%% Calculate the Euroclidian distance matrix between all points of sm to ss
num_feat = size(sm,1);
lm = size(sm,2);
ls = size(ss,2);
d = zeros(lm,ls);
for midx = 1:lm;
   for sidx = 1:ls;
       nan_idx = 1-isnan(sm(:,midx)+ss(:,sidx));
       % calculate the distance without considering the nan, however normalize to full dimensions
       d(midx,sidx) = sum((sm(nan_idx>0,midx)-ss(nan_idx>0,sidx)).^2)/sum(nan_idx)*num_feat;  
   end
end
% mean(d(:))
%% Calculate the lowest cummulative cost of a path from (1,1) to (lm,ls)
% At the start, missing alignment is allowed and the cost for each missing
% frame is denpendent on the pen_chop and the frame position
% At the end, missing alignment is also allowed. The cost is calculated in
% the same way as the start
% Within the sequence, alignment gap up to 1 frame is allowed and will have
% a cost of pen_gap
% Non diagonal alignment (fuse or gap) will have a cost either fixed 
%(mode frame) or weighted in a time-dependent way (mode time)

% Generate cummulative distance matrix and selection index matrix
D = zeros(size(d));
ID = D;
D(1,1) = d(1,1);
ID(1,1) = 0;

switch pen_mode
    case 'frame' % Classical DTW
        % First column: compare aligning the midx-1 frame of the model to the
        % first frame of the sequence and chopping the model's first midx-1
        % frames away
        for midx = 2:lm;
            [D(midx,1),m] = min([d(midx,1)+D(midx-1,1),d(midx,1)+sum(1:(midx-1))*pen_chop(1)]);
            if m == 2;
                ID(midx,1) = 0; % not align
            else
                ID(midx,1) = 2; % align on last model frame
            end
        end
        % First line: compare aligning the sidx-1 frame of the sequence to the
        % first frame of the model and chopping the sequence' first sidx-1
        % frames away
        for sidx = 2:ls;
            [D(1,sidx),m] = min([d(1,sidx)+D(1,sidx-1),d(1,sidx)+sum(1:(sidx-1))*pen_chop(2)]);
            if m == 2;
                ID(1,sidx) = 0;
            else
                ID(1,sidx) = 3; % align on last sequence frame
            end
        end
        % Second column and line, compare between diagonal alignment (from
        % midx-1,1 to midx,2), fuse either model or sequence frames (2
        % frames to 1 frame mapping)
        for midx = 2:lm;
            [D(midx,2),ID(midx,2)] = min([d(midx,2)+D(midx-1,1),d(midx,2)+D(midx-1,2)+pen_fuse(1),d(midx,2)+D(midx,1)+pen_fuse(2)]);
        end
        for sidx = 2:ls;
            [D(2,sidx),ID(2,sidx)] = min([d(2,sidx)+D(1,sidx-1),d(2,sidx)+D(1,sidx)+pen_fuse(1),d(2,sidx)+D(2,sidx-1)+pen_fuse(2)]);
        end
        % Progressive calculation of the cummulative distance matrix. Index
        % stays: 1-diagonal alignment, 2-align on last model frame, 3-align
        % on last sequence frame (both fuse case), 4-align on the 2.
        % previous model frame, 5-align on the 2. previous sequence frame
        % (gap cases)
        for midx = 3:lm;
            for sidx = 3:ls;
                [D(midx,sidx),ID(midx,sidx)] = min([d(midx,sidx)+D(midx-1,sidx-1),d(midx,sidx)+D(midx-1,sidx)+pen_fuse(1),d(midx,sidx)+D(midx,sidx-1)+pen_fuse(2),d(midx,sidx)+D(midx-2,sidx-1)+pen_gap(1),d(midx,sidx)+D(midx-1,sidx-2)+pen_gap(2)]);
            end
        end
        % Last column and last line: calculate the cummulative distance for
        % chopping the model or sequence at midx or sidx.
        for midx = 2:lm;
            D(midx,end) = D(midx,end)+pen_chop(1)*sum(1:(lm-midx));
        end
        for sidx = 2:ls;
            D(end,sidx) = D(end,sidx)+pen_chop(2)*sum(1:(ls-sidx));
        end
    case 'time'  % Weighted distance. Procedure similar as in case "frame"
        for midx = 2:lm;
            [D(midx,1),m] = min([d(midx-1,1)*(tm(midx)-tm(midx-1))+D(midx-1,1),tm(midx)*pen_chop(1)]);
            if m == 2;
                ID(midx,1) = 0;
            else
                ID(midx,1) = 2;
            end
        end
        for sidx = 2:ls;
            [D(1,sidx),m] = min([d(1,sidx-1)*(ts(sidx)-ts(sidx-1))+D(1,sidx-1),ts(sidx)*pen_chop(2)]);
            if m == 2;
                ID(1,sidx) = 0;
            else
                ID(1,sidx) = 3;
            end
        end
        for midx = 2:lm;
            [D(midx,2),ID(midx,2)] = min([d(midx-1,1)*(tm(midx)-tm(midx-1)+ts(2)-ts(1))+D(midx-1,1),(d(midx-1,2)+pen_fuse(1))*(tm(midx)-tm(midx-1))+D(midx-1,2),(d(midx,1)+pen_fuse(2))*(ts(2)-ts(1))+D(midx,1)]);
        end
        for sidx = 2:ls;
            [D(2,sidx),ID(2,sidx)] = min([d(1,sidx-1)*(tm(2)-tm(1)+ts(sidx)-ts(sidx-1))+D(1,sidx-1),(d(1,sidx)+pen_fuse(1))*(tm(2)-tm(1))+D(1,sidx),(d(2,sidx-1)+pen_fuse(2))*(ts(sidx)-ts(sidx-1))+D(2,sidx-1)]);
        end
        for midx = 3:lm;
            for sidx = 3:ls;
                [D(midx,sidx),ID(midx,sidx)] = min([d(midx-1,sidx-1)*(tm(midx)-tm(midx-1)+ts(sidx)-ts(sidx-1))+D(midx-1,sidx-1),(d(midx-1,sidx)+pen_fuse(1))*(tm(midx)-tm(midx-1))+D(midx-1,sidx),(d(midx,sidx-1)+pen_fuse(2))*(ts(sidx)-ts(sidx-1))+D(midx,sidx-1),d(midx-2,sidx-1)*(tm(midx)-tm(midx-2)+ts(sidx)-ts(sidx-1))+D(midx-2,sidx-1)+pen_gap(1)*(tm(midx)-tm(midx-2)),d(midx-1,sidx-2)*(tm(midx)-tm(midx-1)+ts(sidx)-ts(sidx-2))+D(midx-1,sidx-2)+pen_gap(2)*(ts(sidx)-ts(sidx-2))]);
            end
        end
        for midx = 2:lm;
            D(midx,end) = D(midx,end)+d(midx,end)*unit_t+pen_chop(1)*(tm(end)-tm(midx));
        end
        for sidx = 2:ls;
            D(end,sidx) = D(end,sidx)+d(end,sidx)*unit_t+pen_chop(2)*(ts(end)-ts(sidx));
        end
end

%% Find the end of the alignment
% Find the smallest cummulative distance in the last column/line
[v_t,end_t] = min(D(:,end));
[v_r,end_r] = min(D(end,:));

%% Assign the alignment vector
% Complementing the chopped sequence in the matching-vector
if v_t < v_r
    seq_idx = [zeros(lm-end_t,1);ls];
    mod_idx = (lm:-1:end_t)';
else
    seq_idx = (ls:-1:end_r)';
    mod_idx = [zeros(ls-end_r,1);lm];
end
% Cummulative distance
TD = min(v_t,v_r);

% Trace back the alingmend path based on the ID matrix
w = [mod_idx,seq_idx];
seq_idx = w(end,2);
mod_idx = w(end,1);
idx = ID(mod_idx,seq_idx);
while idx > 0;
    switch idx
        case 1
            seq_idx = seq_idx-1;
            mod_idx = mod_idx-1;
        case 2
            mod_idx = mod_idx-1;
        case 3
            seq_idx = seq_idx-1;
        case 4
            w = cat(1,w,[mod_idx-1,0]);
            mod_idx = mod_idx-2;
            seq_idx = seq_idx-1;
        case 5
            w = cat(1,w,[0,seq_idx-1]);
            mod_idx = mod_idx-1;
            seq_idx = seq_idx-2;
    end
    w=cat(1,w,[mod_idx,seq_idx]);
    idx = ID(mod_idx,seq_idx);
end

if w(end,1) > 1;
    w = cat(1,w,[((w(end,1)-1):-1:1)' zeros(w(end,1)-1,1)]);
elseif w(end,2) > 1;
    w = cat(1,w,[zeros(w(end,2)-1,1) ((w(end,2)-1):-1:1)']);
end

% Arange the alignment list from start to end
w = w(end:-1:1,:);
end