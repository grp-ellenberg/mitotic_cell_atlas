function annotate_time(mstmodel_file, gen_opt,  parameters)
%% ANNOTATE_TIME Annotate image sequences to a mitotic standard time (MST) model 
% ANNOTATE_TIME(mstmodel_file) Specify full path to MST model. Use default parameters and paths 
% ANNOTATE_TIME(mstmodel_file, gen_opt)  Use paths as specified in struct gen_opt. Use default parameters
% ANNOTATE_TIME(mstmodel_file, gen_opt, parameters) Use paths as specified in struct gen_opt and specified parameter
% 
% The database file is loaded and updated. Only alignment passed the QC will be classified as valid.
%   INPUT: 
%       * gen_opt    - struct for the general paths
%       * parameters - struct giving the parameters of the alignment
%       See function get_default at end of file for more details.
%
% Author: Yin Cai, EMBL, 2016.05.30
% Update
% 2017.09.13 - fail save reading of mat files using  a variable. Change
%               variable names to more clear ones. Introduce UUID for model identification. Antonio Politi
% 2018.03.20 - Struct for input parameters

%% Default options
[gen_opt_default, parameters_default] = get_defaults();

if nargin < 2
    gen_opt = gen_opt_default;
end

if nargin < 3
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('Second argument parameters does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

if gen_opt.mstalign_reprocess == -2
    return
end

%% LOAD THE MODEL
mst_model = load(mstmodel_file);
% LEGACY FORMAT
if ~isfield(mst_model, 'parameters')
    mst_model.parameters = mst_model.options;
end
th_qc = parameters.qcfactor * mst_model.std_over_time(end,2);

%% Read QC values
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
if gen_opt.mstalign_reprocess == 1
    vec_QC = -ones(length(summary_database.time_alignment),1);
else
    vec_QC = summary_database.time_alignment;
end

proc_history = (summary_database.manualQC~=0).*(summary_database.lm_features > 0);
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);

%% ALIGN SEQUENCE ONE BY ONE
for cell_idx = vec_idx
    tmax = summary_database.tmax(cell_idx);
    tres = summary_database.tres(cell_idx);
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    celldir = path_gen.getcelldir(gen_opt.out_dir, gen_opt.mstalign_dir);
    nuc_feature_path = fullfile(celldir, gen_opt.lmfeatures_fname);
    mst_annotation_path = fullfile(celldir, gen_opt.mstalign_fname);
    mst_timeindex_path = fullfile(celldir, gen_opt.mstalign_timeIndexFilename);
    if ~exist(mst_annotation_path ,'file')
        vec_QC(cell_idx) = -1;
    end
    if gen_opt.mstalign_reprocess == 0 && vec_QC(cell_idx) == 1 % Check processed data for consistancy
        fprintf('Check correct MST model Cell_idx: %d\n', cell_idx); 
        mst_annotation = load(mst_annotation_path);
        if isfield(mst_annotation, 'uuid')                  % new way of identifying mst_model using a unique identifier
            if ~isequal(mst_model.uuid, mst_annotation.uuid)
                vec_QC(cell_idx) = -1;
                warning(['MST model missmatching. Overwrite ' mst_annotation_path]);
            end
        else
            if ~isequal(modelpath, mst_annotation.modelpath) % old way for consitency with old data
                vec_QC(cell_idx) = -1;
                warning(['MST model missmatching. Overwrite ' mst_annotation_path])
            end
        end
    end
    if vec_QC(cell_idx) ~= -1
        continue
    end
    if ~exist(nuc_feature_path, 'file')
        fprintf('No LM feature file %s\n', nuc_feature_path);
        continue
    end
    uuid = mst_model.uuid;
    fprintf('Temporal align to the model Cell_idx: %d\n', cell_idx);
    
    nuc_feature = load(nuc_feature_path);
    %% PREPROCESSING-NORMALIZATIONS
    feat_normalized = (nuc_feature.LM_feats - ...
        repmat(mst_model.p_normalize.mean_0_deriv, [1, tmax]))./...
        repmat(mst_model.p_normalize.std_0_deriv, [1, tmax]);
    t_aq = (1:tmax)*tres/mst_model.parameters.deltat;
    feat_derivative = zeros(size(feat_normalized));
    for fdx = 1:parameters.num_lmfeats
        [smooth_seq, stat] = fit(t_aq',feat_normalized(fdx,:)','smoothingspline');
        feat_derivative(fdx,:) = (differentiate(smooth_seq,t_aq'))';
    end
    feat_derivative = (feat_derivative - ...
        repmat(mst_model.p_normalize.mean_1_deriv, [1, tmax]))./...
        repmat(mst_model.p_normalize.std_1_deriv, [1, tmax]);
    
    %% ALIGNMENT
    align_seq = [feat_normalized; feat_derivative];
    [align_TD, matching_vector] = multidim_DDTW(mst_model.master_seq, ...
        mst_model.timeline/mst_model.parameters.deltat, align_seq, t_aq, parameters.method, parameters.fuse, parameters.chop, parameters.gap, parameters.unit);
    if align_TD > th_qc % Quality control of alignment using comulative distance
        vec_QC(cell_idx) = 0;
    else
        %% Assign the alignment:
        % if a frame is aligned to multiple frames in the model (it
        % will be by the method where the model has 6x more frames
        % than the image sequence), the image frame is then
        % assigned to the closest one among all model frames
        % aligned to
        mitotime = zeros(tmax, 2);
        for aidx = 1:tmax
            alignframe = find(matching_vector(:,2) == aidx);
            modelframe = mst_model.master_seq(:, matching_vector(alignframe(1),1));
            if length(alignframe) > 1
                for fidx = 2:length(alignframe)
                    modelframe = cat(2, modelframe, mst_model.master_seq(:, matching_vector(alignframe(fidx),1)));
                end
            end
            framedist = (modelframe - repmat(align_seq(:,aidx), [1,size(modelframe,2)])).^2;
            framedist(isnan(framedist)) = 0;
            framedist = sum(framedist,1);
            [~,frame] = min(framedist);
            mitotime(aidx,:) = [matching_vector(alignframe(frame),1) mst_model.timeline(matching_vector(alignframe(frame),1))];
        end
        vec_QC(cell_idx) = 1;
        save(mst_annotation_path, 'align_seq', 'mitotime', 'parameters', 'gen_opt', 'th_qc', 'mstmodel_file', 'align_TD', 'uuid');
        for tdx = 1:length(mitotime)
            mitotime(tdx,2) = sum(mst_model.time_cluster.class_start <= mitotime(tdx,1));
            mitotime(tdx,1) = tdx;
        end
        save(mst_timeindex_path, 'mitotime');
    end
    summary_database.time_alignment(cell_idx) = vec_QC(cell_idx);
    writetable(summary_database, exp_database,'Delimiter','\t');
end
end


function [gen_opt_default, parameters_default] = get_defaults()
gen_opt_default = struct(...
    'exp_dir', '', ... % default experimental directory
    'out_dir', '', ...  % default output directory
    'exp_database', 'full_database.txt', ...        % name of processing database stored in out_dir\exp_database
    'cellIdx', [1], ...                             % Cells to process
    'mstalign_dir', 'Temporal_Align', ...           % subdirectory for MST aligned data
    'lmfeatures_fname', 'Nuc_feature_3D.mat', ...   % name of landmark feature file
    'mstalign_fname',  'MST_annotation.mat', ...    % name of MST annotation file
    'mstalign_timeIndexFilename', 'MST_timeindex.mat', ... %
    'mstalign_reprocess', 0);                       % reprocess data

parameters_default = struct( ...
    'num_lmfeats', 3, ...   % number of landmarks features
    'unit', 0.3, ...        % a.u., technical balancing for the end of the sequence alignment
    'method', 'time', ...   % selection between frame and time
    'qcfactor', 3,  ...     % factor to max distance during modelling. Defines how large the inter-sequence distance can be. The higher, the less QC
    'fuse', [0; 360], ...   % penalty for fusing frames (e.g. 2-2;2-3) for the model/sequence to be aligned
    'chop', [15; 540], ...  % penalty for chopping on the model/sequence to be aligned at the beginning and end
    'gap', [36; 540]);     % penalty for introducing unaligned frame in the model/sequence to be aligned
end