function A = AssAligned(A,vecM,numcell,c2)
% Assign the alignment to a cell x frame matrix from the matching vector
APast = A;
A = zeros(numcell,size(vecM,1));
for midx = 1:size(vecM,1);
    if vecM(midx,1) > 0;
        A(:,midx) = APast(:,vecM(midx,1));
    end
end

A(c2,:) = vecM(:,2)';
A(:,sum(A,1)==0) = [];
AS = A(:,1);
for atidx = 2:size(A,2);
    if ~isequal(A(A(:,atidx)>0,atidx),A(A(:,atidx)>0,atidx-1)); 
        AS = cat(2,AS,A(:,atidx));
    end
end
A = AS;
end