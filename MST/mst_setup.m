function [mst_dir] = mst_setup(gen_opt, parameters)
%% MST_SETUP pick cells for MST model generation
% MST_SETUP Use default directories and options
% MST_SETUP(gen_opt) Default directories are specified in struct gen_opt. use default options
% MST_SETIP(gen_opt, options) Specify directories and options
% INPUT:
%   * gen_opt: A struct with following fields
%      out_dir         - full path to directory that stores the results
%      exp_database    - name of file for progression database found in out_dir\exp_database
%      mst_numcells    - number of cells to create MST model. These are selected randomly from data set
%      mst_dir         - full path to directory for storing MST model and mat file with paths to cells for MST
%      mst_fname       - name of MST model file in mst_dir\mst_fname
%   * options: A struct with following fields
%      cellpaths_fname - name of mat file to store paths to cells
%      tmax            - maximal number of time frames
%      deltat          - time interval between frames in minutes
%% Parameter loading
gen_opt_default = struct( ...
    'out_dir', '', ...                          % full path to directory that stores the results
    'exp_database', 'full_database.txt', ...    % name of file for progression database found in out_dir\exp_database
    'mst_numcells', 150, ...                    % number of cells to create MST model. These are selected randomly from data set
    'mst_dir', '', ...                          % full path to directory for storing MST model and mat file with paths to cells for MST
    'mst_fname', 'temporal_alignment.mat');     % name of MST model, i.e. mst_dir\mst_fname

parameters_default = struct( ...
    'cellpaths_fname', 'data_filepath_list.mat', ...     % name of mat file to store paths to cells
    'tmax', 40, ...                                      % maximal number of time frames
    'deltat', 1.5);                                      % time interval between frames in minutes

if nargin < 1
    gen_opt = gen_opt_default;
end

if nargin < 2
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('mst_setup: First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('mst_setup: Second argument options does not contain the expected field(s)s\n%s \n', ...
        strjoin(fieldnames(parameters_default)));
end

[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    mst_dir = '';
    return
end

%% Find if a MST model directory exists eventually pick new cells
mstmodel_process = 'No';
if isempty(gen_opt.mst_dir)
    gen_opt.mst_dir = uigetdir(gen_opt.mst_dir, 'Select the saving directory for the MST model');
end
if ~exist(gen_opt.mst_dir, 'dir')
    try
        mkdir(gen_opt.mst_dir);
    catch
        gen_opt.mst_dir = uigetdir(gen_opt.mst_dir, 'Select the saving directory for the MST model');
    end
end

while strcmp(mstmodel_process, 'No')
    mstmodel_file = fullfile(gen_opt.mst_dir, gen_opt.mst_fname);
    if exist(mstmodel_file, 'file')
        mstmodel_process = questdlg(sprintf('MST file %s exists!\nYes: Update MST file (Old file will be backed up.)\nNo: select another saving directory for the MST model', mstmodel_file), ...
            'Mitotic Standard Time reference');
        switch mstmodel_process
            case 'Cancel'
                return
            case 'Yes'
                FileInfo = dir(mstmodel_file);
                [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
                [path, name, ext] = fileparts(mstmodel_file);
                copyfile(mstmodel_file, fullfile(path, sprintf('%s_%02d%02d%02d.mat', name, Y-2000, M, D)));
            case 'No'
                gen_opt.mst_dir = uigetdir(gen_opt.mst_dir, 'Select the saving directory for the MST model');
        end
    else
        mstmodel_process = 'Yes';
    end
end


%% check if file containing the path to the picked cells exists and overwrite if necessary
data_filepath_name = fullfile(gen_opt.mst_dir, parameters.cellpaths_fname);
mst_pick_data = 1;
if exist(data_filepath_name, 'file')
    data_pick_process = questdlg(sprintf('List of picked cells for MST %s exists!\nYes: update file (old file will be backed up).\nNo: Proceed without update',...
        data_filepath_name), 'Mitotic Standard Time reference');
    switch data_pick_process
        case 'Cancel'
            return
        case 'Yes'
            FileInfo = dir(data_filepath_name);
            [Y, M, D, H, MN, S] = datevec(FileInfo.datenum);
            [path, name, ext] = fileparts(data_filepath_name);
            copyfile(data_filepath_name, fullfile(path, sprintf('%s_%02d%02d%02d.mat', name, Y-2000, M, D)));
            mst_pick_data = 1;
        case 'No'
            mst_pick_data = 0;
    end
end

if mst_pick_data
    % Criteria: QC positive for landmark feature extraction, reference protein
    % but not generated H2B (avoiding double selection)
    % You can change the criteria by modifying the following code line
    exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
    summary_database = readtable(exp_database, 'Delimiter', '\t');
    ref_idx = (summary_database.lm_features>0).* (summary_database.segmentation>0).*...
        (strcmp(summary_database.poi, 'genH2B')==0).*(summary_database.tmax == parameters.tmax).*...
        (summary_database.tres == parameters.deltat);
    
    if sum(ref_idx) > gen_opt.mst_numcells
        randidx = rand(length(ref_idx),1).*ref_idx;
        randsort = sortrows(randidx);
        ref_idx = ref_idx.*(randidx > randsort(end - gen_opt.mst_numcells));
    end
    
    cell_filelist = summary_database.filepath(ref_idx > 0);
    save(data_filepath_name, 'cell_filelist', 'gen_opt');
end
mst_dir = gen_opt.mst_dir;
end