function LM_shape_feature(gen_opt)
%% LM_SHAPE_FEATURE  Calculate 3 landmark features for chromatin used for temporal alignement
%  LM_SHAPE_FEATURE(gen_opt) gen_opt is a struct with path locations and file names. 
%  Such a struct is defined in mitosys_opt.m
%  INPUT:
%   * gen_opt - a struct with pathnames (see code section)
%
% Calculates the 3 landmarks featureus used for the temporal modelling and alignment. 
% The features are 'nuc_dist';'nuc_vol';'nuc_3eigenval'.
% The input of this function is the output of segmentation step (ls_segment_landmarks) of the
% pipeline. Perform the segmentation before using this function.
%
% Author: Yin Cai, EMBL Heidelberg, 2016.05.24
% Update:
%   2017.09.22 Comments and extra cellToProcess option, AP
%   2018.03.15 Struct for parameter input

%% Read parameters
gen_opt_default = struct( ...
    'exp_dir', '',  ...                             % data directory
    'out_dir', '', ...                              % directory for results
    'exp_database', 'full_database.txt', ...        % name of experiment database found in out_dir\exp_database
    'cellIdx', [1], ...                             % index of cells in exp_database to process
    'segmentation_dir', 'Segmentation', ...         % subdirectory where segmentation data is stored (i.e. out_dir\...\segmentation_dir)
    'mstalign_dir', 'Temporal_Align', ...           % subdirectory to store landmarks feature file
    'lmfeatures_fname', 'Nuc_feature_3D.mat', ...   % filename for landmark features
    'lmfeatures_reprocess', 1);                     % 1 to reprocess the data

if nargin < 1
    gen_opt = gen_opt_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error(fprintf('LM_shape_feature: First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default))));
end
[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

%% Load the database file
if gen_opt.lmfeatures_reprocess == -2
    return
end

exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
if gen_opt.lmfeatures_reprocess == 1
    vec_QC = -ones(length(summary_database.lm_features),1);
else
    vec_QC = summary_database.lm_features;
end
proc_history = (summary_database.manualQC ~= 0).*(summary_database.segmentation > 0); % Landmarks extraction is independent of FCS calibration
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);

%% Calculate the features
feature_name = {'nuc_dist'; 'nuc_vol'; 'nuc_3eigenval'};
for cell_idx = vec_idx
    
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    
    temporal_dir = path_gen.getcelldir(gen_opt.out_dir, gen_opt.mstalign_dir);
    if ~exist(temporal_dir,'dir')
        mkdir(temporal_dir);
    end
    LM_feature_seq = fullfile(temporal_dir, gen_opt.lmfeatures_fname);
    
    %% Consistency check
    if gen_opt.lmfeatures_reprocess == 0
        fprintf('Check correct features landmark Cell_idx: %d\n',  cell_idx);
        if vec_QC(cell_idx) == 1
            if exist(LM_feature_seq, 'file')
                previous = load(LM_feature_seq, 'feature_name');
                if ~isequal(feature_name, previous.feature_name)
                    vec_QC(cell_idx) = -1;
                    warning(['LM feature mismatching. Output will be overwritten for ' LM_feature_seq]);
                end
            else
                vec_QC(cell_idx) = -1;
                warning(['LM feature file not found ' LM_feature_seq]);
            end
            
        end
    end
    if vec_QC(cell_idx)~= -1
        continue
    end
    fprintf('Shape features landmark Cell_idx: %d\n',  cell_idx);
    %% Process each time point
    matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
    LM_feats = zeros(length(feature_name), length(matfiles));
    [LM_feats, status] = lm_shape_feature_process(matfiles, LM_feats);
    if ~status
        continue
    end
    save(LM_feature_seq, 'LM_feats', 'feature_name');
    if max(LM_feats(1,end)) > 0 % QC that Chr separation has been detected in the feature
        vec_QC(cell_idx) = 1;
    else
        vec_QC(cell_idx) = 0;
    end
    summary_database.lm_features(cell_idx) = vec_QC(cell_idx);
    writetable(summary_database, exp_database, 'Delimiter','\t');
    fprintf('|\n');
end
end

function [LM_feats, status] = lm_shape_feature_process(matfiles, LM_feats)
%% LM_SHAPE_FEATURE_PROCESS
% LM_SHAPE_FEATURE_PROCESS(matfiles, LM_feats) process matfiles and store results in LM_feats
status = 0;
assert(all(size(LM_feats) == [3,length(matfiles)]), 'Size of feature storage variable should be 3x%d is %d x %d', ...
    length(matfiles), size(LM_feats,1), size(LM_feats,2));

try
    for tpoint = 1:length(matfiles)
        fprintf('-');
        % load the result from the segmentation pipeline. Loading specific variables is faster
        seg = load(matfiles{tpoint}, 'chrDistPix', 'mitoTime', 'chrSptDistPix', 'chrVolMic', 'eigValuesMic', 'numChr');
        
        % extract the 1. feature for each time point: distance between two chromosomal volumes
        LM_feats(1,tpoint) = seg.chrDistPix;
        if isequal(seg.mitoTime{1},'Ana')
            if seg.numChr == 1
                LM_feats(1,tpoint) = seg.chrSptDistPix; % distance between two chromosomes even the chrs are not completely separated
            end
        end
        
        % extract the 2. feature for each time point: total chromosomal volume
        LM_feats(2,tpoint) = seg.chrVolMic;
        
        % extract the 3. feature for each time point: last eigenvalue
        LM_feats(3,tpoint) = seg.eigValuesMic(1,1);
        
    end
    status = 1;
catch ME
    getReport(ME, 'extended', 'hyperlinks', 'off')
    fprintf('lm_shape_feature: File processing is not complete\n');
end
end