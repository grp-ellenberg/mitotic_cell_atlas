function [AS,qc_seq] = qc_masterseq(Dist,A)

mean_TD = mean(Dist);
std_TD = std(Dist,[],1);
qc_seq = double(Dist<=(mean_TD+std_TD));
A(qc_seq==0,:) = 0;
AS = A(:,1);
for atidx = 2:size(A,2);
    if ~isequal(A(A(:,atidx)>0,atidx),A(A(:,atidx)>0,atidx-1)); 
        AS = cat(2,AS,A(:,atidx));
    end
end

end