function [F,T,SD] = GetMaster(featmat,alignmat)
% Update the master sequence and weight matrix
F = zeros(size(featmat,1),size(alignmat,2));
S = zeros(size(F));
SD = S;
for atidx = 1:size(alignmat,2);
    af = zeros(size(featmat,1),sum(alignmat(:,atidx)>0));
    seq_idx = 0;
    for cidx = 1:size(alignmat,1);
        if alignmat(cidx,atidx) > 0;
            seq_idx = seq_idx + 1;
            af(:,seq_idx) = featmat(:,alignmat(cidx,atidx),cidx);
        end
    end
    actual_feat = sum((1-isnan(af)),2);
    calc_f = af;
    calc_f(isnan(calc_f)) = 0;
    F(:,atidx) = sum(calc_f,2)./actual_feat;
    calc_f = af-repmat(F(:,atidx),[1 size(af,2)]);
    calc_f(isnan(calc_f)) = 0;
    actual_feat(actual_feat<2) = 2;
    SD(:,atidx) = sqrt(sum((calc_f).^2,2)./(actual_feat-1)); 
end

T = alignmat(sum(alignmat,2)>0,:);
for cellidx = 1:size(T,1);
    for atidx = 2:size(alignmat,2);
        if T(cellidx,atidx) < T(cellidx,atidx-1);
            T(cellidx,atidx) = T(cellidx,atidx-1);
        end
    end
end
T = diff(cat(1,zeros(1,size(T,1)),T'));
T = mean(T,2);
for atidx = 2:length(T);
    T(atidx) = T(atidx-1) + T(atidx);
end

end