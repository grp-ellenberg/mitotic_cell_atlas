function [status] = preprocess_poi(gen_opt, parameters)
%% PREPROCESS_POI Loads poi raw image data, FCS calibrate and process for further SURF point extraction.
% PREPROCESS_POI Use default options and parameters
% PREPROCESS_POI(gen_opt) Use paths as specified in struct gen_opt
% PREPROCESS_POI(gen_opt, parameters) Use paths as specified in struct gen_opt and parametes as specified.
%
% Loads POI (GFP) raw image, performs FCS calibration to convert in protein amounts, background subtraction, and smoothing.
% Performs distance transform for every pixel such that the relative distance to
% both landmarks can be encoded in one parameter. Background is computed from mask of dextran signal or WT measurments
% Smoothing is a gaussian filter with size given by pr.filter_size = [3 3 1] (function smooth3)
% INPUTS:
%   * gen_opt - struct for the general paths
%   * parameters - struct giving the parameters of the alignment
%   See code for more details.
%
% Author: Yin Cai, EMBL, 2016.06.04
% Update:
%   2016.09.13 Comments. Default parameters. Antonio Politi
%   2018.03.20 Struct as input. Antonio Politi

%% Default paramters
statusall = 0;
gen_opt_default = struct(...
    'exp_dir', '', ...                       % data directory
    'out_dir',  '',...                       % directory to store the results
    'exp_database', 'full_database.txt', ... % filename for experiment database text file (tab delimited file)
    'cellIdx', [1], ...                      % cell to process
    'segmentation_dir', 'Segmentation', ...  % subdirectory for segmentation results
    'preproc_dir', 'Registration',  ...      % subdirectory for preprocessing
    'preproc_reprocess', 1);                 % reprocess if 1 , pass if -2

parameters_default = struct( ...
    'usewt', 0, ...               % use wt cells if measured
    'bitnum', 16, ...             % Bit depth of image
    'filter_size', [3, 3, 1], ... % kernel size for Gaussian smoothing. A value of 1 is not smoothing in the direction. The sd will be 0.65
    'z_resolution', 3);           % ratio of image Z to XY resolution. TODO: Remove hard coding of this parameter

if nargin < 1
    gen_opt = gen_opt_default;
end

if nargin < 2
    parameters = parameters_default;
end

if ~all(isfield(gen_opt, fieldnames(gen_opt_default)))
    error('First argument gen_opt does not contain the expected fields\n%s \n', strjoin(fieldnames(gen_opt_default)));
end

if ~all(isfield(parameters, fieldnames(parameters_default)))
    error('Second argument parameters does not contain the expected fields\n%s \n', strjoin(fieldnames(parameters_default)));
end

[gen_opt.exp_dir, status] = verifyDirectory(gen_opt.exp_dir, 'Specify data directory', false);
if ~status
    return
end
[gen_opt.out_dir, status] = verifyDirectory(gen_opt.out_dir, 'Specify results output directory', true);
if ~status
    return
end

%% Load the database file
if gen_opt.preproc_reprocess == -2
    return
end


%% Read QC values
exp_database = fullfile(gen_opt.out_dir, gen_opt.exp_database);
summary_database = readtable(exp_database,'Delimiter','\t');
if gen_opt.preproc_reprocess == 1
    vec_QC = -ones(length(summary_database.poiprocess),1);
else
    vec_QC = summary_database.poiprocess;
end

if (all(summary_database.fcs_calibration == -1))
    errordlg('If FCS calibration data has been acquired. Change the values of the column fcs_calibration in full_database.txt to 1 accordingly!')
    warning('Column values of fcs_calibration are all -1');
    return
end
proc_history = (summary_database.manualQC > 0).*(summary_database.time_alignment > 0).*(summary_database.fcs_calibration > 0);
vec_idx = gen_opt.cellIdx(proc_history(gen_opt.cellIdx)>0);

%% PROCESS USING THE SEGMENTATION OUTPUT
for cell_idx = vec_idx
    if vec_QC(cell_idx) ~= -1 % check if cell has been processed or not
        continue
    end
    path_gen = pathgen_mitosys(gen_opt.exp_dir, summary_database.imgexp{cell_idx}, summary_database.tmax(cell_idx));
    calibration_file = path_gen.calibrationfile();
    seg_matfiles = path_gen.getmatpaths(gen_opt.out_dir, gen_opt.segmentation_dir);
    preproc_matfiles =  path_gen.getmatpaths(gen_opt.out_dir, gen_opt.preproc_dir);
    if ~exist(path_gen.getcelldir(gen_opt.out_dir,gen_opt.preproc_dir), 'dir')
        mkdir(path_gen.getcelldir(gen_opt.out_dir,gen_opt.preproc_dir));
    end
    if exist(calibration_file, 'file')
        cal = load(calibration_file, 'calibration_factor_C_N_um3', 'voxel_XY_size_um', 'voxel_Z_size_um');
        
        seg_fields = who('-file', seg_matfiles{1});
        if all(ismember({'voxelSizeX', 'voxelSizeY', 'voxelSizeZ'}, seg_fields))
            segmat =  load(seg_matfiles{1}, 'voxelSizeX', 'voxelSizeY', 'voxelSizeZ');
            voxel_V_um3 = segmat.voxelSizeX*segmat.voxelSizeY*segmat.voxelSizeZ;
        else
            % old format of segmentation matfile had no pixel size
            voxel_V_um3 = cal.voxel_XY_size_um^2*cal.voxel_Z_size_um;
        end
    else
        fprintf('!!!! No calibration parameters found for %s !!!!\n', summary_database.imgexp{cell_idx})
        cal = struct( ...
            'calibration_factor_C_N_um3', [0,1]);
        voxel_V_um3 = 1;
    end
    maxInt = 0;
    fprintf('Preprocess Cell_idx: %d\n',  cell_idx);
    
    for tpoint = 1:length(seg_matfiles)
        
        fprintf('-');
        segmat = load(seg_matfiles{tpoint}, 'poi', 'nuc', 'bgMask', 'cellVolume', 'nucVolume' ); % load the result from the segmentation pipeline
        %% PRE-PROCESS POI
        cal.calibration_factor_N_voxel = cal.calibration_factor_C_N_um3(2)*voxel_V_um3;
        % Number of proteins per voxel volume without background subtraction
        proc_poi = segmat.poi*cal.calibration_factor_N_voxel;
        
        % Filter. This avoids too many < 0 intensity pixels
        proc_poi = smooth3(proc_poi, 'gaussian', parameters.filter_size);
        proc_nuc = smooth3(segmat.nuc, 'gaussian', parameters.filter_size);
        % Substract the background
        % background is computed from outer cellular space
        % compared to intracellular background of WT cells this can be 10-20%
        % lower. This is estimated for each image
        if parameters.usewt
            background_488_N_voxel = calibration_factor_C_N_um3(1)*calibration_factor_C_N_um3(2)*voxel_V_um3;
        else
            background_488_N_voxel = sum(proc_poi(:).*segmat.bgMask(:))/sum(segmat.bgMask(:)>0); % unit N per voxel
        end
        proc_poi = proc_poi - background_488_N_voxel;
        
        % set all negative pixels to 0
        proc_poi(proc_poi<0) = 0;
        
        % Calculate the calibrated theoretical saturation value
        satInt = (2^parameters.bitnum-1)*cal.calibration_factor_N_voxel - background_488_N_voxel;
        
        
        % Crop POI within the cell of interest
        proc_poi = proc_poi.*segmat.cellVolume;
        proc_nuc = proc_nuc.*(double(segmat.nucVolume>0));
        
        %% MEASURE THE SIGNAL TO NUC/CELL DISTANCE
        % Get the distance map to the cell and nuclear boundary in 3D
        imgsize = size(proc_poi);
        celldistmap2d = zeros(size(segmat.cellVolume));
        for i = 1:imgsize(3)
            celldistmap2d(:,:,i) = bwdist(bwperim(segmat.cellVolume(:,:,i),4));
        end
        chr_full = imfill(segmat.nucVolume,'holes');
        nucdistmap2d = zeros(size(chr_full));
        for i = 1:imgsize(3)
            nucdistmap2d(:,:,i) = bwdist(bwperim(chr_full(:,:,i),4));
        end
        celldistmap3d = zeros(size(celldistmap2d));
        nucdistmap3d = zeros(size(nucdistmap2d));
        if imgsize(3)>1
            for i = 1:imgsize(3)
                distvec = (parameters.z_resolution*abs((1:imgsize(3))-i)).^2;
                distvec = permute(repmat(distvec,[imgsize(2) 1 imgsize(1)]),[3 1 2]);
                celldistmap3d(:,:,i) = sqrt(min(celldistmap2d.^2+distvec,[],3));
                nucdistmap3d(:,:,i) = sqrt(min(nucdistmap2d.^2+distvec,[],3));
            end
        else
            celldistmap3d = celldistmap2d;
            nucdistmap3d = nucdistmap2d;
        end
        % Indicate inside chromosomal volume
        nucdistmap3d(chr_full>0) = -nucdistmap3d(chr_full>0);
        clear celldistmap2d;
        clear nucdistmap2d;
        clear distvec;
        lm = prod(imgsize)-1;
        trans_matrix = [(0:lm)' segmat.cellVolume(:) celldistmap3d(:) nucdistmap3d(:) proc_poi(:)];
        clear chr_full;
        clear celldistmap3d;
        clear nucdistmap3d;
        trans_matrix(trans_matrix(:,2)==0,:) = []; % only keep the data inside of the cell
        trans_matrix(:,2) = sum(abs(trans_matrix(:,3:4)),2);
        trans_matrix(trans_matrix(:,2)==0,:) = []; % delete totdist=0
        coords = zeros(size(trans_matrix,1),3);
        coords(:,3) = floor(trans_matrix(:,1) / imgsize(1) / imgsize(2)) + 1;
        trans_matrix(:,1) = mod(trans_matrix(:,1), imgsize(1) * imgsize(2));
        coords(:,2) = floor(trans_matrix(:,1) / imgsize(1)) + 1;
        coords(:,1) = mod(trans_matrix(:,1), imgsize(1)) + 1;
        % Generate one distance which indicate the relationship
        % to both landmarks
        normdists = trans_matrix(:,4)./trans_matrix(:,2); % dist to nuc/(abs(dist to nuc)+abs(dist to cell bound))
        dist_to_nuc = trans_matrix(:,4);
        dist_to_pm = trans_matrix(:,3);
        intensities = trans_matrix(:,5);
        maxInt = max(maxInt, max(intensities));
        %% Saving and updating
        calibration_factor_N_voxel = cal.calibration_factor_N_voxel;
        save(preproc_matfiles{tpoint}, 'proc_nuc', 'proc_poi', 'background_488_N_voxel', ...
            'calibration_factor_N_voxel', 'satInt', 'normdists', ...
            'dist_to_nuc', 'dist_to_pm', 'intensities', 'coords', 'parameters');
    end
    fprintf('|\n');
    if maxInt > 0 % QC that positive signal exists in the cell
        vec_QC(cell_idx) = 1;
    else
        vec_QC(cell_idx) = 0;
    end
    summary_database.poiprocess(cell_idx) = vec_QC(cell_idx);
    % output is written for every cell
    writetable(summary_database, exp_database, 'Delimiter','\t');
end
statusall = 1;
end