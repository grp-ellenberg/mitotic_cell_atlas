# FCS Calibration

### pre_processpoi
This function loads the POI raw image and performs a simple processing step such as background subtraction, smoothing and distance transformation for every single pixel such that the relative distance to both landmarks can be encoded in one parameter. GFP intensity is converted to protein amounts using a calibration file. Background is computed from mask of dextran signal or WT cells.

